<?php
namespace Language\Controller;

use Cms\Controller\CmsController;
use Cake\ORM\TableRegistry;

/**
 *  Language controller
 *
 * @copyright  (c)2015 MEDUS MAKONIS SIA / www.medusmakonis.lv
 * @author Artis Bautra <bautra.artis@gmail.com>
 */
class LanguageCmsController extends CmsController {

    public function initialize() {
        parent::initialize();

        $this->modelClass = 'Language.Language';
    }
	
    /**
     * Index method
     *
     * @return void
     */ 
    public function index() {
        $this->showAddBtn = false;

        $this->useNestable = true;

        $languageTable = TableRegistry::get('Language.Language');

        $listItems = $languageTable
            ->find('notDeleted')
            ->orderAsc('seq');

        $this->set('listItems', $listItems);
        $this->set('_serialize', ['listItems']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $language = $this->Language->newEntity();

        if ($this->request->is('post')) {
            $language = $this->Language->patchEntity($language, $this->request->data, ['validate' => true]);

            if ($this->Language->save($language)) {
                $this->Flash->success('Data was saved!', ['plugin' => 'Cms']);

                $this->redirect('/cms/languages/');
            }
        }

        $this->set(compact('language'));
        $this->set('_serialize', ['language']);
    }

    /**
     * Edit method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function edit($id) {
        $language = $this->Language->get($id);
        $this->setPageTitle($language);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $language = $this->Language->patchEntity($language, $this->request->data, ['validate' => true]);

            if (!isset($this->request->data['Language']['is_public']))
                $language->is_public = 'n';
          
            if ($language->errors()) {
                $this->Flash->failed('Please, fill required fields!', ['plugin' => 'Cms']);

                $this->request->data['errors'] = $item->errors();
            } else {
                if ($this->Language->save($language)) {
                    $this->Flash->success('Data was saved!', ['plugin' => 'Cms']);

                    $this->redirect('/cms/languages/');
                }
            }
        } elseif ($id) {
            $language = $this->Language->get($id);

            $this->set(compact('language'));
            $this->set('_serialize', ['language']);
        }

        $this->render('add');
    }

    /**
     * Choose method
     *
     * @return void
     */
    public function choose($slug = null) {
        $this->autoRender = false;

        $language = $this->Language->find('slug', ['slug' => $slug])->first();
        $this->request->session()->write('Cms.Language', $language);

        $this->redirect($this->referer());
    }
}

?>