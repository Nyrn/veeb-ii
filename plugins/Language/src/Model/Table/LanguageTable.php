<?php
namespace Language\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;

class LanguageTable extends AppTable
{
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('languages');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title', 'Title can not be empty!')
            ->notEmpty('slug', 'Url can not be empty!');

        return $validator;
    }
}
?>