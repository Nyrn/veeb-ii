<?php

echo $this->CmsForm->open($language);
echo $this->CmsForm->text('Language.title', 'Title');
echo $this->CmsForm->text('Language.slug', 'Url', ['inputSize' => 1]);
echo $this->CmsForm->checkbox('Language.is_public', 'Public', [], ($language->is_public == 'y') ? true : false);
echo $this->CmsForm->submit('Save');
echo $this->CmsForm->close();