<?php
namespace News\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Lib\CmsRequest;
use Cake\Routing\Router;

/**
 *  Product controller
 *
 * @author Artis Bautra <bautra.artis@gmail.com>
 */
class NewsController extends AppController {
    public function initialize() {
    	parent::initialize();

        $this->modelClass = 'News.News';
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        if (isset(CmsRequest::$urlParts['id'])) {
            $this->view();
        } else {
            $newsTable = TableRegistry::get('News.News');

            // Get news
            $news = $newsTable
                ->find()
                ->find('langId')
                ->find('active')
                ->order(['date' => 'desc'])
                ->limit(5)
                ->all()
                ->toArray();


            $latestNews = [];
            $latestNews['last'] = isset($news[0]) ? $news[0] : false;
            $latestNews['prev'] = isset($news[1]) ? $news[1] : false;

            if ($latestNews['last']) {
                unset($news[0]);
            }

            if ($latestNews['prev']) {
                unset($news[1]);
            }

            $this->set(compact('news', 'latestNews'));
            $this->set('newsUrl', CmsRequest::$page->url);
        }
    }

    /**
     * View method
     *
     * @return void
     */
    public function view() {
        //$this->useUikit = true;
        //$this->useUiKitLightbox = true;
        //$this->useUnslider = true;

        $shareUrls = [];

        $newsTable = TableRegistry::get('News.News');
        //$galleryTable = TableRegistry::get('News.NewsGallery');

        $slug = CmsRequest::$urlParts['id'];

        // Get product
        $query = $newsTable
            ->find('langId')
            ->find('active')
            ->find('slug', ['slug' => $slug])
            ->first();

        $item = $query;

        // Get images
        //$images = $galleryTable->getProductItems($item->id)->toArray();

        // More news
        $moreNews = $newsTable
            ->find('langId')
            ->find('active')
            ->where(function($q) use ($item) {
                return $q->notIn('id', $item->id);
            })
            ->order(['News.date' => 'desc'])
            ->limit(3)
            ->all()
            ->toArray();

        $shareUrls['facebook'] = sprintf('https://www.facebook.com/sharer.php?u=%s&t=%s', Router::url(CmsRequest::$page->url . $item->slug, true), $item->title);
        $shareUrls['twitter'] = sprintf('https://twitter.com/intent/tweet?url=%s&text=%s', Router::url(CmsRequest::$page->url . $item->slug, true), $item->title);
        
        // Set seo
        $this->setSeo($item);

        $this->set(compact(
            'item',
            'images',
            'moreNews',
            'shareUrls'
        ));

        $this->render('view');
    }

    public function loadMore($page = 1) {
        $this->autoRender = false;

        $this->viewBuilder()->layout('ajax');

        if ($this->request->is('ajax')) {
            $newsTable = TableRegistry::get('News.News');

            $limit = 3;
            $offset = 5 * $page;

            $news = $newsTable
                ->find()
                ->find('langId')
                ->find('active')
                ->order(['date' => 'desc'])
                ->page($page)
                ->limit($limit)
                ->offset($offset)
                ->all()
                ->toArray();

            if ($news) {
                $this->set('news', $news);
                $this->set('newsUrl', $this->moduleUrls['news']);
                
                $this->render();  
            }
        }
    }
}
?>