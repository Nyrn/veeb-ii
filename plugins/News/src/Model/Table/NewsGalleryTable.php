<?php
namespace News\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class NewsGalleryTable extends AppTable
{
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('news_gallery');
    }

    /**
     * Get product images
     */
    public function getProductItems($productId = 0) {
        if ($productId > 0) {
            $items = $this
                ->find('notDeleted')
                ->where(['news_id' => $productId])
                ->order(['seq' => 'asc']);

            return $items;
        }
    }
}
?>