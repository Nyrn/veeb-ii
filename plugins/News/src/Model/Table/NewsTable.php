<?php
namespace News\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class NewsTable extends AppTable
{
    public $mirrorCommonFields = ['is_public', 'is_deleted']; // Mirror items common fields

	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('news');
    
        $this->hasMany('NewsGallery', [
            'className' => 'News.NewsGallery',
            'foreignKey' => 'news_id'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title', 'Title can not be empty!');

        return $validator;
    }

    /**
     * Find main image
     */
    public function findMainImage(Query $query) {
        $query->contain([
            'NewsGallery' => function($q) {
                return $q
                    ->find('notDeleted')
                    ->find('paths')
                    ->order(['seq' => 'asc']);
            }
        ]);

        $query = $query->formatResults(function($results) {
            return $results->map(function($row)  {
                if (isset($row->news_gallery[0])) {
                    $row->main_img = $row->news_gallery[0];
                } else {
                    $row->main_img = null;
                }

                return $row;
            });
        });

        return $query;
    }
}
?>