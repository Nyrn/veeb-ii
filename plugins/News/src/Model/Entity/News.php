<?php
namespace News\Model\Entity; 

use Cake\ORM\Entity;

class News extends Entity {
	
	public function _getImgPath() {
		if (!empty($this->_properties['img'])) {
			return '/file/news/default/' . $this->_properties['img'];
		}
	}
}
?>