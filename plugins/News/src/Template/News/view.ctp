<div class="wrapper news_page_single">
    <div class="pagination top container">
        <ul>
            <li><a href="<?php echo (isset($moduleUrls['news'])) ? $moduleUrls['news'] : '#'; ?>"><?php pt('News'); ?></a></li>
            <li><span><?php echo $item->title; ?></span></li>
        </ul>
    </div>


    <div class="news_single">
        <div class="container">
            <div class="top_block">
                <h1><?php echo $item->title; ?></h1>
                <span class="date"><?php echo $item->date->format('d F - Y'); ?></span>
            </div>
            <?php if ($item->img): ?>
                <div class="article_baner">
                    <img src="<?php echo $item->img_path; ?>">
                </div>
            <?php endif; ?>
            <div class="row margin0 between nowrap">
                <div class="article_content">
                    <div class="wrap">
                        <div class="text">
                            <?php echo $item->text; ?>

                            <?php echo $this->Element('Page.share-block'); ?>
                        </div>
                    </div>
                </div>
                <!--  -->
                <div class="right_sidebar">
                    <div class="wrap">
                        <?php echo $this->Element('Page.share-block'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- start all_news -->
    <div class="all_news more_in_category">
        <div class="container">
            <div class="top_row">
                <h4><?php pt('More news'); ?></h4>
            </div>
            <div class="row">
                <?php foreach ($moreNews as $more): ?>
                    <div class="news_item_simple new w33">
                        <?php if ($more->img): ?>
                            <a href="<?php echo $cmsRequest::$page->url . $more->slug; ?>" class="img">
                                <img src="<?php echo $more->img_path; ?>" alt="news">
                            </a>
                        <?php endif; ?>
                        <div class="content">
                            <a href="<?php echo $cmsRequest::$page->url . $more->slug; ?>" class="date_news"><?php echo $more->date->format('d F -  Y'); ?></a>
                            <a href="<?php echo $cmsRequest::$page->url . $more->slug; ?>">
                                <h5><?php echo $more->title; ?></h5>
                                <div class="text_content">
                                    <?php echo $more->intro; ?>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>