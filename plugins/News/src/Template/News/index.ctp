<div class="wrapper news_page">
    <!-- start news_block -->
    <div class="category_block news_block latest_news">
        <div class="container">
            <div class="top_row row margin0 between">
                <h1><?php pt('Latest news'); ?></h1>
            </div>
            <div class="cards row stretch">
                <?php if (isset($latestNews['last']) && !empty($latestNews['last'])): ?>
                    <div class="column w33">
                        <div class="news_item_simple new big show_hover_block w100">
                            <div class="hover_block">
                                <a href="<?php echo $cmsRequest::$page->url . $latestNews['last']->slug; ?>" class="date_news"><?php echo (isset($latestNews['last']->date)) ? $latestNews['last']->date->format('d F - Y') : null; ?></a>
                                <a class="main" href="<?php echo $cmsRequest::$page->url . $latestNews['last']->slug; ?>">
                                    <h5 class="title"><?php echo $latestNews['last']->title; ?></h5>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (isset($latestNews['prev']) && !empty($latestNews['prev'])): ?>
                    <div class="column w67">
                        <div class="news_item news_item_latest">
                            <a href="<?php echo $cmsRequest::$page->url . $latestNews['prev']->slug; ?>" class="date_news"><?php echo (isset($latestNews['prev']->date)) ? $latestNews['prev']->date->format('d F - Y') : null; ?></a>
                            <a class='main' href="<?php echo $cmsRequest::$page->url . $latestNews['prev']->slug; ?>">
                                <div class="img">
                                    <?php if ($latestNews['prev']->img): ?>
                                        <img src="<?php echo $latestNews['prev']->img_path; ?>" alt="news">
                                    <?php endif; ?>
                                </div>
                                <div class="text_content">
                                    <h5><?php echo $latestNews['prev']->title; ?></h5>
                                    <span><?php echo $latestNews['last']->intro; ?></span>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- end news_block -->

    <!-- start all_news -->
    <div class="all_news">
        <div class="container">
            <div class="top_row">
                <h2><?php pt('All news'); ?></h2>
            </div>

            <?php if ($news): ?>
                <div class="row">
                    <?php foreach ($news as $item): ?>
                        <?php echo $this->Element('News.single-item', ['item' => $item]); ?>
                    <?php endforeach; ?>

                    <div class="news-load-more-items w100"></div>
                </div>

                <div class="row bottom_row">
                    <a href="#" class="news-load-more-btn"><?php pt('View more'); ?></a>
                </div>
            <?php else: ?>
                <?php pt('No news found'); ?>
            <?php endif; ?>
        </div>
    </div>
    <!-- end all_news -->
</div>