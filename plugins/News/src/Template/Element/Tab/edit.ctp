<?php if (isset($entry->id)): ?>
    <div class="nav-tabs-horizontal nav-tabs-inverse">
        <ul role="tablist" data-plugin="nav-tabs" class="nav nav-tabs nav-tabs-solid">
            <li class="<?php echo (isset($activeTab) && $activeTab == 'content') ? 'active' : null; ?>"><a href="/cms/news/edit/<?php echo $entry->id; ?>">Content</a></li>
            <li class="<?php echo (isset($activeTab) && $activeTab == 'gallery') ? 'active' : null; ?>"><a href="/cms/news/gallery/index/<?php echo $entry->id; ?>">Gallery</a></li>
        </ul>
    </div>
<?php endif; ?>