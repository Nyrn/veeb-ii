<div class="news_item_simple new w33">
    <?php if ($item->img): ?>
        <a href="<?php echo $newsUrl . $item->slug; ?>" class="img">
            <img src="<?php echo $item->img_path; ?>" alt="news">
        </a>
    <?php endif; ?>

    <div class="content">
        <a href="<?php echo $newsUrl . $item->slug; ?>" class="date_news"><?php echo $item->date->format('d F - Y'); ?></a>
        <a href="<?php echo $newsUrl . $item->slug; ?>">
            <h5><?php echo $item->title; ?></h5>
            <div class="text_content">
                <?php echo $item->intro; ?>
            </div>
        </a>
    </div>
</div>