<?php
use Cake\Routing\Router;

Router::plugin('News', function ($routes) {
    $routes->fallbacks('InflectedRoute');
});