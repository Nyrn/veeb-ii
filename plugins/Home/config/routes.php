<?php
use Cake\Routing\Router;

Router::plugin('Home', function ($routes) {
    $routes->fallbacks('InflectedRoute');
});