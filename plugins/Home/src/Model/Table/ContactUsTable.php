<?php
namespace ContactUs\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;

class ContactUsTable extends AppTable
{
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('contact_us');
    }
}
?>