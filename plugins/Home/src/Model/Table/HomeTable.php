<?php
namespace Home\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;

class HomeTable extends AppTable
{
    public $mirrorCommonFields = ['img']; // Mirror items common fields

	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('home');
    }
}
?>