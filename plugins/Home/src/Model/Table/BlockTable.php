<?php
namespace Home\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;

class BlockTable extends AppTable
{
	public $mirrorCommonFields = ['img', 'is_deleted']; // Mirror items common fields

	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('home_blocks');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        return $validator;
    }
}
?>