<?php
namespace Home\Model\Entity; 

use Cake\ORM\Entity;

class Home extends Entity {
	public function _getImgPath() {
		if (!empty($this->_properties['img'])) {
			return '/file/home/default/' . $this->_properties['img'];
		}
	}

	public function _getVideoPath() {
		if (!empty($this->_properties['video'])) {
			return '/file/home/default/' . $this->_properties['video'];
		}
	}
}
?>