<?php
namespace Home\Model\Entity; 

use Cake\ORM\Entity;

class Block extends Entity {
	
	public function _getImgPath() {
		if (!empty($this->_properties['img'])) {
			return '/file/home_block/default/' . $this->_properties['img'];
		}
	}
}
?>