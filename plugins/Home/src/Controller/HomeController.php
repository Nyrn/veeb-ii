<?php
namespace Home\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;

/**
 *  Home controller
 *
 * @author Artis Bautra <bautra.artis@gmail.com>
 */
class HomeController extends AppController {
    public function initialize() {
        parent::initialize();

        $this->modelClass = 'Home.Home';
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->useAutocomplete = true;
        $this->useChosen = true;
        $this->useUnslider = true;

        $this->showBreadcrumbs = false;
        $this->showRequestPrice = true;

        $homeTable = TableRegistry::get('Home.Home');
        $blockTable = TableRegistry::get('Home.Block');
        $parcelShopTable = TableRegistry::get('Cargobus.ParcelShop');
        $newsTable = TableRegistry::get('News.News');

        if (getDomain() == 'cargobus.lv') $firstTerminalId = 23;
        elseif (getDomain() == 'cargobus.lt') $firstTerminalId = 34;
        else $firstTerminalId = 12;

        // Home data
        $home = $homeTable
            ->find('langId')
            ->first();

        // Terminals
        $terminals = $parcelShopTable
            ->find()
            ->where(['type' => 'STATIONARY_TERMINAL'])
            ->order(['city' => 'asc'])
            ->all();

        $terminalsList = $parcelShopTable->parseAsList($terminals);

        $terminalFirst = $parcelShopTable
            ->find()
            ->where(['terminal_id' => $firstTerminalId])
            ->first();
        
        $terminalAvailability = $parcelShopTable->parseAvailability($terminalFirst->availability);

        $terminalNote = $parcelShopTable->parseNote($terminalFirst->note);

        // Home blocks
        $blocks = $blockTable
            ->find('langId')
            ->find('active')
            ->all();

        $collection = new Collection($blocks);
        $blocks = $collection
            ->indexBy('slug')
            ->toArray();

        // News
        $news = $newsTable
            ->find('langId')
            ->find('active')
            ->order(['News.date' => 'desc'])
            ->limit(3)
            ->all()
            ->toArray();

        $mainNews = isset($news[0]) ? $news[0] : false;

        if ($mainNews) {
            unset($news[0]);
        }

        $this->set('home', $home);
        $this->set('blocks', $blocks);
        $this->set('terminals', $terminals);
        $this->set('terminalsList', $terminalsList);
        $this->set('terminalFirst', $terminalFirst);
        $this->set('terminalAvailability', $terminalAvailability);
        $this->set('terminalNote', $terminalNote);
        $this->set('news', $news);
        $this->set('mainNews', $mainNews);
    }
}
?>