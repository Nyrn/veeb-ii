<div class="wrapper main_page page_slide_wrap" id='wrapper'>
    <!-- start top_block_main_page -->
    <div class="section pp-scrollable top_section">
        <div class="top_block_main_page" style='background-image: url(<?php echo (!empty($home->img_path)) ? $home->img_path : '/img/top_block_img.jpg'; ?>)'>
            <?php if (!empty($home->video_path)): ?>
                <?php /*<div class="fullscreen-bg" id="video_html" data-video-src="<?php echo $home->video_path; ?>"  style='background-image: url(<?php echo (!empty($home->img_path)) ? $home->img_path : '/img/top_block_img.jpg'; ?>)'>*/ ?>
                <div class="fullscreen-bg" id="video_html" data-video-src-mp4="<?php echo $home->video_path; ?>" data-video-src-webm="/video/cargo.webm">
                    <!-- video -->
                </div>
            <?php endif; ?>
            
            <?php /*<img src="/img/triangle.svg" alt="triangle" class="triangle">*/ ?>
            <div class="container">
                <div class="row main_row nowrap margin0">
                    <div class="left">
                        <!-- calc -->
                        <div id="calculator" class='calculator'>
                            <form action="<?php echo $aliasUrls['order_calculate']; ?>" method="post">
                                <div class="row">
                                    <div class="row radio_wrap">
                                        <label class="w50 radio">
                                            <input type="radio" name="from" value="courier" checked>
                                            <span><?php pt('From courier'); ?></span>
                                        </label>
                                        <label class="w50 radio">
                                            <input type="radio" name="from" value="terminal">
                                            <span><?php pt('From terminal'); ?></span>
                                        </label>
                                    </div>
                                    <div class="tabs_el_wrap">
                                        <div class="tabs_el" data-for='courier'>
                                            <label class="with_image w100">
                                                <svg width="14" height="19" viewBox="0 0 14 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M6.29688 17.8984C5.6875 17.0078 4.90234 15.8828 3.94141 14.5234C2.76953 12.8359 1.99609 11.6992 1.62109 11.1133C1.08203 10.2695 0.71875 9.56641 0.53125 9.00391C0.34375 8.41797 0.25 7.75 0.25 7C0.25 5.78125 0.554687 4.65625 1.16406 3.625C1.77344 2.59375 2.59375 1.77344 3.625 1.16406C4.65625 0.554688 5.78125 0.25 7 0.25C8.21875 0.25 9.34375 0.554688 10.375 1.16406C11.4062 1.77344 12.2266 2.59375 12.8359 3.625C13.4453 4.65625 13.75 5.78125 13.75 7C13.75 7.75 13.6562 8.41797 13.4688 9.00391C13.2812 9.56641 12.918 10.2695 12.3789 11.1133C12.0039 11.6992 11.2305 12.8359 10.0586 14.5234L7.70312 17.8984C7.53906 18.1328 7.30469 18.25 7 18.25C6.69531 18.25 6.46094 18.1328 6.29688 17.8984ZM7 9.8125C7.77344 9.8125 8.42969 9.54297 8.96875 9.00391C9.53125 8.44141 9.8125 7.77344 9.8125 7C9.8125 6.22656 9.53125 5.57031 8.96875 5.03125C8.42969 4.46875 7.77344 4.1875 7 4.1875C6.22656 4.1875 5.55859 4.46875 4.99609 5.03125C4.45703 5.57031 4.1875 6.22656 4.1875 7C4.1875 7.77344 4.45703 8.44141 4.99609 9.00391C5.55859 9.54297 6.22656 9.8125 7 9.8125Z"
                                                        fill="#111111" />
                                                </svg>
                                                <input type="text" name="courier_from" value="" placeholder="<?php pt('Type to search for address..'); ?>" class="autocomplete" id="autocomplete-from-address" data-autocomplete-target="#google-id-from">
                                            </label>
                                        </div>
                                        <div class="tabs_el" data-for='terminal'>
                                            <label class="with_image w100">
                                                <svg width="16" height="13" viewBox="0 0 16 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M12.4922 8.65625H3.50781C3.45898 8.65625 3.41016 8.68066 3.36133 8.72949C3.32878 8.76204 3.3125 8.80273 3.3125 8.85156V10.0234C3.3125 10.0723 3.32878 10.1211 3.36133 10.1699C3.41016 10.2025 3.45898 10.2188 3.50781 10.2188H12.4922C12.541 10.2188 12.5817 10.2025 12.6143 10.1699C12.6631 10.1211 12.6875 10.0723 12.6875 10.0234V8.85156C12.6875 8.80273 12.6631 8.76204 12.6143 8.72949C12.5817 8.68066 12.541 8.65625 12.4922 8.65625ZM12.4922 11H3.50781C3.45898 11 3.41016 11.0163 3.36133 11.0488C3.32878 11.0977 3.3125 11.1465 3.3125 11.1953V12.3672C3.3125 12.416 3.32878 12.4567 3.36133 12.4893C3.41016 12.5381 3.45898 12.5625 3.50781 12.5625H12.4922C12.541 12.5625 12.5817 12.5381 12.6143 12.4893C12.6631 12.4567 12.6875 12.416 12.6875 12.3672V11.1953C12.6875 11.1465 12.6631 11.0977 12.6143 11.0488C12.5817 11.0163 12.541 11 12.4922 11ZM12.4922 6.3125H3.53223C3.46712 6.3125 3.41829 6.33691 3.38574 6.38574C3.35319 6.41829 3.33691 6.45898 3.33691 6.50781V7.67969C3.33691 7.72852 3.35319 7.77734 3.38574 7.82617C3.41829 7.85872 3.46712 7.875 3.53223 7.875H12.4922C12.541 7.875 12.5817 7.85872 12.6143 7.82617C12.6631 7.77734 12.6875 7.72852 12.6875 7.67969V6.50781C12.6875 6.45898 12.6631 6.41829 12.6143 6.38574C12.5817 6.33691 12.541 6.3125 12.4922 6.3125ZM15.1045 2.91895L8.43945 0.160156C8.14648 0.0299479 7.85352 0.0299479 7.56055 0.160156L0.919922 2.91895C0.692057 3.0166 0.513021 3.16309 0.382812 3.3584C0.252604 3.55371 0.1875 3.7653 0.1875 3.99316V12.3672C0.1875 12.416 0.203776 12.4567 0.236328 12.4893C0.285156 12.5381 0.333984 12.5625 0.382812 12.5625H2.33594C2.38477 12.5625 2.42546 12.5381 2.45801 12.4893C2.50684 12.4567 2.53125 12.416 2.53125 12.3672V6.3125C2.53125 6.10091 2.60449 5.92188 2.75098 5.77539C2.91374 5.61263 3.10905 5.53125 3.33691 5.53125H12.6631C12.891 5.53125 13.0781 5.61263 13.2246 5.77539C13.3874 5.92188 13.4688 6.10091 13.4688 6.3125V12.3672C13.4688 12.416 13.485 12.4567 13.5176 12.4893C13.5664 12.5381 13.6152 12.5625 13.6641 12.5625H15.6172C15.666 12.5625 15.7067 12.5381 15.7393 12.4893C15.7881 12.4567 15.8125 12.416 15.8125 12.3672V3.99316C15.8125 3.7653 15.7474 3.55371 15.6172 3.3584C15.487 3.16309 15.3079 3.0166 15.0801 2.91895H15.1045Z"
                                                        fill="#111111" />
                                                </svg>
                                                <select name="terminal_from" class="search__select">
                                                    <?php foreach ($terminalsList as $id => $address): ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $address; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="row radio_wrap">
                                        <label class="w50 radio">
                                            <input type="radio" name="to" value="courier" checked>
                                            <span><?php pt('To courier'); ?></span>
                                        </label>
                                        <label class="w50 radio">
                                            <input type="radio" name="to" value="terminal">
                                            <span><?php pt('To terminal'); ?></span>
                                        </label>
                                    </div>
                                    <div class="tabs_el_wrap">
                                        <div class="tabs_el" data-for='courier'>
                                            <label class="with_image w100">
                                                <svg width="14" height="19" viewBox="0 0 14 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M6.29688 17.8984C5.6875 17.0078 4.90234 15.8828 3.94141 14.5234C2.76953 12.8359 1.99609 11.6992 1.62109 11.1133C1.08203 10.2695 0.71875 9.56641 0.53125 9.00391C0.34375 8.41797 0.25 7.75 0.25 7C0.25 5.78125 0.554687 4.65625 1.16406 3.625C1.77344 2.59375 2.59375 1.77344 3.625 1.16406C4.65625 0.554688 5.78125 0.25 7 0.25C8.21875 0.25 9.34375 0.554688 10.375 1.16406C11.4062 1.77344 12.2266 2.59375 12.8359 3.625C13.4453 4.65625 13.75 5.78125 13.75 7C13.75 7.75 13.6562 8.41797 13.4688 9.00391C13.2812 9.56641 12.918 10.2695 12.3789 11.1133C12.0039 11.6992 11.2305 12.8359 10.0586 14.5234L7.70312 17.8984C7.53906 18.1328 7.30469 18.25 7 18.25C6.69531 18.25 6.46094 18.1328 6.29688 17.8984ZM7 9.8125C7.77344 9.8125 8.42969 9.54297 8.96875 9.00391C9.53125 8.44141 9.8125 7.77344 9.8125 7C9.8125 6.22656 9.53125 5.57031 8.96875 5.03125C8.42969 4.46875 7.77344 4.1875 7 4.1875C6.22656 4.1875 5.55859 4.46875 4.99609 5.03125C4.45703 5.57031 4.1875 6.22656 4.1875 7C4.1875 7.77344 4.45703 8.44141 4.99609 9.00391C5.55859 9.54297 6.22656 9.8125 7 9.8125Z"
                                                        fill="#111111" />
                                                </svg>
                                                <input type="text" name="courier_to" value="" placeholder="<?php pt('Type to search for address..'); ?>"class="autocomplete" id="autocomplete-to-address" data-autocomplete-target="#google-id-to">
                                            </label>
                                        </div>
                                        <div class="tabs_el" data-for='terminal'>
                                            <label class="with_image w100">
                                                <svg width="16" height="13" viewBox="0 0 16 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M12.4922 8.65625H3.50781C3.45898 8.65625 3.41016 8.68066 3.36133 8.72949C3.32878 8.76204 3.3125 8.80273 3.3125 8.85156V10.0234C3.3125 10.0723 3.32878 10.1211 3.36133 10.1699C3.41016 10.2025 3.45898 10.2188 3.50781 10.2188H12.4922C12.541 10.2188 12.5817 10.2025 12.6143 10.1699C12.6631 10.1211 12.6875 10.0723 12.6875 10.0234V8.85156C12.6875 8.80273 12.6631 8.76204 12.6143 8.72949C12.5817 8.68066 12.541 8.65625 12.4922 8.65625ZM12.4922 11H3.50781C3.45898 11 3.41016 11.0163 3.36133 11.0488C3.32878 11.0977 3.3125 11.1465 3.3125 11.1953V12.3672C3.3125 12.416 3.32878 12.4567 3.36133 12.4893C3.41016 12.5381 3.45898 12.5625 3.50781 12.5625H12.4922C12.541 12.5625 12.5817 12.5381 12.6143 12.4893C12.6631 12.4567 12.6875 12.416 12.6875 12.3672V11.1953C12.6875 11.1465 12.6631 11.0977 12.6143 11.0488C12.5817 11.0163 12.541 11 12.4922 11ZM12.4922 6.3125H3.53223C3.46712 6.3125 3.41829 6.33691 3.38574 6.38574C3.35319 6.41829 3.33691 6.45898 3.33691 6.50781V7.67969C3.33691 7.72852 3.35319 7.77734 3.38574 7.82617C3.41829 7.85872 3.46712 7.875 3.53223 7.875H12.4922C12.541 7.875 12.5817 7.85872 12.6143 7.82617C12.6631 7.77734 12.6875 7.72852 12.6875 7.67969V6.50781C12.6875 6.45898 12.6631 6.41829 12.6143 6.38574C12.5817 6.33691 12.541 6.3125 12.4922 6.3125ZM15.1045 2.91895L8.43945 0.160156C8.14648 0.0299479 7.85352 0.0299479 7.56055 0.160156L0.919922 2.91895C0.692057 3.0166 0.513021 3.16309 0.382812 3.3584C0.252604 3.55371 0.1875 3.7653 0.1875 3.99316V12.3672C0.1875 12.416 0.203776 12.4567 0.236328 12.4893C0.285156 12.5381 0.333984 12.5625 0.382812 12.5625H2.33594C2.38477 12.5625 2.42546 12.5381 2.45801 12.4893C2.50684 12.4567 2.53125 12.416 2.53125 12.3672V6.3125C2.53125 6.10091 2.60449 5.92188 2.75098 5.77539C2.91374 5.61263 3.10905 5.53125 3.33691 5.53125H12.6631C12.891 5.53125 13.0781 5.61263 13.2246 5.77539C13.3874 5.92188 13.4688 6.10091 13.4688 6.3125V12.3672C13.4688 12.416 13.485 12.4567 13.5176 12.4893C13.5664 12.5381 13.6152 12.5625 13.6641 12.5625H15.6172C15.666 12.5625 15.7067 12.5381 15.7393 12.4893C15.7881 12.4567 15.8125 12.416 15.8125 12.3672V3.99316C15.8125 3.7653 15.7474 3.55371 15.6172 3.3584C15.487 3.16309 15.3079 3.0166 15.0801 2.91895H15.1045Z"
                                                        fill="#111111" />
                                                </svg>
                                                <select name="terminal_to" class="search__select">
                                                    <?php foreach ($terminalsList as $id => $address): ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $address; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="row_title">
                                        <span><?php pt('Weight'); ?></span>
                                    </div>
                                    <div class="row weight nowrap">
                                        <div class="label_wrap weight_calc_block">
                                            <div class="text mobile_only">
                                                <span></span>
                                                <span></span>
                                            </div>
                                            <div class="wrap_input_prefix with_image" data-prefix='kg'>
                                                <svg width="23" height="13" viewBox="0 0 23 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <rect x="1" y="1" width="21" height="11" rx="1" stroke="#CFCFCF" stroke-width="2" />
                                                    <rect x="10" y="13" width="8" height="2" transform="rotate(-90 10 13)" fill="#4F4F4F" />
                                                </svg>
                                                <input type="text" name="parcels[1][weight]" class='weight only_integer'>
                                            </div>
                                            <div class="price__range price__range_slider range_slider" data-minCost='0' data-maxCost='60'
                                                data-createCost='30'></div>
                                        </div>
                                        <div class="visual_weight">
                                            <div class="icon">
                                                <div class="box"></div>
                                                <svg class='desktop' width="42" height="42" viewBox="0 0 22 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M10.6609 6.90042C12.5664 6.90042 14.1111 5.35571 14.1111 3.45021C14.1111 1.54471 12.5664 0 10.6609 0C8.7554 0 7.21069 1.54471 7.21069 3.45021C7.21069 5.35571 8.7554 6.90042 10.6609 6.90042Z"
                                                        fill="#E6E6E6" />
                                                    <path d="M21.3048 22.1166C20.3318 11.185 15.1223 9.00453 15.1223 9.00453C15.1223 9.00453 10.0875 5.97782 4.78737 9.83264C1.35133 12.9601 0.557849 17.7344 0.0146945 22.3457C-0.260031 24.6891 3.40508 24.6607 3.67745 22.3457C4.00176 19.5937 4.46462 16.8291 5.83038 14.4746L5.82409 17.1314L5.7989 29.8986V40.0273C5.7989 41.116 6.5971 42 7.71804 42C8.8382 42 9.74739 41.116 9.74739 40.0273V25.5495H11.5406C11.5406 28.9414 11.5406 36.6833 11.5406 40.0737C11.5406 42.4353 15.2026 42.4353 15.2026 40.0737C15.2026 36.6818 15.2026 33.289 15.2026 29.8979L15.3576 17.1054C15.3592 15.8845 15.3608 14.7934 15.3608 14.0346C16.8241 16.4324 17.3075 19.2804 17.6405 22.115C17.9152 24.4317 21.5795 24.46 21.3048 22.1166Z"
                                                        fill="#E6E6E6" />
                                                </svg>

                                                <svg class='mobile' width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <circle cx="26" cy="26" r="25.5" stroke="#D4D4D4" />
                                                    <path d="M25.4992 9.05049C27.7223 9.05049 29.5245 7.24833 29.5245 5.02525C29.5245 2.80216 27.7223 1 25.4992 1C23.2762 1 21.474 2.80216 21.474 5.02525C21.474 7.24833 23.2762 9.05049 25.4992 9.05049Z"
                                                        fill="#D4D4D4" />
                                                    <path d="M37.9173 26.8027C36.7822 14.0492 30.7044 11.5053 30.7044 11.5053C30.7044 11.5053 24.8305 7.97414 18.647 12.4714C14.6383 16.1201 13.7126 21.6901 13.0789 27.0699C12.7584 29.804 17.0344 29.7709 17.3521 27.0699C17.7305 23.8593 18.2705 20.634 19.8639 17.8871L19.8565 20.9866L19.8271 35.8818V47.6985C19.8271 48.9686 20.7584 50 22.0662 50C23.373 50 24.4337 48.9686 24.4337 47.6985V30.8077H26.5258C26.5258 34.765 26.5258 43.7973 26.5258 47.7527C26.5258 50.5078 30.7981 50.5078 30.7981 47.7527C30.7981 43.7954 30.7981 39.8372 30.7981 35.8808L30.979 20.9563C30.9808 19.5319 30.9827 18.259 30.9827 17.3737C32.6899 20.1711 33.2538 23.4938 33.6423 26.8009C33.9628 29.5036 38.2379 29.5367 37.9173 26.8027Z"
                                                        fill="#D4D4D4" />
                                                </svg>
                                            </div>
                                            <div class="text">
                                                <!-- <span>(large box)</span>
                                                                    <span>like washing machine</span> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row size nowrap">
                                    <label>
                                        <span class="row_title">
                                            <span><?php pt('Length'); ?></span>
                                        </span>
                                        <span class="wrap_input_prefix with_image" data-prefix='cm'>
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect x="1" y="1" width="14" height="14" rx="1" stroke="#CFCFCF" stroke-width="2" />
                                                <rect x="7" y="16" width="16" height="1.88235" transform="rotate(-90 7 16)" fill="#4F4F4F" />
                                            </svg>
                                            <input type="text" name="parcels[1][length]" value="30">
                                        </span>
                                    </label>
                                    <label>
                                        <span class="row_title">
                                            <span><?php pt('Width'); ?></span>
                                        </span>
                                        <span class="wrap_input_prefix with_image" data-prefix='cm'>
                                            <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect x="1" y="1" width="15" height="14" rx="1" stroke="#CFCFCF" stroke-width="2" />
                                                <rect y="7" width="17" height="2" fill="#4F4F4F" />
                                            </svg>
                                            <input type="text" name="parcels[1][width]" value="30">
                                        </span>
                                    </label>
                                    <label>
                                        <span class="row_title">
                                            <span><?php pt('Height'); ?></span>
                                        </span>
                                        <span class="wrap_input_prefix with_image" data-prefix='cm'>
                                            <svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M5 1.88235H8L8 0H5H3H0L0 1.88235H3V3.76471H2V4.70588H3L3 7.52941H2V8.47059H3V11.2941H2V12.2353H3L3 14.1176H0L0 16H3H5H8V14.1176H5V12.2353H6V11.2941H5L5 8.47059H6V7.52941H5V4.70588H6V3.76471H5V1.88235Z"
                                                    fill="#CFCFCF" />
                                            </svg>
                                            <input type="text" name="parcels[1][height]" value="30">
                                        </span>
                                    </label>
                                    <button class='main_btn'><?php pt('Get a quote'); ?></button>
                                </div>
                                <div class="row bottom nowrap">
                                    <div class="text">
                                        <?php pt('A lot of parcels? Use the %s detailed calculator %s', ['<a href="' . $aliasUrls['order'] . '">', '</a>']); ?>
                                    </div>

                                </div>

                                <input type="hidden" name="google_id_from" value="" id="google-id-from">
                                <input type="hidden" name="google_id_to" value="" id="google-id-to">
                            </form>
                        </div>
                        <!-- calc -->
                    </div>
                    <div class="right" style='background-image: url(/img/cargobus_image.png)'>
                        <?php if (!empty($home->title)): ?>
                            <h1>
                                <?php
                                    $pattern = '/\*(.*?)\*/';
                                    $replacement = '<span>$1</span>';
                                    echo preg_replace($pattern, $replacement, $home->title);
                                ?>
                            </h1>
                        <?php endif; ?>

                        <?php if (!empty($home->subtitle)): ?>
                            <h2><?php echo $home->subtitle; ?></h2>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end top_block_main_page -->

    <!-- start terminals_block -->
    <div class="section pp-scrollable">
        <div class="terminals_block category_block">
            <svg class='parallax el1' width="154" height="140" viewBox="0 0 154 140" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M108.757 25.9689L161.295 139.947L0.679997 89.7713L108.757 25.9689Z" fill="white" />
            </svg>
            <svg class='parallax el2 fixed' width="154" height="140" viewBox="0 0 154 140" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M108.757 25.9689L161.295 139.947L0.679997 89.7713L108.757 25.9689Z" fill="white" />
            </svg>
            <div class="rectangle"></div>
            <div class="container">
                <!--  -->
                <div class="terminals_controll">
                    <div class="row margin0 nowrap">
                        <div class="left">
                        <div class="line">
                        <div class="top_row row margin0 between">
                                <h2><?php pt('Terminals in'); ?>: </h2>
                                <div class="search_block_interactive" id='search_block_interactive'>
                                    <form action="<?php echo $aliasUrls['ajax_terminal']; ?>">
                                        <input type="text" placeholder="<?php echo $terminalFirst->city; ?>" onkeyup="find_text($(this).val())">
                                        <button class='icon'>
                                            <svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M21.2891 21.2891L27.5621 27.5626" stroke="#006ADE" stroke-width="2"
                                                    stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                                <path d="M15.9062 22.8125C19.7205 22.8125 22.8125 19.7205 22.8125 15.9062C22.8125 12.092 19.7205 9 15.9062 9C12.092 9 9 12.092 9 15.9062C9 19.7205 12.092 22.8125 15.9062 22.8125Z"
                                                    stroke="#006ADE" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round"
                                                    stroke-linejoin="round" />
                                            </svg>
                                        </button>
                                        <div class="results">
                                            <ul>
                                                <?php foreach ($terminals as $terminal): ?>
                                                    <?php $title = implode(', ', [$terminal->city, $terminal->address]); ?>
                                                    <li class="result_item" data-id="<?php echo $terminal->terminal_id; ?>" data-lat="<?php echo $terminal->pos_lat; ?>" data-lng="<?php echo $terminal->pos_lng; ?>" data-title='<?php echo $title; ?>'><?php echo $title; ?></li>
                                                <?php endforeach; ?>
                                            </ul>
                                            <div class="terminals_empty"><p><?php pt('No results'); ?></p></div> 
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="terminal_list">
                                <div class="terminal_list_item active" data-lat='<?php echo $terminalFirst->pos_lat; ?>' data-lng='<?php echo $terminalFirst->pos_lng; ?>' data-title='<?php echo $terminalFirst->city; ?>'>
                                    <div class="row margin0 between top_row">
                                        <span class="name"><?php pt('Terminal'); ?> №1</span>
                                        <span class="place"><?php echo $terminalFirst->city; ?></span>
                                    </div>
                                    <div class="row margin0 bottom_row">
                                        <div class="address icon">
                                            <span><?php pt('Address'); ?></span><br>
                                            <i style="font-style: normal;"><?php echo $terminalFirst->address; ?></i>
                                        </div>
                                        <div class='time monday icon'>
                                            <span><?php pt('Mo-Fr'); ?>:</span><br>
                                            <i style="font-style: normal;"><?php echo implode('<br/>', $terminalAvailability['monday']); ?></i>
                                        </div>
                                        <div class='mail icon'>
                                            <span><?php pt('Email'); ?></span><br>
                                            <a href="mailto:<?php echo $terminalFirst->email; ?>" target="_blank"><?php echo $terminalFirst->email; ?></a>
                                        </div>
                                        <div class='time saturday icon'>
                                            <span><?php pt('Sa'); ?>:</span><br>
                                            <i style="font-style: normal;"><?php echo ($terminalAvailability['saturday'][0] == 'Closed') ? gt('Closed') : implode('<br/>', $terminalAvailability['saturday']); ?></i>
                                        </div>
                                        <div class='tel icon'>
                                            <span><?php pt('Phone'); ?></span><br>
                                            <a href="tel:<?php echo $terminalFirst->phone; ?>"><?php echo $terminalFirst->phone; ?></a>
                                        </div>
                                        <div class='check sunday icon'>
                                            <span><?php pt('Su'); ?>:</span><br>
                                            <i style="font-style: normal;"><?php echo ($terminalAvailability['sunday'][0] == 'Closed') ? gt('Closed') : implode('<br/>', $terminalAvailability['sunday']); ?></i>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="terminal_list_change note" style="display: <?php echo (empty($terminalNote)) ? 'none' : 'block'; ?>;">
                                    <p><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z"></path></svg>
                                    <span> <?php echo $terminalNote; ?></span></p>
                                </div>
                                <a href="<?php echo (isset($moduleUrls['terminals'])) ? $moduleUrls['terminals'] : '#'; ?>" class="link"> <?php pt('All terminals'); ?></a>
                            </div>

                </div>


                            <div class="order_courier">
                                <div class="text row margin0">
                                    <?php if (isset($contactUs->phone)): ?>
                                        <div class="line">
                                            <span><?php pt('Courier'); ?>!</span>
                                            <?php echo parsePhone($contactUs->phone, null, null, 1); ?>
                                        </div>
                                    <?php endif; ?>
                                    <div class="line bottom"><?php pt('Save your time! Order now'); ?></div>
                                </div>
                                <button class="main_btn" onclick="document.location = '<?php echo isset($aliasUrls['order']) ? $aliasUrls['order'] : 'javascript:void();' ?>';"><?php pt('Calculate & order'); ?></button>
                            </div>
                        </div>

                        <div class="map right">
                            <!-- <img class='mumble' src="/img/map.jpg" alt="map"> -->
                            <div id='google_map'>
                                <!-- empty -->
                            </div>
                            <script>
                                // выводим координаты в шаблон
                                // изначально покажет эту точку
                                var markers_map = [
                                    ['<?php echo $terminalFirst->city; ?>', <?php echo $terminalFirst->pos_lat; ?>, <?php echo $terminalFirst->pos_lng; ?>],
                                ];
                            </script>
                            <div class="map_controll_btns">
                                <div class="btn minus">
                                    <svg width="13" height="1" viewBox="0 0 13 1" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect x="13" width="1" height="13" transform="rotate(90 13 0)" fill="black" />
                                    </svg>
                                </div>
                                <div class="btn plus">
                                    <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect x="6" width="1" height="13" fill="black" />
                                        <rect x="13" y="6" width="1" height="13" transform="rotate(90 13 6)" fill="black" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  -->
            </div>
        </div>
    </div>
    <!-- end terminals_block -->

    <!-- start news_block -->
    <div class="section pp-scrollable with_footer">
        <div class="category_block news_block">
            <img src="/img/triangle.svg" alt="triangle" class="triangle">
            <div class="container">
                <div class="top_row row margin0 between">
                    <h2><?php pt('News'); ?></h2>
                    <a href="<?php echo (isset($moduleUrls['news'])) ? $moduleUrls['news'] : '#'; ?>" class="link"><?php pt('See all news'); ?></a>
                </div>
                <?php if ($mainNews): ?>
                    <div class="cards row">
                        <div class="column w50">
                            <div class="news_item big">
                                <a href="<?php echo (isset($moduleUrls['news'])) ? $moduleUrls['news'] . $mainNews->slug : '#'; ?>" class="date_news"><?php echo $mainNews->date->format('d F'); ?></a>
                                <a class='main' href="<?php echo (isset($moduleUrls['news'])) ? $moduleUrls['news'] . $mainNews->slug : '#'; ?>">
                                    <?php if ($mainNews->img): ?>
                                        <div class="img">
                                            <img src="<?php echo $mainNews->img_path; ?>" alt="<?php echo $mainNews->title; ?>">
                                        </div>
                                    <?php endif; ?>
                                    <div class="text_content">
                                        <h5><?php echo $mainNews->title; ?></h5>
                                        <span><?php echo $mainNews->intro; ?></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                        
                        <div class="column w50">
                            <?php if ($news): ?>
                                <?php foreach ($news as $entity): ?>
                                    <div class="news_item">
                                        <a href="<?php echo (isset($moduleUrls['news'])) ? $moduleUrls['news'] . $entity->slug : '#'; ?>" class="date_news"><?php echo $entity->date->format('d F'); ?></a>
                                        <a class='main' href="<?php echo (isset($moduleUrls['news'])) ? $moduleUrls['news'] . $entity->slug : '#'; ?>">
                                            <?php if ($entity->img): ?>
                                                <div class="img">
                                                    <img src="<?php echo $entity->img_path; ?>" alt="<?php echo $entity->title; ?>">
                                                </div>
                                            <?php endif; ?>
                                            <div class="text_content">
                                                <h5><?php echo $entity->title; ?></h5>
                                                <span><?php echo $entity->intro; ?></span>
                                            </div>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- end news_block -->
</div>