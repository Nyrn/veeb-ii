<div class="nav-tabs-horizontal nav-tabs-inverse">
    <ul class="nav nav-tabs nav-tabs-solid">
        <li class="<?php echo (isset($tab) && $tab == 'content') ? 'active' : null; ?>">
            <a href="/cms/pages/home/edit/<?php echo $itemId; ?>">Content</a>
        </li>

        <li class="<?php echo (isset($tab) && $tab == 'blocks') ? 'active' : null; ?>">
            <a href="/cms/pages/home/blocks/index/<?php echo $itemId; ?>">Blocks</a>
        </li>
    </ul>
 </div>