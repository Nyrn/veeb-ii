<?php
use Cake\Routing\Router;

Router::plugin('Page', function ($routes) {
    $routes->fallbacks('InflectedRoute');
});