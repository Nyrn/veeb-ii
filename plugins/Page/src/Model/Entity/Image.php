<?php
namespace Page\Model\Entity; 

use Cake\ORM\Entity;

class Image extends Entity {
	
	public function _getImgPath() {
		if (!empty($this->_properties['file'])) {
			return '/file/pages_images/589x274/' . $this->_properties['file'];
		}
	}
}
?>