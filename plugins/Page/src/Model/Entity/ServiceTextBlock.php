<?php
namespace Page\Model\Entity; 

use Page\Model\Entity\Block;

class ServiceTextBlock extends Block {
	public function _getImgPath() {
		if (!empty($this->_properties['img'])) {
			return '/file/content/960x/' . $this->_properties['img'];
		}
	}
}
?>