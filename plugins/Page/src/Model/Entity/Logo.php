<?php
namespace Page\Model\Entity; 

use Cake\ORM\Entity;

class Logo extends Entity {
	
	public function _getImgPath() {
		if (!empty($this->_properties['img'])) {
			return '/file/logo/default/' . $this->_properties['img'];
		}
	}
}
?>