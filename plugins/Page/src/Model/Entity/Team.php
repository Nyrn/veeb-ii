<?php
namespace Page\Model\Entity; 

use Cake\ORM\Entity;

class Team extends Entity {
	
	public function _getImgPath() {
		if (!empty($this->_properties['img'])) {
			return '/file/team/140x140/' . $this->_properties['img'];
		}
	}
}
?>