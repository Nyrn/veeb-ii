<?php
namespace Page\Model\Entity; 

use Cake\ORM\Entity;

class Page extends Entity {
	public function _getImgPath() {
		if (!empty($this->_properties['img'])) {
			return '/file/content/486x/' . $this->_properties['img'];
		}
	}
}
?>