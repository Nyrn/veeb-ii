<?php
namespace Page\Model\Entity; 

use Page\Model\Entity\Block;

class HowToSend extends Block {
	public function _getImgPath() {
		if (!empty($this->_properties['img'])) {
			return '/file/content/440x/' . $this->_properties['img'];
		}
	}
}
?>