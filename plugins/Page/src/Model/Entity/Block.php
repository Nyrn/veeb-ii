<?php
namespace Page\Model\Entity; 

use Cake\ORM\Entity;

class Block extends Entity {
	public function _getImgPath() {
		if (!empty($this->_properties['img'])) {
			return '/file/content/default/' . $this->_properties['img'];
		}
	}
}
?>