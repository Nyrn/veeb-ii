<?php
namespace Page\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;

class TextTable extends AppTable
{
    public $mirrorCommonFields = ['is_public', 'is_deleted']; // Mirror items common fields
    
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pages_texts');
    }
}
?>