<?php
namespace Page\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;

class PageAliasTable extends AppTable
{
    public $saveUrl = false;
    public $savePageAlias = false;

	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pages_alias');

        $this->belongsTo('Module', [
            'className' => 'Page.Module'
        ]);

        $this->belongsTo('Language', [
            'className' => 'Language.Language',
            'foreignKey' => 'lang_id' 
        ]);
    }

    public function saveAlias($item) {
        $exists = $this->find('url', ['url' => $item->url]);

        if ($exists->count() > 0) {
            $this->delete($exists);
        }

        $save = $this->newEntity();

        $save->lang_id = $item->lang_id;
        $save->module_id = $item->module_id;
        $save->url = $item->url;

        $this->save($save);
    }
}
?>