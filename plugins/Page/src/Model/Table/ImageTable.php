<?php
namespace Page\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;
use Cake\Event\Event;
use Cake\ORM\Entity;

class ImageTable extends AppTable
{
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pages_images');
    }
}
?>