<?php
namespace Page\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;

class FaqTable extends AppTable
{
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pages_faq');
    }
}
?>