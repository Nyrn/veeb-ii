<?php
namespace Page\Model\Table; 

use Cake\ORM\Query;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;
use Cake\Collection\Collection;

class BlockTable extends AppTable
{
    public $mirrorCommonFields = ['mirror_id']; // Mirror items common fields

	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pages_blocks');
    }



    public function parseByNumbers($blocks = []) {
    	if ($blocks) {
    		return (new Collection($blocks))->combine('number', function ($entity) { return $entity; })->toArray();
    	}

    	return false;
    }
}
?>