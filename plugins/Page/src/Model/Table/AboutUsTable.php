<?php
namespace Page\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use Cms\Model\Table\CmsTable;

class AboutUsTable extends BlockTable
{	
	public function initialize(array $config)
    {
        parent::initialize($config);
    }

	public function beforeFind($event, $query, $options) {
		return $query->where(['section' => 'about_us']);
	}
}
?>