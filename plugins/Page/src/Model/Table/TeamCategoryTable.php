<?php
namespace Page\Model\Table; 

use Cake\ORM\Query;
use Cake\Event\Event;
use Cake\ORM\Entity;
use App\Model\Table\AppTable;

class TeamCategoryTable extends AppTable
{   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pages_team_category');
    }
}
?>