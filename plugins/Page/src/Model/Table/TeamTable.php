<?php
namespace Page\Model\Table; 

use Cake\ORM\Query;
use Cake\Event\Event;
use Cake\ORM\Entity;
use App\Model\Table\AppTable;

class TeamTable extends AppTable
{   
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pages_team');

        $this->hasMany('TeamCategory', [
            'className' => 'Page.TeamCategory',
            'foreignKey' => 'team_id'
        ]);
    }

    public function groupByCategories($items = []) {
        $team = [];

        if ($items) {
            foreach ($items as $item) {
                foreach ($item->team_category as $category) {
                    $team[$category->category][$item->id] = $item;
                }
            }      
        }

        return $team;
    }
}
?>