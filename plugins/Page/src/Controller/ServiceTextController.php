<?php
namespace Page\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Lib\CmsRequest;

class ServiceTextController extends AppController {
    public function initialize() {
    	parent::initialize();

        $this->modelClass = 'Page.ServiceText';
    }

    public function index() {
        $textTable = TableRegistry::get('Page.Text');
        $blockTable = TableRegistry::get('Page.ServiceTextBlock');

        $text = $textTable
            ->find('langId')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->first();

        $blocks = $blockTable
            ->find('langId')
            ->find('active')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->all();

        $this->set('text', $text);
        $this->set('blocks', $blocks);
    }
}
?>