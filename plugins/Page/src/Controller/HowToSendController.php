<?php
namespace Page\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Lib\CmsRequest;

class HowToSendController extends AppController {
    public function initialize() {
    	parent::initialize();

        $this->modelClass = 'Page.HowToSend';
    }

    public function index() {
        $textTable = TableRegistry::get('Page.Text');
        $blockTable = TableRegistry::get('Page.HowToSend');

        $text = $textTable
            ->find('langId')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->first();

        $blocks = $blockTable
            ->find('langId')
            ->find('active')
            ->all();

        $this->set('text', $text);
        $this->set('blocks', $blocks);
    }
}
?>