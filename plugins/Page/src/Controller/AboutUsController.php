<?php
namespace Page\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Lib\CmsRequest;
use Cake\Collection\Collection;

class AboutUsController extends AppController {
    public function initialize() {
    	parent::initialize();

        $this->modelClass = 'Page.AboutUs';
    }

    public function index() {
        $textTable = TableRegistry::get('Page.Text');
        $blockTable = TableRegistry::get('Page.AboutUs');
        $teamTable = TableRegistry::get('Page.Team');
        $contactUsTable = TableRegistry::get('Page.ContactUs');

        $text = $textTable
            ->find('langId')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->first();

        $blocks = $blockTable
            ->find('langId')
            ->find('active')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->all();

        $blocks = $blockTable->parseByNumbers($blocks);

        $team = $teamTable
            ->find('langId')
            ->find('active')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->contain(['TeamCategory'])
            ->all();

        $team = $teamTable->groupByCategories($team);

        $contactDetails = $contactUsTable
            ->find('langId')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->first();

        $this->set('text', $text);
        $this->set('blocks', $blocks);
        $this->set('team', $team);
        $this->set('contactDetails', $contactDetails);
    }
}
?>