<?php
namespace Page\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Lib\CmsRequest;

class FaqController extends AppController {
    public function initialize() {
    	parent::initialize();

        $this->modelClass = 'Page.Faq';
    }

    public function index() {
        $textTable = TableRegistry::get('Page.Text');
        $faqTable = TableRegistry::get('Page.Faq');

        $text = $textTable
            ->find('langId')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->first();

        $faqs = $faqTable
            ->find('langId')
            ->find('active')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->all();

        $this->set('text', $text);
        $this->set('faqs', $faqs);
    }
}
?>