<?php
namespace Page\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Lib\CmsRequest;

class TermsController extends AppController {
    public function initialize() {
    	parent::initialize();

        $this->modelClass = 'Page.Text';
    }

    public function index() {
        $textTable = TableRegistry::get('Page.Text');

        // Get text page
        $query = $textTable
            ->find('langId')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->first();

        $text = $query;

        $this->set(compact(
            'text'
        ));
    }
}
?>