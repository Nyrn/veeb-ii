<?php
namespace Page\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Lib\CmsRequest;
use Cake\Collection\Collection;

class TerminalController extends AppController {
    public function initialize() {
    	parent::initialize();
    }

    public function index() {
        $letter = false;
        $lettersList = [];

        if (isset(CmsRequest::$urlParts['id'])) {
            $this->view();
        } else {
            if ($this->request->query('letter')) {
                $letter = $this->request->query('letter');
            }

            for ($i = 65; $i < 91; $i++) {
                $lettersList[] = chr($i);
            }

            $parcelShopTable = TableRegistry::get('Cargobus.ParcelShop');

            $query = $parcelShopTable
                ->find()
                ->where(['type' => 'STATIONARY_TERMINAL']);

            $allTerminals = $query->all();

            if ($letter) {
                $query->where(new \Cake\Database\Expression\Comparison('city', "$letter%", 'string', 'LIKE'));
            }

            $terminals = $query->all();

            $terminalsList = $parcelShopTable->parseAsLettersList($terminals);
            $allTerminalsList = $parcelShopTable->parseAsLettersList($allTerminals);

            $lettersList = array_unique(array_merge($lettersList, array_keys($terminalsList)));

            usort($lettersList, 'customSort');

            $this->set('letterSet', $letter);
            $this->set('lettersList', $lettersList);
            $this->set('terminals', $terminals);
            $this->set('terminalsList', $terminalsList);
            $this->set('terminalsLetters', array_keys($terminalsList));
            $this->set('allTerminalsLetters', array_keys($allTerminalsList));
        }
    }

    public function view() {
        $id = CmsRequest::$urlParts['id'];

        $parcelShopTable = TableRegistry::get('Cargobus.ParcelShop');

        $terminal = $parcelShopTable
            ->find()
            ->where(['terminal_id' => $id, 'type' => 'STATIONARY_TERMINAL'])
            ->first();

        if (!$terminal) {
            $this->redirect(CmsRequest::$page->url);
        }

        $availability = $parcelShopTable->parseAvailability($terminal->availability);

        $this->set('terminal', $terminal);
        $this->set('availability', $availability);

        $this->render('view');
    }
}
?>