<?php
namespace Page\Controller;

use App\Controller\AppController;
use App\Lib\CmsRequest;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Core\Configure;
use Cake\Routing\Router;
use UploadHandler;

require_once(ROOT . DS . 'vendor' . DS  . 'UploadHandler.php');

class PageController extends AppController {
    public function initialize() {
    	parent::initialize();

        $this->modelClass = 'Page.Page';
    }

    public function index() {
        
    }

    public function error404() {
        
    }

    public function maintenance() {
    	$this->render('error404');
    }
}
?>