<?php
namespace Page\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Lib\CmsRequest;

class ServiceController extends AppController {
    public function initialize() {
    	parent::initialize();
    }

    public function index() {
        $textTable = TableRegistry::get('Page.Text');
        $imageTable = TableRegistry::get('Page.Image');
        $blockTable = TableRegistry::get('Page.ServiceBlock');

        $text = $textTable
            ->find('langId')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->first();

        $images = $imageTable
            ->find()
            ->find('notDeleted')
            ->where(['page_id' => CmsRequest::$page->id])
            ->order(['seq' => 'asc'])
            ->all();

        $blocks = $blockTable
            ->find('langId')
            ->find('active')
            ->find('pageId', ['page_id' => CmsRequest::$page->id])
            ->all();

        $blocks = $blockTable->parseByNumbers($blocks);

        $this->set('text', $text);
        $this->set('images', $images);
        $this->set('blocks', $blocks);
    }
}
?>