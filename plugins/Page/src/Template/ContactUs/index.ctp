<div class="contacts-page">
    <div class="row">
        <div class="col-xs-24 col-md-12">
            <ul class="details">
                <?php if (isset($contactUs) && $contactUs->phone): ?><li class="icon phone"><?php echo $contactUs->phone; ?></li><?php endif; ?>
                <?php if (isset($contactUs) && $contactUs->email): ?><li class="icon email"><a href="mailto:<?php echo $contactUs->email; ?>"><?php echo $contactUs->email; ?> </a></li><?php endif; ?>
                <?php if (isset($contactUs) && $contactUs->address): ?><li class="icon address"><?php echo $contactUs->address; ?></li><?php endif; ?>
            </ul>

            <h4 style="margin-top: 40px;"><?php echo __('Darba laiks'); ?></h4>

            <ul class="details">
                <?php if (isset($contactUs) && $contactUs->working_hours_monday_friday): ?><li><?php echo __('P-Pk'); ?>: <?php echo $contactUs->working_hours_monday_friday; ?></li><?php endif; ?>
                <?php if (isset($contactUs) && $contactUs->working_hours_saturday): ?><li><?php echo __('S'); ?>: <?php echo $contactUs->working_hours_saturday; ?></li><?php endif; ?>
                <?php if (isset($contactUs) && $contactUs->working_hours_sunday): ?><li><?php echo __('Sv'); ?>: <?php echo $contactUs->working_hours_sunday; ?></li><?php endif; ?>
            </ul>

             <div style="margin-top: 40px;">
                <a href="#" class="request-price-btn size-med" data-toggle="modal" data-target="#request-price"><?php echo __('Nosūtīt cenas pieprasījumu'); ?></a>
            </div>
        </div>

        <div class="col-xs-24 col-md-12">
            <div id="contact">
                <div class="row">
                    <div class="col-xs-24 col-md-24">
                        <div class="form-wrapper">
                            <h3><?php echo __('Iesūti savu jautājumu'); ?></h3>

                            <?php echo $this->Flash->render() ?>

                            <?php echo $this->Form->create('ContactUs', ['class' => 'form-horizontal contact-form']); ?>
                                <div class="row">
                                    <div class="col-xs-24">
                                        <div class="form-group">
                                            <div class="col-sm-24">
                                                <?php 
                                                    echo $this->Form->input('ContactUs.company', [
                                                        'label' => false,
                                                        'class' => 'form-control company',
                                                        'placeholder' => __('Uzņēmuma nosaukums')
                                                    ]); 
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-24">
                                        <div class="form-group">
                                            <div class="col-sm-24">
                                                <?php 
                                                    echo $this->Form->input('ContactUs.phone', [
                                                        'label' => false,
                                                        'class' => 'form-control phone',
                                                        'placeholder' => __('Telefona numurs')
                                                    ]); 
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-24">
                                        <div class="form-group">
                                            <div class="col-sm-24">
                                                <?php 
                                                    echo $this->Form->input('ContactUs.email', [
                                                        'label' => false,
                                                        'class' => 'form-control email',
                                                        'placeholder' => __('E-pasta adrese')
                                                    ]); 
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-24">
                                        <div class="form-group">
                                            <div class="col-sm-24">
                                                <?php 
                                                    echo $this->Form->input('ContactUs.text', [
                                                        'label' => false,
                                                        'type' => 'textarea',
                                                        'class' => 'form-control',
                                                        'placeholder' => __('Ziņojuma teksts')
                                                    ]); 
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-24">
                                        <?php echo $this->Form->submit(__('Nosūtīt ziņojumu'), ['class' => 'btn btn-primary pull-right']); ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php /*<div class="col-xs-12 col-md-12">
            <h4><?php pt('Rekvizīti'); ?></h4>

            <ul class="details">
                <?php if (isset($contactUs) && $contactUs->company_name): ?><li><?php echo $contactUs->company_name; ?></li><?php endif; ?>
                <?php if (isset($contactUs) && $contactUs->company_regnr): ?><li><?php echo $contactUs->company_regnr; ?></li><?php endif; ?>
                <?php if (isset($contactUs) && $contactUs->company_bank): ?><li><?php echo $contactUs->company_bank; ?></li><?php endif; ?>
                <?php if (isset($contactUs) && $contactUs->company_account): ?><li><?php echo $contactUs->company_account; ?></li><?php endif; ?>
            </ul>
        </div>*/ ?>
    </div>
</div>  

