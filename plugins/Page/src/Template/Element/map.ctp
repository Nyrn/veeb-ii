<div id="map-canvas"></div>

<?php if (isset($contactUs) && !empty($contactUs)): ?>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
	<script>
		function initMap() {
		  	var object_latlng = new google.maps.LatLng( <?php echo $contactUs->map_lat; ?>, <?php echo $contactUs->map_lng; ?> );

			var mapOptions = {
				scrollwheel: true,
				zoom: 14,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: object_latlng, 
				disableDefaultUI: true,
				minZoom: 6, 
				maxZoom: 21
			}

			var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

			var marker = new google.maps.Marker({
				clickable: true,
				position: object_latlng,
				map: map,
				icon: '/img/icons/pin.png',
				title: 'Title'
			});
			
		}

		//init map
		google.maps.event.addDomListener(window, 'load', initMap);

	</script>
<?php endif; ?>