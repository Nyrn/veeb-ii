<div class="wrapper page404">

    <!-- default svg parallax -->
    <svg class='parallax el10' width="148" height="109" viewBox="0 0 148 109" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M84.6109 20.1141L125.44 108.69L0.621549 69.6967L84.6109 20.1141Z" fill="white" />
    </svg>
    <svg class='parallax el11' width="148" height="109" viewBox="0 0 148 109" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M84.6109 20.1141L125.44 108.69L0.621549 69.6967L84.6109 20.1141Z" fill="white" />
    </svg>
    <svg class='parallax el12' width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M44.4661 44.2075L0.599145 43.948L40.2853 0.539418L44.4661 44.2075Z" fill="white" />
    </svg>
    <svg class='parallax el13' width="156" height="223" viewBox="0 0 156 223" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M132.486 124.33L0.791827 197.096L46.7674 0.674452L132.486 124.33Z" fill="white" />
    </svg>
    <svg class='parallax el14' width="51" height="92" viewBox="0 0 51 92" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M51 46L3.30637e-06 91.8993L7.31902e-06 0.100652L51 46Z" fill="white" />
    </svg>

    <div class="container">
        <div class="content_404">
            <p class="error_text">404</p>
            <p><?php pt('We can’t find the page %s that are you looking for :(', ['<br>']); ?></p>
			
			<a href="/<?php echo $cmsRequest::$urlParts['first']; ?>" class="main_btn no_bg"><?php pt('Back to home'); ?></a>
        </div>
    </div>
</div>