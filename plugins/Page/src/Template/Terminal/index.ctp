<div class="wrapper order_page">


    <!-- тут основной контент страницы -->
    <!-- start order_block -->
    <div class="top_row_inner_page top_row container">
        <h1><?php echo $cmsRequest::$page->title; ?></h1>
    </div>
    <div class="terminals_data container">
        <div class="terminals_city_toolbar row">
            <div class="w33">
                <div class="terminals_sidebar">
                    <a href="javascript:void(0);" class="show_city active"><?php pt('City'); ?></a>
                    <a href="javascript:void(0);" class="show_map"><?php pt('Map'); ?></a>
                </div>
            </div>
            <div class="w67">
                <div class="terminals_city_alphabet">
                    <p class="<?php echo (!$letterSet) ? 'active' : null; ?>"><a href="<?php echo $cmsRequest::$page->url; ?>"><?php pt('All'); ?></a></p>
                    <ol>
                        <?php foreach ($lettersList as $letter): ?>
                            <?php if (in_array($letter, $allTerminalsLetters)): ?><li class="<?php echo ($letterSet == $letter) ? 'active' : null; ?>"><a href="?letter=<?php echo $letter; ?>"><?php echo $letter; ?></a></li><?php endif; ?>  
                        <?php endforeach; ?>                                        
                    </ol>
                </div>
            </div>
        </div>
        <div class="terminals_city row">
            <?php foreach ($terminalsLetters as $letter): ?>
                <div class="terminals_city_list w33">
                    <p><span><?php echo $letter; ?></span><span><?php echo count($terminalsList[$letter]); ?></span></p>
                    <ol>
                        <?php foreach ($terminalsList[$letter] as $id => $terminal): ?>
                            <li><a href="<?php echo $cmsRequest::$page->url . $id; ?>"><?php echo $terminal['city']; ?>, <?php echo $terminal['address']; ?></a></li>
                        <?php endforeach; ?>
                    </ol>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="terminals_map">
            <div id="google_map"></div>

            <script>
                var markers_map = [
                                        <?php foreach ($terminals as $terminal): ?>
                        <?php echo ((!empty($terminal->pos_lat)) ? "[" . "'" . $terminal->name ."', " . $terminal->pos_lat . ', ' . $terminal->pos_lng . "]," : null) . "\n"; ?>
                    <?php endforeach; ?>
                ];
            </script>
        </div>
        
        <div class="terminals_city_text">
            <p><?php pt('Information about opening times during national holidays you can find in %s News section %s', ['<a href="' . (isset($moduleUrls['news']) ? $moduleUrls['news'] : '#') . '">', '</a>']); ?></p>
            <p><?php pt('In Baltic States we have terminals and warehouses in Riga and Vilnius. Additionally there are opened parcel points in Latvia and Lithuania, as well.'); ?></p>
        </div>
    </div>
    <!-- end order_block -->


</div>
