<div class="wrapper terminal_page_container">
    <div class="top_row_inner_page top_row terminal_page container">
        <h1><?php echo $terminal->city; ?></h1>
        <a href="<?php echo $cmsRequest::$page->url; ?>">All terminals</a>
    </div>
    <div class="terminal_page_wrap container">
        <div class="terminals_controll">
            <div class="row margin0 nowrap">
                <div class="left">
                    <div class="line">
                        
                        <div class="terminal_list terminal_page_list">
                            <div class="terminal_list_item active" data-lat='<?php echo $terminal->pos_lat; ?>' data-lng='<?php echo $terminal->pos_lng; ?>' data-title='<?php echo $terminal->address; ?>'>
                                <div class="row margin0 between top_row">
                                    <span class="name"><?php pt('Terminal'); ?> №1</span>
                                    <span class="place"><?php echo $terminal->city; ?></span>
                                </div>
                                <div class="row margin0 bottom_row">
                                    <div class="address icon">
                                        <span><?php pt('Address'); ?></span><br>
                                        <?php echo $terminal->address; ?>
                                    </div>
                                    <div class='time icon'>
                                        <span><?php pt('Mo-Fr'); ?>:</span><br>
                                        <?php echo implode('<br/>', $availability['monday']); ?>
                                    </div>
                                    <div class='mail icon'>
                                        <span><?php pt('Email'); ?></span><br>
                                        <a href="<?php echo $terminal->email; ?>" target="_blank"><?php echo $terminal->email; ?></a>
                                    </div>
                                    <div class='time icon'>
                                        <span><?php pt('Sa'); ?>:</span><br>
                                        <?php echo implode('<br/>', $availability['saturday']); ?>
                                    </div>
                                    <div class='tel icon'>
                                        <span><?php pt('Phone'); ?></span><br>
                                        <a href="tel:<?php echo $terminal->phone; ?>"><?php echo $terminal->phone; ?></a>
                                    </div>
                                    <div class='time icon'>
                                        <span><?php pt('Su'); ?>:</span><br>
                                        <?php echo implode('<br/>', $availability['sunday']); ?>
                                    </div>
                                </div>
                            </div>
                            <?php /*<div class="terminal_list_item" data-lat='56.940309' data-lng='24.119642'
                                data-title='Tallin №1'>
                                <div class="row margin0 between top_row">
                                    <span class="name">Terminal №2</span>
                                    <span class="place">Tallin</span>
                                </div>
                                <div class="row margin0 bottom_row">
                                    <div class="address icon">
                                        <span>address</span><br>
                                        Lastekodu 46</div>
                                    <div class='time icon'>
                                        <span>Mo-Fr:</span><br>
                                        7:30AM – 8PM
                                    </div>
                                    <div class='mail icon'>
                                        <span>email</span><br>
                                        <a href="mailto:info@cargobus.ee" target="_blank">info@cargobus.ee</a>
                                    </div>
                                    <div class='check icon'>
                                        <span>Sa-su:</span><br>
                                        Check closer
                                    </div>
                                    <div class='tel icon'>
                                        <span>phone</span><br>
                                        <a href="tel:+3726813444">+372 6 813 444</a>
                                    </div>
                                </div>
                            </div>*/ ?>
                        </div>

                    </div>
                </div>

                <div class="map right">
                    <!-- <img class='mumble' src="/img/map.jpg" alt="map"> -->
                    <div id='google_map'>
                        <!-- empty -->
                    </div>
                    <script>
                        // выводим координаты в шаблон
                        // изначально покажет эту точку
                        var markers_map = [
                            ['<?php echo $terminal->address; ?>', <?php echo $terminal->pos_lat; ?>, <?php echo $terminal->pos_lng; ?>],
                        ];
                    </script>
                    <div class="map_controll_btns">
                        <div class="btn minus">
                            <svg width="13" height="1" viewBox="0 0 13 1" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <rect x="13" width="1" height="13" transform="rotate(90 13 0)" fill="black" />
                            </svg>
                        </div>
                        <div class="btn plus">
                            <svg width="13" height="13" viewBox="0 0 13 13" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <rect x="6" width="1" height="13" fill="black" />
                                <rect x="13" y="6" width="1" height="13" transform="rotate(90 13 6)" fill="black" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="terminal_order_courier">
                    <div class="text row margin0">
                        <?php /*<p>You can order a courier until 14:00 in <span>Tallin</span></p>*/ ?>
                    </div>
                    <button class="main_btn" onclick="document.location = '<?php echo isset($aliasUrls['order']) ? $aliasUrls['order'] : 'javascript:void();' ?>';"><?php pt('Order a courier'); ?></button>
                </div>
        </div>
    </div>
</div>