<div class="wrapper order_page">
    <div class="services_additional_block container">
        <?php foreach ($blocks as $block): ?>
            <div class="services_additional_list">
                <div class="row">
                    <div class="additional_list_text w50">
                        <div class="list_text_wrap">
                            <h3><?php echo $block->title; ?></h3>
                            <?php echo $block->text; ?>
                        </div>

                    </div>

                    <?php if ($block->img): ?>
                        <div class="additional_list_img w50" style="background-image: url(<?php echo $block->img_path; ?>);"></div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>