<div class="wrapper service_page">

    <div class="service_template">
        <div class="service_info_wrap">
            <div class="container">
                <div class="row">
                    <div class="w50">
                        <?php if (!empty($cmsRequest::$page->img_path)): ?>
                            <div class="service_info_img">
                                <img src="<?php echo $cmsRequest::$page->img_path; ?>" alt="">
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="service_info_text w50">
                        <h1><?php echo $cmsRequest::$page->title; ?></h1>
                        <?php if ($text) echo $text->content; ?>
                    </div>
                </div>
            </div>
        </div>
        
        <?php if (isset($blocks) && !empty($blocks)): ?>
            <div class="service_description_wrap">
                <div class="container">
                    <div class="row">
                        <div class="w50 service_description_advantage">
                            <?php if (isset($blocks[1])): ?>
                                <p><?php echo $blocks[1]->text; ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="w50 service_description_text">
                            <?php if (isset($blocks[2])): ?>
                                <h3><?php echo $blocks[2]->title; ?></h3>

                                <?php echo $blocks[2]->text; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($images->count() > 0): ?>
            <div class="service_info_slider_wrap">
                <div class="container">
                    <div class="service_info_slider_control">
                        <div class="slider_control_wrap">
                            <div class="service_info_prev">
                                <svg width="17" height="17" viewBox="0 0 17 17" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="8.5" cy="8.5" r="8.5" />
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M6.14582 9.03555C5.95058 8.84027 5.9506 8.52369 6.14588 8.32844L9.32812 5.14671C9.52339 4.95147 9.83998 4.95149 10.0352 5.14677C10.2305 5.34205 10.2304 5.65863 10.0352 5.85388L7.20651 8.68208L10.0347 11.5107C10.23 11.706 10.2299 12.0226 10.0347 12.2178C9.83937 12.4131 9.52279 12.4131 9.32755 12.2178L6.14582 9.03555Z"
                                        fill="#F5F8FD" />
                                </svg>

                            </div>
                            <div class="service_info_dots"></div>
                            <div class="service_info_next">
                                <svg width="17" height="17" viewBox="0 0 17 17" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <circle r="8.5" transform="matrix(-1 0 0 1 8.5 8.5)" />
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M10.8542 9.03555C11.0494 8.84027 11.0494 8.52369 10.8541 8.32844L7.67188 5.14671C7.47661 4.95147 7.16002 4.95149 6.96478 5.14677C6.76953 5.34205 6.76956 5.65863 6.96484 5.85388L9.79349 8.68208L6.96529 11.5107C6.77004 11.706 6.77007 12.0226 6.96535 12.2178C7.16063 12.4131 7.47721 12.4131 7.67245 12.2178L10.8542 9.03555Z"
                                        fill="#F5F8FD" />
                                </svg>

                            </div>
                        </div>
                    </div>
                    <div class="service_info_slider">
                        <?php foreach ($images as $img): ?>
                            <div class="info_slider_item">
                                <img src="<?php echo $img->img_path; ?>" alt="">
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>