<div class="wrapper how_send_page">
    <svg class='parallax el4' width="150" height="270" viewBox="0 0 150 270" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path opacity="0.5" d="M150 135L0.750003 269.234L0.750014 0.766061L150 135Z" fill="white" />
    </svg>
    <svg class='parallax el5' width="156" height="140" viewBox="0 0 156 140" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M108.757 25.9689L161.295 139.947L0.679993 89.7714L108.757 25.9689Z" fill="white" />
    </svg>
    <svg class='parallax el6' width="134" height="188" viewBox="0 0 134 188" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path opacity="0.6" d="M22.1445 81.8402L133.874 24.675L90.3223 187.211L22.1445 81.8402Z" fill="white" />
    </svg>
    <svg class='parallax el7' width="100" height="140" viewBox="0 0 100 140" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M18.7566 25.9689L71.2951 139.947L-89.32 89.7714L18.7566 25.9689Z" fill="white" />
    </svg>
    <svg class='parallax el8' width="175" height="365" viewBox="0 0 175 365" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M2.51019e-06 182.5L201.75 0.201663L201.75 364.798L2.51019e-06 182.5Z" fill="white" />
    </svg>


    <div class="top_row_inner_page top_row container no_padding_bottom">
        <h1><?php echo $cmsRequest::$page->title; ?></h1>
        
        <?php if ($text): ?>
            <p><?php echo $text->content; ?></p>
        <?php endif; ?>
    </div>

    <div class="landing">
        <div class="container no_padding_top">
            <div class="landing_content">
                <?php foreach ($blocks as $k => $block): ?>
                    <div class="row between align-center">
                        <div class="text w50">
                            <div class="top_row row margin0 align-center">
                                <div class="count"><span><?php echo ($k+1); ?></span></div>
                                <h2><?php echo $block->title; ?></h2>
                            </div>
                            <!--  -->
                            <div class="content">
                                <?php echo $block->text; ?>
                            </div>
                        </div>
                        <div class="img w50">
                            <?php if (!empty($block->img)): ?>
                                <img src="<?php echo $block->img_path; ?>" alt="landing">
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                
                <div class="row bottom_row align-center">
                    <h5><?php pt('Don’t waste your time. %s Send a cargo or order a courier.', ['<br/>']); ?></h5>
                    <a href="<?php echo (isset($aliasUrls['order'])) ? $aliasUrls['order'] : '#'; ?>" class="main_btn"><?php pt('Delivery calculator'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>