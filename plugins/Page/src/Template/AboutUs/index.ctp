<div class="wrapper order_page">
    <div class="about_us_main">
        <div class="about_top_bg">
            <div class="about_us_title container">
                <?php if (isset($blocks[1])): ?>
                    <h1><?php echo $blocks[1]->title; ?></h1>
                    <p><?php echo $blocks[1]->text; ?></p>
                <?php endif; ?>
            </div>
        </div>

        <div class="about_bottom_bg">
            <div class="about_us_text container">
                <div class="row">
                    <div class="w50">
                        <?php if (isset($blocks[2])): ?>
                            <?php echo $blocks[2]->text; ?>
                        <?php endif; ?>
                    </div>
                    <div class="w50">
                        <?php if (isset($blocks[3])): ?>
                            <h4><?php echo preg_replace('/\*(.*?)\*/', '<span>$1</span>', $blocks[3]->text); ?></h4>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="about_us_service container">
        <div class="row">
            <div class="about_us_service_img w50">
                <div class="service_img_mask">
                    <img src="/img/service_img.png" alt="">
                </div>
            </div>
            <div class="about_us_service_text w50">
                <?php if (isset($blocks[4])): ?>
                    <?php echo $blocks[4]->text; ?>
                <?php endif; ?>

                <?php if (isset($blocks[4]->custom_text)): ?>
                    <?php $customText = json_decode($blocks[4]->custom_text, true); ?>

                    <h3><?php echo $customText['title']; ?></h3>
                    <ol>
                        <li><?php echo preg_replace('/\*(.*?)\*/', '<span>$1</span>', $customText['value'][1]); ?></li>
                        <li><?php echo preg_replace('/\*(.*?)\*/', '<span>$1</span>', $customText['value'][2]); ?></li>
                        <li><?php echo preg_replace('/\*(.*?)\*/', '<span>$1</span>', $customText['value'][3]); ?></li>
                    </ol>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php if ($team): ?>
        <div class="about_team">
            <!-- start tabs -->
            <div class="tabs swipe container">
                <h2><?php pt('Our managers'); ?></h2>
                <div class="managers_slider-tabs">
                <div class="main_tabs">
                    <div class="wrap">
                    <div class="main_tab  active" data-tab-index='customer_service'>
                        <span><?php pt('Customer service'); ?></span>
                    </div>
                    <div class="main_tab" data-tab-index='sales_and_logistic'>
                        <span><?php pt('Sales and logistic'); ?></span>
                    </div>
                    <div class="main_tab" data-tab-index='accounting'>
                        <span><?php pt('Accounting'); ?></span>
                    </div>
                    <div class="main_tab" data-tab-index='administration'>
                        <span><?php pt('Administration'); ?></span>
                    </div>
                </div>
                </div>
                <div class="managers_slider_control">
                    <div class="slider_control_wrap">
                        <div class="service_info_prev">
                            <svg width="17" height="17" viewBox="0 0 17 17" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <circle cx="8.5" cy="8.5" r="8.5" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M6.14582 9.03555C5.95058 8.84027 5.9506 8.52369 6.14588 8.32844L9.32812 5.14671C9.52339 4.95147 9.83998 4.95149 10.0352 5.14677C10.2305 5.34205 10.2304 5.65863 10.0352 5.85388L7.20651 8.68208L10.0347 11.5107C10.23 11.706 10.2299 12.0226 10.0347 12.2178C9.83937 12.4131 9.52279 12.4131 9.32755 12.2178L6.14582 9.03555Z"
                                    fill="#F5F8FD" />
                            </svg>
                        </div>
                        <div class="service_info_dots"></div>
                        <div class="service_info_next">
                            <svg width="17" height="17" viewBox="0 0 17 17" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <circle r="8.5" transform="matrix(-1 0 0 1 8.5 8.5)" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M10.8542 9.03555C11.0494 8.84027 11.0494 8.52369 10.8541 8.32844L7.67188 5.14671C7.47661 4.95147 7.16002 4.95149 6.96478 5.14677C6.76953 5.34205 6.76956 5.65863 6.96484 5.85388L9.79349 8.68208L6.96529 11.5107C6.77004 11.706 6.77007 12.0226 6.96535 12.2178C7.16063 12.4131 7.47721 12.4131 7.67245 12.2178L10.8542 9.03555Z"
                                    fill="#F5F8FD" />
                            </svg>

                        </div>
                    </div>
                </div>
                </div>
                <!--  -->
                <div class="tabs_content">
                    <!-- tab -->
                    <?php foreach ($team as $category => $items): ?>
                        <div class="item" data-tab-index='<?php echo $category; ?>'>
                            <div class="managers_slider row margin0">
                                <?php foreach ($items as $item): ?>
                                    <div class="manager_item">
                                        <div class="top">
                                            <img src="src" data-src="<?php echo $item->img_path; ?>" alt="manager" class='owl-lazy'>
                                            <span><?php echo $item->title; ?></span>
                                        </div>
                                        <div class="bottom">
                                            <span class="name"><?php echo $item->name; ?></span>
                                            <?php if ($item->phone): echo parsePhone($item->phone); endif; ?>
                                            <?php if ($item->email): echo parseEmail($item->email); endif; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <!--  -->
            </div>
            <!-- end tabs -->
        </div>
    <?php endif; ?>

    <?php if ($contactDetails): ?>
        <div class="about_map container">
            <div class="row align-center">
                <div class="content content_text w50">
                    <h1><?php pt('Contact us'); ?></h1>
                    <div class="row wrap">
                        <div class="w50">
                            <?php if ($contactDetails->phone): echo parsePhone($contactDetails->phone); endif; ?>
                            <?php if ($contactDetails->email): echo parseEmail($contactDetails->email); endif; ?>
                            <?php if ($contactDetails->working_hours_monday_friday): ?>
                                <p class="clock icon">
                                    <?php if ($contactDetails->working_hours_monday_friday): ?><span><b><?php pt('Mo-fr'); ?></b> <?php echo $contactDetails->working_hours_monday_friday; ?></span><?php endif; ?>
                                    <?php if ($contactDetails->working_hours_saturday): ?><span><b><?php pt('Sa'); ?></b> <?php echo $contactDetails->working_hours_saturday; ?></span><?php endif; ?>
                                    <?php if ($contactDetails->working_hours_sunday): ?><span><b><?php pt('Su'); ?></b> <?php echo $contactDetails->working_hours_sunday; ?></span><?php endif; ?>
                                </p>
                            <?php endif; ?>
                        </div>
                        <div class=" w50">
                            <h3><?php pt('Company name'); ?></h3>
                            <p>
                                <?php if ($contactDetails->company_name): ?><span><?php echo $contactDetails->company_name; ?></span><?php endif; ?>
                                <?php if ($contactDetails->address): ?><p class="icon"><?php echo $contactDetails->address; ?></p><?php endif; ?>
                                <?php /*if ($contactDetails->company_regnr): ?><span><?php echo $contactDetails->company_regnr; ?></span><?php endif; ?>
                                <?php if ($contactDetails->company_vatnr): ?><span><?php echo $contactDetails->company_vatnr; ?></span><?php endif;*/ ?>
                            </p>
                            
                            <?php /*<p>
                                <?php if ($contactDetails->company_address): ?><span><?php echo $contactDetails->company_address; ?></span><?php endif; ?>
                                <?php if ($contactDetails->company_bank): ?><span><?php echo $contactDetails->company_bank; ?></span><?php endif; ?>
                                <?php if ($contactDetails->company_account): ?><span><?php echo $contactDetails->company_account; ?></span><?php endif; ?>
                            </p>*/ ?>
                        </div>
                    </div>
                </div>

                <div class="map w50">
                    <div id="google_map" data-no-point="true" data-lat="<?php echo ($contactDetails->google_lat) ? $contactDetails->google_lat : null; ?>" data-lng="<?php echo ($contactDetails->google_lng) ? $contactDetails->google_lng : null; ?>" style="overflow: hidden;"></div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
