<div class="wrapper help_page">

    <!-- default svg parallax -->
    <svg class='parallax el17' data-direction='bottom' width="148" height="109" viewBox="0 0 148 109" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M84.8374 19.5125L125.667 108.088L0.848111 69.0952L84.8374 19.5125Z" fill="white" />
    </svg>
    <svg class='parallax el18 no_move' width="119" height="200" viewBox="0 0 119 200" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M42.9354 136.328C42.9354 126.464 44.0839 118.274 46.3808 111.757C48.6778 105.24 51.5932 99.7798 55.1269 95.3765C58.6607 90.9731 62.3712 87.2743 66.2584 84.2801C70.3222 81.2858 74.121 78.3796 77.6548 75.5614C81.1886 72.5672 84.1039 69.3087 86.4009 65.786C88.6978 62.0872 89.8463 57.4196 89.8463 51.7834C89.8463 43.8573 86.931 37.4284 81.1002 32.4967C75.4462 27.3888 68.1136 24.8349 59.1025 24.8349C50.4447 24.8349 43.4655 27.1246 38.1648 31.7041C33.0408 36.1074 29.0653 41.6557 26.2383 48.3487L0 38.5733C4.06385 27.4769 11.0431 18.3179 20.9376 11.0964C31.0089 3.69881 43.9072 0 59.6325 0C68.2903 0 76.2413 1.23294 83.4855 3.69881C90.7298 6.16468 97.0022 9.68736 102.303 14.2668C107.604 18.8463 111.667 24.3945 114.494 30.9115C117.498 37.2523 119 44.2977 119 52.0476C119 60.3258 117.763 67.1951 115.29 72.6552C112.816 78.1154 109.724 82.7829 106.013 86.6579C102.303 90.3567 98.2391 93.7032 93.8218 96.6975C89.4046 99.5156 85.3408 102.686 81.6303 106.209C77.9198 109.555 74.8278 113.606 72.3541 118.362C69.8805 123.118 68.6437 129.106 68.6437 136.328V138.97H42.9354V136.328ZM55.9221 200C49.9146 200 44.9673 198.063 41.0802 194.188C37.3697 190.313 35.5145 185.469 35.5145 179.657C35.5145 173.844 37.3697 169 41.0802 165.126C44.9673 161.251 49.9146 159.313 55.9221 159.313C61.9295 159.313 66.7884 161.251 70.4989 165.126C74.2094 169 76.0646 173.844 76.0646 179.657C76.0646 185.469 74.121 190.313 70.2338 194.188C66.5234 198.063 61.7528 200 55.9221 200Z"
            fill="white" />
    </svg>
    <svg class='parallax el14 no_move' data-direction='top' width="57" height="54" viewBox="0 0 57 54" fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path d="M19.1486 16.5329L56.0049 22.1373L18.3528 53.8044L19.1486 16.5329Z" fill="white" />
    </svg>
    <svg class='parallax el15 no_move' width="119" height="111" viewBox="0 0 119 111" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M41.7866 78.0518L35.844 0.223081L118.615 64.2641L41.7866 78.0518Z" fill="white" />
    </svg>
    <svg class='parallax el16' data-direction='top' width="119" height="89" viewBox="0 0 119 89" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M50.3997 16.8682L118.196 55.55L18.7768 88.2307L50.3997 16.8682Z" fill="white" />
    </svg>
    <!--  -->

    <div class="top_row_inner_page top_row container">
        <h1><?php echo $cmsRequest::$page->title; ?></h1>
    </div>
    <div class="faq container container_without_padding">
        <div class="row">
            <div class="column w50">
            	<?php if ($text): ?>
                	<p><?php echo $text->content; ?></p>
                <?php endif; ?>

                <?php if ($faqs): ?>
	                <div class="accordion_wrap">
	                	<?php foreach ($faqs as $faq): ?>
		                    <div class="accordion">
		                        <div class="top">
		                            <p><?php echo $faq->title; ?></p>
		                            <svg class='icon' width="20" height="20" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
		                                <circle cx="15" cy="15" r="14.5" stroke="#006ADE" />
		                                <path fill-rule="evenodd" clip-rule="evenodd" d="M20.2946 14.7052C20.6842 14.3157 20.6842 13.684 20.2946 13.2945C19.9053 12.9052 19.2743 12.9048 18.8846 13.2937L15 17.1698L11.1154 13.2937C10.7257 12.9048 10.0947 12.9052 9.70538 13.2945C9.31581 13.684 9.31581 14.3157 9.70538 14.7052L15 19.9998L20.2946 14.7052Z"
		                                    fill="#006ade" />
		                            </svg>
		                        </div>
		                        <div class="content text">
		                            <p><?php echo $faq->text; ?></p>
		                        </div>
		                    </div>
		                <?php endforeach; ?>
	                </div>
	            <?php endif; ?>
            </div>
            <div class="column w50">
                <div class="contacts in_page">
                    <p><?php pt('Still have a questions? Please contact us'); ?></p>

                    <?php if (isset($contactUs->phone) && !empty($contactUs->phone)): ?>
                        <div class="tels">
                            <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13.2921 10.4971L10.8626 8.90908C10.619 8.75478 10.3253 8.70024 10.0426 8.75677C9.7598 8.81331 9.50969 8.97658 9.3442 9.21266C8.78355 9.8899 8.10609 11.0108 5.5598 8.46537C3.0135 5.91991 4.11145 5.21933 4.7889 4.65886C5.02506 4.49342 5.18839 4.24339 5.24494 3.96071C5.3015 3.67803 5.24693 3.38443 5.09259 3.14092L3.50407 0.712225C3.29383 0.408637 3.0135 -0.0817725 2.35941 0.011639C1.70532 0.10505 0 1.06252 0 3.16427C0 5.26603 1.65859 7.83485 3.92456 10.1001C6.19053 12.3653 8.76019 14 10.8393 14C12.9184 14 13.9229 12.1318 13.9929 11.6647C14.063 11.1977 13.5958 10.7072 13.2921 10.4971Z" fill="#006ADE"></path></svg>
                            <?php echo parsePhone($contactUs->phone, '<a href="tel:{phone}">{phone}</a>', ', '); ?>
                        </div>
                    <?php endif; ?>

                    <?php if (isset($contactUs->email) && !empty($contactUs->email)): ?>
                        <svg width="15" height="10" viewBox="0 0 15 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0 0.695422V9.28595L4.32409 4.87538L0 0.695422ZM0.700209 10.0002H14.3L9.95715 5.57042L7.84763 7.60961C7.65383 7.79695 7.34641 7.79695 7.15261 7.60961L5.0431 5.57042L0.700209 10.0002ZM15.0002 9.28595V0.695422L10.6762 4.87538L15.0002 9.28595ZM7.50012 6.5547L1.23664 0.5H13.7636L7.50012 6.5547Z"
                                fill="#006ADE"></path>
                        </svg>
                        <?php echo parseEmail($contactUs->email, '<a href="mailto:{email}"><span>{email}</span></a>', ', '); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>