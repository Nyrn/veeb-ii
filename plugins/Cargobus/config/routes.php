<?php
use Cake\Routing\Router;

Router::plugin('Language', function ($routes) {
    $routes->fallbacks('InflectedRoute');
});