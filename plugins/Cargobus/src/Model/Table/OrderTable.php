<?php
namespace Cargobus\Model\Table; 

require_once(ROOT . DS . 'vendor' . DS  . 'API' . DS . 'CargobusApi.php');

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;
use Cake\Network\Session;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use CargobusApi;
use ApiException;

class OrderTable extends AppTable
{
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table(false);
    }

	public function apiOrderPrice() {
		$session = new Session();

		$order = $session->read('Cargobus.Order');
        $orderApiResponse = $session->read('Cargobus.OrderApiResponse');

        $settingsTable = TableRegistry::get('Settings');

        $success = false;

        if (isset($order['from']) && isset($order['to']) && isset($order['parcels'])) {
            $api = new CargobusApi();
            $api->token = $settingsTable->getValue('cargobus_api_token', 0, getDomain());

            $additionalServices = $this->parseOrderAdditionalServices();

            $data = [
                'parcelType' => 1,
                'numberOfPackages' => count($order['parcels']),
                'parcelInsuranceValue' => $this->parseOrderInsuranceValue(),
                'cashOnDeliveryAmount' => 0,
                'floors' => 0,
                'weight' => $this->parseOrderWeight(),
                'parcelDimensions' => $this->parseOrderParcelDimensions(),
                'additionalServices' => $additionalServices
            ];

            $pickupDelivery = $this->parseApiDataPickupDelivery();
            $data['pickUp'] = $pickupDelivery['pickup'];
            $data['delivery'] = $pickupDelivery['delivery'];

            try {
                $service = $api->getQuote($data);

                if (!isset($service['validationErrors']) && !empty($service)) {
                    $service['complexUnitPriceNet'] = roundNumber($service['complexUnitPriceNet']);
                    $service['totalNetCost'] = roundNumber($service['totalNetCost']);
                    $service['totalCost'] = roundNumber($service['totalCost']);
                    $service['vatAmount'] = roundNumber($service['vatAmount']);

                    foreach ($additionalServices as $key => $s) {
                        if (isset($service['additionalServices'][$key])) {
                            $tmp = $service['additionalServices'][$key];

                            $service['additionalServices'][$s] = $tmp;
                            unset($service['additionalServices'][$key]);
                        }
                    }

                    $order['service'] = $service;
                    $orderApiResponse['errorCode'] = 0;
                    $orderApiResponse['quote'] = $service;

                    $success = true;
                } else {
                    unset($order['service']);
                    unset($order['additionalServices']);
                }

            } catch (ApiException $e) {
                unset($order['service']);
                unset($order['additionalServices']);
                
                $orderApiResponse['errorCode'] = $e->getCode();
            }

            $session->write('Cargobus.Order', $order);
            $session->write('Cargobus.OrderApiResponse', $orderApiResponse);

            if ($success) return true;
        }

        return false;
	}    

    public function apiOrderTransport() {
        $session = new Session();

        $order = $session->read('Cargobus.Order');
        $orderApiResponse = $session->read('Cargobus.OrderApiResponse');

        $settingsTable = TableRegistry::get('Settings');

        $data = [];
        $success = false;

        if (isset($order['from']) && isset($order['to']) && isset($order['parcels']) && isset($order['service']) && isset($order['sender']) && isset($order['recipient'])) {
            $api = new CargobusApi();
            $api->token = $settingsTable->getValue('cargobus_api_token', 0, getDomain());

            $pickupDelivery = $this->parseApiDataPickupDelivery();

            $data[] = [
                'weight' => $this->parseOrderWeight(),
                'units' => count($order['parcels']),
                'parcelDimensions' => $this->parseOrderParcelDimensions(),
                'sender' => [
                    'name' => $order['sender']['name'],
                    'phone' => $order['sender']['phone'],
                    'email' => $order['sender']['email'],
                    'pickUp' => $pickupDelivery['pickup'] + ['time' => (isset($order['pickup']['date'])) ? date('Y-m-d', strtotime($order['pickup']['date'])) : date('Y-m-d')]
                ],
                'receiver' => [
                    'name' => $order['recipient']['name'],
                    'phone' => $order['recipient']['phone'],
                    'email' => $order['recipient']['email'],
                    'delivery' => $pickupDelivery['delivery'] + ['time' => (isset($order['delivery']['date'])) ? date('Y-m-d', strtotime($order['delivery']['date'])) : date('Y-m-d')]
                ],
                'parcelService' => '',
                'additionalInfo' => isset($order['additionalText']) ? $order['additionalText'] : '',
                'payment' => '',
                'sendSMS' => false,
                'sendMail' => true,
                'additionalServices' => $this->parseOrderAdditionalServices(),
                'parcelInsuranceValue' => $this->parseOrderInsuranceValue(),
                'floors' => 0
            ];
           
            try {
                $apiOrder = $api->createOrder($data);
            
                if (!isset($apiOrder['validationErrors']) && !empty($apiOrder)) {
                    $orderApiResponse['errorCode'] = 0;
                    $orderApiResponse['transportOrder'] = $apiOrder['parcelOrders'][0]['parcelOrder'];

                    $session->write('Cargobus.OrderId', $orderApiResponse['transportOrder']['parcelCode']);

                    $success = true;
                } elseif (isset($apiOrder['validationErrors'])) {
                    $orderApiResponse['transportOrder'] = $apiOrder['validationErrors'];
                }
            } catch (ApiException $e) {
                $orderApiResponse['errorCode'] = $e->getCode();       
            }

            $session->write('Cargobus.OrderApiResponse', $orderApiResponse);

            if ($success) return true;
        }

        return false;
    }

	public function parseOrderAdditionalServicesPrice() {
		$session = new Session();
		$order = $session->read('Cargobus.Order');

		$response = [];

		if (isset($order['service']['additionalServices'])) {
			foreach ($order['service']['additionalServices'] as $service => $item) {
				$response[$service] = $item['priceNet'];
			}
		}

		return $response;
	}

    public function parseOrderVatCost() {
        $session = new Session();
        $order = $session->read('Cargobus.Order');

        $vatCost = 0;

        if (isset($order['service'])) {
            $vatCost = $order['service']['vatAmount'];
        }

        return $vatCost;
    }

    public function parseOrderWeight() {
        $session = new Session();
        $order = $session->read('Cargobus.Order');

        $weight = 0;

        if (isset($order['parcels'])) {
            foreach ($order['parcels'] as $parcel) {
                $weight += $parcel['weight'];
            }
        }

        return $weight;
    }

    public function parseOrderInsuranceValue() {
        $session = new Session();
        $order = $session->read('Cargobus.Order');

        $insuranceValue = 0;

        if (isset($order['additionalServices'])) {
            foreach ($order['additionalServices'] as $service => $value) {
                if ($service == 'insurance' && $value > 0) {
                    $insuranceValue = $value;
                } 
            }
        }

        return $insuranceValue;
    }

    public function parseOrderParcelDimensions() {
        $session = new Session();
        $order = $session->read('Cargobus.Order');

        $parcels = [];

        if (isset($order['parcels'])) {
            foreach ($order['parcels'] as $parcel) {
                $parcels[] = [
                    'width' => $parcel['width'],
                    'length' => $parcel['length'],
                    'height' => $parcel['height']
                ];
            }
       }

       return $parcels;
    }

    public function parseOrderAdditionalServices() {
        $session = new Session();
        $order = $session->read('Cargobus.Order');

        $services = [];

        if (isset($order['additionalServices'])) {
            foreach ($order['additionalServices'] as $service => $value) {
                if ($service == 'insurance') {
                    if ($value > 0) {
                        $services[] = $service;
                    }
                } else {
                    $services[] = $service;
                }
            }
        }

        return $services;
    }

    public function parseApiDataPickupDelivery() {
        $session = new Session();
        $order = $session->read('Cargobus.Order');

        $response = [];

        if (isset($order['from'])) {
            $typeFrom = isset($order['from']['type']) ? $order['from']['type'] : null;
            $terminalIdFrom = ($typeFrom == 'terminal') ? $order['from']['address'] : 0;
            $googleIdFrom = ($typeFrom == 'courier') ? $order['from']['google_id'] : null;

            $response['pickup'] = ($typeFrom == 'terminal') ? ['terminalId' => $terminalIdFrom] : ['googlePlaceId' => $googleIdFrom];
        }

        if (isset($order['to'])) {
            $typeTo = isset($order['to']['type']) ? $order['to']['type'] : null;
            $terminalIdTo = ($typeTo == 'terminal') ? $order['to']['address'] : 0;
            $googleIdTo = ($typeTo == 'courier') ? $order['to']['google_id'] : null;

            $response['delivery'] = ($typeTo == 'terminal') ? ['terminalId' => $terminalIdTo] : ['googlePlaceId' => $googleIdTo];
        }

        return $response;
    }

    public function sendOrderDetails($order = array()) {
        $session = new Session();
        

        $info = [];
        $infoFrom = [];
        $infoTo = [];
        $infoServices = [];
        $weight = null;

        if (!empty($order)) {
            $cargobusTable = TableRegistry::get('Cargobus.Cargobus');

            if (getDomain() == 'cargobus.lv') {
                $to = 'cargobus@cargobus.lv';
                $sendgrid = 'sendgrid_latvia';
            } elseif (getDomain() == 'cargobus.lt') {
                $to = 'cargobus@cargobus.lt';
                $sendgrid = 'sendgrid_lithuania';
            } else {
                $to = 'info@cargobus.ee';
                $sendgrid = 'sendgrid_estonia';
            }

            $orderId = $session->read('Cargobus.OrderId');

            $infoSession = $cargobusTable->createInfoFromSession($order);

            if ($infoSession) {
                $info = $infoSession['info'];
                $infoFrom = $infoSession['infoFrom'];
                $infoTo = $infoSession['infoTo'];
                $infoServices = $infoSession['infoServices'];
                $weight = $infoSession['weight'];
            }

            $email = new Email($sendgrid);
            $result = @$email
                ->to($to)
                ->subject(gt('New order information'))
                ->emailFormat('html')
                ->template('Cargobus.new_order_information')
                ->viewVars(['order' => $order, 'orderId' => $orderId, 'info' => $info, 'infoFrom' => $infoFrom, 'infoTo' => $infoTo, 'infoServices' => $infoServices, 'weight' => $weight])
                ->send();
        } else {
            return false;
        }
    }
}
?>