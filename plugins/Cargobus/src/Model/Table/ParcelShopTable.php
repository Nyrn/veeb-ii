<?php
namespace Cargobus\Model\Table; 

use Cake\ORM\Query;
use App\Model\Table\AppTable;
use Cake\Core\Configure;
use Cake\Network\Session;

class ParcelShopTable extends AppTable
{
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('parcel_shop');
    }

    public function parseAsList($terminals = []) {
    	$list = [];
        $countries = Configure::read('Countries');

    	if ($terminals) {
    		foreach ($terminals as $terminal) {
    			$list[$terminal->terminal_id] = implode(', ', [$terminal->city, $terminal->address, gt($countries[$terminal->country])]);
    		}
    	}

    	return $list;
    }

    public function parseAsLettersList($terminals = []) {
        $list = [];

        if ($terminals) {
            foreach ($terminals as $terminal) {
                $fl = mb_substr($terminal->city, 0, 1, "UTF-8");
                
                $list[$fl][$terminal->terminal_id] = [
                    'country' => $terminal->country,
                    'city' => $terminal->city,
                    'address' => $terminal->address,
                    'full' => implode(', ', [$terminal->country, $terminal->city, $terminal->address])
                ];
            }
        }

        return $list;
    }

    public function parseAvailability($availability = null) {
        $r = [
            'monday' => [],
            'saturday' => [],
            'sunday' => []
        ];

        if ($availability) {
            $data = json_decode($availability, true);

            foreach ($data as $day => $hours) {
                $label = ($day != 'saturday' && $day != 'sunday') ? 'monday' : $day;

                foreach ($hours as $h) {
                    $hString = date('H:i', strtotime($h['from'])) . ' - ' . date('H:i', strtotime($h['to']));

                    $r[$label][$hString] = $hString;
                }
            }

            if (empty($r['saturday'])) $r['saturday'][] = 'Closed';
            if (empty($r['sunday'])) $r['sunday'][] = 'Closed';
        }

        return $r;
    }

    public function parseNote($note = null) {
        if ($note) {
            $session = new Session();

            $language = $session->read('Language');
            $domain = getDomain();

            $n = json_decode($note, true);

            return (isset($n[$domain][$language->id])) ? $n[$domain][$language->id] : false;
        }
    }
}
?>