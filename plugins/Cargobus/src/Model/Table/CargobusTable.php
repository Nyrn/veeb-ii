<?php
namespace Cargobus\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class CargobusTable extends AppTable
{
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table(false);
    }

    public function getApiUsername($domain = null) {
        if ($domain == 'cargobus.ee') {
            return 'cargobus.ee';
        } elseif ($domain == 'cargobus.lv') {
            return 'cargobus.lv'; // test: cargo.lv
        } elseif ($domain == 'cargobus.lt') {
            return 'cargobus.lt';
        }

        return false;
    }

    public function getApiPassword($domain = null) {
        if ($domain == 'cargobus.ee') {
            return '*?h?b%9Vet*YevF#C392&b6QmqdVr&'; // live: *?h?b%9Vet*YevF#C392&b6QmqdVr&
        } elseif ($domain == 'cargobus.lv') {
            return '$q,P-Z&3g!SzrdrW39pd:RaUMPU5de'; // test: c4rg0lvW3bD3v
        } elseif ($domain == 'cargobus.lt') {
            return '5k=mgmjAf7A7kpRC-gn?!@d_S%E23+';
        }

        return false;
    }

    public function parseParcelInfoParties($info = []) {
    	if ($info) {
    		$parties = [];

    		foreach ($info['parties'] as $party) {
        		if ($party['type']['value'] == 'PARCEL_PARTY_TYPE.SENDER') $parties['from'] = $party; 
        		elseif ($party['type']['value'] == 'PARCEL_PARTY_TYPE.RECEIVER') $parties['to'] = $party; 
        		elseif ($party['type']['value'] == 'PARCEL_PARTY_TYPE.PAYER') $parties['payer'] = $party; 
        	}

    		return $parties;
    	}

    	return false;
    }

    public function parseParcelInfoServices($info = []) {
    	if ($info) {
    		$infoServices = [];

    		foreach ($info['services'] as $service) {
        		$sType = $service['type'];

        		if ($sType == 'unitable') $sType = $service['serviceName'];

        		$infoServices[$sType] = $service;
        	}

        	return $infoServices;
    	}

    	return false;
    }

    public function parseParcelEvents($tracking = []) {
    	if ($tracking) {
    		$events = [];

    		foreach ($tracking['parcelHistoryEvents'] as $ev) {
    			if ($ev['eventType'] == 'PARCEL_STATUS.FORMALIZED') {
    				$events['created'] = $ev;
    			} elseif ($ev['eventType'] == 'PARCEL_STATUS.IN_PICK_UP_TERMINAL') {
    				$events['shipped'] = $ev;
    			} elseif ($ev['eventType'] == 'PARCEL_STATUS.DELIVERED') {
    				$events['delivered'] = $ev;
    			} else {
    				$events['out_of_delivery'][] = $ev;
    			}
    		}

    		return $events;
    	}

    	return false;
    }

    public function createInfoFromSession($order = null) {
        if (isset($order) && !empty($order)) {
            $parcelShopTable = TableRegistry::get('Cargobus.ParcelShop');
            $orderTable = TableRegistry::get('Cargobus.Order');

            if ($order['from']['type'] == 'terminal') {
                $terminalFrom = $parcelShopTable
                    ->find()
                    ->where(['terminal_id' => $order['from']['address']])
                    ->first();
            }

            if ($order['to']['type'] == 'terminal') {
                $terminalTo = $parcelShopTable
                    ->find()
                    ->where(['terminal_id' => $order['to']['address']])
                    ->first();
            }

            $info = [
                'createdOn' => date('Y-m-d H:i'),
                'additionalInfo' => $order['additionalText'],
                'vatRate' => $order['service']['vatRate'],
                'totalPriceWithVat' => $order['service']['totalCost']
            ];

            $infoFrom = [
                'deliveryMethod' => $order['from']['type'],
                'googleAddress' => [
                    'formattedAddress' => $order['from']['address']
                ],
                'name' => $order['sender']['name'],
                'email' => $order['sender']['email'],
                'phone' => $order['sender']['phone']
            ];

            if (isset($terminalFrom)) {
                $infoFrom['terminal'] = [
                    'city' => $terminalFrom->city,
                    'address' => $terminalFrom->address
                ];
            }

            $infoTo = [
                'deliveryMethod' => $order['to']['type'],
                'googleAddress' => [
                    'formattedAddress' => $order['to']['address']
                ],
                'name' => $order['recipient']['name'],
                'email' => $order['recipient']['email'],
                'phone' => $order['recipient']['phone']
            ];

            if (isset($terminalTo)) {
                $infoTo['terminal'] = [
                    'city' => $terminalTo->city,
                    'address' => $terminalTo->address
                ];
            }

            $infoServices = [
                'complex' => [
                    'serviceName' => $order['service']['complexServiceName'],
                    'priceWithoutVat' => $order['service']['complexUnitPriceNet']
                ]
            ];

            foreach ($order['service']['additionalServices'] as $sType => $service) {
                $infoServices[$sType]['priceWithoutVat'] = $service['priceNet'];
            }

            $weight = $orderTable->parseOrderWeight();

            return [
                'info' => $info,
                'infoFrom' => $infoFrom,
                'infoTo' => $infoTo,
                'infoServices' => $infoServices,
                'weight' => $weight
            ];
        }

        return false;
    }
}
?>