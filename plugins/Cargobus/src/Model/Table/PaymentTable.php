<?php
namespace Cargobus\Model\Table; 

require_once(ROOT . DS . 'vendor' . DS  . 'Paysera' . DS . 'WebToPay.php');
require_once(ROOT . DS . 'vendor' . DS  . 'API' . DS . 'CargobusApi.php');

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;
use Cake\Core\Configure;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use WebToPay;
use CargobusApi;
use ApiException;

class PaymentTable extends AppTable
{
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table(false);
    }

    public function paysera($type = 'banklink') {
        $session = new Session();

        $language = $session->read('Language');

        $order = $session->read('Cargobus.Order');
        $orderApiResponse = $session->read('Cargobus.OrderApiResponse');

        $parcelCode = $orderApiResponse['transportOrder']['parcelCode'];

        $payseraTable = TableRegistry::get('Cargobus.Paysera');

        $price = (IS_LIVE) ? ($orderApiResponse['transportOrder']['costFull'] * 100) : 1;

        $countryCode = 'EE';
        $lang = 'EST';

        if (HOSTNAME == DOMAIN_LV) {
            $countryCode = 'LV';
            $lang = 'LAV';
        }

        if (HOSTNAME == DOMAIN_LT) {
            $countryCode = 'LT';
            $lang = 'LIT';
        }

        if ($language->slug == 'en') $lang = 'ENG';
        if ($language->slug == 'ru') $lang = 'RUS';

        try {
            $data = array(
                'projectid'     => Configure::read('Paysera.projectId'),
                'sign_password' => Configure::read('Paysera.password'),
                'orderid'       => $parcelCode,
                'amount'        => $price,
                'currency'      => 'EUR',
                'country'       => $countryCode,
                'payment'       => $type,
                'accepturl'     => Configure::read('Paysera.acceptUrl'),
                'cancelurl'     => Configure::read('Paysera.cancelUrl'),
                'callbackurl'   => Configure::read('Paysera.callbackUrl'),
                'p_countrycode' => $countryCode,
                'lang' => $lang
                //'test'          => 1,
            );
            
            $request = WebToPay::buildRequest($data);

            $payseraTable->transaction($data['orderid'], ['amount' => $data['amount'], 'currency' => $data['currency'], 'request' => $request['data']]);

            WebToPay::redirectToPayment($data);
        } catch (WebToPayException $e) {
            Log::emergency($parcelCode . ': ' . $e->getData(), 'payment');
            // handle exception
        } 
    }

    public function paypal() {
        $session = new Session();

        $order = $session->read('Cargobus.Order');
        $orderApiResponse = $session->read('Cargobus.OrderApiResponse');

        $config = array(
            'mode' => Configure::read('Paypal.environment')
        );

        $price = (Configure::read('Paypal.environment') == 'live') ? $orderApiResponse['transportOrder']['costFull'] : 0.01;
        //$price = 0.01;

        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                Configure::read('Paypal.clientId'),     // ClientID
                Configure::read('Paypal.secretId')      // ClientSecret
            )
        );

        $apiContext->setConfig($config);

        $payer = new \PayPal\Api\Payer();
        $payer->setPaymentMethod('paypal');

        $item1 = new \PayPal\Api\Item(); 
        $item1->setName($orderApiResponse['transportOrder']['parcelCode'])
            ->setCurrency('EUR') 
            ->setQuantity(1) 
            ->setPrice($price);

        $itemList = new \PayPal\Api\ItemList(); 
        $itemList->setItems(array($item1));

        $amount = new \PayPal\Api\Amount();
        $amount->setTotal($price);
        $amount->setCurrency('EUR');

        $transaction = new \PayPal\Api\Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setInvoiceNumber($orderApiResponse['transportOrder']['parcelCode']);

        $redirectUrls = new \PayPal\Api\RedirectUrls();
        $redirectUrls->setReturnUrl(Configure::read('Paypal.returnUrl'))
            ->setCancelUrl(Configure::read('Paypal.cancelUrl'));

        $payment = new \PayPal\Api\Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);

        try {
            $payment->create($apiContext);

            header("Location: ". $payment->getApprovalLink());
            exit;
        }
        catch (\PayPal\Exception\PayPalConnectionException $ex) {
            // This will print the detailed information on the exception.
            //REALLY HELPFUL FOR DEBUGGING
            echo $ex->getData();
            exit;
        }
    }

    public function apiPaymentAdd($orderId = 0, $paymentResponse = null) {
        if ($orderId && $paymentResponse) {
            $settingsTable = TableRegistry::get('Settings');

            $api = new CargobusApi();
            $api->token = $settingsTable->getValue('cargobus_api_token', 0, getDomain());

            try {
                $info = $api->getParcelInfo($orderId);

                $data = [
                    'invoiceId' => $info['invoiceId'],
                    'basketId' => $info['basketId'],
                    'paymentType' => 'PAYMENT_TYPE.EXTERNAL_3RD_PARTY',
                    'amountPaid' => ($paymentResponse['amount'] / 100),
                    'paymentTime' => date('Y-m-d H:i:s'),
                    'voucherId' => 0,
                    'payerName' => $paymentResponse['name'] . ' ' . $paymentResponse['surename'],
                    'payerAccountNumber' => $paymentResponse['account'],
                    'paymentResponse' => base64_encode(json_encode($paymentResponse)),
                    'referenceNumber' => null
                ];

                $api->addPayment($data);
            } catch (ApiException $e) {
                // failed
            }
        }
    }
}
?>