<?php
namespace Cargobus\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;

class PayseraTable extends AppTable
{
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('paysera');
    }

    public function transaction($orderId = 0, $data = array()) {
    	if ($orderId) {
    		$entity = $this
    			->find()
    			->where(['order_id' => $orderId])
    			->first();

    		if (!$entity) {
    			$entity = $this->newEntity();
    		}

    		$entity->order_id = $orderId;

    		if (isset($data['amount'])) $entity->amount = $data['amount'];
    		if (isset($data['currency'])) $entity->currency = $data['currency'];
    		if (isset($data['country'])) $entity->country = $data['country'];
    		if (isset($data['test'])) $entity->test = $data['test'];
    		if (isset($data['version'])) $entity->version = $data['version'];
    		if (isset($data['project_id'])) $entity->project_id = $data['projectid'];
    		if (isset($data['type'])) $entity->type = $data['type'];
    		if (isset($data['lang'])) $entity->lang = $data['lang'];
    		if (isset($data['payment'])) $entity->payment = $data['payment'];
    		if (isset($data['paytext'])) $entity->paytext = $data['paytext'];
    		if (isset($data['p_email'])) $entity->p_email = $data['p_email'];
    		if (isset($data['m_pay_restored'])) $entity->m_pay_restored = $data['m_pay_restored'];
    		if (isset($data['tried_changing_email'])) $entity->tried_changing_email = $data['tried_changing_email'];
    		if (isset($data['account'])) $entity->account = $data['account'];
    		if (isset($data['status'])) $entity->status = $data['status'];
    		if (isset($data['payamount'])) $entity->payamount = $data['payamount'];
    		if (isset($data['paycurrency'])) $entity->paycurrency = $data['paycurrency'];
    		if (isset($data['requestid'])) $entity->requestid = $data['requestid'];
    		if (isset($data['name'])) $entity->name = $data['name'];
    		if (isset($data['surename'])) $entity->surename = $data['surename'];
    		if (isset($data['payment_country'])) $entity->payment_country = $data['payment_country'];
    		if (isset($data['payer_country'])) $entity->payer_country = $data['payer_country'];
    		if (isset($data['request'])) $entity->request = $data['request'];
    		if (isset($data['ip'])) $entity->ip = $data['ip'];
    		if (isset($data['is_confirmed'])) $entity->is_confirmed = $data['is_confirmed'];

    		$this->save($entity);
    	}

    	return false;
    } 
}
?>