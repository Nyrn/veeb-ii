<?php
namespace Cargobus\Controller;

require_once(ROOT . DS . 'vendor' . DS  . 'Paysera' . DS . 'WebToPay.php');
require_once(ROOT . DS . 'vendor' . DS  . 'API' . DS . 'CargobusApi.php');

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use App\Lib\CmsRequest;
use Cake\Network\Session;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\Core\Configure;
use WebToPay;
use WebToPayException;
use CargobusApi;
use ApiException;

class PaymentController extends AppController {

	public function process() {
		$session = new Session();

		try {
			$response = WebToPay::validateAndParseData(removeQuotes($_GET), Configure::read('Paysera.projectId'), Configure::read('Paysera.password'));

			if ($response) {
				$orderId = $response['orderid'];

				$payseraTable = TableRegistry::get('Cargobus.Paysera');
				$cargobusTable = TableRegistry::get('Cargobus.Cargobus');
				$orderTable = TableRegistry::get('Cargobus.Order');
				$settingsTable = TableRegistry::get('Settings');

				if ($response['status'] == 1 || $response['status'] == 2) {
					$api = new CargobusApi();
			        $api->token = $settingsTable->getValue('cargobus_api_token', 0, getDomain());

					// Order information
					$order = $session->read('Cargobus.Order');
					$orderApiResponse = $session->read('Cargobus.OrderApiResponse');

					$info = [];
					$infoFrom = [];
					$infoTo = [];
					$infoPayer = [];
					$infoServices = [];
					$weight = null;

			        try {
			        	$info = $api->getParcelInfo($orderId);

			        	$infoParties = $cargobusTable->parseParcelInfoParties($info);
			        	$infoServices = $cargobusTable->parseParcelInfoServices($info);

			        	if ($infoParties) {
			        		$infoFrom = $infoParties['from'];
			        		$infoTo = $infoParties['to'];
			        		$infoPayer = $infoParties['payer'];
			        	}
			        } catch (ApiException $e) {	
			        	$infoSession = $cargobusTable->createInfoFromSession($order);

			        	if ($infoSession) {
			        		$info = $infoSession['info'];
			        		$infoFrom = $infoSession['infoFrom'];
			        		$infoTo = $infoSession['infoTo'];
			        		$infoServices = $infoSession['infoServices'];
			        		$weight = $infoSession['weight'];
			        	}
			        }

			        $orderTable->sendOrderDetails($order);
			        $this->removeOrderSession();

			        $this->set('orderId', $orderId);
		        	$this->set('info', $info);
		        	$this->set('infoFrom', $infoFrom);
		        	$this->set('infoTo', $infoTo);
		        	$this->set('infoPayer', $infoPayer);
		        	$this->set('infoServices', $infoServices);
		        	$this->set('order', $order);
		        	$this->set('orderApiResponse', $orderApiResponse);
		        	$this->set('weight', $weight);

			        $this->render('accepted');
				} else {
					$this->redirect($this->aliasUrls['order_payment'] . '?error');
				}
			} else {
				$this->redirect($this->aliasUrls['order_payment'] . '?error');
			}
		} catch (WebToPayException $e) {
			$this->redirect($this->aliasUrls['order_payment'] . '?error');
		}
	}

	public function callback() {
		$this->autoRender = false;

		$payseraTable = TableRegistry::get('Cargobus.Paysera');
		$paymentTable = TableRegistry::get('Cargobus.Payment');

		try {
		    $response = WebToPay::validateAndParseData(removeQuotes($_GET), Configure::read('Paysera.projectId'), Configure::read('Paysera.password'));

		    Log::emergency($_GET, 'payment');
		    Log::emergency($response, 'payment');
		 
		    if ($response['test'] !== '0') {
		        //throw new WebToPayException('Testing, real payment was not made');
		    }
		    if ($response['type'] !== 'macro') {
		        throw new WebToPayException('Only macro payment callbacks are accepted');
		    }
		 
		    $orderId = $response['orderid'];
		    $amount = $response['amount'];
		    $currency = $response['currency'];
		    $status = $response['status'];

		    if ($response['status'] == 1) {
				$response['is_confirmed'] = 'y';
				$paymentTable->apiPaymentAdd($orderId, $response);
			}

		    $payseraTable->transaction($orderId, $response);

		    echo 'OK';
		} catch (WebToPayException $e) {
		    $error = get_class($e) . ': ' . $e->getMessage();
		    Log::emergency($error, 'payment');
		}
	}

	public function paypal() {
		$session = new Session();

		if (isset($_GET['paymentId']) && isset($_GET['PayerID'])) {
			$paymentId = $_GET['paymentId'];
			$payerId = $_GET['PayerID'];

			// Order information
			$order = $session->read('Cargobus.Order');
			$orderApiResponse = $session->read('Cargobus.OrderApiResponse');

			$parcelCode = $orderApiResponse['transportOrder']['parcelCode'];

			$config = array(
	            'mode' => Configure::read('Paypal.environment')
	        );

	        $price = (Configure::read('Paypal.environment') == 'live') ? $orderApiResponse['transportOrder']['costFull'] : 0.01;
        	//$price = 0.01;

			$apiContext = new \PayPal\Rest\ApiContext(
	            new \PayPal\Auth\OAuthTokenCredential(
	                Configure::read('Paypal.clientId'),     // ClientID
	                Configure::read('Paypal.secretId')      // ClientSecret
	            )
	        );


			$apiContext->setConfig($config);

		    $payment = \PayPal\Api\Payment::get($paymentId, $apiContext);
		    
		    $execution = new \PayPal\Api\PaymentExecution();
		    $execution->setPayerId($payerId);
		   
		    $transaction = new \PayPal\Api\Transaction();
		    $amount = new \PayPal\Api\Amount();

		    $amount->setTotal($price);
        	$amount->setCurrency('EUR');
		    $transaction->setAmount($amount);

		    $execution->addTransaction($transaction);

		    try {
		        $result = $payment->execute($execution, $apiContext);

		        try {
		            $payment = \PayPal\Api\Payment::get($paymentId, $apiContext);
		        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
		        	Log::emergency($parcelCode . ': ' . $ex->getData(), 'payment');
		           	header("Location: " . $this->aliasUrls['order_payment'] . '?error');
		           	exit;
		        }
		    } catch (\PayPal\Exception\PayPalConnectionException $ex) {
		        Log::emergency($parcelCode . ': ' . $ex->getData(), 'payment');
		        header("Location: " . $this->aliasUrls['order_payment'] . '?error');
		        exit;
		    }

		    $response = json_decode($payment, true);

		    if ($response['state'] == 'approved') {
		    	$cargobusTable = TableRegistry::get('Cargobus.Cargobus');
		    	$paymentTable = TableRegistry::get('Cargobus.Payment');
		    	$orderTable = TableRegistry::get('Cargobus.Order');
		    	$settingsTable = TableRegistry::get('Settings');

		    	// Transactions
		    	$transactions = $payment->getTransactions();
				$orderId = $transactions[0]->invoice_number;

				// Send to API that payment is success
				$paymentDetails = [
					'amount' => ($transactions[0]->amount->total * 100),
					'name' => $response['payer']['payer_info']['first_name'],
					'surename' => $response['payer']['payer_info']['last_name'],
					'account' => null
				];

				$paymentResponse = array_merge($response, $paymentDetails);
		    	$paymentTable->apiPaymentAdd($orderId, $paymentResponse);

		    	// Cargobus API
		    	$api = new CargobusApi();
		    	$api->token = $settingsTable->getValue('cargobus_api_token', 0, getDomain());

				$info = [];
				$infoFrom = [];
				$infoTo = [];
				$infoPayer = [];
				$infoServices = [];
				$weight = null;

		        try {
		        	$info = $api->getParcelInfo($orderId);

		        	$infoParties = $cargobusTable->parseParcelInfoParties($info);
		        	$infoServices = $cargobusTable->parseParcelInfoServices($info);

		        	if ($infoParties) {
		        		$infoFrom = $infoParties['from'];
		        		$infoTo = $infoParties['to'];
		        		$infoPayer = $infoParties['payer'];
		        	}
		        } catch (ApiException $e) {	
		        	$infoSession = $cargobusTable->createInfoFromSession($order);

		        	if ($infoSession) {
		        		$info = $infoSession['info'];
		        		$infoFrom = $infoSession['infoFrom'];
		        		$infoTo = $infoSession['infoTo'];
		        		$infoServices = $infoSession['infoServices'];
		        		$weight = $infoSession['weight'];
		        	}
		        }

		        $orderTable->sendOrderDetails($order);
		        $this->removeOrderSession();

		        $this->set('orderId', $orderId);
	        	$this->set('info', $info);
	        	$this->set('infoFrom', $infoFrom);
	        	$this->set('infoTo', $infoTo);
	        	$this->set('infoPayer', $infoPayer);
	        	$this->set('infoServices', $infoServices);
	        	$this->set('order', $order);
	        	$this->set('orderApiResponse', $orderApiResponse);
	        	$this->set('weight', $weight);

		    	$this->render('accepted');
		    } else {
		    	$this->redirect($this->aliasUrls['order_payment'] . '?error');
		    }
		} else {
			$this->redirect($this->aliasUrls['order_payment'] . '?error');
		}
	}
}
?>