<?php
namespace Cargobus\Controller;

require_once(ROOT . DS . 'vendor' . DS  . 'API' . DS . 'CargobusApi.php');

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use App\Lib\CmsRequest;
use Cake\Network\Session;
use Cake\Event\Event;
use Cake\Core\Configure;
use CargobusApi;
use ApiException;

class OrderController extends AppController {

    public $order = [];
    public $orderApiResponse = [];
    public $orderError = false;
    public $orderApiError = false;
    public $orderErrorCodes = [];
    public $orderStepCompleted = 0;

    public function initialize() {
    	parent::initialize();

        $session = new Session();

        $this->order = $session->read('Cargobus.Order');
        $this->orderApiResponse = $session->read('Cargobus.OrderApiResponse');

        //pr($this->order);exit;
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);

        $this->useNoindex = true;
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);

        $this->set('order', $this->order);
        $this->set('orderApiResponse', $this->orderApiResponse);
        $this->set('orderError', $this->orderError);
        $this->set('orderApiError', $this->orderApiError);
        $this->set('orderErrorCodes', $this->orderErrorCodes);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->useAutocomplete = true;
        $this->useChosen = true;

        $parcelShopTable = TableRegistry::get('Cargobus.ParcelShop');

        // Terminals
        $terminals = $parcelShopTable
            ->find()
            ->where(['type' => 'STATIONARY_TERMINAL'])
            ->order(['city' => 'asc'])
            ->all();

        $terminalsList = $parcelShopTable->parseAsList($terminals);

        $query = $parcelShopTable
            ->find()
            ->where(['type' => 'STATIONARY_TERMINAL']);

        if (isset($this->order['from']['type']) && $this->order['from']['type'] == 'terminal') {
            $query->where(['terminal_id' => $this->order['from']['address']]);
        }

        $terminalFrom = $query
            ->order(['city' => 'asc'])
            ->first();

        $terminalFromAvailability = $parcelShopTable->parseAvailability($terminalFrom->availability);

        if (isset($this->order['to']['type']) && $this->order['to']['type'] == 'terminal') {
            $terminalTo = $parcelShopTable
                ->find()
                ->where(['type' => 'STATIONARY_TERMINAL', 'terminal_id' => $this->order['to']['address']])
                ->order(['city' => 'asc'])
                ->first();

            $terminalToAvailability = $parcelShopTable->parseAvailability($terminalTo->availability);
        } else {
            $terminalTo = $terminalFrom;
            $terminalToAvailability = $terminalFromAvailability;
        }

        // Pickup dates
        $pickupDatesList = [];
        $date = date('Y-m-d');

        while (strtotime($date) <= strtotime(date('Y-m-d', strtotime('+7 days')))) {
            if (date('N', strtotime($date)) < 6) {
                $pickupDatesList[$date] = date('d.m.Y', strtotime($date));
            }

            $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
        }

        if ($this->request->is('post')) {
            $post = $this->request->data;
            
            $this->createOrderSession($post);
            $this->apiOrderPrice();
            $this->updateOrder();
            $this->orderSessionValidate(1);
            $this->orderInsuranceValidate();

            if (!$this->orderError && $this->orderStepCompleted == 1) {
                $this->redirect($this->aliasUrls['order_contacts']);
            }
        }

        $this->set('terminalsList', $terminalsList);
        $this->set('pickupDatesList', $pickupDatesList);
        $this->set('terminalFrom', $terminalFrom);
        $this->set('terminalTo', $terminalTo);
        $this->set('terminalFromAvailability', $terminalFromAvailability);
        $this->set('terminalToAvailability', $terminalToAvailability);
    }

    public function contacts() {
        if (!$this->orderSessionValidate(1) || !$this->isOrderSessionActive()) {
            $this->redirect($this->aliasUrls['order']);
        }

        if ($this->request->is('post')) {
            $post = $this->request->data;

            $this->createOrderSession($post);
            $this->updateOrder();
            $this->orderSessionValidate(2);
            $this->orderInsuranceValidate();

            if (!$this->orderError && $this->orderStepCompleted == 2) {
                $this->redirect($this->aliasUrls['order_payment']);
            }
        }
    }

    public function payment() {
        $this->showPopup['payment_error'] = true;

        if (!$this->orderSessionValidate(1) || !$this->orderSessionValidate(2) || !$this->isOrderSessionActive()) {
            $this->redirect($this->aliasUrls['order_contacts']);
        }

        if ($this->request->is('post')) {
            $orderTable = TableRegistry::get('Cargobus.Order');

            $post = $this->request->data;

            $this->createOrderSession($post);
            $this->orderInsuranceValidate();

            if (!isset($post['accept_terms'])) {
                $this->orderError = gt('Please, accept Terms of service');
            } elseif (!$this->orderError) {
                if (!isset($this->orderApiResponse['transportOrder']) || isset($this->orderApiResponse['transportOrder']['validationErrors'])) {
                    
                    $doOrder = $orderTable->apiOrderTransport();
                } else {
                    $doOrder = true;
                }

                $this->updateOrder();

                if ($doOrder) {
                    $paymentTable = TableRegistry::get('Cargobus.Payment');

                    switch ($this->order['payment_method']) {
                        case 'card':
                            $paymentTable->paysera('card');
                        break;

                        case 'banklink':
                            $paymentTable->paysera('banklink');
                        break;

                        case 'paypal':
                            $paymentTable->paypal();
                        break;
                    }
                } /*else {
                    $this->orderError = 'Diemžēl, neizdodas izveidot pasūtījumu';
                }*/
            }
        }
    }

    public function calculate() {
        $this->autoRender = false;

        $session = new Session();

        // Reset order array
        $this->order = [];
        $this->orderApiResponse = [];

        $session->delete('Cargobus.Order');
        $session->delete('Cargobus.OrderApiResponse');
        $session->delete('Cargobus.OrderId');

        if ($this->request->is('post')) {
            $post = $this->request->data;

            $this->createOrderSession($post);
            $this->apiOrderPrice();
        }

        $this->redirect($this->aliasUrls['order']);
    }

    public function tracking() {
        if ($this->request->is('post')) {
            $post = $this->request->data;

            if (!empty($post['parcel_code'])) {
                $this->redirect($this->aliasUrls['order_tracking'] . '?id=' . $post['parcel_code']);
            }
        }

        $cargobusTable = TableRegistry::get('Cargobus.Cargobus');
        $settingsTable = TableRegistry::get('Settings');

        $parcelCode = $this->request->query('id');

        $api = new CargobusApi();
        $api->token = $settingsTable->getValue('cargobus_api_token', 0, getDomain());

        try {
            $info = $api->getParcelInfo($parcelCode);
            $tracking = $api->getTracking($parcelCode);

            $infoServices = $cargobusTable->parseParcelInfoServices($info);
            $trackingEvents = $cargobusTable->parseParcelEvents($tracking);

            $trackingEventTitles = ['created' => gt('Label created'), 'shipped' => gt('Shipped'), 'out_of_delivery' => gt('Out of delivery'), 'delivered' => gt('Delivered')];

            $this->set('parcelCode', $parcelCode);
            $this->set('orderInfo', $info);
            $this->set('orderInfoServices', $infoServices);
            $this->set('trackingInfo', $tracking);
            $this->set('trackingEvents', $trackingEvents);
            $this->set('trackingEventTitles', $trackingEventTitles);
        } catch (ApiException $e) {
            $this->render('tracking_not_found');
            //$this->redirect($this->referer());
        }
    }

    private function createOrderSession($data = []) {
        $session = new Session();

        if ($data) {
            // From
            if (isset($data['from'])) {
                $this->order['from']['type'] = $data['from'];

                // From courier or terminal
                if ($data['from'] == 'courier') {
                    $this->order['from']['address'] = $data['courier_from'];
                    $this->order['from']['google_id'] = $data['google_id_from'];

                    if (isset($data['number_from'])) {
                        $this->order['from']['number'] = $data['number_from'];
                    }
                } elseif ($data['from'] == 'terminal') {
                    $this->order['from']['address'] = $data['terminal_from'];
                }
            }

            // To
            if (isset($data['to'])) {
                $this->order['to']['type'] = $data['to'];

                // To courier or terminal
                if ($data['to'] == 'courier') {
                    $this->order['to']['address'] = $data['courier_to'];
                    $this->order['to']['google_id'] = $data['google_id_to'];

                    if (isset($data['number_to'])) {
                        $this->order['to']['number'] = $data['number_to'];
                    }
                } elseif ($data['to'] == 'terminal') {
                    $this->order['to']['address'] = $data['terminal_to'];
                }
            }

            // Pickup
            if (isset($data['pickup_date'])) {
                $this->order['pickup']['date'] = $data['pickup_date'];
                //$this->order['pickup']['comments'] = $data['pickup_comments'];
            }

            // Delivery
            if (isset($data['delivery_date'])) {
                $this->order['delivery']['date'] = $data['delivery_date'];
            }

            // Parcels
            if (isset($data['parcels'])) {
                $parcels = [];

                foreach ($data['parcels'] as $key => $parcel) {
                    $parcels[$key] = [
                        'weight' => $parcel['weight'],
                        'length' => $parcel['length'],
                        'width' => $parcel['width'],
                        'height' => $parcel['height']
                    ];  

                    if (isset($parcel['additional_text'])) {
                        $parcels[$key]['text'] = $parcel['additional_text'];
                    }
                }

                $this->order['parcels'] = $parcels;
            }

            // Additional text
            if (isset($data['additional_text'])) {
                $this->order['additionalText'] = $data['additional_text'];
            }

            // Sender
            if (isset($data['sender'])) {
                $this->order['sender'] = $data['sender'];
            }

            // Recipient
            if (isset($data['recipient'])) {
                $this->order['recipient'] = $data['recipient'];
            }

            // Payment method
            if (isset($data['payment_method'])) {
                $this->order['payment_method'] = $data['payment_method'];
            }
        }
        
        $session->write('Cargobus.Order', $this->order);
    }

    private function orderSessionValidate($step = 0) {
        if ($this->order) {
            $parcelsValid = true;

            if (isset($this->order['parcels'])) {
                foreach ($this->order['parcels'] as $key => $parcel) {
                    if (empty($parcel['weight']) || empty($parcel['length']) || empty($parcel['width']) || empty($parcel['height'])) {
                        $parcelsValid = false;
                    }
                }
            }

            // Validate order flow step 1
            if ($step == 1) {
                if (isset($this->order['from']['type']) && $this->order['from']['type'] == 'courier' && empty($this->order['from']['google_id'])) {
                    $this->orderError = gt('Please fill the exact pick-up address');
                } elseif (isset($this->order['to']['type']) && $this->order['to']['type'] == 'courier' && empty($this->order['to']['google_id'])) {
                    $this->orderError = gt('Please fill the exact delivery address');
                } elseif (!$parcelsValid) {
                    $this->orderError = gt('Please fill exact parcel information');
                } elseif (!isset($this->order['service'])) {
                    $this->orderError = true;
                } else {
                    $this->orderStepCompleted++;
                }
            }

            // Validate order flow step 2
            if (!$this->orderError && $step == 2) {
                if (!isset($this->order['sender']) || (isset($this->order['sender']) && (empty($this->order['sender']['name']) || empty($this->order['sender']['email']) || empty($this->order['sender']['phone'])))) {
                    $this->orderError = gt('Please fill sender information');
                } elseif (!isset($this->order['recipient']) || (isset($this->order['recipient']) && (empty($this->order['recipient']['name']) || empty($this->order['recipient']['email']) || empty($this->order['recipient']['phone'])))) {
                    $this->orderError = gt('Please fill recipient information');
                } elseif (!filter_var($this->order['sender']['email'], FILTER_VALIDATE_EMAIL) || !filter_var($this->order['recipient']['email'], FILTER_VALIDATE_EMAIL)) {
                    $this->orderError = gt('Please fill valid e-mail address');
                } else {
                    $this->orderStepCompleted++;
                }
            }

            // Validate order flow step 3
            if (!$this->orderError && $step == 3) {
                $this->orderStepCompleted++;
            }

            // Api error
            /*if (isset($this->orderApiResponse['errorCode']) && $this->orderApiResponse['errorCode'] > 0) {
                $this->orderApiError = 'API error';
            }*/

            if (!$this->orderError && $step == $this->orderStepCompleted) {
                return true;
            }
        }

        return false;
    }

    private function orderInsuranceValidate() {
        if (!$this->orderError) {
            if (isset($this->order['additionalServices']['insurance']) && $this->order['additionalServices']['insurance'] == 0) {
                $this->orderError = gt('Please fill value of insurance');
                $this->orderErrorCodes['additional_text'] = true;
            } elseif (isset($this->order['additionalServices']['insurance']) && $this->order['additionalServices']['insurance'] > 0 && (!isset($this->order['additionalText']) || (isset($this->order['additionalText']) && empty($this->order['additionalText'])))) {
                $this->orderError = gt('Please fill additional information');
                $this->orderErrorCodes['additional_text'] = true;
            }
        }
    }

    private function isOrderSessionActive() {
        if ($this->order) {
            if (isset($this->order['from'])) {
                return true;
            }
        }

        return ($this->aliasUrls['order'] == CmsRequest::$page->url) ? true : false;
    }

    private function apiOrderPrice() {
        $orderTable = TableRegistry::get('Cargobus.Order');
        $orderTable->apiOrderPrice();
    }

    private function updateOrder() {
        $session = new Session();

        $this->order = $session->read('Cargobus.Order');
        $this->orderApiResponse = $session->read('Cargobus.OrderApiResponse');
    }
}
?>