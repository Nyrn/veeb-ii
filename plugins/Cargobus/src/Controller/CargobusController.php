<?php
namespace Cargobus\Controller;

require_once(ROOT . DS . 'vendor' . DS  . 'API' . DS . 'CargobusApi.php');

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use App\Lib\CmsRequest;
use Cake\Network\Session;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Http\Response;
use CargobusApi;
use ApiException;

class CargobusController extends AppController {

	public function generateToken() {
		$this->autoRender = false;
		
        $cargobusTable = TableRegistry::get('Cargobus.Cargobus');  
        $settingsTable = TableRegistry::get('Settings');  

        $domains = ['cargobus.ee', 'cargobus.lv', 'cargobus.lt'];

        foreach ($domains as $domain) {
            $api = new CargobusApi($cargobusTable->getApiUsername($domain), $cargobusTable->getApiPassword($domain), 1);
            $token = $api->login();

            if ($token) {
                $settingsTable->setValue('cargobus_api_token', 0, $token, $domain);
            }
        }
    }

	public function ajaxTerminal() {
		$this->autoRender = false;

		$parcelShopTable = TableRegistry::get('Cargobus.ParcelShop');

		$response = [];

		if ($this->request->is('post')) {
			$post = $this->request->data;
			$terminalId = $post['terminalId'];
		
			$terminal = $parcelShopTable
				->find()
				->where(['terminal_id' => $terminalId])
				->first();

			if ($terminal) {
				$response['name'] = $terminal->name;
				$response['email'] = $terminal->email;
				$response['phone'] = $terminal->phone;
				$response['city'] = $terminal->city;
				$response['address'] = $terminal->address;
				$response['country'] = $terminal->country;
				$response['postal_code'] = $terminal->postal_code;
				$response['availability'] = $parcelShopTable->parseAvailability($terminal->availability);
				$response['note'] = $parcelShopTable->parseNote($terminal->note);
				$response['description'] = $terminal->description;
			}
		}

		$this->response->type('json');
		$this->response->body(json_encode($response));

		return $this->response;
	}

	public function ajaxFindAddress() {
		$this->autoRender = false;

		$response = [];

		$cargobusTable = TableRegistry::get('Cargobus.Cargobus');  
		$settingsTable = TableRegistry::get('Settings');

		if ($this->request->is('ajax')) {
			$api = new CargobusApi();
            $api->token = $settingsTable->getValue('cargobus_api_token', 0, getDomain());

	        $addresses = $api->getGoogleAddresses(['partOfAddress' => $this->request->query('phrase'), 'onlyWithPostalCode' => true]);

	        if (!empty($addresses)) {
	        	foreach ($addresses as $address) {
	        		$response[] = ['name' => $address['fullAddress'], 'code' => $address['googlePlaceId']];
	        	}
	        } else {
	        	$response[] = ['name' => gt('No address found'), 'code' => 'error'];
	        }
		}

		$this->response->type('json');
		$this->response->body(json_encode($response));

		return $this->response;
	}

	public function ajaxAdditionalService() {
		$this->autoRender = false;

		$session = new Session();
		$order = $session->read('Cargobus.Order');

		if ($this->request->is('ajax')) {
			$post = $this->request->data;

			$order['additionalServices'] = [];

			if (isset($post['services'])) {
				foreach ($post['services'] as $service) {
					if ($service == 'insurance') {
						//if ($post['insuranceValue'] > 0) {
							$order['additionalServices'][$service] = $post['insuranceValue'];
						//}
					} else {
						$order['additionalServices'][$service] = null;
					}
				}
			}

			$session->write('Cargobus.Order', $order);

			$orderTable = TableRegistry::get('Cargobus.Order');
        	$orderTable->apiOrderPrice();

        	$response = $orderTable->parseOrderAdditionalServicesPrice();
        	$response['vat'] = $orderTable->parseOrderVatCost();
		}

		$this->response->type('json');
		$this->response->body(json_encode($response));

		return $this->response;
	}

	public function getLabel() {
		$this->autoRender = false;

		$success = false;

		if ($this->request->query('id')) {
			$code = $this->request->query('id');

			$settingsTable = TableRegistry::get('Settings');

			$api = new CargobusApi();
            $api->token = $settingsTable->getValue('cargobus_api_token', 0, getDomain());

            try {
            	$label = $api->getLabel($code);

	            if (!empty($label)) {
	            	$path = ROOT . '/tmp/';
					$file = $code . '.pdf';
					file_put_contents($path . $file, $label);

					header('Content-Description: File Transfer');
				    header('Content-Type: application/octet-stream');
				    header('Content-Disposition: attachment; filename="'.basename($file).'"');
				    header('Expires: 0');
				    header('Cache-Control: must-revalidate');
				    header('Pragma: public');
				    header('Content-Length: ' . filesize($path . $file));
				    readfile($path . $file);

				    unlink($path . $file);
				    exit;
	            }
            } catch (ApiException $e) {

            }
            
		}

		if (!$success) {
			$this->redirect($this->referer());
		}
	}
}
?>