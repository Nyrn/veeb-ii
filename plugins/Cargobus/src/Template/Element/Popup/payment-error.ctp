<!--  -->
<div class="payment_error popup_block">
    <img src="/img/close.svg" class='close' alt="close">
    <div class="content">
        <div class="payment_error_icon">
            <svg width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M23 45C35.1503 45 45 35.1503 45 23C45 10.8497 35.1503 1 23 1C10.8497 1 1 10.8497 1 23C1 35.1503 10.8497 45 23 45Z"
                    stroke="#DF0000" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round"
                    stroke-linejoin="round" />
                <path d="M23 27V11" stroke="#DF0000" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round"
                    stroke-linejoin="round" />
                <circle cx="23.0004" cy="34.1" r="1.1" fill="#DF0000" />
            </svg>
        </div>
        <div class="payment_error_text">
            <h3><?php pt('Your order was not paid, please select another payment method'); ?></h3>
        </div>
        <div class="payment_error_btn">
            <button type="button" class="main_btn blue"><?php pt('Select another payment method'); ?></button>
        </div>
    </div>
</div>