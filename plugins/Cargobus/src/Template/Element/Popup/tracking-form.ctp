<div class="tracking_form popup_block">
    <img src="/img/close.svg" class='close' alt="close">
        <div class="content">
            <div class="tracking_modal_title">
                <h3>Enter your email so we could send you the order information</h3>
            </div>
            <form class="order_form">
                <div class="row mobile_100_w wrap no_margin_bottom">
                    <label class="w50">
                        <span class="required">Email</span>
                        <input type="email" placeholder="newmail@">
                    </label>
                    <div class="label w50">
                        <span></span>
                        <div class="main_btn blue">Send</div>
                    </div>
                </div>
            </form>

            <div class="tracking_form_table">
                <div class="tracking_progress_top">
                    <div class="order_sidebar">
                        <h3>Order Information</h3>
                    </div>
                </div>
                <!-- ship_order_data -->
                <div class='tracking_data'>
                    <table class='tracking_table'>
                        <tr>
                            <td>Tracking Number</td>
                            <td>1ZW957899155973761</td>
                        </tr>
                        <tr>
                            <td>Destination point</td>
                            <td>PP – Ugniagesių gatve 1 <br><span>(shop Tip top)</span><br>Alytus, Lithuania</td>
                        </tr>
                    </table>
                </div>
                <!-- ship_order_data -->
            </div>
        </div>
    </div>