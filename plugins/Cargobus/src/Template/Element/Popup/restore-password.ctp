<div class="restore_password popup_block">
    <img src="/img/close.svg" class='close' alt="close">
        <div class="content">
            <div class="registration_modal">
                    <div class="registration_modal_title">
                        <h3>Restore password</h3>
                        <p>Specify the email and we will send a password to restore</p>
                    </div>
                    <form class="login_form">
                    <div class="row mobile_100_w wrap">
                        <label class="w100">
                            <span>Email</span>
                            <input type="text" placeholder="Email" value="example@gmail.com">
                        </label>
                    </div>
                    <div class="login_form_btn">
                    <button class="main_btn blue">Confirm</button>
                    </div>
                    </form>
            </div>
        </div>
    </div>