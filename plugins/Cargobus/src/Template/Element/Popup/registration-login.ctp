<div class="registration_login popup_block">
    <img src="/img/close.svg" class='close' alt="close">
        <div class="content">
            <div class="registration_modal">
                    <div class="registration_modal_title">
                        <h3>Login</h3>
                    </div>
                    <form class="login_form">
                        <div class="registration_notice">
                            <span>
                            <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#rect0)">
                                <path d="M11.8944 9.98518L6.62962 1.25403C6.49657 1.03337 6.25767 0.898438 5.99998 0.898438C5.74228 0.898438 5.50338 1.03334 5.37033 1.25403L0.105608 9.98518C-0.0313137 10.2123 -0.0353684 10.4955 0.0950379 10.7264C0.225444 10.9573 0.470061 11.1001 0.735233 11.1001H11.2647C11.5299 11.1001 11.7745 10.9573 11.9049 10.7264C12.0353 10.4955 12.0313 10.2123 11.8944 9.98518ZM6.00391 3.91864C6.30628 3.91864 6.56212 4.08922 6.56212 4.39156C6.56212 5.31413 6.45358 6.6399 6.45358 7.56247C6.45358 7.80282 6.18998 7.90358 6.00391 7.90358C5.75585 7.90358 5.54651 7.8028 5.54651 7.56247C5.54651 6.6399 5.43799 5.31413 5.43799 4.39156C5.43799 4.08922 5.68605 3.91864 6.00391 3.91864ZM6.01167 9.63249C5.67056 9.63249 5.41469 9.3534 5.41469 9.03554C5.41469 8.70992 5.67054 8.43859 6.01167 8.43859C6.32953 8.43859 6.60089 8.70992 6.60089 9.03554C6.60089 9.3534 6.32953 9.63249 6.01167 9.63249Z" fill="#F4B000"/>
                            </g>
                            <defs>
                                <clipPath id="rect0">
                                    <rect width="12" height="12" fill="white"/>
                                </clipPath>
                            </defs>
                            </svg>
                            </span>
                            <p>Only for corporate clients</p>
                        </div>
                    <div class="row mobile_100_w wrap">
                        <label class="w100">
                            <span>Username</span>
                            <input type="text" placeholder="Username" value="username_example">
                        </label>
                        <label class="w100">
                            <span>Password</span>
                            <input type="password" placeholder="***********" value="username_example">
                        </label>
                    </div>
                    <div class="login_form_btn">
                        <a href="javascript:void(0);">forgot password</a>
                    <button class="main_btn blue">Login</button>
                    </div>
                    </form>
                    <div class="no_account">
                        <p>You do not have an account? <a href="javascript:void(0);">registration</a></p>
                    </div>
            </div>
        </div>
    </div>