<div class="top_row">
    <h5>Services</h5>
</div>
<!-- ship_order_data -->
<div class="data">
    <table class='simple_table'>
        <tr>
            <td><?php echo $order['service']['complexServiceName']; ?></td>
            <td><span class="price start_cost" data-start-cost='<?php echo $order['service']['complexUnitPriceNet']; ?>' data-start-vat="<?php echo $order['service']['vatAmount']; ?>"><?php echo $order['service']['complexUnitPriceNet']; ?></span> EUR</span></td>
        </tr>
    </table>
    <div class="line_hr"></div>
    <table class='simple_table calc_cost'>
        <tr>
            <td>
                <label class="checkbox">
                    <input type="checkbox" name='insurance' data-dop-cost="<?php echo (isset($order['service']['additionalServices']['insurance'])) ? $order['service']['additionalServices']['insurance']['priceNet'] : 0; ?>" <?php echo (isset($order['additionalServices']['insurance'])) ? 'checked' : null; ?>>
                    <span class="check"></span>
                    <span class='main'><?php pt('Insurance'); ?>, EUR</span>
                </label>
                <input type="number" value="<?php echo (isset($order['additionalServices']['insurance'])) ? $order['additionalServices']['insurance'] : 0; ?>" class="only_integer" style="display: <?php echo (isset($order['additionalServices']['insurance'])) ? 'inline-block' : 'none'; ?>;">
            </td>
            <td><span class="price"><?php echo (isset($order['service']['additionalServices']['insurance'])) ? $order['service']['additionalServices']['insurance']['priceNet'] : '0,00'; ?></span> EUR</span></td>
        </tr>
        <?php /*<tr>
            <td>
                <label class="checkbox">
                    <input type="checkbox" name='floor_fee' data-dop-cost = '2.95'>
                    <span class="check"></span>
                    <span class='main'>Floor fee</span>
                </label>
            </td>
            <td><span class="price">0,00</span> EUR</span></td>
        </tr>*/ ?>

        <tr>
            <td>
                <label class="checkbox">
                    <input type="checkbox" name='fragile' data-dop-cost='<?php echo (isset($order['service']['additionalServices']['fragile'])) ? $order['service']['additionalServices']['fragile']['priceNet'] : 0; ?>' <?php echo (isset($order['service']['additionalServices']['fragile'])) ? 'checked' : null; ?>>
                    <span class="check"></span>
                    <span class='main'><?php pt('Fragile'); ?></span>
                </label>
            </td>
            <td><span class="price"><?php echo (isset($order['service']['additionalServices']['fragile'])) ? $order['service']['additionalServices']['fragile']['priceNet'] : '0,00'; ?></span> EUR</span></td>
        </tr>

        <tr>
            <td>
                <label class="checkbox">
                    <input type="checkbox" name='upwards' data-dop-cost = '<?php echo (isset($order['service']['additionalServices']['upwards'])) ? $order['service']['additionalServices']['upwards']['priceNet'] : 0; ?>' <?php echo (isset($order['service']['additionalServices']['upwards'])) ? 'checked' : null; ?>>
                    <span class="check"></span>
                    <span class='main'><?php pt('Upwards'); ?></span>
                </label>
            </td>
            <td><span class="price"><?php echo (isset($order['service']['additionalServices']['upwards'])) ? $order['service']['additionalServices']['upwards']['priceNet'] : '0,00'; ?></span> EUR</span></td>
        </tr>
    </table>
    <!-- важно  -->
    <!-- line_hr пишем после каждой таблицы -->
    <div class="line_hr"></div>
</div>
<!-- ship_order_data -->
<div class="bottom row margin0 between align-center" style="padding-bottom: 10px;">
    <div class="left">
        <span style="font-size: 14px;"><?php pt('Total without VAT'); ?></span>
    </div>
    <span class="price" style="font-size: 16px;">
        <span class="total_price_without_vat"><?php echo $order['service']['totalNetCost']; ?></span> EUR
    </span>
</div>

<div class="bottom row margin0 between align-center">
    <div class="left">
        <span><?php pt('Total'); ?></span>
        <div class="tooltip">
            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="8" cy="8" r="7.5" stroke="#AEAEAE" />
                <path d="M7.28943 9.58H8.51043V9.239C8.51043 8.81 8.76343 8.623 9.21443 8.381C10.0504 7.941 10.7104 7.512 10.7104 6.434C10.7104 5.015 9.58843 4.168 8.05943 4.168C6.55243 4.168 5.38643 5.004 5.28743 6.698H6.54143C6.64043 5.642 7.24543 5.323 8.01543 5.323C8.98343 5.323 9.45643 5.807 9.45643 6.467C9.45643 7.017 9.13743 7.226 8.49943 7.556C7.69643 7.974 7.28943 8.216 7.28943 9.008V9.58ZM7.90543 12.132C8.37843 12.132 8.74143 11.813 8.74143 11.351C8.74143 10.889 8.37843 10.581 7.90543 10.581C7.43243 10.581 7.08043 10.889 7.08043 11.351C7.08043 11.813 7.43243 12.132 7.90543 12.132Z"
                    fill="#AEAEAE" />
            </svg>

            <div class="tooltip_content">
                <div class="wrap text">
                    <?php pt('Including VAT'); ?> <?php echo ($order['service']['vatRate'] * 100); ?>%
                </div>
            </div>
        </div>
    </div>
    <span class="price">
        <span class="total_price"><?php echo $order['service']['totalCost']; ?></span> EUR
    </span>
</div>