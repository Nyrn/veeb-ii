<div class="no_result">
    <div class="no_result_notice" style="<?php echo (empty($order)) ? 'display: none;' : null; ?>">
        <div class="no_result_title">
            <h3>
                <span>
                    <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M18.9258 14.7344C19.1029 15.0885 19.1582 15.4427 19.0918 15.7969C19.0254 16.1732 18.8483 16.4831 18.5605 16.7266C18.2728 16.9922 17.9297 17.125 17.5312 17.125H1.59375C1.19531 17.125 0.852214 16.9922 0.564453 16.7266C0.276693 16.4831 0.0996094 16.1732 0.0332031 15.7969C-0.0332031 15.4427 0.0221354 15.0885 0.199219 14.7344L8.16797 0.921875C8.36719 0.567708 8.64388 0.335286 8.99805 0.224609C9.37435 0.0917969 9.73958 0.0917969 10.0938 0.224609C10.4701 0.335286 10.7578 0.567708 10.957 0.921875L18.9258 14.7344ZM9.5625 11.8789C9.14193 11.8789 8.77669 12.0339 8.4668 12.3438C8.17904 12.6315 8.03516 12.9857 8.03516 13.4062C8.03516 13.8268 8.17904 14.1921 8.4668 14.502C8.77669 14.7897 9.14193 14.9336 9.5625 14.9336C9.98307 14.9336 10.3372 14.7897 10.625 14.502C10.9349 14.1921 11.0898 13.8268 11.0898 13.4062C11.0898 12.9857 10.9349 12.6315 10.625 12.3438C10.3372 12.0339 9.98307 11.8789 9.5625 11.8789ZM8.10156 6.40039L8.36719 10.916C8.36719 11.0046 8.40039 11.0931 8.4668 11.1816C8.55534 11.248 8.65495 11.2812 8.76562 11.2812H10.3594C10.4701 11.2812 10.5586 11.248 10.625 11.1816C10.7135 11.0931 10.7578 11.0046 10.7578 10.916L11.0234 6.40039C11.0234 6.26758 10.9792 6.16797 10.8906 6.10156C10.8242 6.01302 10.7357 5.96875 10.625 5.96875H8.5C8.38932 5.96875 8.28971 6.01302 8.20117 6.10156C8.13477 6.16797 8.10156 6.26758 8.10156 6.40039Z" fill="#333333"/>
                    </svg>
                </span>
                <?php pt('No calculation result'); ?>
            </h3>
        </div>
        <div class="no_result_text">
            <p><?php pt('We were not able to calculate the price according given data'); ?>.</p>
        </div>
    </div>
    <?php /*<div class="no_result_form">
        <p><?php pt('Send your email address and our manager will contact you and help with the calculation of the cargo'); ?>:</p>
        <form>
            <label>
                <input type="email" placeholder="example@mail.com">
            </label>
            <button type="button" class="main_btn"><?php pt('Send'); ?></button>
        </form>
    </div>*/ ?>
    <?php if (isset($contactUs) && (!empty($contactUs->phone) || !empty($contactUs->working_hours_monday_friday))): ?>
        <div class='no_result_table' style="<?php echo (empty($order)) ? 'padding-top: 0px;' : null; ?>">
            <table>
                <?php if ($contactUs->phone): ?>
                    <tr>
                        <td><?php pt('Contact us'); ?></td>
                        <td><?php echo parsePhone($contactUs->phone, '<a href="tel:{phone}">{phone}</a>', ','); ?></td>
                    </tr>
                <?php endif; ?>

                <?php if ($contactUs->working_hours_monday_friday): ?>
                    <tr>
                        <td><?php pt('Working hours'); ?></td>
                        <td><?php pt('Mo-Fr'); ?>, <?php echo $contactUs->working_hours_monday_friday; ?></td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
    <?php endif; ?>
</div>