<div class="wrapper order_page">
    <!--  -->
    <div class="top_row_inner_page top_row container">
        <h1><?php pt('The payment has been proceeded'); ?></h1>
    </div>
    <!-- тут основной контент страницы -->
    <!-- start order_block -->
    <div class="order_block container">
        <div class="row nowrap">
            <!-- новый класс thanks_block -->
            <div class="column w67 left content thanks_block">
                <div class="row info_item">
                    <div class="title">
                        <h5><?php pt('Thank you! Your Cargo order is confirmed!'); ?><br>
                            <?php pt('Tracking'); ?> <?php echo $orderId; ?>.</h5>
                        <p><?php pt('Our courier will contact You for the pick-up.'); ?> </p>
                    </div>
                    <div class="row">
                        <div class="w50">
                            <div class="data">
                                <p><?php pt('Please print the label and %s attach it to the box: %s', ['<b>', '</b>']); ?></p>
                            </div>
                            <div class="data">
                                <a href="<?php echo $aliasUrls['get_label'] . '?id=' . $orderId; ?>" class="main_btn w100"><?php pt('Print label'); ?></a>
                            </div>
                        </div>
                        <div class="w50">
                            <div class="data">
                                <p><?php pt('To trace the shipment on the courier website %s use tracking: %s', ['<b>', '</b>']); ?></p>
                            </div>
                            <div class="data">
                                <a href='<?php echo $aliasUrls['order_tracking'] . '?id=' . $orderId; ?>' class="main_btn no_bg w100"><?php pt('Track order'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if (isset($info) && !empty($info)): ?>
                <div class="row info_item row_from">
                    <div class="title">
                        <h5><?php pt('Order Information'); ?></h5>
                    </div>

                    <?php if (!empty($infoFrom)): ?>
                        <div class="row">
                            <div class="data w50">
                                <h6><?php pt('From'); ?></h6>
                                <ul>
                                    <li><?php pt('Delivery method'); ?>: <b><?php echo ucfirst(strtolower($infoFrom['deliveryMethod'])); ?></b></li>
                                    <li><?php echo (strtolower($infoFrom['deliveryMethod']) == 'courier') ? $infoFrom['googleAddress']['formattedAddress'] : implode(', ', [$infoFrom['terminal']['address'], $infoFrom['terminal']['city']]); ?></li>
                                </ul>
                            </div>
                            <div class="data w50">
                                <h6><?php pt('Sender'); ?></h6>
                                <ul>
                                    <li><?php echo $infoFrom['name']; ?></li>
                                    <li><?php echo $infoFrom['email']; ?></li>
                                    <li><?php echo $infoFrom['phone']; ?></li>
                                </ul>
                            </div>
                            <?php /*<div class="info w100">
                                <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M9 0.53125C7.42969 0.53125 5.97656 0.929687 4.64062 1.72656C3.30469 2.5 2.23828 3.55469 1.44141 4.89062C0.667969 6.22656 0.28125 7.67969 0.28125 9.25C0.28125 10.8203 0.667969 12.2734 1.44141 13.6094C2.23828 14.9453 3.30469 16 4.64062 16.7734C5.97656 17.5703 7.42969 17.9688 9 17.9688C10.5703 17.9688 12.0234 17.5703 13.3594 16.7734C14.6953 16 15.75 14.9453 16.5234 13.6094C17.3203 12.2734 17.7188 10.8203 17.7188 9.25C17.7188 7.67969 17.3203 6.22656 16.5234 4.89062C15.75 3.55469 14.6953 2.5 13.3594 1.72656C12.0234 0.929687 10.5703 0.53125 9 0.53125ZM9 4.39844C9.39844 4.39844 9.73828 4.55078 10.0195 4.85547C10.3242 5.13672 10.4766 5.47656 10.4766 5.875C10.4766 6.27344 10.3242 6.625 10.0195 6.92969C9.73828 7.21094 9.39844 7.35156 9 7.35156C8.60156 7.35156 8.25 7.21094 7.94531 6.92969C7.66406 6.625 7.52344 6.27344 7.52344 5.875C7.52344 5.47656 7.66406 5.13672 7.94531 4.85547C8.25 4.55078 8.60156 4.39844 9 4.39844ZM10.9688 13.3281C10.9688 13.4453 10.9219 13.5508 10.8281 13.6445C10.7578 13.7148 10.6641 13.75 10.5469 13.75H7.45312C7.33594 13.75 7.23047 13.7148 7.13672 13.6445C7.06641 13.5508 7.03125 13.4453 7.03125 13.3281V12.4844C7.03125 12.3672 7.06641 12.2734 7.13672 12.2031C7.23047 12.1094 7.33594 12.0625 7.45312 12.0625H7.875V9.8125H7.45312C7.33594 9.8125 7.23047 9.77734 7.13672 9.70703C7.06641 9.61328 7.03125 9.50781 7.03125 9.39062V8.54688C7.03125 8.42969 7.06641 8.33594 7.13672 8.26562C7.23047 8.17188 7.33594 8.125 7.45312 8.125H9.70312C9.82031 8.125 9.91406 8.17188 9.98438 8.26562C10.0781 8.33594 10.125 8.42969 10.125 8.54688V12.0625H10.5469C10.6641 12.0625 10.7578 12.1094 10.8281 12.2031C10.9219 12.2734 10.9688 12.3672 10.9688 12.4844V13.3281Z"
                                        fill="#DCDEE3" />
                                </svg>
                                <p>Курьер забирает посылки в рабочие дни с 9:00 до 17:00 и свяжется по телефону.</p>
                            </div>*/ ?>
                        </div>
                    <?php endif; ?>
                    
                    <?php if (!empty($infoTo)): ?>
                        <div class="row">
                            <div class="data w50">
                                <h6><?php pt('To'); ?></h6>
                                <ul>
                                    <li><?php pt('Delivery method'); ?>: <b><?php echo ucfirst(strtolower($infoTo['deliveryMethod'])); ?></b></li>
                                    <li><?php echo (strtolower($infoTo['deliveryMethod']) == 'courier') ? $infoTo['googleAddress']['formattedAddress'] : implode(', ', [$infoTo['terminal']['address'], $infoTo['terminal']['city']]); ?></li>
                                </ul>
                            </div>
                            <div class="data w50">
                                <h6><?php pt('Recipient'); ?></h6>
                                <ul>
                                    <li><?php echo $infoTo['name']; ?></li>
                                    <li><?php echo $infoTo['email']; ?></li>
                                    <li><?php echo $infoTo['phone']; ?></li>
                                </ul>
                            </div>
                            <?php /*<div class="info w100">
                                <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M9 0.53125C7.42969 0.53125 5.97656 0.929687 4.64062 1.72656C3.30469 2.5 2.23828 3.55469 1.44141 4.89062C0.667969 6.22656 0.28125 7.67969 0.28125 9.25C0.28125 10.8203 0.667969 12.2734 1.44141 13.6094C2.23828 14.9453 3.30469 16 4.64062 16.7734C5.97656 17.5703 7.42969 17.9688 9 17.9688C10.5703 17.9688 12.0234 17.5703 13.3594 16.7734C14.6953 16 15.75 14.9453 16.5234 13.6094C17.3203 12.2734 17.7188 10.8203 17.7188 9.25C17.7188 7.67969 17.3203 6.22656 16.5234 4.89062C15.75 3.55469 14.6953 2.5 13.3594 1.72656C12.0234 0.929687 10.5703 0.53125 9 0.53125ZM9 4.39844C9.39844 4.39844 9.73828 4.55078 10.0195 4.85547C10.3242 5.13672 10.4766 5.47656 10.4766 5.875C10.4766 6.27344 10.3242 6.625 10.0195 6.92969C9.73828 7.21094 9.39844 7.35156 9 7.35156C8.60156 7.35156 8.25 7.21094 7.94531 6.92969C7.66406 6.625 7.52344 6.27344 7.52344 5.875C7.52344 5.47656 7.66406 5.13672 7.94531 4.85547C8.25 4.55078 8.60156 4.39844 9 4.39844ZM10.9688 13.3281C10.9688 13.4453 10.9219 13.5508 10.8281 13.6445C10.7578 13.7148 10.6641 13.75 10.5469 13.75H7.45312C7.33594 13.75 7.23047 13.7148 7.13672 13.6445C7.06641 13.5508 7.03125 13.4453 7.03125 13.3281V12.4844C7.03125 12.3672 7.06641 12.2734 7.13672 12.2031C7.23047 12.1094 7.33594 12.0625 7.45312 12.0625H7.875V9.8125H7.45312C7.33594 9.8125 7.23047 9.77734 7.13672 9.70703C7.06641 9.61328 7.03125 9.50781 7.03125 9.39062V8.54688C7.03125 8.42969 7.06641 8.33594 7.13672 8.26562C7.23047 8.17188 7.33594 8.125 7.45312 8.125H9.70312C9.82031 8.125 9.91406 8.17188 9.98438 8.26562C10.0781 8.33594 10.125 8.42969 10.125 8.54688V12.0625H10.5469C10.6641 12.0625 10.7578 12.1094 10.8281 12.2031C10.9219 12.2734 10.9688 12.3672 10.9688 12.4844V13.3281Z"
                                        fill="#DCDEE3" />
                                </svg>
                                <p>Режим работы терминала: пн–пт, 9:00–20:00.<br>
                                    Контактный телефон: <a href="tel:+372 6 813 444">+372 6 813 444</a></p>
                            </div>*/ ?>
                        </div>
                    <?php endif; ?>

                    <div class="row">
                        <?php if (isset($order['parcels'])): ?>
                            <div class="data w50">
                                <h6><?php pt('Parcel'); ?></h6>
                                <ul>
                                    <li><?php pt('Weight'); ?>: <?php echo $weight; ?> kg </li>

                                    <?php foreach ($order['parcels'] as $parcel): ?>
                                        <li><?php pt('Size'); ?>: <?php echo $parcel['length']; ?>×<?php echo $parcel['width']; ?>×<?php echo $parcel['height']; ?> cm </li>
                                    <?php endforeach; ?>

                                    <?php /*<li>Description: Shoes and accessories</li>*/ ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <div class="data w50">
                            <h6><?php pt('Date'); ?></h6>
                            <ul>
                                <li><?php pt('Formalization'); ?>: <?php echo date('d.m.Y H:i', strtotime($info['createdOn'])); ?></li>
                                <?php if (isset($order['pickup']['date'])): ?>
                                    <li><?php pt('Pick-up'); ?>: <?php echo date('d.m.Y', strtotime($order['pickup']['date'])); ?></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <!--  -->

                    <?php if (isset($info['additionalInfo']) && !empty($info['additionalInfo'])): ?>
                        <div class="row">
                            <div class="data w100">
                                <h6><?php pt('Additional information'); ?></h6>
                                <p><?php echo $info['additionalInfo']; ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
            </div>

            <?php if (isset($info) && !empty($info)): ?>
            <div class="column w33 right">
                <div class="wrap">
                    <div class="ship_order">
                        <div class="top_row">
                            <h5><?php pt('Services'); ?></h5>
                        </div>
                        <!-- ship_order_data -->
                        <div class="data">
                            <?php if (isset($infoServices['complex'])): ?>
                                <table class='simple_table'>
                                    <tr>
                                        <td><?php echo $infoServices['complex']['serviceName']; ?></td>
                                        <td><span class="price start_cost"><?php echo $infoServices['complex']['priceWithoutVat']; ?></span> EUR</span></td>
                                    </tr>
                                </table>
                                <div class="line_hr"></div>
                                <?php unset($infoServices['complex']); ?>
                            <?php endif; ?>
                        
                            <?php if (!empty($infoServices)): ?>
                                <table class='simple_table calc_cost final_table'>
                                    <?php foreach ($infoServices as $sType => $service): ?>
                                        <tr>
                                            <?php /*<td><?php echo ($sType == 'insurance') ? ucfirst($sType) . ' ' . $value . ' EUR' : ucfirst($sType); ?></td>*/ ?>
                                            <td><?php pt(ucfirst($sType)); ?></td>
                                            <td><span class="price"><?php echo $service['priceWithoutVat']; ?></span> EUR</span></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                                
                                <div class="line_hr"></div>
                            <?php endif; ?>
                        </div>
                        <!-- ship_order_data -->
                        <div class="bottom row margin0 between align-center">
                            <div class="left">
                                <span><?php pt('Total'); ?></span>
                                <div class="tooltip">
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="8" cy="8" r="7.5" stroke="#AEAEAE" />
                                        <path d="M7.28943 9.58H8.51043V9.239C8.51043 8.81 8.76343 8.623 9.21443 8.381C10.0504 7.941 10.7104 7.512 10.7104 6.434C10.7104 5.015 9.58843 4.168 8.05943 4.168C6.55243 4.168 5.38643 5.004 5.28743 6.698H6.54143C6.64043 5.642 7.24543 5.323 8.01543 5.323C8.98343 5.323 9.45643 5.807 9.45643 6.467C9.45643 7.017 9.13743 7.226 8.49943 7.556C7.69643 7.974 7.28943 8.216 7.28943 9.008V9.58ZM7.90543 12.132C8.37843 12.132 8.74143 11.813 8.74143 11.351C8.74143 10.889 8.37843 10.581 7.90543 10.581C7.43243 10.581 7.08043 10.889 7.08043 11.351C7.08043 11.813 7.43243 12.132 7.90543 12.132Z"
                                            fill="#AEAEAE" />
                                    </svg>
                                    <div class="tooltip_content">
                                        <div class="wrap text">
                                            <?php pt('Including VAT'); ?> <?php echo ($info['vatRate'] * 100); ?>%
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span class="price">
                                <span class="total_price"><?php echo $info['totalPriceWithVat']; ?></span> EUR
                            </span>
                        </div>
                        <!-- new content -->
                        <!-- order_bottom -->
                        <?php /*<div class="order_bottom">
                            <div class="line_hr"></div>
                            <a href="javascript:void(0);" onclick="window.print();return false;">
                                <svg width="12" height="13" viewBox="0 0 12 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10.5 5V2.30469C10.5 2.10156 10.4297 1.92969 10.2891 1.78906L9.21094 0.710938C9.07031 0.570312 8.89844 0.5 8.69531 0.5H2.25C2.04688 0.5 1.86719 0.578125 1.71094 0.734375C1.57031 0.875 1.5 1.04687 1.5 1.25V5C1.07812 5 0.71875 5.14844 0.421875 5.44531C0.140625 5.72656 0 6.07812 0 6.5V9.125C0 9.23438 0.03125 9.32812 0.09375 9.40625C0.171875 9.46875 0.265625 9.5 0.375 9.5H1.5V11.75C1.5 11.9531 1.57031 12.125 1.71094 12.2656C1.86719 12.4219 2.04688 12.5 2.25 12.5H9.75C9.95312 12.5 10.125 12.4219 10.2656 12.2656C10.4219 12.125 10.5 11.9531 10.5 11.75V9.5H11.625C11.7344 9.5 11.8203 9.46875 11.8828 9.40625C11.9609 9.32812 12 9.23438 12 9.125V6.5C12 6.07812 11.8516 5.72656 11.5547 5.44531C11.2734 5.14844 10.9219 5 10.5 5ZM9 11H3V8.75H9V11ZM9 5.75H3V2H7.5V3.125C7.5 3.23438 7.53125 3.32813 7.59375 3.40625C7.67188 3.46875 7.76562 3.5 7.875 3.5H9V5.75ZM10.125 7.4375C9.96875 7.4375 9.83594 7.38281 9.72656 7.27344C9.61719 7.16406 9.5625 7.03125 9.5625 6.875C9.5625 6.71875 9.61719 6.58594 9.72656 6.47656C9.83594 6.36719 9.96875 6.3125 10.125 6.3125C10.2812 6.3125 10.4141 6.36719 10.5234 6.47656C10.6328 6.58594 10.6875 6.71875 10.6875 6.875C10.6875 7.03125 10.6328 7.16406 10.5234 7.27344C10.4141 7.38281 10.2812 7.4375 10.125 7.4375Z"
                                        fill="#006ADE" />
                                </svg>
                                <span><?php pt('Print receipt'); ?></span>
                            </a>
                            <a href="javascript:void(0);">
                                <svg width="12" height="9" viewBox="0 0 12 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11.7656 2.97656C11.8125 2.92969 11.8594 2.92188 11.9062 2.95312C11.9688 2.98438 12 3.03125 12 3.09375V7.875C12 8.1875 11.8906 8.45312 11.6719 8.67188C11.4531 8.89062 11.1875 9 10.875 9H1.125C0.8125 9 0.546875 8.89062 0.328125 8.67188C0.109375 8.45312 0 8.1875 0 7.875V3.09375C0 3.03125 0.0234375 2.99219 0.0703125 2.97656C0.132812 2.94531 0.1875 2.94531 0.234375 2.97656C0.75 3.36719 1.95313 4.25 3.84375 5.625L4.07812 5.83594C4.45312 6.11719 4.75 6.32031 4.96875 6.44531C5.34375 6.64844 5.6875 6.75 6 6.75C6.3125 6.75 6.65625 6.64062 7.03125 6.42188C7.26562 6.29688 7.5625 6.09375 7.92188 5.8125L8.15625 5.625C10 4.29688 11.2031 3.41406 11.7656 2.97656ZM6 6C6.20312 6 6.45312 5.90625 6.75 5.71875C6.92188 5.625 7.16406 5.45312 7.47656 5.20312L7.71094 5.03906C9.67969 3.61719 11 2.64062 11.6719 2.10938L11.7891 2.01562C11.9297 1.90625 12 1.75781 12 1.57031V1.125C12 0.8125 11.8906 0.546875 11.6719 0.328125C11.4531 0.109375 11.1875 0 10.875 0H1.125C0.8125 0 0.546875 0.109375 0.328125 0.328125C0.109375 0.546875 0 0.8125 0 1.125V1.57031C0 1.75781 0.0703125 1.90625 0.210938 2.01562L0.375 2.13281C1.0625 2.66406 2.36719 3.63281 4.28906 5.03906L4.52344 5.20312C4.83594 5.45312 5.07812 5.625 5.25 5.71875C5.54688 5.90625 5.79688 6 6 6Z"
                                        fill="#006ADE" />
                                </svg>
                                <span><?php pt('Send receipt to email'); ?></span>
                            </a>
                        </div>*/ ?>
                    </div>
                    <!-- navigation нет -->
                </div>
            </div>
            <?php endif; ?>
        </div>

    </div>
    <!-- end order_block -->


</div>