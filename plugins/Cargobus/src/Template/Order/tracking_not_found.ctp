<div class="wrapper order_page">

    <div class="top_row_inner_page top_row container">
        <h1><span><?php pt('Track order'); ?></span> </h1>
    </div>

    <div class="order_block container">
        <div class="row nowrap">
            <div class="column w67 left content" style="padding-top: 30px;">
                <?php pt('No tracking available for this number!'); ?>
            </div>
        </div>
    </div>
</div>