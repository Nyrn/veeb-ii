<div class="wrapper order_page">
    <!--  -->
    <div class="top_row_inner_page top_row container">
        <h1><?php pt('Order flow'); ?></h1>
    </div>
    <!-- тут основной контент страницы -->
    <!-- start order_block -->
    <div class="order_block container">
        <?php if (($orderError && strlen($orderError) > 1) || ($orderApiError && strlen($orderApiError) > 1)): ?>
            <div class="alert failed"><?php echo ($orderError) ? $orderError : $orderApiError; ?></div>
        <?php endif; ?>

        <div class="row nowrap">
            <div class="column w67 left content">
                <div class="top_row">
                    <div class="order_steps">
                        <div class='item checked'>
                            <div class="number"><span>1</span></div>
                            <div class='text'>
                                <span><?php pt('Calculate'); ?></span>
                            </div>
                        </div>
                        <div class='item checked'>
                            <div class="number"><span>2</span></div>
                            <div class='text'>
                                <span><?php pt('Contacts'); ?></span>
                            </div>
                        </div>
                        <div class='item active'>
                            <div class="number"><span>3</span></div>
                            <div class='text'>
                                <span><?php pt('Payments'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $aliasUrls['order_payment']; ?>" class="order_form form-order" method="post">
                    <div class="row wrap payment_method">
                        <div class="row wrap">
                            <?php if (in_array(HOSTNAME, ['www.cargobus.lv'])): ?>
                            <label class="checkbox radio_btn w33">
                                <input type="radio" name="payment_method" value="card" <?php echo (isset($order['payment_method']) && $order['payment_method'] == 'card') ? 'checked' : null; ?>>
                                <span class="check"></span>
                                <span class="main">
                                    <svg width="39" height="28" viewBox="0 0 39 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M3.7453 27.8571H35.2547C37.3716 27.8571 39 26.2255 39 24.1959V3.66122C39 1.59184 37.3309 0 35.2547 0H3.7453C1.62839 0 0 1.63163 0 3.66122V24.1959C0 26.2255 1.6691 27.8571 3.7453 27.8571ZM35.2547 26.3847H3.7453C2.4833 26.3847 1.50626 25.4296 1.50626 24.1959V12.4561H37.4937V24.1959C37.4937 25.4296 36.5167 26.3847 35.2547 26.3847ZM3.7453 1.47245H35.2547C36.5167 1.47245 37.4937 2.42755 37.4937 3.66122V6.60612H1.50626V3.66122C1.50626 2.42755 2.4833 1.47245 3.7453 1.47245Z"
                                            fill="#2F80ED" />
                                        <path d="M12.2405 15.4003H10.7342C9.0651 15.4003 7.72168 16.7135 7.72168 18.3452V19.8176C7.72168 21.4492 9.0651 22.7625 10.7342 22.7625H12.2405C13.9096 22.7625 15.253 21.4492 15.253 19.8176V18.3452C15.2123 16.7135 13.8689 15.4003 12.2405 15.4003ZM13.706 19.7778C13.706 20.5737 13.014 21.2503 12.1998 21.2503H10.6935C9.8793 21.2503 9.18723 20.5737 9.18723 19.7778V18.3054C9.18723 17.5095 9.8793 16.8329 10.6935 16.8329H12.1998C13.014 16.8329 13.706 17.5095 13.706 18.3054V19.7778Z"
                                            fill="#2F80ED" />
                                        <path d="M37.36 16.993H17.4937V18.4655H37.36V16.993Z" fill="#2F80ED" />
                                        <path d="M33.6147 19.9381H17.4937V21.4106H33.6147V19.9381Z" fill="#2F80ED" />
                                    </svg>
                                    <span><?php pt('Credit card'); ?></span>
                                </span>
                            </label>
                            <?php endif; ?>
                            <label class="checkbox radio_btn w33">
                                <input type="radio" name="payment_method" value="banklink" <?php echo (!isset($order['payment_method']) || (isset($order['payment_method']) && $order['payment_method'] == 'banklink')) ? 'checked' : null; ?>>
                                <span class="check"></span>
                                <span class="main">
                                    <svg width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M24.8743 23.7699C24.8743 24.4893 24.2691 25.0726 23.5225 25.0726C22.776 25.0726 22.1708 24.4893 22.1708 23.7699V12.7125C22.1708 11.9931 22.776 11.4098 23.5225 11.4098C24.2691 11.4098 24.8743 11.9931 24.8743 12.7125V23.7699ZM6.6569 12.7125C6.6569 11.9931 6.05173 11.4098 5.30517 11.4098C4.55861 11.4098 3.95343 11.9931 3.95343 12.7125V23.7696C3.95343 24.489 4.55861 25.0722 5.30517 25.0722C6.05173 25.0722 6.6569 24.489 6.6569 23.7696V12.7125ZM12.7292 12.7125C12.7292 11.9931 12.1241 11.4098 11.3775 11.4098C10.6309 11.4098 10.0258 11.9931 10.0258 12.7125V23.7696C10.0258 24.489 10.6309 25.0722 11.3775 25.0722C12.1241 25.0722 12.7292 24.489 12.7292 23.7696V12.7125ZM18.8016 12.7125C18.8016 11.9931 18.1964 11.4098 17.4499 11.4098C16.7033 11.4098 16.0981 11.9931 16.0981 12.7125V23.7696C16.0981 24.489 16.7033 25.0722 17.4499 25.0722C18.1964 25.0722 18.8016 24.489 18.8016 23.7696V12.7125ZM27.3958 5.97604L14.5 0L1.60424 5.97604H27.3958ZM28.3916 8.73247C28.3916 8.05692 27.847 7.50918 27.1748 7.50918H1.82518C1.15302 7.50918 0.608394 8.05692 0.608394 8.73247C0.608394 9.40802 1.15302 9.95576 1.82518 9.95576H27.1748C27.8467 9.95544 28.3916 9.40802 28.3916 8.73247ZM29 27.7231C29 27.0179 28.4312 26.4463 27.7301 26.4463H1.26993C0.568779 26.4466 0 27.0182 0 27.7231C0 28.4284 0.568779 29 1.26993 29H27.7297C28.4312 29 29 28.4284 29 27.7231Z"
                                            fill="#2F80ED" />
                                    </svg>
                                    <span><?php pt('Banklink'); ?></span>
                                </span>
                            </label>
                            <label class="checkbox radio_btn w33">
                                <input type="radio" name="payment_method" value="paypal" <?php echo (isset($order['payment_method']) && $order['payment_method'] == 'paypal') ? 'checked' : null; ?>>
                                <span class="check"></span>
                                <span class="main">
                                    <img src="/img/paypal-logo-png.png" alt="paypal-logo-png">
                                    <span><?php pt('Paypal'); ?></span>
                                </span>
                            </label>
                        </div>
                        <div class="accept">
                            <label class="checkbox">
                                <input type="checkbox" name="accept_terms">
                                <span class="check"></span>
                            </label>
                            <div class="required">
                                <?php pt('I accept %s Terms of service %s', ['<a href="' . (isset($moduleUrls['terms']) ? $moduleUrls['terms'] : '#') . '" target="_blank">', '</a>']); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row additional-information-row" style="display: <?php echo (isset($order['additionalServices']['insurance'])) ? 'block' : 'none'; ?>;">
                        <div class="input_grid"></div>
                        
                        <label class="w100">
                            <h5 class="required"><?php pt('Additional information'); ?></h5>
                            <input type="text" name="additional_text" value="<?php echo (isset($order['additionalText'])) ? $order['additionalText'] : null; ?>" style="max-width: 100%;">
                        </label>
                    </div>
                    <?php /*<!--  -->
                    <div class="row wrap">
                        <div class="invoice">
                            <div class="switch__container">
                                <input id="switch-flat" class="switch switch--flat" type="checkbox">
                                <label for="switch-flat"></label>
                            </div>
                            <span>Invoice receipt</span>
                            <div class="tooltip">
                                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="7" cy="7" r="6.5" stroke="#AEAEAE" />
                                    <path d="M6.26855 8.04883V7.76758C6.26855 7.44531 6.32715 7.17725 6.44434 6.96338C6.56152 6.74951 6.7666 6.52832 7.05957 6.2998C7.4082 6.02441 7.63232 5.81055 7.73193 5.6582C7.83447 5.50586 7.88574 5.32422 7.88574 5.11328C7.88574 4.86719 7.80371 4.67822 7.63965 4.54639C7.47559 4.41455 7.23975 4.34863 6.93213 4.34863C6.65381 4.34863 6.396 4.38818 6.15869 4.46729C5.92139 4.54639 5.68994 4.6416 5.46436 4.75293L5.09521 3.97949C5.68994 3.64844 6.32715 3.48291 7.00684 3.48291C7.58105 3.48291 8.03662 3.62354 8.37354 3.90479C8.71045 4.18604 8.87891 4.57422 8.87891 5.06934C8.87891 5.28906 8.84668 5.48535 8.78223 5.6582C8.71777 5.82812 8.61963 5.99072 8.48779 6.146C8.35889 6.30127 8.13477 6.50342 7.81543 6.75244C7.54297 6.96631 7.35986 7.14355 7.26611 7.28418C7.17529 7.4248 7.12988 7.61377 7.12988 7.85107V8.04883H6.26855ZM6.08838 9.45068C6.08838 9.0083 6.30371 8.78711 6.73438 8.78711C6.94531 8.78711 7.10645 8.8457 7.21777 8.96289C7.3291 9.07715 7.38477 9.23975 7.38477 9.45068C7.38477 9.65869 7.32764 9.82422 7.21338 9.94727C7.10205 10.0674 6.94238 10.1274 6.73438 10.1274C6.52637 10.1274 6.3667 10.0688 6.25537 9.95166C6.14404 9.83154 6.08838 9.66455 6.08838 9.45068Z"
                                        fill="#AEAEAE" />
                                </svg>
                                <div class="tooltip_content">
                                    <div class="wrap text">
                                        Tooltips example
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row wrap input_grid invoice_content margin0 no_margin_bottom">
                            <div class="row mobile_100_w wrap no_margin_bottom">
                                <label class="w50">
                                    <span class="required">Company name</span>
                                    <input type="text" placeholder="Enter your Company name" value='alexsmith@gmail.com'>
                                </label>
                                <label class="w50">
                                    <span class="required">Registration number</span>
                                    <input type="text" placeholder="Enter your Registration number" value="1932193EEW2">
                                </label>
                                <label class="w50">
                                    <span class="required">Country</span>
                                    <select>
                                        <option>Germany</option>
                                        <option>Ukraine</option>
                                    </select>
                                </label>
                                <label class="w50">
                                    <span class="required">City</span>
                                    <input type="text" value="Viechtach" placeholder="Enter your City name">
                                </label>
                                <label class="w50">
                                    <span class="required">Adress</span>
                                    <input type="text" value="Henny-Wiedemann-Ring 9/1" placeholder="Enter your Adress">
                                </label>
                                <label class="w50">
                                    <span>VAT number</span>
                                    <select>
                                        <option>Bianka-Mann-Weg 2/3</option>
                                        <option>Bann-Weg 2/3</option>
                                    </select>
                                </label>
                            </div>
                            <div class="accept">
                                <label class="checkbox">
                                    <input type="checkbox">
                                    <span class="check"></span>
                                </label>
                                <div>Send Receipt to Email (shevchenko@gmail.com)
                                    <a href="javascript:void(0);" onclick="change_email_invoice($(this))">
                                        <span>Change</span>
                                        <span class="hiden">Cancel change</span>
                                    </a>
                                </div>
                            </div>
                            <div class="change_email">
                                <div class="row mobile_100_w wrap no_margin_bottom">
                                    <label class="w50">
                                        <span>Your new email</span>
                                        <input type="email" placeholder="newmail@">
                                    </label>
                                    <div class="label w50">
                                        <span></span>
                                        <div class="main_btn blue">save</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                    </div>*/ ?>
                </form>
            </div>
            <div class="column w33 right">
                <div class="wrap">
                    <div class="ship_order">
                        <?php echo (isset($order['service']) && !empty($order['service'])) ? $this->Element('Cargobus.order-service') : $this->Element('Cargobus.order-no-service'); ?>
                    </div>
                </div>
                <div class="navigation">
                    <button class="go_back_btn" onclick="window.location.href='<?php echo $aliasUrls['order_contacts']; ?>'">
                        <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle opacity="0.3" cx="10.5" cy="10.5" r="10.5" fill="#AEAEAE"></circle>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M8.14582 11.0355C7.95058 10.8403 7.9506 10.5237 8.14588 10.3284L11.3281 7.14671C11.5234 6.95147 11.84 6.95149 12.0352 7.14677C12.2305 7.34205 12.2304 7.65863 12.0352 7.85388L9.20651 10.6821L12.0347 13.5107C12.23 13.706 12.2299 14.0226 12.0347 14.2178C11.8394 14.4131 11.5228 14.4131 11.3275 14.2178L8.14582 11.0355Z"
                                fill="white"></path>
                        </svg>
                        <span><?php pt('Go back'); ?></span>
                    </button>
                    <button class='main_btn form-order-submit'><?php pt('Make payment'); ?></button>
                </div>
            </div>
        </div>

    </div>
    <!-- end order_block -->
</div>

<script type="text/javascript">
$(document).ready(function() {
    <?php echo (isset($_GET['cancel']) || isset($_GET['error'])) ? 'show_popup("payment_error");' : null; ?>
});
</script>