<div class="wrapper order_page">

    <!-- default svg parallax -->
    <?php /*
    <svg class='parallax el9' width="117" height="209" viewBox="0 0 117 209" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path opacity="0.5" d="M117 104.5L0.750005 208.856L0.750015 0.143931L117 104.5Z" fill="white" />
    </svg>
    <svg class='parallax el10' width="148" height="109" viewBox="0 0 148 109" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M84.6109 20.1141L125.44 108.69L0.621549 69.6967L84.6109 20.1141Z" fill="white" />
    </svg>
    <svg class='parallax el11' width="148" height="109" viewBox="0 0 148 109" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M84.6109 20.1141L125.44 108.69L0.621549 69.6967L84.6109 20.1141Z" fill="white" />
    </svg> 
    */ ?>
    <!--  -->
    <div class="top_row_inner_page top_row container">
        <h1><?php pt('Order flow'); ?></h1>
    </div>
    <!-- тут основной контент страницы -->
    <!-- start order_block -->
    <div class="order_block container">
        <?php if ($orderError && strlen($orderError) > 1): ?>
            <div class="alert failed"><?php echo $orderError; ?></div>
        <?php endif; ?>
        
        <div class="row nowrap">
            <div class="column w67 left content">
                <div class="top_row">
                    <div class="order_steps">
                        <div class='item checked'>
                            <div class="number"><span>1</span></div>
                            <div class='text'>
                                <span><?php pt('Calculate'); ?></span>
                            </div>
                        </div>
                        <div class='item active'>
                            <div class="number"><span>2</span></div>
                            <div class='text'>
                                <span><?php pt('Contacts'); ?></span>
                            </div>
                        </div>
                        <div class='item'>
                            <div class="number"><span>3</span></div>
                            <div class='text'>
                                <span><?php pt('Payments'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $aliasUrls['order_contacts']; ?>" class="order_form form-order-contacts" method="post">
                    <!-- invoice content -->
                    <!-- при смене способа оплаты нужно подменять весь блок payment_method_content  -->
                    <!-- на нем будут уникальные классы -->
                    <div class="row wrap input_grid">
                        <h5><?php pt('Sender'); ?></h5>
                        <div class="row mobile_100_w wrap">
                            <label class="w50">
                                <span class="required"><?php pt('Full name'); ?></span>
                                <input type="text" name="sender[name]" value="<?php echo (isset($order['sender'])) ? $order['sender']['name'] : null; ?>" placeholder="<?php pt('Enter your full name'); ?>" required>
                            </label>
                            <?php /*<label class="w50">
                                <span>Company name</span>
                                <input type="text" name="sender[company]" value="<?php echo (isset($order['sender'])) ? $order['sender']['company'] : null; ?>" placeholder="Enter your Company name">
                            </label>*/ ?>
                            <label class="w50">
                                <span class="required"><?php pt('Email'); ?></span>
                                <input type="email" name="sender[email]" value="<?php echo (isset($order['sender'])) ? $order['sender']['email'] : null; ?>" placeholder="<?php pt('Enter your email'); ?>">
                            </label>
                            <label class="w50">
                                <span class="required"><?php pt('Phone'); ?></span>
                                <input type="text" name="sender[phone]" value="<?php echo (isset($order['sender'])) ? $order['sender']['phone'] : null; ?>" class="tel_mask" placeholder="+372 6813444">
                            </label>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row wrap input_grid">
                        <h5><?php pt('Recipient'); ?></h5>
                        <div class="row mobile_100_w wrap">
                            <label class="w50">
                                <span class="required"><?php pt('Full name'); ?></span>
                                <input type="text" name="recipient[name]" value="<?php echo (isset($order['recipient'])) ? $order['recipient']['name'] : null; ?>" placeholder="<?php pt('Enter your full name'); ?>" required>
                            </label>
                            <?php /*<label class="w50">
                                <span>Company name</span>
                                <input type="text" name="recipient[company]" value="<?php echo (isset($order['recipient'])) ? $order['recipient']['company'] : null; ?>" placeholder="Enter your Company name">
                            </label>*/ ?>
                            <label class="w50">
                                <span><?php pt('Email'); ?></span>
                                <input type="email" name="recipient[email]" value="<?php echo (isset($order['recipient'])) ? $order['recipient']['email'] : null; ?>" placeholder="<?php pt('Enter your email'); ?>">
                            </label>
                            <label class="w50">
                                <span class="required"><?php pt('Phone'); ?></span>
                                <input type="text" name="recipient[phone]" value="<?php echo (isset($order['recipient'])) ? $order['recipient']['phone'] : null; ?>" class="tel_mask" placeholder="+372 6813444" required>
                            </label>
                        </div>
                    </div>
                    <!--  -->

                    <div class="row additional-information-row" style="display: <?php echo (isset($order['additionalServices']['insurance'])) ? 'block' : 'none'; ?>;">
                        <label class="w100">
                            <h5 class="required"><?php pt('Additional information'); ?></h5>
                            <input type="text" name="additional_text" value="<?php echo (isset($order['additionalText'])) ? $order['additionalText'] : null; ?>" style="max-width: 100%;">
                        </label>
                    </div>
                </form>
                <!--  -->
            </div>
            <div class="column w33 right">
                <div class="wrap">
                    <div class="ship_order">
                        <?php echo (isset($order['service']) && !empty($order['service'])) ? $this->Element('Cargobus.order-service') : $this->Element('Cargobus.order-no-service'); ?>
                    </div>

                    <div class="navigation">
                        <button class="go_back_btn" onclick="window.location.href='<?php echo $aliasUrls['order']; ?>'">
                            <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle opacity="0.3" cx="10.5" cy="10.5" r="10.5" fill="#AEAEAE"></circle>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M8.14582 11.0355C7.95058 10.8403 7.9506 10.5237 8.14588 10.3284L11.3281 7.14671C11.5234 6.95147 11.84 6.95149 12.0352 7.14677C12.2305 7.34205 12.2304 7.65863 12.0352 7.85388L9.20651 10.6821L12.0347 13.5107C12.23 13.706 12.2299 14.0226 12.0347 14.2178C11.8394 14.4131 11.5228 14.4131 11.3275 14.2178L8.14582 11.0355Z"
                                    fill="white"></path>
                            </svg>
                            <span><?php pt('Go back'); ?></span>
                        </button>
                        <button class='main_btn form-order-contacts-submit'><?php pt('Continue'); ?></button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end order_block -->


</div>