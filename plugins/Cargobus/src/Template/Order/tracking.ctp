<div class="wrapper order_page">

    <div class="top_row_inner_page top_row container">
        <h1><span><?php pt('Track order'); ?></span> <?php echo $parcelCode; ?></h1>
    </div>

    <div class="order_block container">
        <div class="row nowrap">
            <div class="column w67 left content">
                <div class="tracking_progress_top">
                    <div class="progress_name">
                        <h3><?php pt('Status'); ?></h3>
                    </div>
                    <div class="progress_date">
                        <h3><?php pt('Date'); ?></h3>
                    </div>
                    <div class="progress_city">
                        <h3><?php pt('Location'); ?></h3>
                    </div>
                </div>
                <div class="tracking_progress">
                    <?php $i = 1; ?>
                    <?php foreach ($trackingEvents as $type => $tr): ?>
                        <div class="progress_stages <?php echo ($i == 1) ? 'active' : null; ?>">
                            <?php if (!isset($tr[0])): ?>
                                <div class="progress_name">
                                    <span></span>
                                    <p><?php echo (isset($trackingEventTitles[$type])) ? $trackingEventTitles[$type] : $type; ?></p>
                                </div>
                                <div class="progress_date">
                                    <h5>Date:</h5>
                                    <p><?php echo date('d.m.Y H:i', strtotime($tr['time'])); ?></p>
                                </div>
                                <div class="progress_city">
                                        <h5>Location:</h5>
                                    <p><?php echo parseTrackingLocation($tr['location']); ?></p>
                                </div>
                            <?php else: ?>
                                <?php $m = 1; ?>
                                <?php foreach ($tr as $trSub): ?>
                                    <div class="progress_name">
                                        <?php if ($m == 1): ?>
                                            <span></span>
                                            <p><?php echo (isset($trackingEventTitles[$type])) ? $trackingEventTitles[$type] : $type; ?></p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="progress_date">
                                        <h5>Date:</h5>
                                        <p><?php echo date('d.m.Y H:i', strtotime($trSub['time'])); ?></p>
                                    </div>
                                    <div class="progress_city">
                                            <h5>Location:</h5>
                                        <p><?php echo parseTrackingLocation($trSub['location']); ?></p>
                                    </div>
                                    <?php $m++; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <?php $i++; ?>
                    <?php endforeach; ?>

                    <?php /*<div class="progress_stages">
                        <div class="progress_name">
                            <span></span>
                            <p>Out for Delivery</p>
                        </div>
                        <div class="progress_date">
                                <h5>Date:</h5>
                            <p>15.02.2019 05:00</p>
                        </div>
                        <div class="progress_city">
                                <h5>Location:</h5>
                            <p>Alytus, Lithuania</p>
                        </div>
                        <div class="progress_name">
                        </div>
                        <div class="progress_date">
                                <h5>Date:</h5>
                            <p>15.02.2019 05:36</p>
                        </div>
                        <div class="progress_city">
                                <h5>Location:</h5>
                            <p>Tallinn, Estonia</p>
                        </div>

                        <div class="progress_name">
                        </div>
                        <div class="progress_date">
                                <h5>Date:</h5>
                            <p>15.02.2019 03:12</p>
                        </div>
                        <div class="progress_city">
                                <h5>Location:</h5>
                            <p>Tallinn, Estonia</p>
                        </div>
                        <div class="progress_name">
                        </div>
                        <div class="progress_date">
                                <h5>Date:</h5>
                            <p>15.02.2019 00:24</p>
                        </div>
                        <div class="progress_city">
                                <h5>Location:</h5>
                            <p>Tallinn, Estonia</p>
                        </div>
                    </div>

                    <div class="progress_stages">
                        <div class="progress_name">
                            <span></span>
                            <p>Shipped</p>
                        </div>
                        <div class="progress_date">
                                <h5>Date:</h5>
                            <p>14.02.2019 12:31</p>
                        </div>
                        <div class="progress_city">
                                <h5>Location:</h5>
                            <p>Tallinn, Estonia</p>
                        </div>
                    </div>
                    <?php if (isset($trackingEvents['created'])): ?>
                        <div class="progress_stages">
                            <div class="progress_name">
                                <span></span>
                                <p>Label Created</p>
                            </div>
                            <div class="progress_date">
                                    <h5>Date:</h5>
                                <p><?php echo date('d.m.Y H:i', strtotime($trackingEvents['created']['time'])); ?></p>
                            </div>
                            <div class="progress_city">
                                    <h5>Location:</h5>
                                <p><?php echo $trackingEvents['created']['location']; ?></p>
                            </div>
                        </div>
                    <?php endif; ?>*/ ?>
                </div>
            </div>
            <div class="column w33 right">
                <div class="wrap">
                    <div class="ship_order">
                            <div class="tracking_progress_top">
                                    <div class="order_sidebar">
                                        <h3><?php pt('Order information'); ?></h3>
                                    </div>
                                </div>
                        
                         <!-- ship_order_data -->
                        <div class="data">
                            <?php if (isset($orderInfoServices['complex'])): ?>
                                <table class='simple_table'>
                                    <tr>
                                        <td><?php echo $orderInfoServices['complex']['serviceName']; ?></td>
                                        <td></td>
                                        <?php /*<td><span class="price start_cost"><?php echo $orderInfoServices['complex']['priceWithoutVat']; ?></span> EUR</span></td>*/ ?>
                                    </tr>
                                </table>
                                <div class="line_hr"></div>
                                <?php unset($orderInfoServices['complex']); ?>
                            <?php endif; ?>
                        
                            <?php /*if (!empty($orderInfoServices)): ?>
                                <table class='simple_table calc_cost final_table'>
                                    <?php foreach ($orderInfoServices as $sType => $service): ?>
                                        <tr>
                                            <td><?php echo ucfirst($sType); ?></td>
                                            <td></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            <?php endif;*/ ?>
                        </div>
                        <!-- ship_order_data -->
                        <?php /*<div class="bottom row margin0 between align-center">
                            <div class="left">
                                <span>Total</span>
                                <div class="tooltip">
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="8" cy="8" r="7.5" stroke="#AEAEAE" />
                                        <path d="M7.28943 9.58H8.51043V9.239C8.51043 8.81 8.76343 8.623 9.21443 8.381C10.0504 7.941 10.7104 7.512 10.7104 6.434C10.7104 5.015 9.58843 4.168 8.05943 4.168C6.55243 4.168 5.38643 5.004 5.28743 6.698H6.54143C6.64043 5.642 7.24543 5.323 8.01543 5.323C8.98343 5.323 9.45643 5.807 9.45643 6.467C9.45643 7.017 9.13743 7.226 8.49943 7.556C7.69643 7.974 7.28943 8.216 7.28943 9.008V9.58ZM7.90543 12.132C8.37843 12.132 8.74143 11.813 8.74143 11.351C8.74143 10.889 8.37843 10.581 7.90543 10.581C7.43243 10.581 7.08043 10.889 7.08043 11.351C7.08043 11.813 7.43243 12.132 7.90543 12.132Z"
                                            fill="#AEAEAE" />
                                    </svg>
                                    <div class="tooltip_content">
                                        <div class="wrap text">
                                            <?php pt('Including VAT'); ?> <?php echo ($orderInfo['vatRate'] * 100); ?>%
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span class="price">
                                <span class="total_price"><?php echo $orderInfo['totalPriceWithVat']; ?></span> EUR
                            </span>
                        </div>*/ ?>
                    </div>
                    <?php /*<div class="tracking_progress_btn navigation">
                        <button class="main_btn no_bg red" type="button">
                            print order info
                        </button>
                        <button class='main_btn modal_open' type="button">
                            send to email
                        </button>
                    </div>*/ ?>
                </div>
            </div>
        </div>
    </div>
</div>