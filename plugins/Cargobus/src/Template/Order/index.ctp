<div class="wrapper order_page">

    <!--  -->
    <div class="top_row_inner_page top_row container">
        <h1><?php pt('Order flow'); ?></h1>
    </div>

    <!-- тут основной контент страницы -->
    <!-- start order_block -->
    <div class="order_block container">
        <?php if (($orderError && strlen($orderError) > 1) || ($orderApiError && strlen($orderApiError) > 1)): ?>
            <div class="alert failed"><?php echo ($orderError) ? $orderError : $orderApiError; ?></div>
        <?php endif; ?>

        <div class="row nowrap">
            <div class="column w67 left content">
                <div class="top_row">
                    <div class="order_steps">
                        <div class='item active'>
                            <div class="number"><span>1</span></div>
                            <div class='text'>
                                <span><?php pt('Calculate'); ?></span>
                            </div>
                        </div>
                        <div class='item'>
                            <div class="number"><span>2</span></div>
                            <div class='text'>
                                <span><?php pt('Contacts'); ?></span>
                            </div>
                        </div>
                        <div class='item'>
                            <div class="number"><span>3</span></div>
                            <div class='text'>
                                <span><?php pt('Payments'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="calc_wrap">
                    <div id="calculator" class='calculator'>
                        <form action="<?php echo $aliasUrls['order']; ?>" class="form-order" method="post">
                            <div class="row">
                                <h5><?php pt('From'); ?></h5>
                                <span class="required"><?php pt('Delivery method'); ?></span>
                                <div class="row radio_wrap">
                                    <label class="w50 radio">
                                        <input type="radio" name="from" value="courier" <?php echo (!isset($order['from']['type']) || (isset($order['from']['type']) && $order['from']['type'] == 'courier')) ? 'checked' : null; ?>>
                                        <span><?php pt('Courier'); ?></span>
                                    </label>
                                    <label class="w50 radio">
                                        <input type="radio" name="from" value="terminal" <?php echo (isset($order['from']['type']) && $order['from']['type'] == 'terminal') ? 'checked' : null; ?>>
                                        <span><?php pt('Terminal'); ?></span>
                                    </label>
                                </div>
                                <div class="tabs_el_wrap">
                                    <!-- el 1 -->
                                    <div class="tabs_el" data-for='courier' style="display: <?php echo (!isset($order['from']['type']) || (isset($order['from']['type']) && $order['from']['type'] == 'courier')) ? 'block' : 'none'; ?>;">
                                        <div class="row margin0 nowrap">
                                            <label class="with_image w100 <?php echo ((isset($order['from']['type']) && $order['from']['type'] == 'courier') && empty($order['from']['address'])) ? 'error' : null; ?>">
                                                <span class="required"><?php pt('Sender address'); ?></span>
                                                <svg style="transform: translateY(0px);" width="14" height="19" viewBox="0 0 14 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M6.29688 17.8984C5.6875 17.0078 4.90234 15.8828 3.94141 14.5234C2.76953 12.8359 1.99609 11.6992 1.62109 11.1133C1.08203 10.2695 0.71875 9.56641 0.53125 9.00391C0.34375 8.41797 0.25 7.75 0.25 7C0.25 5.78125 0.554687 4.65625 1.16406 3.625C1.77344 2.59375 2.59375 1.77344 3.625 1.16406C4.65625 0.554688 5.78125 0.25 7 0.25C8.21875 0.25 9.34375 0.554688 10.375 1.16406C11.4062 1.77344 12.2266 2.59375 12.8359 3.625C13.4453 4.65625 13.75 5.78125 13.75 7C13.75 7.75 13.6562 8.41797 13.4688 9.00391C13.2812 9.56641 12.918 10.2695 12.3789 11.1133C12.0039 11.6992 11.2305 12.8359 10.0586 14.5234L7.70312 17.8984C7.53906 18.1328 7.30469 18.25 7 18.25C6.69531 18.25 6.46094 18.1328 6.29688 17.8984ZM7 9.8125C7.77344 9.8125 8.42969 9.54297 8.96875 9.00391C9.53125 8.44141 9.8125 7.77344 9.8125 7C9.8125 6.22656 9.53125 5.57031 8.96875 5.03125C8.42969 4.46875 7.77344 4.1875 7 4.1875C6.22656 4.1875 5.55859 4.46875 4.99609 5.03125C4.45703 5.57031 4.1875 6.22656 4.1875 7C4.1875 7.77344 4.45703 8.44141 4.99609 9.00391C5.55859 9.54297 6.22656 9.8125 7 9.8125Z"
                                                        fill="#111111" />
                                                </svg>
                                                <input type="text" name="courier_from" value="<?php echo (isset($order['from']['type']) && $order['from']['type'] == 'courier') ? $order['from']['address'] : null; ?>" placeholder="<?php pt('Type to search for address..'); ?>" class="autocomplete" id="autocomplete-from-address" data-autocomplete-target="#google-id-from">
                                                <?php if ((isset($order['from']['type']) && $order['from']['type'] == 'courier') && empty($order['from']['address'])): ?><span class="error"><?php pt('Еmpty field'); ?></span><?php endif; ?>
                                            </label>
                                            <?php /*<label class="small">
                                                <span>Apt / Office</span>
                                                <input type="text" name="number_from" value="<?php echo (isset($order['from']['type']) && $order['from']['type'] == 'courier' && isset($order['from']['number'])) ? $order['from']['number'] : null; ?>" required>
                                            </label>*/ ?>
                                        </div>
                                        <div class="row margin0 nowrap" style="z-index: 0;">
                                            <label class="middle">
                                                <span class="required"><?php pt('Pick-up date'); ?></span>
                                                <select name="pickup_date">
                                                    <?php foreach ($pickupDatesList as $dk => $d): ?>
                                                        <option value="<?php echo $dk; ?>" <?php echo (isset($order['pickup']['date']) && $order['pickup']['date'] == $dk) ? 'selected' : null; ?>><?php echo $d; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </label>
                                            <?php /*<label class="w100">
                                                <span>Comments for courier</span>
                                                <input type="text" name="pickup_comments" value="<?php echo (isset($order['pickup']['comments'])) ? $order['pickup']['comments'] : null; ?>" placeholder="Your message">
                                                <div class="tooltip">
                                                    <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <circle cx="8.5" cy="8.5" r="8" stroke="#4F4F4F"></circle>
                                                        <path d="M7.62158 9.61523V9.27148C7.62158 8.8776 7.6932 8.54997 7.83643 8.28857C7.97965 8.02718 8.23031 7.75684 8.58838 7.47754C9.01449 7.14095 9.28841 6.87956 9.41016 6.69336C9.53548 6.50716 9.59814 6.28516 9.59814 6.02734C9.59814 5.72656 9.49788 5.49561 9.29736 5.33447C9.09684 5.17334 8.80859 5.09277 8.43262 5.09277C8.09245 5.09277 7.77734 5.14111 7.4873 5.23779C7.19727 5.33447 6.91439 5.45085 6.63867 5.58691L6.1875 4.6416C6.91439 4.23698 7.6932 4.03467 8.52393 4.03467C9.22575 4.03467 9.78255 4.20654 10.1943 4.55029C10.6061 4.89404 10.812 5.36849 10.812 5.97363C10.812 6.24219 10.7726 6.4821 10.6938 6.69336C10.6151 6.90104 10.4951 7.09977 10.334 7.28955C10.1764 7.47933 9.90251 7.7264 9.51221 8.03076C9.1792 8.29215 8.9554 8.50879 8.84082 8.68066C8.72982 8.85254 8.67432 9.0835 8.67432 9.37354V9.61523H7.62158ZM7.40137 11.3286C7.40137 10.7879 7.66455 10.5176 8.19092 10.5176C8.44873 10.5176 8.64567 10.5892 8.78174 10.7324C8.91781 10.8721 8.98584 11.0708 8.98584 11.3286C8.98584 11.5828 8.91602 11.7852 8.77637 11.9355C8.6403 12.0824 8.44515 12.1558 8.19092 12.1558C7.93669 12.1558 7.74154 12.0841 7.60547 11.9409C7.4694 11.7941 7.40137 11.59 7.40137 11.3286Z"
                                                            fill="#4F4F4F"></path>
                                                    </svg>
                                                    <div class="tooltip_content">
                                                        <div class="wrap text">
                                                            Tooltips example
                                                        </div>
                                                    </div>
                                                </div>
                                            </label>*/ ?>
                                        </div>
                                        <div class="text">
                                            <?php pt('The courier picks up the parcel on %s working days from 9:00 to 17:00 %s and will contact you by phone.', ['<b>', '</b>']); ?>
                                        </div>
                                    </div>
                                    <!-- el 1 -->
                                    <div class="row margin0 tabs_el nowrap" data-for='terminal' style="display: <?php echo (isset($order['from']['type']) && $order['from']['type'] == 'terminal') ? 'block' : 'none'; ?>;">
                                        <div class="row margin0 nowrap">
                                            <label class="with_image w100">
                                                <span class="required"><?php pt('Sender address'); ?></span>
                                                <svg style="transform: translateY(3px);" width="16" height="13"
                                                    viewBox="0 0 16 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M12.4922 8.65625H3.50781C3.45898 8.65625 3.41016 8.68066 3.36133 8.72949C3.32878 8.76204 3.3125 8.80273 3.3125 8.85156V10.0234C3.3125 10.0723 3.32878 10.1211 3.36133 10.1699C3.41016 10.2025 3.45898 10.2188 3.50781 10.2188H12.4922C12.541 10.2188 12.5817 10.2025 12.6143 10.1699C12.6631 10.1211 12.6875 10.0723 12.6875 10.0234V8.85156C12.6875 8.80273 12.6631 8.76204 12.6143 8.72949C12.5817 8.68066 12.541 8.65625 12.4922 8.65625ZM12.4922 11H3.50781C3.45898 11 3.41016 11.0163 3.36133 11.0488C3.32878 11.0977 3.3125 11.1465 3.3125 11.1953V12.3672C3.3125 12.416 3.32878 12.4567 3.36133 12.4893C3.41016 12.5381 3.45898 12.5625 3.50781 12.5625H12.4922C12.541 12.5625 12.5817 12.5381 12.6143 12.4893C12.6631 12.4567 12.6875 12.416 12.6875 12.3672V11.1953C12.6875 11.1465 12.6631 11.0977 12.6143 11.0488C12.5817 11.0163 12.541 11 12.4922 11ZM12.4922 6.3125H3.53223C3.46712 6.3125 3.41829 6.33691 3.38574 6.38574C3.35319 6.41829 3.33691 6.45898 3.33691 6.50781V7.67969C3.33691 7.72852 3.35319 7.77734 3.38574 7.82617C3.41829 7.85872 3.46712 7.875 3.53223 7.875H12.4922C12.541 7.875 12.5817 7.85872 12.6143 7.82617C12.6631 7.77734 12.6875 7.72852 12.6875 7.67969V6.50781C12.6875 6.45898 12.6631 6.41829 12.6143 6.38574C12.5817 6.33691 12.541 6.3125 12.4922 6.3125ZM15.1045 2.91895L8.43945 0.160156C8.14648 0.0299479 7.85352 0.0299479 7.56055 0.160156L0.919922 2.91895C0.692057 3.0166 0.513021 3.16309 0.382812 3.3584C0.252604 3.55371 0.1875 3.7653 0.1875 3.99316V12.3672C0.1875 12.416 0.203776 12.4567 0.236328 12.4893C0.285156 12.5381 0.333984 12.5625 0.382812 12.5625H2.33594C2.38477 12.5625 2.42546 12.5381 2.45801 12.4893C2.50684 12.4567 2.53125 12.416 2.53125 12.3672V6.3125C2.53125 6.10091 2.60449 5.92188 2.75098 5.77539C2.91374 5.61263 3.10905 5.53125 3.33691 5.53125H12.6631C12.891 5.53125 13.0781 5.61263 13.2246 5.77539C13.3874 5.92188 13.4688 6.10091 13.4688 6.3125V12.3672C13.4688 12.416 13.485 12.4567 13.5176 12.4893C13.5664 12.5381 13.6152 12.5625 13.6641 12.5625H15.6172C15.666 12.5625 15.7067 12.5381 15.7393 12.4893C15.7881 12.4567 15.8125 12.416 15.8125 12.3672V3.99316C15.8125 3.7653 15.7474 3.55371 15.6172 3.3584C15.487 3.16309 15.3079 3.0166 15.0801 2.91895H15.1045Z"
                                                        fill="#111111"></path>
                                                </svg>
                                                <select name="terminal_from" class="search__select chosen-terminal-select">
                                                    <?php foreach ($terminalsList as $id => $address): ?>
                                                        <option value="<?php echo $id; ?>" <?php echo (isset($order['from']['type']) && $order['from']['type'] == 'terminal' && $order['from']['address'] == $id) ? 'selected' : null; ?>><?php echo $address; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="text">
                                            <div class="terminal-operating-mode"><?php pt('Terminal operating mode'); ?>: <?php pt('Mo-Fr'); ?>: <div class="monday"><b><?php echo implode(', ', $terminalFromAvailability['monday']); ?></b></div>, <?php pt('Sa'); ?>: <div class="saturday"><b><?php echo ($terminalFromAvailability['saturday'][key($terminalFromAvailability['saturday'])] == 'Closed') ? gt('Closed') : implode(', ', $terminalFromAvailability['saturday']); ?></b></div>, <?php pt('Su'); ?>: <div class="sunday"><b><?php echo ($terminalFromAvailability['sunday'][key($terminalFromAvailability['sunday'])] == 'Closed') ? gt('Closed') : implode(', ', $terminalFromAvailability['sunday']); ?></b></div></div>
                                            <div class="terminal-phone"><?php pt('Telephone'); ?>: <a href="tel:<?php echo $terminalFrom->phone; ?>"><?php echo $terminalFrom->phone; ?></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <h5><?php pt('To'); ?></h5>
                                <span class="required"><?php pt('Delivery method'); ?></span>
                                <div class="row radio_wrap">
                                    <label class="w50 radio">
                                        <input type="radio" name="to" value="courier" <?php echo (!isset($order['to']['type']) || (isset($order['to']['type']) && $order['to']['type'] == 'courier')) ? 'checked' : null; ?>>
                                        <span><?php pt('Courier'); ?></span>
                                    </label>
                                    <label class="w50 radio">
                                        <input type="radio" name="to" value="terminal" <?php echo (isset($order['to']['type']) && $order['to']['type'] == 'terminal') ? 'checked' : null; ?>>
                                        <span><?php pt('Terminal'); ?></span>
                                    </label>
                                </div>
                                <div class="tabs_el_wrap">
                                    <!-- el 1 -->
                                    <div class="tabs_el" data-for='courier' style="display: <?php echo (!isset($order['to']['type']) || (isset($order['to']['type']) && $order['to']['type'] == 'courier')) ? 'block' : 'none'; ?>;">
                                        <div class="row margin0 nowrap">
                                            <label class="with_image w100 <?php echo ((isset($order['to']['type']) && $order['to']['type'] == 'courier') && empty($order['to']['address'])) ? 'error' : null; ?>">
                                                <span class="required"><?php pt('Recipient address'); ?></span>
                                                <svg style="transform: translateY(0px);" width="14" height="19" viewBox="0 0 14 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M6.29688 17.8984C5.6875 17.0078 4.90234 15.8828 3.94141 14.5234C2.76953 12.8359 1.99609 11.6992 1.62109 11.1133C1.08203 10.2695 0.71875 9.56641 0.53125 9.00391C0.34375 8.41797 0.25 7.75 0.25 7C0.25 5.78125 0.554687 4.65625 1.16406 3.625C1.77344 2.59375 2.59375 1.77344 3.625 1.16406C4.65625 0.554688 5.78125 0.25 7 0.25C8.21875 0.25 9.34375 0.554688 10.375 1.16406C11.4062 1.77344 12.2266 2.59375 12.8359 3.625C13.4453 4.65625 13.75 5.78125 13.75 7C13.75 7.75 13.6562 8.41797 13.4688 9.00391C13.2812 9.56641 12.918 10.2695 12.3789 11.1133C12.0039 11.6992 11.2305 12.8359 10.0586 14.5234L7.70312 17.8984C7.53906 18.1328 7.30469 18.25 7 18.25C6.69531 18.25 6.46094 18.1328 6.29688 17.8984ZM7 9.8125C7.77344 9.8125 8.42969 9.54297 8.96875 9.00391C9.53125 8.44141 9.8125 7.77344 9.8125 7C9.8125 6.22656 9.53125 5.57031 8.96875 5.03125C8.42969 4.46875 7.77344 4.1875 7 4.1875C6.22656 4.1875 5.55859 4.46875 4.99609 5.03125C4.45703 5.57031 4.1875 6.22656 4.1875 7C4.1875 7.77344 4.45703 8.44141 4.99609 9.00391C5.55859 9.54297 6.22656 9.8125 7 9.8125Z"
                                                        fill="#111111" />
                                                </svg>
                                                <input type="text" name="courier_to" value="<?php echo (isset($order['to']['type']) && $order['to']['type'] == 'courier') ? $order['to']['address'] : null; ?>" placeholder="<?php pt('Type to search for address..'); ?>" class="autocomplete" id="autocomplete-to-address" data-autocomplete-target="#google-id-to">
                                                <?php if ((isset($order['to']['type']) && $order['to']['type'] == 'courier') && empty($order['to']['address'])): ?><span class="error"><?php pt('Еmpty field'); ?></span><?php endif; ?>
                                            </label>
                                            <?php /*<label class="small">
                                                <span>Apt / Office</span>
                                                <input type="text" name="number_to" value="<?php echo (isset($order['to']['type']) && $order['to']['type'] == 'courier' && isset($order['to']['number'])) ? $order['to']['number'] : null; ?>" required>
                                            </label>*/ ?>
                                        </div>

                                        <?php /*<div class="row margin0 nowrap" style="z-index: 0;">
                                            <label class="middle">
                                                <span class="required"><?php pt('Delivery date'); ?></span>
                                                <select name="delivery_date">
                                                    <?php foreach ($pickupDatesList as $dk => $d): ?>
                                                        <option value="<?php echo $dk; ?>" <?php echo (isset($order['delivery']['date']) && $order['delivery']['date'] == $dk) ? 'selected' : null; ?>><?php echo $d; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </label>
                                        </div>*/ ?>
                                    </div>
                                    <!-- el 1 -->
                                    <div class="row margin0 tabs_el nowrap" data-for='terminal' style="display: <?php echo (isset($order['to']['type']) && $order['to']['type'] == 'terminal') ? 'block' : 'none'; ?>;">
                                        <div class="row margin0 nowrap">
                                            <label class="with_image w100">
                                                <span class="required"><?php pt('Recipient address'); ?></span>
                                                <svg style="transform: translateY(3px);" width="16" height="13"
                                                    viewBox="0 0 16 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.2" d="M12.4922 8.65625H3.50781C3.45898 8.65625 3.41016 8.68066 3.36133 8.72949C3.32878 8.76204 3.3125 8.80273 3.3125 8.85156V10.0234C3.3125 10.0723 3.32878 10.1211 3.36133 10.1699C3.41016 10.2025 3.45898 10.2188 3.50781 10.2188H12.4922C12.541 10.2188 12.5817 10.2025 12.6143 10.1699C12.6631 10.1211 12.6875 10.0723 12.6875 10.0234V8.85156C12.6875 8.80273 12.6631 8.76204 12.6143 8.72949C12.5817 8.68066 12.541 8.65625 12.4922 8.65625ZM12.4922 11H3.50781C3.45898 11 3.41016 11.0163 3.36133 11.0488C3.32878 11.0977 3.3125 11.1465 3.3125 11.1953V12.3672C3.3125 12.416 3.32878 12.4567 3.36133 12.4893C3.41016 12.5381 3.45898 12.5625 3.50781 12.5625H12.4922C12.541 12.5625 12.5817 12.5381 12.6143 12.4893C12.6631 12.4567 12.6875 12.416 12.6875 12.3672V11.1953C12.6875 11.1465 12.6631 11.0977 12.6143 11.0488C12.5817 11.0163 12.541 11 12.4922 11ZM12.4922 6.3125H3.53223C3.46712 6.3125 3.41829 6.33691 3.38574 6.38574C3.35319 6.41829 3.33691 6.45898 3.33691 6.50781V7.67969C3.33691 7.72852 3.35319 7.77734 3.38574 7.82617C3.41829 7.85872 3.46712 7.875 3.53223 7.875H12.4922C12.541 7.875 12.5817 7.85872 12.6143 7.82617C12.6631 7.77734 12.6875 7.72852 12.6875 7.67969V6.50781C12.6875 6.45898 12.6631 6.41829 12.6143 6.38574C12.5817 6.33691 12.541 6.3125 12.4922 6.3125ZM15.1045 2.91895L8.43945 0.160156C8.14648 0.0299479 7.85352 0.0299479 7.56055 0.160156L0.919922 2.91895C0.692057 3.0166 0.513021 3.16309 0.382812 3.3584C0.252604 3.55371 0.1875 3.7653 0.1875 3.99316V12.3672C0.1875 12.416 0.203776 12.4567 0.236328 12.4893C0.285156 12.5381 0.333984 12.5625 0.382812 12.5625H2.33594C2.38477 12.5625 2.42546 12.5381 2.45801 12.4893C2.50684 12.4567 2.53125 12.416 2.53125 12.3672V6.3125C2.53125 6.10091 2.60449 5.92188 2.75098 5.77539C2.91374 5.61263 3.10905 5.53125 3.33691 5.53125H12.6631C12.891 5.53125 13.0781 5.61263 13.2246 5.77539C13.3874 5.92188 13.4688 6.10091 13.4688 6.3125V12.3672C13.4688 12.416 13.485 12.4567 13.5176 12.4893C13.5664 12.5381 13.6152 12.5625 13.6641 12.5625H15.6172C15.666 12.5625 15.7067 12.5381 15.7393 12.4893C15.7881 12.4567 15.8125 12.416 15.8125 12.3672V3.99316C15.8125 3.7653 15.7474 3.55371 15.6172 3.3584C15.487 3.16309 15.3079 3.0166 15.0801 2.91895H15.1045Z"
                                                        fill="#111111"></path>
                                                </svg>
                                                <select name="terminal_to" class="search__select chosen-terminal-select">
                                                    <?php foreach ($terminalsList as $id => $address): ?>
                                                        <option value="<?php echo $id; ?>" <?php echo (isset($order['to']['type']) && $order['to']['type'] == 'terminal' && $order['to']['address'] == $id) ? 'selected' : null; ?>><?php echo $address; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="text">
                                            <div class="terminal-operating-mode"><?php pt('Terminal operating mode'); ?>: <?php pt('Mo-Fr'); ?>: <div class="monday"><b><?php echo implode(', ', $terminalToAvailability['monday']); ?></b></div>, <?php pt('Sa'); ?>: <div class="saturday"><b><?php echo ($terminalToAvailability['saturday'][key($terminalToAvailability['saturday'])] == 'Closed') ? gt('Closed') : implode(', ', $terminalToAvailability['saturday']); ?></b></div>, <?php pt('Su'); ?>: <div class="sunday"><b><?php echo ($terminalToAvailability['sunday'][key($terminalToAvailability['sunday'])] == 'Closed') ? gt('Closed') : implode(', ', $terminalToAvailability['sunday']); ?></b></div></div>
                                            <div class="terminal-phone"><?php pt('Telephone'); ?>: <a href="tel:<?php echo $terminalTo->phone; ?>"><?php echo $terminalTo->phone; ?></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="parcel_repeater">
                                    <script>
                                        var parcel_repeater_cnt = <?php echo (isset($order['parcels'])) ? count($order['parcels']) : 1; ?>;
                                        var parcel_data = [];

                                        <?php if (isset($order['parcels'])): ?>
                                            <?php foreach ($order['parcels'] as $key => $parcel): ?>
                                                parcel_data.push({'weight' : <?php echo ($parcel['weight']) ? $parcel['weight'] : 0; ?>, 'length' : <?php echo ($parcel['length']) ? $parcel['length'] : 0; ?>, 'width' : <?php echo ($parcel['width']) ? $parcel['width'] : 0; ?>, 'height' : <?php echo ($parcel['height']) ? $parcel['height'] : 0; ?>, 'text' : '<?php echo (isset($parcel['text'])) ? $parcel['text'] : null; ?>'})
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </script>
                                </div>
                            </div>

                            <div class="row">
                                <label class="w100">
                                    <h5 class="<?php echo (isset($order['additionalServices']['insurance'])) ? 'required' : null; ?>"><?php pt('Additional information'); ?></h5>
                                    <input type="text" name="additional_text" value="<?php echo (isset($order['additionalText'])) ? $order['additionalText'] : null; ?>">
                                </label>
                            </div>

                            <input type="hidden" name="terminal_id_from" value="" id="terminal-id-from">
                            <input type="hidden" name="terminal_id_to" value="" id="terminal-id-to">
                            <input type="hidden" name="google_id_from" value="<?php echo (isset($order['from']['type']) && $order['from']['type'] == 'courier') ? $order['from']['google_id'] : null; ?>" id="google-id-from">
                            <input type="hidden" name="google_id_to" value="<?php echo (isset($order['to']['type']) && $order['to']['type'] == 'courier') ? $order['to']['google_id'] : null; ?>" id="google-id-to">
                        </form>
                    </div>
                </div>
                <div class="order_controll_btn">
                    <div onclick="add_parcel_item()">+ <?php pt('Add a new parcel'); ?></div>
                </div>
                <!--  -->
            </div>
            <div class="column w33 right">
                <div class="wrap">
                    <div class="ship_order">
                        <?php echo (isset($order['service']) && !empty($order['service'])) ? $this->Element('Cargobus.order-service') : $this->Element('Cargobus.order-no-service'); ?>
                    </div>

                    <div class="navigation">
                        <button class='main_btn form-order-submit'><?php pt('Continue'); ?></button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end order_block -->


</div>
