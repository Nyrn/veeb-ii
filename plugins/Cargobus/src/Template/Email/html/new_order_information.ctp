<strong>Shipment nr.</strong>
<table>
	<tr>	
		<td>Number</td>
		<td><?php echo $orderId; ?></td>
	</tr>
</table><br/><br/>

<strong>Parcels</strong>
<table>
	<tr>	
		<td>Weight</td>
		<td><?php echo $weight; ?></td>
	</tr>

	<?php foreach ($order['parcels'] as $parcel): ?>
		<tr>
			<td>Size</td>
			<td><?php echo $parcel['length']; ?>×<?php echo $parcel['width']; ?>×<?php echo $parcel['height']; ?> cm </td>
		</tr>
    <?php endforeach; ?>
</table><br/><br/>

<strong>Pickup</strong>
<table>
	<tr>	
		<td>Date</td>
		<td><?php echo date('d.m.Y', strtotime($order['pickup']['date'])); ?></td>
	</tr>
</table><br/><br/>

<strong>From</strong>
<table>
	<tr>	
		<td>Type</td>
		<td><?php echo ucfirst(strtolower($infoFrom['deliveryMethod'])); ?></td>
	</tr>

	<tr>
		<td>Address</td>
		<td><?php echo (strtolower($infoFrom['deliveryMethod']) == 'courier') ? $infoFrom['googleAddress']['formattedAddress'] : implode(', ', [$infoFrom['terminal']['address'], $infoFrom['terminal']['city']]); ?></td>
	</tr>
</table><br/><br/>

<strong>Sender</strong>
<table>
	<tr>	
		<td>Name</td>
		<td><?php echo $infoFrom['name']; ?></td>
	</tr>

	<tr>	
		<td>Email</td>
		<td><?php echo $infoFrom['email']; ?></td>
	</tr>

	<tr>	
		<td>Phone</td>
		<td><?php echo $infoFrom['phone']; ?></td>
	</tr>
</table><br/><br/>

<strong>To</strong>
<table>
	<tr>	
		<td>Type</td>
		<td><?php echo ucfirst(strtolower($infoTo['deliveryMethod'])); ?></td>
	</tr>

	<tr>
		<td>Address</td>
		<td><?php echo (strtolower($infoTo['deliveryMethod']) == 'courier') ? $infoTo['googleAddress']['formattedAddress'] : implode(', ', [$infoTo['terminal']['address'], $infoTo['terminal']['city']]); ?></td>
	</tr>
</table><br/><br/>

<strong>Recipient</strong>
<table>
	<tr>	
		<td>Name</td>
		<td><?php echo $infoTo['name']; ?></td>
	</tr>

	<tr>	
		<td>Email</td>
		<td><?php echo $infoTo['email']; ?></td>
	</tr>

	<tr>	
		<td>Phone</td>
		<td><?php echo $infoTo['phone']; ?></td>
	</tr>
</table><br/><br/>

<strong>Services</strong>
<table>
	<tr>
		<td><?php echo $infoServices['complex']['serviceName']; ?></td>
		<td><?php echo $infoServices['complex']['priceWithoutVat']; ?> EUR (without VAT)</td>
	</tr>

	<?php unset($infoServices['complex']); ?>

	<?php if (!empty($infoServices)): ?>
		<table class='simple_table calc_cost final_table'>
            <?php foreach ($infoServices as $sType => $service): ?>
                <tr>
                    <td><?php pt(ucfirst($sType)); ?></td>
                    <td><?php echo $service['priceWithoutVat']; ?> EUR (without VAT)</td>
                </tr>
            <?php endforeach; ?>
        </table>
	<?php endif; ?>
</table><br/><br/>

<?php if (isset($info['additionalInfo']) && !empty($info['additionalInfo'])): ?>
	<strong>Notes</strong>
	<table>
		<tr>	
			<td><?php echo $info['additionalInfo']; ?></td>
		</tr>
	</table>
<?php endif; ?>