<?php
namespace Translation\Model\Table; 

use Cake\ORM\Query;
use Cake\Validation\Validator;
use App\Model\Table\AppTable;

class TranslationTable extends AppTable
{
	public $mirrorCommonFields = []; // Mirror items common fields

	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('translations');
    }
}
?>