<?php
use Cake\Routing\Router;

Router::plugin('Translation', function ($routes) {
    $routes->fallbacks('InflectedRoute');
});