
var isIOS = navigator.platform.match(/(iPhone|iPod|iPad)/i) ? true : false;
var isAndroid = /(android)/i.test(navigator.userAgent);
// device.js
(function () { var n, e, o, t, i, r, d, a, c, l; e = window.device, n = {}, window.device = n, t = window.document.documentElement, l = window.navigator.userAgent.toLowerCase(), n.ios = function () { return n.iphone() || n.ipod() || n.ipad() }, n.iphone = function () { return !n.windows() && i("iphone") }, n.ipod = function () { return i("ipod") }, n.ipad = function () { return i("ipad") }, n.android = function () { return !n.windows() && i("android") }, n.androidPhone = function () { return n.android() && i("mobile") }, n.androidTablet = function () { return n.android() && !i("mobile") }, n.blackberry = function () { return i("blackberry") || i("bb10") || i("rim") }, n.blackberryPhone = function () { return n.blackberry() && !i("tablet") }, n.blackberryTablet = function () { return n.blackberry() && i("tablet") }, n.windows = function () { return i("windows") }, n.windowsPhone = function () { return n.windows() && i("phone") }, n.windowsTablet = function () { return n.windows() && i("touch") && !n.windowsPhone() }, n.fxos = function () { return (i("(mobile;") || i("(tablet;")) && i("; rv:") }, n.fxosPhone = function () { return n.fxos() && i("mobile") }, n.fxosTablet = function () { return n.fxos() && i("tablet") }, n.meego = function () { return i("meego") }, n.cordova = function () { return window.cordova && "file:" === location.protocol }, n.nodeWebkit = function () { return "object" == typeof window.process }, n.mobile = function () { return n.androidPhone() || n.iphone() || n.ipod() || n.windowsPhone() || n.blackberryPhone() || n.fxosPhone() || n.meego() }, n.tablet = function () { return n.ipad() || n.androidTablet() || n.blackberryTablet() || n.windowsTablet() || n.fxosTablet() }, n.desktop = function () { return !n.tablet() && !n.mobile() }, n.television = function () { var n; for (television = ["googletv", "viera", "smarttv", "internet.tv", "netcast", "nettv", "appletv", "boxee", "kylo", "roku", "dlnadoc", "roku", "pov_tv", "hbbtv", "ce-html"], n = 0; n < television.length;) { if (i(television[n])) return !0; n++ } return !1 }, n.portrait = function () { return 1 < window.innerHeight / window.innerWidth }, n.landscape = function () { return window.innerHeight / window.innerWidth < 1 }, n.noConflict = function () { return window.device = e, this }, i = function (n) { return -1 !== l.indexOf(n) }, d = function (n) { var e; return e = new RegExp(n, "i"), t.className.match(e) }, o = function (n) { var e = null; d(n) || (e = t.className.replace(/^\s+|\s+$/g, ""), t.className = e + " " + n) }, c = function (n) { d(n) && (t.className = t.className.replace(" " + n, "")) }, n.ios() ? n.ipad() ? o("ios ipad tablet") : n.iphone() ? o("ios iphone mobile") : n.ipod() && o("ios ipod mobile") : n.android() ? n.androidTablet() ? o("android tablet") : o("android mobile") : n.blackberry() ? n.blackberryTablet() ? o("blackberry tablet") : o("blackberry mobile") : n.windows() ? n.windowsTablet() ? o("windows tablet") : n.windowsPhone() ? o("windows mobile") : o("desktop") : n.fxos() ? n.fxosTablet() ? o("fxos tablet") : o("fxos mobile") : n.meego() ? o("meego mobile") : n.nodeWebkit() ? o("node-webkit") : n.television() ? o("television") : n.desktop() && o("desktop"), n.cordova() && o("cordova"), r = function () { n.landscape() ? (c("portrait"), o("landscape")) : (c("landscape"), o("portrait")) }, a = Object.prototype.hasOwnProperty.call(window, "onorientationchange") ? "orientationchange" : "resize", window.addEventListener ? window.addEventListener(a, r, !1) : window.attachEvent ? window.attachEvent(a, r) : window[a] = r, r(), "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function () { return n }) : "undefined" != typeof module && module.exports ? module.exports = n : window.device = n }).call(this);
// 

var load_svg = "\n<svg version=\"1.1\" class='load_svg' width='100' height='100' id=\"L9\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n  viewBox=\"0 0 100 100\" enable-background=\"new 0 0 0 0\" xml:space=\"preserve\">\n    <rect x=\"20\" y=\"50\" width=\"4\" height=\"10\" fill=\"#df0000\">\n      <animateTransform attributeType=\"xml\"\n        attributeName=\"transform\" type=\"translate\"\n        values=\"0 0; 0 20; 0 0\"\n        begin=\"0\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n    </rect>\n    <rect x=\"30\" y=\"50\" width=\"4\" height=\"10\" fill=\"#df0000\">\n      <animateTransform attributeType=\"xml\"\n        attributeName=\"transform\" type=\"translate\"\n        values=\"0 0; 0 20; 0 0\"\n        begin=\"0.2s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n    </rect>\n    <rect x=\"40\" y=\"50\" width=\"4\" height=\"10\" fill=\"#df0000\">\n      <animateTransform attributeType=\"xml\"\n        attributeName=\"transform\" type=\"translate\"\n        values=\"0 0; 0 20; 0 0\"\n        begin=\"0.4s\" dur=\"0.6s\" repeatCount=\"indefinite\" />\n    </rect>\n</svg>\n";

function simple_rand(min, max) {
  var rand = min - 0.5 + Math.random() * (max - min + 1);
  rand = Math.round(rand);
  return rand;
}

function randomInteger(min, max) {
  var not_zero = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  var rand = simple_rand(min, max);
  if (rand == 0) //console.log("rand = 0")
    if (not_zero) {
      if (rand == 0) {
        // console.log("rand == 0")
        do {
          rand = simple_rand(min, max); // console.log("do " + rand)
        } while (rand == 0);
      }
    }
  return rand;
}

;

function move_to_tablet() {
  // $(".header_mobile_block .wrap").append($("nav, header .lang_block, header .account_block"))
  $(".header_mobile_block .wrap .mobile_menu_wrap").append($("nav")); // $(".header_mobile_block .wrap .top_mobile_block").prepend($("header .account_block"));
  // if ($(".top_block_main_page").length){
  //     $(".top_block_main_page .right").append($("#video_html"));
  // }
}

;

function move_to_desktop() {
  $("header .main_logo").after($("nav")); // $("header .right").append($("header .account_block"));
}

;

function moveVideo() {
  var tablet = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  if (!$(".top_block_main_page").length) return false;

  if (tablet) {
    $(".top_block_main_page .right").append($("#video_html"));
  } else {
    $(".top_block_main_page").prepend($("#video_html"));
  }
}

function pMethod() {
  var data = $(".payment_method .checkbox input:checked").data('method');
  $(".payment_method_form[data-method='" + data + "']").show();
} // избавляем страницу от скрола


function hold_scroll_page(fix) {
  var body_scrollTop = Math.max.apply(Math, [$(window).scrollTop(), $("body").scrollTop(), $("html").scrollTop()]);

  if (fix == true) {
    $("body, html").addClass("hold").css({
      "overflow-y": "hidden",
      height: "100vh"
    });
    $("html").css({
      "overflow-y": "scroll",
      height: "100vh"
    });
    $("body").attr("hold_scroll_page", body_scrollTop);
  } else {
    body_scrollTop = $("body").attr("hold_scroll_page");
    $("body, html").removeClass("hold").css({
      overflow: "",
      height: ""
    });
  }

  $("body, html").scrollTop(body_scrollTop);
}

function hide_mob_menu() {
  var time = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 250;
  hold_scroll_page(false);
  $("header").removeClass("show");
  $(".menu-btn").removeClass("active");
  $(".header_mobile_block").stop(true).slideUp(time);
  setTimeout(function () {
    menu_btn_access = true;
  }, time);
}

function show_mob_menu() {
  var time = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 250;
  hold_scroll_page(true);
  $("header").addClass("show");
  $(".header_mobile_block").stop(true).slideDown(time);
  set_size_mob_nav();
  setTimeout(function () {
    menu_btn_access = true;
  }, time);
} // var desktop, tablet;
// function check_resize() {
//     desktop = false;
//     tablet = false;
//     if ($(window).innerWidth() > 991) {
//         desktop = true;
//     } else {
//         tablet = true;
//         move_to_tablet();
//     }
//     var timer;
//     var window_width = $(window).innerWidth();
//     $(window).resize(function () {
//         window_width = $(window).innerWidth();
//         clearTimeout(timer);
//         timer = setTimeout(function () {
//             if (window_width < 992 && desktop && !tablet) {
//                 desktop = false;
//                 tablet = true;
//                 move_to_tablet();
//             }
//             if (window_width > 991 && !desktop && tablet) {
//                 desktop = true;
//                 tablet = false;
//                 move_to_desktop();
//             }
//         }, 250);
//     });
// };
// check_resize


function get_responsive(responsive) {
  var window_width = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : $(window).innerWidth();
  var prev = false;
  var i;
  responsiveArr = responsive.slice();
  responsiveArr.push(99999999);
  $.each(responsiveArr, function (index, element) {
    if (prev !== false && window_width <= responsiveArr[index] && window_width > prev) {
      i = index - 1;
      return false;
    } else {
      prev = element;
    }
  });
//   console.log('responsive = ' + responsiveArr);
//   console.log(responsive[i]);
  return responsiveArr[i];
}

;
var responsiveNow;
var responsiveSteps = [0, 585, 991];

function check_resize() {
  responsiveNow = get_responsive(responsiveSteps);
  check_resizeDo(responsiveNow);
  $(window).resize(function () {
    if (responsiveNow != get_responsive(responsiveSteps)) {
      responsiveNow = get_responsive(responsiveSteps);
      check_resizeDo(responsiveNow);
    }
  });
}

function check_resizeDo(responsiveNow) {
  switch (responsiveNow) {
    case 0:
      move_to_tablet();
      moveVideo(true);
      break;

    case 585:
      move_to_tablet();
      moveVideo(false);
      break;

    case 991:
      move_to_desktop();
      break;

    default:
      break;
  }
} // check_resize
// 


var terminals = [];
$("#search_block_interactive .results ul li").each(function (i) {
  var title = $(this).text().toLowerCase();
  $(this).attr("data-index", i);
  terminals.push(title);
});
var positiveArr = [];
var terminals_empty = $(".terminals_empty");

function find_text(text) {
  var grid = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  if (text == undefined) return false;

  if (text === '') {
    if (grid) grid_accordions();
    terminals_empty.hide();
    return false;
  }

  positiveArr = [];
  text = text.toLowerCase();
  terminals.forEach(function (item, i, arr) {
    if (item.indexOf(text) >= 0) {
      positiveArr.push(i);
    }
  });
  $("#search_block_interactive .results ul li").hide();
  positiveArr.forEach(function (item, i, arr) {
    $("#search_block_interactive .results ul li").eq(item).show();
  });

  if (positiveArr.length === 0) {
    terminals_empty.show();
  } else {
    terminals_empty.hide();
  }
} // popup


var hide_popup_timer;
$(".popup_block .content, .popup_block .personal-area-table").wrap("<div class='content_wrap_inner'></div>");
$(".popup_block .content, .popup_block .personal-area-table").wrap("<div class='content_wrap'></div>");

function show_popup(target_block, event, $this) {
  var set_time = 0;

  if ($(".popup_block:visible").length) {
    $(".popup_block:visible").css({
      animation: "popup_out 0.5s both"
    }); // если есть видимая модалка то даем ей плавно скрыться

    set_time = 500;
    hold_scroll_page(true);
  }

  setTimeout(function () {
    hold_scroll_page(true);
    $(".popup_block").removeAttr("style");

    if ($($this).is("a")) {
      event.preventDefault(); // если такнули на ссылку то убираем стандартный обработчик клика
    }

    clearTimeout(hide_popup_timer);
    var block = $("div.popup_block").filter("." + target_block);
    block.css("height", "");
    $(".popup_bg:hidden").fadeIn(1000);
    var scroll_top = $(window).scrollTop();
    block.css({
      position: "fixed",
      top: 35,
      bottom: 35,
      "max-height": "calc(100vh - 70px)"
    });
    block.stop(true).show().css({
      display: "block",
      animation: "popup_in 1s both"
    });
    $(".popup_block form").trigger("reset");
    popup_block_scroll_fix();
  }, set_time);
}

function popup_block_scroll_fix() {
  var block = $(".popup_block:visible");
  block.find(".content").css("margin-bottom", "");
  block.find(".content_wrap_inner").getNiceScroll().remove();
  block.find(".content_wrap_inner").removeAttr("style");
  popup_height = block.find(".content_wrap").innerHeight() + parseInt($(".popup_block").css("padding-bottom"));

  if (popup_height < $(window).innerHeight()) {
    block.find(".content").css("margin-bottom", 20);
    block.css("padding", 0);
  } else {
    block.css("padding", "");
    block.find(".content").css("margin-bottom", 50);
    if (!device.mobile()) {
        block.find(".content_wrap_inner").niceScroll({
      cursorcolor: "#ff0f21",
      cursoropacitymin: 0.9,
      railpadding: {
        top: 30,
        right: 1,
        left: 0,
        bottom: 10
      }
    });
    }
  };
  
  popup_height = block.find(".content_wrap").innerHeight() + parseInt($(".popup_block").css("padding-bottom"));
  block.css("height", popup_height);
}

function hide_popup() {
    hold_scroll_page(false);
  if ($(window).innerWidth() <= 540) {
    var animation_text = "popup_out 1.5s both";
    $(".popup_bg").fadeOut(1000);
    var hide_popup_timer_time = 1500;
  } else {
    var animation_text = "popup_out 1s both";
    $(".popup_bg").fadeOut(500);
    var hide_popup_timer_time = 1000;
  }

  $("header").fadeIn(0);
  hold_scroll_page(false);
  $(".popup_block:visible").css({
    animation: animation_text
  });
  hide_popup_timer = setTimeout(function () {
    $(".popup_block").hide();
  }, hide_popup_timer_time);
} // show_popup("done");

$(window).load(function () {
  if (!$("body > .popup_wrap").length) {
    $("body").append($(".popup_wrap"));
  }

  $(window).resize(function () {
    if ($(".popup_block:visible").length) {
      popup_block_scroll_fix();
    }
  });
});
$(".popup_bg, .popup_block .close").click(function () {
  hide_popup();
});
$(".payment_error_btn button").click(function () {
  hide_popup();
}); // popup

$(".tracking_progress_btn .modal_open").click(function () {
  show_popup('tracking_form');
});



function header_search_show_results() {
  body_scrollTop = Math.max($("body").scrollTop(), $("html").scrollTop());
  $("body, html").css({
    "overflow-y": "hidden",
    "height": "100vh"
  });
  $("html").css({
    "overflow-y": "scroll",
    "height": "100vh"
  });
  $("body, html").scrollTop(body_scrollTop);
  $(".header_search .search_result").slideDown(250);
};

function fix_min_height_page() {
  $(".wrapper").removeAttr("style");
  var header_h = $("header").outerHeight();
  var wrapper_h = $(".wrapper").outerHeight();
  var footer_h = $("footer").outerHeight();
  $(".wrapper").css("min-height", $(window).innerHeight() - header_h - footer_h - 20);
}


var menu_btn_access = true;
$(".menu-btn").click(function () {
  if (menu_btn_access) {
    $(this).toggleClass("active");
    menu_btn_access = false;

    if ($(this).hasClass("active")) {
      show_mob_menu();
    } else {
      hide_mob_menu();
    }
  } // клик за пределами моб миню если он открыт


  $(document).on("click", function (e) {
    if (!$(e.target).closest(".menu-btn, header").length && $(".menu-btn").hasClass("active")) {
      hide_mob_menu(250);
    }

    e.stopPropagation();
  }); // клик за пределами моб миню если он открыт
}); // $("#mobile_menu .menu-btn").click(function () {
//     hide_mob_menu(250);
// });



// map -----------------------------------
var markers_map_default = [
    ['Cargobus', 59.42717975, 24.77437149],
];
var map, map_point;
var markers = [];
function initMap_prev() {
  // Маркеры
  // если в шаблоне нет маркеров то выводим поинт каргобуса
  if (typeof markers_map == "undefined") {
    markers_map = markers_map_default.slice();
  }

  initMap(markers_map);
}
function initMap(markers_map) {
    var mapOptions = {
        zoom: 17,
        disableDefaultUI: true,
        // scrollwheel: false,
        // gestureHandling: 'cooperative'
    };

    map_point = new google.maps.MarkerImage('/img/point.svg',
        new google.maps.Size(45, 90),
        new google.maps.Point(0, 0),
        new google.maps.Point(24, 29)
    );
    var mapElement = document.getElementById('google_map');
    map = new google.maps.Map(mapElement, mapOptions);

    if ($("#google_map").data("no-point") != true) {
        addMarkers(markers_map, map_point, map);
    } else if ($('#google_map').data("lat") != '' && $('#google_map').data("lng") != '') {
        var markerPosition = new google.maps.LatLng($('#google_map').data("lat"), $('#google_map').data("lng"));
        map.setCenter(markerPosition);
    } else {
        var markerPosition = new google.maps.LatLng(markers_map[0][1], markers_map[0][2]);
        map.setCenter(markerPosition);
    }
}
function addMarkers(markers_map, map_point, map) {
  var zoom = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 17;
  // Область показа маркеров
  // console.log(markers_map)
  var markersBounds = new google.maps.LatLngBounds();

  for (var i = 0; i < markers_map.length; i++) {
    var markerPosition = new google.maps.LatLng(markers_map[i][1], markers_map[i][2]); // Добавляем координаты маркера в область

    markersBounds.extend(markerPosition); // Создаём маркер

    var marker = new google.maps.Marker({
      position: markerPosition,
      map: map,
      icon: map_point,
      draggable: false,
      title: markers_map[i][0],
      labelClass: "labels",
      link: markers_map[i][3],
      label: {
        color: '#61727d',
        text: markers_map[i][0],
        fontWeight: '400',
        fontSize: '14px'
      }
    });
    markers.push(marker);
    marker.addListener('click', function () {
      if ($("#google_map").hasClass("go_to_link")) {
        window.location.href = $(this)[0].link;
      }

      ;

      if ($("#google_map").hasClass("zoom_point_click")) {
        map.setZoom(12);
        map.setCenter($(this)[0].getPosition());
      }

      ;
    });
  } // Центрируем и масштабируем карту


  if (markers_map.length > 1) {
    map.setCenter(markersBounds.getCenter(), map.fitBounds(markersBounds));
  } else {
    map.setCenter(markersBounds.getCenter());
  }

  map.setZoom(zoom);
} // Sets the map on all markers in the array.


// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}
function clearMarkers() {
    setMapOnAll(null);
}
// Shows any markers currently in the array.
function showMarkers() {
    setMapOnAll(map);
}
// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    clearMarkers();
    markers = [];
}

function init_autocomplete(element) {
    var target = $(element).data('autocomplete-target');

    var options = {
        url: function(phrase) {
            return ajax_find_address + '?phrase=' + phrase;
        },
        getValue: "name",
        list: {
            maxNumberOfElements : 15,
            match: {
                enabled: false
            },
            onClickEvent: function(e) {
                var code = $(element).getSelectedItemData().code;

                if (code != 'error') {
                    $(target).val(code);
                } else {
                    $(target).val('');
                    $(element).val('');
                }
            },
            onLoadEvent: function() {
                if ($(element).getItemData(0).code == 'error') {
                    $(target).val('');
                }
            }
        },
        requestDelay: 500,
        adjustWidth: false
    };

    $(element).easyAutocomplete(options);
}

$(document).on("click", '.terminal_list_item', function () {
  $(this).parent().find(".terminal_list_item").removeClass("active");
  $(this).addClass("active"); // 

  var lat = $(this).data("lat");
  var lng = $(this).data("lng");
  var title = $(this).data("title");
  deleteMarkers();
  addMarkers([[title, lat, lng]], map_point, map);
});
$(document).on("click", '.search_block_interactive .result_item', function () {
    let id = $(this).data("id");
    let lat = $(this).data("lat");
    let lng = $(this).data("lng");
    let title = $(this).data("title") || ' ';
    deleteMarkers();
    addMarkers([[' ', lat, lng]], map_point, map, 13);
    // close
    var ul = $("#search_block_interactive .results ul");
    ul.getNiceScroll().remove();
    ul.stop(true).slideUp(400);
    setTimeout(() => {
        $("#search_block_interactive").removeClass("active");
    }, 400);
    $(this).closest('.search_block_interactive').find("form > input").val(title)

    $.post($(this).closest('form').attr('action'), {terminalId : id}, function(response) {
        $('.terminal_list_item .place').text(response.city);
        $('.terminal_list_item .address i').text(response.address);
        $('.terminal_list_item .mail a').text(response.email);
        $('.terminal_list_item .tel a').text(response.phone);

        $('.terminal_list_item .time i').text('')
        $('.terminal_list_item .saturday i').text('')
        $('.terminal_list_item .sunday i').text('')

        $.each(response.availability.monday, function(key, value) {
            $('.terminal_list_item .monday i').append(value + "<br/>")
        });

        $.each(response.availability.saturday, function(key, value) {
            $('.terminal_list_item .saturday i').append(value + "<br/>")
        });

        $.each(response.availability.sunday, function(key, value) {
            $('.terminal_list_item .sunday i').append(value + "<br/>")
        });
    });
})


$(document).on("click", '.map_controll_btns .btn', function () {
    if ($(this).hasClass("plus")) {
        map.setZoom(++map.zoom);
    } else {
        map.setZoom(--map.zoom);
    }
});
// map -----------------------------------



// функция для отображения табов
function tabs_show(main_items, current_item, tabs_content) {
  main_items.stop(true).removeClass("active hidding"); // если такого таба нет то показываем первый таб

  if (!$(current_item).length) {
    current_item = main_items.eq(0);
    $(".tabs .main_tabs .main_tab").removeClass("active");
    $(".tabs .main_tabs .main_tab").eq(0).addClass("active");
  } else {
    $(".tabs .main_tabs .main_tab").removeAttr("style");
  }

  current_item.stop(true).show();
  var current_item_height = current_item.innerHeight();
  current_item.find('.owl-carousel').trigger('refresh.owl.carousel');
  current_item.stop(true).fadeTo(0, 0).fadeTo(250, 1);
  tabs_content.stop(true).animate({
    height: current_item_height
  }, 250, function () {
    tabs_content.css("height", "");
  });

  if ($(".managers_slider_control").length) {
    control();
  }
}


// load
$(window).load(function () {}); // end load

function show_descriprion_product(text_block, direction) {
  var max_height = text_block.css("max-height");
  var height_now = text_block.innerHeight();

  if (direction == 'close') {
    text_block.removeClass("open");
    var height_now_up = parseInt(text_block.attr("max-height"));
  } else {
    text_block.attr("max-height", max_height);
    text_block.addClass("open");
    text_block.css("max-height", "initial");
    var height_now_up = text_block.innerHeight();
  }

  ; // debugger;

  text_block.css("height", height_now);
  text_block.stop(true).animate({
    "height": height_now_up
  }, 250, function () {
    text_block.css("max-height", height_now_up);
    text_block.css({
      "height": ""
    });
  });
}

var window_width, scroll_top;
var device_size = vhCheck();

function control() {
  var actTab = $(".about_team .item:visible").find(".managers_slider");
  $(".managers_slider_control .service_info_prev, .managers_slider_control .service_info_next").removeClass("disabled");

  if (actTab.find(".owl-stage .owl-item").eq(0).hasClass("active")) {
    $(".managers_slider_control .service_info_prev").addClass("disabled");
  } else if (actTab.find(".owl-stage .owl-item").eq(-1).hasClass("active")) {
    $(".managers_slider_control .service_info_next").addClass("disabled");
  } else {
    $(".managers_slider_control .service_info_prev, .managers_slider_control .service_info_next").removeClass("disabled");
  }
}

// ready
$(document).ready(function () {
    window_width = $(window).innerWidth();
    scroll_top = $(this).scrollTop();
    set_size_mob_nav();
    $(window).resize(function () {
        window_width = $(window).innerWidth();
        device_size = vhCheck();
        if (window_width <= 991) {
            set_size_mob_nav();
        }
    });

    // ставим плагин dropdown 
    jQuery("select").each(function () {
        if($(this).hasClass("search__select")) {
            if ($(this).hasClass('chosen-terminal-select')) {
                $(this).chosen().change(function() {
                    var select = $(this);
                    var terminalId = $(this).val();
                    $("input").blur();

                    $.post(ajax_terminal, {terminalId: terminalId}, function(response) {
                        select.closest('.tabs_el').find('.terminal-operating-mode .monday').text('');
                        select.closest('.tabs_el').find('.terminal-operating-mode .saturday').text('');
                        select.closest('.tabs_el').find('.terminal-operating-mode .sunday').text('');

                        $.each(response.availability.monday, function(key, value) {
                            if (value == 'Closed') value = closedText;
                            select.closest('.tabs_el').find('.terminal-operating-mode .monday').append("<b>" + value + "</b> ")
                        });

                        $.each(response.availability.saturday, function(key, value) {
                            if (value == 'Closed') value = closedText;
                            select.closest('.tabs_el').find('.terminal-operating-mode .saturday').append("<b>" + value + "</b> ")
                        });

                        $.each(response.availability.sunday, function(key, value) {
                            if (value == 'Closed') value = closedText;
                            select.closest('.tabs_el').find('.terminal-operating-mode .sunday').append("<b>" + value + "</b> ")
                        });

                        select.closest('.tabs_el').find('.terminal-phone a').attr('href', 'tel:' + response.phone);
                        select.closest('.tabs_el').find('.terminal-phone a').html(response.phone);
                    });
                });;
            } else {
                $(this).chosen().change(function() {
                    $("input").blur();
                });;
            }
        } else {
            var label = $(this).attr("data-label");
        if (label == undefined) {
            $(this).dropdown();
        } else {
            $(this).dropdown({
                label: label
            });
        }
        }
    });
    
    // вешаем прокрутку niceScroll для dropdown
    $(".fs-dropdown-options, .chosen-results, .chosen-choices").niceScroll({
        cursorcolor: "#2F80ED",
        zindex: 888,
        touchbehavior: true,
        railpadding: {
          top: 0,
          right: 1,
          left: 0,
          bottom: 0
        }
      }); // галерея

    // tabs trigger
    // первый таб делаем активным
    $(".contacts_page .tabs .main_tab").eq(0).addClass("active");
  $(".tabs .main_tabs .main_tab").click(function () {
    if (!$(this).hasClass("active") && !$(this).hasClass("no_click")) {
      var main_tab = $(this);
      $(this).parent().find(".main_tab").removeClass("active");
      $(this).addClass("active");
      var index = $(this).data('tab-index');
      var main_items = $(this).closest('.tabs').find(".tabs_content > .item");
      var tabs_content = $(this).closest('.tabs').find(".tabs_content");
      var tabs_content_height = tabs_content.innerHeight();
      tabs_content.css('height', tabs_content_height).attr("height_now", tabs_content_height);
      var current_item = main_items.filter("[data-tab-index = '" + index + "']"); // если есть видимые блоки то стандартные действия

      if (main_items.filter(":visible").length) {
        main_items.stop(true).filter(":visible").fadeOut(250, function () {
          tabs_show(main_items, current_item, tabs_content);
        });
      } else {
        // если видимых блоков нет то фича
        tabs_show(main_items, current_item, tabs_content);
      }
    }

    ;
  }); // tabs trigger




    // google_map

    if ($("#google_map").length > 0) {
        // $("#google_map").after('<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIrHnqmBNnQLq2tyC5dzmK0a0vykv5lJY&callback=initMap_prev"></script>');
        initMap_prev();
    };

    // $("input.tel_mask").mask("+38(055)555-55-55");
    //$("input.tel_mask").mask("+99 - 555 - 555 - 55");

    if ($(".service_info_slider").length) {
        /*serviceSlider.addClass("owl-carousel").owlCarousel({
            loop: false,
            // rewind: true,
            margin: 24,
            dots: true,
            responsiveClass: true,
            items: 9,
            lazyLoad: true,
            dotsContainer: ".service_info_dots",
            // autoplay: true,
            responsive: {
                0: {
                    items: 1
                },
                540: {
                    items: 1
                },
                992: {
                    items: 2
                }
            }
        });*/
        var callback = function callback(event) {
          if (serviceSlider.find(".owl-stage .owl-item").eq(0).hasClass("active")) {
            $(".service_info_slider_control .service_info_prev").addClass("disabled");
          } else if (serviceSlider.find(".owl-stage .owl-item").eq(-1).hasClass("active")) {
            $(".service_info_slider_control .service_info_next").addClass("disabled");
          } else {
            $(".service_info_slider_control .service_info_prev, .service_info_slider_control .service_info_next").removeClass("disabled");
          }
        };

        var serviceSlider = $(".service_info_slider");
        serviceSlider.addClass("owl-carousel").owlCarousel({
          nav: false,
          loop: false,
          rewind: true,
          dots: true,
          responsiveClass: true,
          items: 2,
          lazyLoad: true,
          lazyLoadEager: true,
          autoWidth: true,
          dotsContainer: ".service_info_slider_wrap .service_info_dots",
          onInitialized: callback,
          onTranslated: callback,
          responsive: {
            0: {
              margin: 14
            },
            550: {
              margin: 20
            },
            1200: {
              margin: 24
            }
          }
        });
        $('.service_info_slider_wrap .service_info_prev').click(function () {
          serviceSlider.trigger('prev.owl.carousel');
        }); // Go to the previous item

        $('.service_info_slider_wrap .service_info_next').click(function () {
          serviceSlider.trigger('next.owl.carousel');
        });
      }

    /*if ($("#brands_footer").length) {
        var slider = $("#brands_footer");
        slider.addClass("owl-carousel").owlCarousel({
            loop: true,
            // rewind: true,
            dots: true,
            responsiveClass: true,
            items: 9,
            lazyLoad: true,
            // autoplay: true,
            responsive: {
                0: {
                    items: 3
                },
                540: {
                    items: 5
                },
                840: {
                    items: 7
                },
                992: {
                    items: 9
                }
            }
        });
    }*/
    if ($(".managers_slider").length) {
        // $(".about_team .tabs_content .item").each(function (i) {
        // $(".managers_slider_dots").append("<div class='service_info_dots " + i + "'></div>");
        var slider = $(".managers_slider");
        slider.on('initialized.owl.carousel', function (event) {
          slider.fadeTo(500, 1);
        }); // function control(event) {
        //     if(slider.find(".owl-stage .owl-item").eq(0).hasClass("active")){
        //         $(".managers_slider_control .service_info_prev").addClass("disabled");
        //      } else if (slider.find(".owl-stage .owl-item").eq(-1).hasClass("active")){
        //          $(".managers_slider_control .service_info_next").addClass("disabled");
        //      } else {
        //          $(".managers_slider_control .service_info_prev, .managers_slider_control .service_info_next").removeClass("disabled");
        //      }
        //  } 

        slider.addClass("owl-carousel").owlCarousel({
          loop: false,
          // rewind: true,
          dots: true,
          responsiveClass: true,
          items: 4,
          lazyLoad: true,
          mouseDrag: false,
          touchDrag: false,
          // autoplay: true,
          dotsContainer: $(".managers_slider_control .service_info_dots"),
          onInitialized: control,
          onTranslated: control,
          responsive: {
            0: {
              items: 1,
              loop: true
            },
            607: {
              items: 2,
              loop: false
            },
            875: {
              items: 3,
              loop: false
            },
            1160: {
              items: 4,
              loop: false
            }
          }
        }); // })

        $('.about_team .service_info_prev').click(function () {
          $(".about_team .item:visible").find(".managers_slider").trigger('prev.owl.carousel'); // control();
        }); // Go to the previous item

        $('.about_team .service_info_next').click(function () {
          $(".about_team .item:visible").find(".managers_slider").trigger('next.owl.carousel'); // control();
        });
      } // открываем полность блок с обрезанным текстом, блоку ставим max-height

    // открываем полность блок с обрезанным текстом, блоку ставим max-height
    $(".account_table .mobile_btn").click(function () {
        var text_block = $(this).closest("tr");
        text_block.toggleClass("open");
        if (text_block.hasClass("open")) {
            show_descriprion_product(text_block, 'open')
        } else {
            show_descriprion_product(text_block, 'close')
        }
    })

    // custom_dropdown
    // $("footer .custom_dropdown .top").mouseenter(function () {
    //     $(this).click();
    // })
    // $(".custom_dropdown").mouseleave(function (time = 250) {
    //     $(".custom_dropdown").removeClass("open").find(".ul").slideUp(time);
    // })
    $(".terminals_data .terminals_sidebar a.show_city").click(function () {
        $(".terminals_sidebar a").removeClass("active");
        $(this).addClass("active");
        $(".terminals_map").hide();
        $(".terminals_city").show();
    })
    $(".terminals_data .terminals_sidebar a.show_map").click(function () {
        $(".terminals_sidebar a").removeClass("active");
        $(this).addClass("active");
        $(".terminals_city").hide();
        $(".terminals_map").show();

    })
    $(".custom_dropdown .top").click(function () {
        var time = 250;
        var open = false;
        var custom_dropdown = $(this).closest(".custom_dropdown");
        custom_dropdown.addClass("this_now");

        if (custom_dropdown.hasClass("open")) {
          open = true;
        } // проверяем хватит ли места снизу при открытии


        var ul = custom_dropdown.find(".ul");
        var ul_top = ul.stop(true).show().offset().top;
        ul.stop(true).hide();
        var body_height = Math.max($("body").innerHeight(), $("html").innerHeight()); // скрываем открытые

        $(".custom_dropdown.open:not(.this_now)").removeClass("open").find(".ul").stop(true).slideUp(time); // возвращаем класс open

        if (open) {
          custom_dropdown.addClass("open");
          ul.show();
        }

        custom_dropdown.toggleClass("open");

        if (custom_dropdown.hasClass("open")) {
          if (ul_top + ul.outerHeight() >= body_height) {
            ul.addClass("slide_top");
          }

          ;
          custom_dropdown.find(".ul").stop(true).slideDown(time);
        //   console.log("1");
        } else {
          custom_dropdown.find(".ul").stop(true).slideUp(time, function () {
            ul.removeClass("slide_top");
            // console.log("2");
          });
        }

        $(this).closest(".custom_dropdown").removeClass("this_now"); // клик за пределами списка номеров в хедере если он открыт

        var thise = $(this);
        $(document).on("click", function (e) {
          if (!$(e.target).closest($(".custom_dropdown")).length && $(".custom_dropdown").hasClass("open")) {
            $(".custom_dropdown").removeClass("open").find(".ul").slideUp(time); // $(document).unbind("click");
          }

          e.stopPropagation();
        }); // клик за пределами списка номеров в хедере если он открыт
      }); // custom_dropdown





    // плавный скролл навигации
  $("#nav a").click(function () {
    var id = $(this).attr("href");

    if ($(id).length) {
      event.preventDefault();
      $("body, html").animate({
        scrollTop: $(id).offset().top
      }, 1000);
      $(this).parent().addClass("active").siblings().removeClass("active");
      hide_mob_menu(250);
    }
  }); // плавный скролл навигации

    check_resize();
  $("label.with_line input, label.with_line textarea").focus(function () {
    $(this).parent().addClass("hover");
  });
  $("label.with_line input, label.with_line textarea").blur(function () {
    $(this).parent().removeClass("hover");
  }); // if ($(".terminals_block").length) {
  //     $(".terminals_block .terminal_list").niceScroll({
  //         cursorcolor: "#006ade",
  //         cursoropacitymin: 0.5,
  //         railpadding: { top: 0, right: 1, left: 0, bottom: 0 }
  //     });
  // }

    if ($("#search_block_interactive").length) {
        $("#search_block_interactive input").focus(function () {
          var _this = this;

          $(this).closest(".search_block_interactive").addClass("active");
          setTimeout(function () {
            $(_this).parent().find(".results ul").stop(true).slideDown(400, function () {
              $(this).niceScroll({
                cursorcolor: "#006ade",
                cursoropacitymin: 0.5,
                railpadding: {
                  top: 5,
                  right: 1,
                  left: 0,
                  bottom: 0
                }
              });
            });
          }, 400);
        }); //     // клик за пределами моб миню если он открыт

        $(document).on("click", function (e) {
          if (!$(e.target).closest("#search_block_interactive").length && $("#search_block_interactive").hasClass("active")) {
            var ul = $("#search_block_interactive .results ul");
            ul.getNiceScroll().remove();
            ul.stop(true).slideUp(400);
            setTimeout(function () {
              $("#search_block_interactive").removeClass("active");
            }, 400);
          }

          e.stopPropagation();
        }); //     // клик за пределами моб миню если он открыт
      } // nav_dropdown

    // nav_dropdown
    var time_nav_dropdown = 0;
  var mobile_size = 991; // > 991px

  $(".nav_dropdown").mouseenter(function () {
    if (window_width > mobile_size) {
      $(this).find(".ul_level1").stop(true).fadeIn(time_nav_dropdown);
      $("nav").addClass("hover"); // $(this).addClass("hover");
    }
  });
  $(".nav_dropdown").mouseleave(function () {
    if (window_width > mobile_size) {
      $(this).find(".ul_level1, .ul_level2").stop(true).fadeOut(time_nav_dropdown);
      $(this).find(".active").removeClass("active");
      $("nav").removeClass("hover");
    }
  });
  $(".nav_dropdown .top").mouseenter(function () {
    if (window_width > mobile_size) {
      $(this).addClass("active");
    }
  });
  $(".nav_dropdown li").mouseenter(function () {
    if (window_width > mobile_size) {
      // clear
      $(this).parent().find(".active").removeClass("active");
      $(this).parent().find(".ul_level2").stop(true).fadeOut(time_nav_dropdown);
    }
  });
  $(".nav_dropdown li.multilevel").mouseenter(function () {
    if (window_width > mobile_size) {
      $(this).addClass("active");
      $(this).find(".ul_level2").stop(true).fadeIn(time_nav_dropdown);
    }
  }); // < 991px

  $(".nav_dropdown .top").click(function () {
    if (window_width <= mobile_size) {
      $(this).toggleClass("active");

      if ($(this).hasClass("active")) {
        $(this).parent().find(".ul_level1").stop(true).slideDown(250, function () {
          set_size_mob_nav();
        });
      } else {
        $(this).parent().find(".ul_level1").stop(true).slideUp(250);
      }
    }
  });
  $(".nav_dropdown li.multilevel > a").click(function () {
    if (window_width <= mobile_size) {
      $(this).parent().toggleClass("active");

      if ($(this).parent().hasClass("active")) {
        $(this).next().stop(true).slideDown(250, function () {
          set_size_mob_nav();
        });
      } else {
        $(this).next().stop(true).slideUp(250);
      }
    }
  }); // nav_dropdown
  // range_slider     ------------
  // слайдер-ползунок для price__range
  // слайдер-ползунок для price__range

    init_range_slider();
  $(".accordion .top").click(function () {
    $(this).parent().toggleClass("open");

    if ($(this).parent().hasClass("open")) {
      $(this).next().stop(true).slideDown(250);
    } else {
      $(this).next().stop(true).slideUp(250);
    }
  }); // parallax

    $(".wrapper > .parallax").wrapAll("<div class='parallax_wrap'></div>");
  $(".parallax_wrap .parallax, .parallax").fadeTo(500, 1);
  animateParalax("parallax");
  $(".all_news").each(function () {
    var length = $(this).find(".news_item_simple").length;
    $(this).addClass("twin" + length % 3);
    $(this).addClass("twin-mob-" + length % 2);

    if ($(this).find(".load_more").length) {
      $(this).addClass("with_load_more");
    }
  });
  $("footer .column > .top").click(function () {
    if (window_width <= 540) {
      $(this).parent().toggleClass("open");

      if ($(this).parent().hasClass("open")) {
        $(this).next().stop(true).slideDown(250);
      } else {
        $(this).next().stop(true).slideUp(250);
      }
    }
  });
  $(".header_bg").click(function () {
    hide_mob_menu();
  }); //

    //
    var move_access = false;
  var scrollLeft;
  $(".tabs.swipe .main_tabs").on("mousedown touchstart", function (event) {
    move_access = true;

    if (event.type == "touchstart") {
      var touch = event.originalEvent.touches[0] || event.originalEvent.changedTouches[0];
      var offset = touch.clientX;
    } else {
      //если нажата кнопка мышки:
      var offset = event.clientX;
    }

    touchstart = offset;
    scrollLeft = $(".tabs.swipe:visible .main_tabs").scrollLeft();
  });
  $(document).on("mouseup touchend", function (event) {
    move_access = false;

    if (event.type == "touchend") {
      var touch = event.originalEvent.touches[0] || event.originalEvent.changedTouches[0];
      var offset = touch.clientX;
    } else {
      //если нажата кнопка мышки:
      var offset = event.clientX;
    }

    touchstart = offset;
  });
  $(".tabs.swipe .main_tabs").on("mousemove touchmove", function (event) {
    if (move_access) {
      if (event.type == "touchmove") {
        var touch = event.originalEvent.touches[0] || event.originalEvent.changedTouches[0];
        var offset = touch.clientX;
      } else {
        //если нажата кнопка мышки:
        var offset = event.clientX;
      }

      $(".tabs.swipe:visible .main_tabs").scrollLeft(scrollLeft + (touchstart - offset)); //отменяем "всплытие сообщений", чтобы не вызывался клик на тач-устройствах.

      event.stopPropagation();
      event.preventDefault();
    }
  });
  $(".search_icon").click(function () {
    $("header .search_block").addClass("active");
  });
  $(".search_block .close").click(function () {
    $("header .search_block").removeClass("active");
  });
  $(".calculator .exchange").click(function () {
    var from_select = $(this).closest(".row").find(".from select");
    var to_select = $(this).closest(".row").find(".to select");
    var from_code = $(this).closest(".row").find(".from .postal_code");
    var to_code = $(this).closest(".row").find(".to .postal_code");
    var codes = {
      from_code: from_code.val(),
      to_code: to_code.val(),
      from_select: from_select.html(),
      to_select: to_select.html()
    };
    to_code.val(codes.from_code);
    from_code.val(codes.to_code);
    from_select.html(codes.to_select);
    to_select.html(codes.from_select);
    from_select.dropdown("update");
    to_select.dropdown("update");
  });


   $(".tooltip").mouseenter(function () {
    var tooltip_content = $(this).find(".tooltip_content");
    tooltip_content.addClass("transition0s");
    tooltip_content.removeAttr("style");

    if (tooltip_content.offset().left + tooltip_content.innerWidth() > window_width) {
      var offset = window_width - (tooltip_content.offset().left + tooltip_content.innerWidth());
      tooltip_content.css({
        "transform": "translateX(".concat(offset - 10, "px)")
      });
    }

    setTimeout(function () {
      tooltip_content.removeClass("transition0s");
    }, 5);
  });


    $("#calculator .count .count_span").click(function () {
        var input = $(this).closest(".wrap").find("input");
        var val = parseInt(input.val());
        if (isNaN(val)) val = 1;
        if ($(this).hasClass("minus")) {
            input.val(val - 1 < 1 ? 1 : val - 1)
        } else {
            input.val(val + 1)
        }
    });

    // добавляем одну карточку в калькулятор
    if ($(".parcel_repeater").length) {
        var parcelsCount = parcel_repeater_cnt;

        for (var cnt = 1; cnt <= parcelsCount; cnt++) {
            add_parcel_item(cnt - 1);
        }
    }

    $(".invoice .switch__container input").change(function () {
        let check = $(this).prop("checked");
        check ? $(".invoice_content").stop(true).slideDown() : $(".invoice_content").stop(true).slideUp();
    })

    pMethod()
    $(".payment_method .checkbox input").change(function () {
        //let check = $(this).prop("checked");
        let data = $(this).data('method');
        $(".payment_method_form").hide();
        $(".payment_method_form[data-method='" + data + "']").show();
    })

    $(".ship_order table .checkbox input").change(function () {
        let check = $(this).prop("checked");
        // показываем \ скрываем инпут
        let input_additional_text = $("[name *= 'additional_text']");
        
        if (check) {
            $(this).parent().next().show();
            if ($(this).attr('name') == 'insurance') {
                input_additional_text.prev().addClass("required")
                input_additional_text.attr('required', 'required');
                $('.additional-information-row').css('display', 'block');
            }
        } else {
            $(this).parent().next().hide();
            if ($(this).attr('name') == 'insurance') {
                input_additional_text.prev().removeClass("required")
                input_additional_text.removeAttr('required');
                $('.additional-information-row').css('display', 'none');
            }
        };

        ajaxAdditionalService();

        // пересчитываем total price
        //total_price_ship_order();
    })
    var inputTimer = null;
    $(".ship_order table .checkbox + input").on('input', function() {
        clearTimeout(inputTimer);
        inputTimer = setTimeout(() => {
            ship_order_start_load();
            ajaxAdditionalService();
        }, 300);
    });
    // делаем липкий блок
    var order_block_right = $(".order_block  .right .wrap");
    if (order_block_right.length) {
        order_block_right.stick_in_parent({
            offset_top: $("header").innerHeight() + 24,
        });
    }
    var right_sidebar = $(".right_sidebar > .wrap");
    if (right_sidebar.length) {
        right_sidebar.stick_in_parent({
            offset_top: $("header").innerHeight() + 24,
        });
    }
    
    // меняем время и телефон терминала при смене селекта в калькуляторе
    $(".calculator .tabs_el select").on("change", function () {
        let option = $(this).find("option:selected");
        let tel = option.data('tel');
        let operating_mode = option.data('operating-mode');
        let ul = $(this).closest('.tabs_el').find('.text ul');
        ul.find('.time b').text(operating_mode);
        ul.find('.tel a').text(tel).attr('href', tel);
    });
    // запускаем autocomplete для инпутов в калькуляторе
    if ($("#autocomplete-from-address").length) {
        init_autocomplete('#autocomplete-from-address');
    }
    if ($("#autocomplete-to-address").length) {
        init_autocomplete('#autocomplete-to-address');
    }

    if ($(".thanks_block").length) {
        $(".order_block").addClass("with_thanks_block")
    }
    // end ready
});
$(".calculator .tabs_el select").eq(0).find("option:selected").val()

function ajaxAdditionalService() {
    var services = [];
    var insuranceValue = 0;

    ship_order_start_load();

    $(".ship_order .calc_cost .checkbox input").each(function () {
        if ($(this).prop('checked')) {
            services.push($(this).attr('name'));

            if ($(this).attr('name') == 'insurance') {
                insuranceValue = $(this).closest('td').find('input[type=number]').val();
            }
        }
    });

    $.post(ajax_additional_service, {'services' : services, 'insuranceValue' : insuranceValue}, function(response) {
        $(".ship_order .calc_cost .checkbox input[name=insurance]").data('dop-cost', 0)
        $(".ship_order .calc_cost .checkbox input[name=fragile]").data('dop-cost', 0)
        $(".ship_order .calc_cost .checkbox input[name=upwards]").data('dop-cost', 0)

        if (response.insurance) $(".ship_order .calc_cost .checkbox input[name=insurance]").data('dop-cost', response.insurance);
        if (response.fragile) $(".ship_order .calc_cost .checkbox input[name=fragile]").data('dop-cost', response.fragile);
        if (response.upwards) $(".ship_order .calc_cost .checkbox input[name=upwards]").data('dop-cost', response.upwards);
        if (response.vat) $(".ship_order .start_cost").data("start-vat", response.vat);

        total_price_ship_order();
        ship_order_end_load();
    });
}

let access_total_price_ship_order = true;
function total_price_ship_order() {
    if (!access_total_price_ship_order) return false;
    access_total_price_ship_order = false;
    //ship_order_start_load();
    setTimeout(() => {
        let start_cost_without_vat = 0;
        let start_cost = parseFloat($(".ship_order .start_cost").data("start-cost"));
        let start_vat = parseFloat($(".ship_order .start_cost").data("start-vat"));
        $(".ship_order .calc_cost .checkbox input").each(function () {
            let check = $(this).prop("checked");
            let dop_cost = check ? parseFloat($(this).data("dop-cost")) : 0;
            let find_input = $(this).data("find-input");
            let row = $(this).closest("tr");
            let price_span = row.find('.price');
            if (find_input && check) {
                let val = parseFloat(row.find(".checkbox + input").val());
                isNaN(val) || val == '' ? val = 0 : '';
                dop_cost = val / 10;
            }
            price_span.text(dop_cost.toString().slice(0, 5))
            //    debugger;
            start_cost += dop_cost;
        });
        start_cost_without_vat = start_cost;
        start_cost += start_vat;

        $(".ship_order .total_price_without_vat").text(start_cost_without_vat.toString().slice(0, 8));
        $(".ship_order .total_price").text(start_cost.toString().slice(0, 8));
        access_total_price_ship_order = true;
        //ship_order_end_load();
    }, 0);


    // clearTimeout(temp_timer_ship_order);
    // temp_timer_ship_order = setTimeout(ship_order_end_load, 500);

}

// переключаем все чекбоксы в таблице
$(document).on("change", ".account_table th .checkbox input, .select_date_block .checkbox input", function () {
    var all_checkbox = $(this).closest('.item').find("td .checkbox input");
    if ($(this).prop("checked")) {
        all_checkbox.prop('checked', true)
    } else {
        all_checkbox.prop('checked', false)
    }
})

$(document).on('keypress', '.calculator .size input, .only_integer', function (event) {
    var key, keyChar;
    if (!event) var event = window.event;

    if (event.keyCode) key = event.keyCode;
    else if (event.which) key = event.which;

    if (key == null || key == 0 || key == 8 || key == 13 || key == 9 || key == 46 || key == 37 || key == 39) return true;
    keyChar = String.fromCharCode(key);

    if (!/\d/.test(keyChar)) return false;
});
// $(".wrap_input_prefix").each(function () {
//     var current_val = parseInt($(this).find("input").val());
//     var prefix = $(this).data("prefix");
//     $(this).append(`<div class='up_block'><span class='val'>${current_val}</span><span class='prefix'> ${prefix}</span></div>`);
// })
$(".wrap_input_prefix input").on("input", function () {
    var val = $(this).val();
    val ? isNaN(val) : val = '';
    $(this).parent().find(".up_block .val").text(val);
})
$(".weight_calc_block input").on("input", function () {
    var val = $(this).val();
    console.log(val)
    var price__range = $(this).closest(".weight_calc_block").find(".price__range");
    var minCost = parseInt(price__range.data("mincost"));
    var maxCost = parseInt(price__range.data("maxcost"));

    const weight_calc_block = $(this).closest(".weight_calc_block");
    const bg = weight_calc_block.find('.bg');
    // const maxCost = parseInt($(this).data("maxcost"));
    // console.log(mincost)
    // console.log(maxcost)
    // if (val < minCost && val != '') val = minCost;
    if (val > maxCost) val = maxCost;
    // console.log(val)
    change_weight_calc(val, $(this))
    $(this).val(val);
    $(this).parent().find(".up_block .val").text(val);
    price__range.slider("value", val);

    bg.css({
        "clip-path": `inset(0 ${100 - val / maxCost * 100}% 0 0)`,
        "-webkit-clip-path": `inset(0 ${100 - val / maxCost * 100}% 0 0)`,
    });
    
    
    
});



function animateParalax(find_block) {
  var scroll_size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 250;
  if ($(".page404").length) scroll_size = 170;
  $("." + find_block).css({
    "transition": "0.5s cubic-bezier(0.165, 0.84, 0.44, 1)"
  });
  var controller = new ScrollMagic.Controller();
  $("body").each(function (index, elem) {
    var tween = "tween-" + index;
    tween = new TimelineMax();
    var lengthBox = $(elem).find("." + find_block).length;

    for (var i = 0; i < lengthBox; i++) {
      var speed = 1;
      var j1 = 0.2 * (i + 1);
      var k1 = 0.5 * i;

      if ($(elem).find("." + find_block).eq(i).data("direction") == "bottom") {
        var prefix = -1;
      } else if ($(elem).find("." + find_block).eq(i).data("direction") == "top") {
        var prefix = 1;
      } else {
        var prefix = randomInteger(-1, 1, true);
      }

      tween.to($(elem).find("." + find_block + ":eq(" + i + ")"), 1, {
        y: prefix * (scroll_size * speed),
        ease: Linear.easeNone
      }, "-=1");
    }

    new ScrollMagic.Scene({
      triggerElement: elem,
      duration: $(this).outerHeight(),
      triggerHook: 0.7
    }).setTween(tween).addTo(controller);
  });
}

var calc_size_content = [
    {
        from: 0,
        to: 14,
        size: 22,
        text: calcSizeContent[0]
    },
    {
        from: 15,
        to: 29,
        size: 35,
        text: calcSizeContent[1]
    },
    {
        from: 30,
        to: 49,
        size: 51,
        text: calcSizeContent[2]
    },
    {
        from: 50,
        to: 69,
        size: 77,
        text: calcSizeContent[3]
    },
    {
        from: 70,
        to: 89,
        size: 93,
        text: calcSizeContent[4]
    },
    {
        from: 90,
        to: '',
        size: 100,
        text: calcSizeContent[5]
    }
]
var visual_weight = $("#calculator .visual_weight");
function change_weight_calc(val, thise) {
  var current_index;
  var weight = thise.closest(".weight").find(".visual_weight");
  var weight_mob = thise.closest(".weight").find(".text.mobile_only");
  $.each(calc_size_content, function (index, value) {
    if (value.to == '' && val >= value.from) {
      current_index = index;
      return false;
    }

    if (val >= value.from && val <= value.to) {
      current_index = index;
      return false;
    }
  });
  weight.find(".text").html(calc_size_content[current_index].text);
  weight_mob.html(calc_size_content[current_index].text);
  weight.find(".icon .box").css({
    width: calc_size_content[current_index].size + '%',
    height: calc_size_content[current_index].size + '%'
  });
}



/*$("table.fixed").each(function () {
    var height = $(this).innerHeight();
    $(this).wrap("<div class='table_scroll_wrap'></div>");
    var table_wrap = $(this).parent();
    // if($(this).hasClass("large")){
    //     table_wrap.addClass("large");
    // }
    $(this).fixedHeaderTable({
        fixedColumn: true,
        fixedColumns: 1,
        height: height + 25,
        cloneHeadToFoot: false,
        create: function () {
            table_wrap.addClass("created");
            var h = table_wrap.find(".fht-fixed-body .fht-tbody table.fixed").innerHeight();
            table_wrap.find(".fht-table-wrapper").css({
                height: h + 50
            });
            table_wrap.find(".fht-table-wrapper .fht-fixed-column .fht-tbody").css({
                height: h + 50
            });
            table_wrap.find(".fht-tbody").css({
                height: h - table_wrap.find("th").innerHeight()
            });
        }
    });
})*/


if (!device.mobile()) {
    $(".fht-fixed-body .fht-tbody, .table-wrap").niceScroll({
        cursorcolor: "#2f80ed",
        // horizrailenabled: true,
        touchbehavior: true,
        cursorwidth: 6,
        cursoropacitymin: 1,
        cursorminheight: 50,
        // railpadding: { top: 0, right: 0, left: 0, bottom: 0 },
        // railoffset: {left: 150}
    });
}


// $('table').fixedHeaderTable({ height: 500 });


function set_size_mob_nav() {
  var max_height = device_size.windowHeight - $("nav").offset().top - $(".header_mobile_block .account_block").innerHeight() - parseInt($(".header_mobile_block .wrap").css("padding-bottom")) - parseInt($(".header_mobile_block .wrap").css("padding-top")) * 2;
  $("nav ol").css({
    'max-height': max_height
  });
} // let map_html = `<div class="map fixed"><div id='google_map'></div></div>`;


// let map_html = `<div class="map fixed"><div id='google_map'></div></div>`;

$(window).load(function () {
  if (!device.mobile() && !device.tablet() && $(".page_slide_wrap").length) {
    $(".wrapper").css("padding-top", 0);
    $("body").append('<link rel="stylesheet" type="text/css" href="/css/jquery.pagepiling.min.css" />');
    $("body").addClass("init_slide");
    $('.page_slide_wrap .section').eq(-1).append($("footer"));
    $('.page_slide_wrap').pagepiling({
      sectionSelector: '.section',
      verticalCentered: true,
      onLeave: function onLeave(index, nextIndex, direction) {
        if (device.desktop()) {
          // $(".top_block_main_page").removeClass("active");
          setTimeout(function () {
            $('#myVideo').trigger('pause');
          }, 250);
        } // $('header').fadeTo(100, 0);


        $(".fs-dropdown-open").removeClass("fs-dropdown-open fs-dropdown-focus");
      },
      afterLoad: function afterLoad(anchorLink, index) {
        var section = $(".page_slide_wrap .section").eq(index - 1); // section.append($('header').fadeTo(250, 1));

        if (index - 1 == 0) {
          // $('header').removeClass("with_bg");
          $("#pp-nav span").css("border-color", '#ffffff');
          $("#pp-nav .active span").css("background", '#ffffff');

          if (device.desktop()) {
            $('#myVideo').trigger('play');
            $(".top_block_main_page").addClass("active");
          }
        } else {
          // $('header').addClass("with_bg");
          $("#pp-nav span").css("background", '');
          $("#pp-nav span").css("border-color", '#000000');
        } // if ($('.map.fixed').length) {
        //     $(".map.fixed").remove();
        //     map = undefined;
        // }
        // if (section.find(".terminals_block").length) {
        //     section.append(map_html);
        //     initMap_prev();
        // }

      },
      afterRender: function afterRender(anchorLink, index) {
        // $(window).resize();
        $("#pp-nav .active span").css("background", '#ffffff');
        $("#pp-nav span").css("border-color", '#ffffff'); // $(".page_slide_wrap .section").eq(0).append($('header').fadeTo(250, 1));
      }
    }); // $("#map_location").on('input', function () {
    //     if (!$(".map.fixed").length) {
    //         $(this).closest(".section").append(map_html);
    //         initMap_prev();
    //         $("#pp-nav span").css("border-color", '#000000');
    //         $("#pp-nav .active span").css("background", '#000000');
    //         $("header").addClass("with_bg");
    //     }
    // })
  } else {
    $("body").addClass("default_page"); // $(".page_slide_wrap .section").css('overflow', 'hidden')

    $(".page_slide_wrap").css("height", 'auto');
  }

  $("#calculator .radio input").change(function () {
    var val = $(this).val();
    var row = $(this).closest('.row').next('.tabs_el_wrap');
    row.find('.tabs_el').hide();

    if (val == 'courier') {
      row.find('.tabs_el[data-for=courier]').show();
    } else {
      row.find('.tabs_el[data-for=terminal]').show();
    }
  }); // if (device.desktop()) {
  // }

  if ($(".fullscreen-bg").length) {
    // if ($(".fullscreen-bg").length && !isAndroid) {
    var fullscreen = $(".fullscreen-bg");
    var videoSrcMP4 = fullscreen.data("video-src-mp4");
    var videoSrcWEBM = fullscreen.data("video-src-webm"); // let poster = fullscreen.data("poster");

    var MP4 = '<source src=' + videoSrcMP4 + ' type="video/mp4">';
    var WEBM = '<source src=' + videoSrcWEBM + ' type="video/webm">'; // if (videoSrcMP4) $(".fullscreen-bg video").append(MP4);
    // if (videoSrcMP4) $(".fullscreen-bg video").append(WEBM);

    var video = '<video loop muted playsinline autoplay class="fullscreen-bg__video" id="myVideo">' + MP4 + '' + WEBM + '</video>';
    fullscreen.append(video); // $(".top_block_main_page").addClass("active");

    var vid = document.getElementById("myVideo");

    vid.oncanplaythrough = function () {
      fullscreen.fadeTo(0, 1);
    };

    if (device.mobile() || device.tablet()) {
      fullscreen.fadeTo(0, 1);
    }
  }
  /*var videoSrc = $(".fullscreen-bg").data("video-src");
  jwplayer.key = 'bJdlPVXM6SqyoIzdrIdu/cbsBgxcHwdAwk4JOg==';
  var player = jwplayer('video_html').setup({
          // image: this.image,
          file: videoSrc,
      stretching: 'fill',
      autostart: true,
      mute: true,
      repeat: true,
      volume: 0,
      controls: false,
      preload: false,
  });
  player.on('ready', () => {
          $(".top_block_main_page").addClass("active");
      })*/
  //     var e = document.getElementById("myVideo");
  // e.style.opacity = 0;
  // var vid = document.getElementById("myVideo");
  // vid.oncanplaythrough = function() {
  // var e = document.getElementById('myVideo');
  // fade(e);
  // };
  // function fade(element) {
  //     var op = 0;
  //     var timer = setInterval(function() {
  //         if (op >= 1) clearInterval(timer);
  //         element.style.opacity = op;
  //         element.style.filter = 'alpha(opacity=' + op * 100 + ")";
  //         op += op * 0.1 || 0.1;
  //     }, 50);
  // }

});

function show_search_block_form() {
  $("header .search_block .buttons").fadeOut(250, function () {
    $(this).next('form').fadeIn(250, function () {
      $(this).find('input').focus();
    });
  });
}

function show_search_block_default() {
  if (window.innerWidth > 991) {
    $("header .search_block form").fadeOut(250, function () {
      $(this).prev('.buttons').fadeIn(250);
    });
  } else {
    $('header .search_block').removeClass('active');
  }
}








function delete_parcel_item(thise) {
    thise.closest(".item").remove();
    if ($(".parcel_repeater .item").length > 1) {
        $(".parcel_repeater .item").each(function (i) {
            $(this).find("h5 span").text('#' + (i + 1));
        });
        $(".parcel_repeater").addClass("more");
    } else {
        $(".parcel_repeater .item h5 span").text('');
        $(".parcel_repeater").removeClass("more");
    }
}
function add_parcel_item(index = 0) {
    var weight = (parcel_data[index]) ? parcel_data[index].weight : 30;
    var length = (parcel_data[index]) ? parcel_data[index].length : 30;
    var width = (parcel_data[index]) ? parcel_data[index].width : 30;
    var height = (parcel_data[index]) ? parcel_data[index].height : 30;
    var text = (parcel_data[index]) ? parcel_data[index].text : null;

    let parcel_repeater_item = `
    <div class="item" style='order: ${parcel_repeater_cnt}; z-index: 1;'>
    <div class="top row between align-center">
        <h5>${parcelText} <span></span></h5>
        <div class="delete" onclick="delete_parcel_item($(this))">× ${parcelDeleteText}</div>
    </div>
    <div class="row">
        <span class="required">${weightText}</span>
        <div class="row weight nowrap">
            <div class="label_wrap weight_calc_block">
                <div class="text mobile_only">
                    <span>(large box)</span>
                    <span>like washing machine</span>
                </div>
                <div class="wrap_input_prefix with_image" data-prefix="kg">
                    <svg width="23" height="13" viewBox="0 0 23 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="1" y="1" width="21" height="11" rx="1" stroke="#CFCFCF"
                            stroke-width="2"></rect>
                        <rect x="10" y="13" width="8" height="2" transform="rotate(-90 10 13)"
                            fill="#4F4F4F"></rect>
                    </svg>
                    <input type="text" name="parcels[${parcel_repeater_cnt}][weight]" class="weight only_integer">
                </div>
                <div class="price__range price__range_slider range_slider" data-minCost='0' data-maxCost='500'
                data-createCost='${weight}'></div>
            </div>
            <div class="visual_weight">
                <div class="icon">
                    <div class="box" style="width: 51%; height: 51%;"></div>
                    <svg class="desktop" width="42" height="42" viewBox="0 0 22 42"
                        fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.6609 6.90042C12.5664 6.90042 14.1111 5.35571 14.1111 3.45021C14.1111 1.54471 12.5664 0 10.6609 0C8.7554 0 7.21069 1.54471 7.21069 3.45021C7.21069 5.35571 8.7554 6.90042 10.6609 6.90042Z"
                            fill="#E6E6E6"></path>
                        <path d="M21.3048 22.1166C20.3318 11.185 15.1223 9.00453 15.1223 9.00453C15.1223 9.00453 10.0875 5.97782 4.78737 9.83264C1.35133 12.9601 0.557849 17.7344 0.0146945 22.3457C-0.260031 24.6891 3.40508 24.6607 3.67745 22.3457C4.00176 19.5937 4.46462 16.8291 5.83038 14.4746L5.82409 17.1314L5.7989 29.8986V40.0273C5.7989 41.116 6.5971 42 7.71804 42C8.8382 42 9.74739 41.116 9.74739 40.0273V25.5495H11.5406C11.5406 28.9414 11.5406 36.6833 11.5406 40.0737C11.5406 42.4353 15.2026 42.4353 15.2026 40.0737C15.2026 36.6818 15.2026 33.289 15.2026 29.8979L15.3576 17.1054C15.3592 15.8845 15.3608 14.7934 15.3608 14.0346C16.8241 16.4324 17.3075 19.2804 17.6405 22.115C17.9152 24.4317 21.5795 24.46 21.3048 22.1166Z"
                            fill="#E6E6E6"></path>
                    </svg>
                    <svg class="mobile" width="52" height="52" viewBox="0 0 52 52" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <circle cx="26" cy="26" r="25.5" stroke="#D4D4D4"></circle>
                        <path d="M25.4992 9.05049C27.7223 9.05049 29.5245 7.24833 29.5245 5.02525C29.5245 2.80216 27.7223 1 25.4992 1C23.2762 1 21.474 2.80216 21.474 5.02525C21.474 7.24833 23.2762 9.05049 25.4992 9.05049Z"
                            fill="#D4D4D4"></path>
                        <path d="M37.9173 26.8027C36.7822 14.0492 30.7044 11.5053 30.7044 11.5053C30.7044 11.5053 24.8305 7.97414 18.647 12.4714C14.6383 16.1201 13.7126 21.6901 13.0789 27.0699C12.7584 29.804 17.0344 29.7709 17.3521 27.0699C17.7305 23.8593 18.2705 20.634 19.8639 17.8871L19.8565 20.9866L19.8271 35.8818V47.6985C19.8271 48.9686 20.7584 50 22.0662 50C23.373 50 24.4337 48.9686 24.4337 47.6985V30.8077H26.5258C26.5258 34.765 26.5258 43.7973 26.5258 47.7527C26.5258 50.5078 30.7981 50.5078 30.7981 47.7527C30.7981 43.7954 30.7981 39.8372 30.7981 35.8808L30.979 20.9563C30.9808 19.5319 30.9827 18.259 30.9827 17.3737C32.6899 20.1711 33.2538 23.4938 33.6423 26.8009C33.9628 29.5036 38.2379 29.5367 37.9173 26.8027Z"
                            fill="#D4D4D4"></path>
                    </svg>
                </div>
                <div class="text">'${calcSizeContent[2]}'</div>
            </div>
        </div>
    </div>
    <div class="row size nowrap">
        <label>
            <span class="required">${lengthText}</span>
            <span class="wrap_input_prefix with_image" data-prefix="cm">
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect x="1" y="1" width="14" height="14" rx="1" stroke="#CFCFCF"
                        stroke-width="2"></rect>
                    <rect x="7" y="16" width="16" height="1.88235" transform="rotate(-90 7 16)"
                        fill="#4F4F4F"></rect>
                </svg>
                <input type="text" name="parcels[${parcel_repeater_cnt}][length]" value="${length}">
            </span>
        </label>
        <label>
            <span class="required">${widthText}</span>
            <span class="wrap_input_prefix with_image" data-prefix="cm">
                <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect x="1" y="1" width="15" height="14" rx="1" stroke="#CFCFCF"
                        stroke-width="2"></rect>
                    <rect y="7" width="17" height="2" fill="#4F4F4F"></rect>
                </svg>
                <input type="text" name="parcels[${parcel_repeater_cnt}][width]" value="${width}">
            </span>
        </label>
        <label>
            <span class="required">${heightText}</span>
            <span class="wrap_input_prefix with_image" data-prefix="cm">
                <svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M5 1.88235H8L8 0H5H3H0L0 1.88235H3V3.76471H2V4.70588H3L3 7.52941H2V8.47059H3V11.2941H2V12.2353H3L3 14.1176H0L0 16H3H5H8V14.1176H5V12.2353H6V11.2941H5L5 8.47059H6V7.52941H5V4.70588H6V3.76471H5V1.88235Z"
                        fill="#CFCFCF"></path>
                </svg>
                <input type="text" name="parcels[${parcel_repeater_cnt}][height]" value="${height}">
            </span>
        </label>
    </div>
    </div>
    `;
    $(".parcel_repeater").append(parcel_repeater_item)
    init_range_slider();
    parcel_repeater_cnt++;
    if ($(".parcel_repeater .item").length > 1) {
        $(".parcel_repeater .item").each(function (i) {
            $(this).find("h5 span").text('#' + (i + 1));
        });
        $(".parcel_repeater").addClass("more");
    } else {
        $(".parcel_repeater").removeClass("more");
    }
    $(".ship_order table .checkbox input").change();
}

$(".form-order-submit").on('click', function (e) {
    $('.form-order').submit();
});

$(".form-order-contacts-submit").on('click', function (e) {
    $('.form-order-contacts').submit();
});

// слайдер-ползунок для price__range
function init_range_slider() {
  $(".range_slider:not(.ui-slider)").each(function () {
    var weight_calc_block = $(this).closest(".weight_calc_block");
    var createCost = parseInt($(this).data("createcost"));
    var minCost = parseInt($(this).data("mincost"));
    var maxCost = parseInt($(this).data("maxcost"));
    weight_calc_block.append("<div class='bg'></div>");
    var bg = weight_calc_block.find('.bg');
    $(this).slider({
      step: 0.01,
      value: createCost,
      min: minCost,
      max: maxCost,
      orientation: "horizontal",
      create: function create(event, ui) {
        weight_calc_block.find("input.weight").val(createCost);
        weight_calc_block.find(".up_block .val").text(createCost);
        change_weight_calc(createCost, $(this));
        bg.css({
          "clip-path": "inset(0 ".concat(100 - createCost / maxCost * 100, "% 0 0)"),
          "-webkit-clip-path": "inset(0 ".concat(100 - createCost / maxCost * 100, "% 0 0)")
        });
      },
      slide: function slide(event, ui) {
        weight_calc_block.find("input.weight").val(ui.value);
        weight_calc_block.find(".up_block .val").text(ui.value);
        change_weight_calc(Math.ceil(ui.value), $(this));
        bg.css({
          "clip-path": "inset(0 ".concat(100 - ui.value / maxCost * 100, "% 0 0)"),
          "-webkit-clip-path": "inset(0 ".concat(100 - ui.value / maxCost * 100, "% 0 0)")
        });
      }
    });
  });
}


function change_email_invoice(thise) {
  if (thise.hasClass("active")) {
    thise.closest(".invoice_content").find(".change_email").slideUp();
  } else {
    thise.closest(".invoice_content").find(".change_email").slideDown();
  }

  thise.toggleClass("active");
}

var temp_timer_ship_order;

function ship_order_start_load() {
  $(".ship_order").addClass("loading").append(load_svg);
  $(".navigation button").attr("disabled", 'disabled');
}

function ship_order_end_load() {
  $(".ship_order").removeClass("loading").find('.load_svg').remove();
  $(".navigation button").removeAttr("disabled");
}

$(document).on('change', '.account_panel_table .table_checkbox.check_all input', function () {
  if ($(this).is(':checked')) {
    $(this).closest("table").find("tbody .table_checkbox.check_line input").prop('checked', true);
  } else {
    $(this).closest("table").find("tbody .table_checkbox.check_line input").prop('checked', false);
  }
});
$(document).on('change', '.account_panel_table.editable .table_checkbox input', function () {
  var edit = $(this).closest(".account_panel_adresses").find(".adresses_panel_edit"),
      length = $(this).closest(".account_panel_table").find(".table_checkbox.check_line input:checked").length;

  if (length) {
    edit.addClass("active");
  } else {
    edit.removeClass("active");
    $(this).closest(".account_panel_table").find(".table_checkbox input").prop('checked', false);
  }
});
$(".edit_table_line").click(function () {
  $(this).closest(".account_panel_wrap").find("tbody .table_checkbox.check_line input:checked").closest("tr").addClass("active");
  $(this).closest(".adresses_panel_edit").find(".table_actions").hide();
  $(this).closest(".adresses_panel_edit").find(".table_save").show();
});
$(".delete_table_line").click(function () {
  $(this).closest(".account_panel_wrap").find("tbody .table_checkbox.check_line input:checked").closest("tr").remove();
  $(this).closest(".account_panel_wrap").find(".table_checkbox input").prop('checked', false);
});
$(".add_line_btn button").click(function () {
  add_table_item();
});

function add_table_item() {
  var parcel_repeater_item = "\n    <tr>\n    <td><label class=\"table_checkbox check_line\"><input type=\"checkbox\"><span></span></label></td>\n    <td><div class=\"table_data\"><p>Company name</p><input type=\"text\" value=\"Company name\" placeholder=\"Company name\"></div></td>\n    <td><div class=\"table_data tel\"><p>+99 - *** - *** - **</p><input class=\"tel_mask\" type=\"tel\" value=\"+99 - *** - *** - **\" placeholder=\"+99 - *** - *** - **\"></div></td>\n    <td><div class=\"table_data\"><p>Lossi plats 1a, Kesklinna linnaosa, Tallinn, 15165</p><input type=\"text\"  value=\"Lossi plats 1a, Kesklinna linnaosa, Tallinn, 15165\" placeholder=\"Adresses\"></div></td>\n    </tr>";
  $(".account_panel_table.editable tbody").append(parcel_repeater_item); // $("input.tel_mask").mask("+99 - 555 - 555 - 55");
}

$(document).on('click', '.table_save', function () {
  $(this).closest(".account_panel_wrap").find(".account_panel_table.editable tbody tr.active .table_data").each(function () {
    $(this).find('p').html($(this).find("input").val());
  });
  $(this).closest(".account_panel_wrap").find(".account_panel_table.editable tbody tr.active").removeClass("active");
  $(this).closest(".adresses_panel_edit").find(".table_actions").show();
  $(this).closest(".adresses_panel_edit").find(".table_save").hide();
});
$(document).on('click', ".settings_panel_btn button.settings_panel_edit", function () {
  $(this).hide();
  $(this).closest(".settings_panel_btn").find(".settings_panel_save").show();
  $(this).closest(".settings_panel_item").find(".settings_panel_data input").prop("readonly", false);
});
$(document).on('click', ".settings_panel_btn button.settings_panel_save", function () {
  $(this).hide();
  $(this).closest(".settings_panel_btn").find(".settings_panel_edit").show();
  $(this).closest(".settings_panel_item").find(".settings_panel_data input").prop("readonly", true);
});
$(document).on('click', "header .right .account_block:not(.logged_user)", function () {
  show_popup('registration_login');
});
$(document).on('click', ".mobile_account_block .mobile-in", function () {
  hide_mob_menu();
  show_popup('registration_login');
});
$(document).on('click', ".login_form_btn a", function () {
//   hide_popup('registration_login');
  show_popup('restore_password');
});
$(function () {
  $(".orders_panel_date input").datepicker();
});