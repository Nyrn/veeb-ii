$(document).ready(function () {
	if ($('.news-load-more-btn').length > 0) {
		var page = 1;

		$('.news-load-more-btn').on('click', function() {
			var btn = $(this);

			$.post('/lv/news/news/loadMore/' + page, function(response) {
				if ($(response).length) {
					$('.all_news .news-load-more-items').append(response);
				} else {
					btn.css('display', 'none');
				}
			});

			page++;

			return false;
		});
	}
});