

var arr_timers_form_input = [];
var arr_timers_form_textarea = [];
function create_timer(type, index, item) {
    var mass;
    (type == 'input') ? mass = arr_timers_form_input[index] : arr_timers_form_textarea[index];
    clearTimeout(mass);
    // console.log(mass);
    item.parent().addClass("error");
    item.parent().find(".text_error").fadeIn(250);
    mass = setTimeout(function () {
        item.parent().removeClass("error");
        item.parent().find(".text_error").fadeOut(250);
    }, 3000);
}
$(document).ready(function () {
    $("form input").each(function (i) {
        arr_timers_form_input.push('timer' + i);
    });

    $("form button").click(function (e) {
        e.preventDefault();
        var thise = $(this);
        var form = $(this).closest("form");
        var error = false;
        form.find(".error").removeClass("error");
        form.find("input").each(function () {
            if ($(this).val() == "" && $(this).prop('required')) {
                $(this).parent().addClass("error");
                error = true;
                var index = $("form input").index($(this));
                create_timer('input', index, $(this));
            };
            // для телефона
            if ($(this).attr("type") == "tel") {
                if ($(this).val().length < 20) {
                    error = true;
                    create_timer('input', $("form input").index($(this)), $(this));
                };
            };
            // для почты
            if ($(this).attr("type") == "email") {
                var val = $(this).val();
                if (!~val.indexOf("@") || !~val.indexOf(".") || val.length < 4) {
                    error = true;
                    create_timer('input', $("form input").index($(this)), $(this));
                };
            };
            // if ($(this).data("type") == "tel_mail") {
            //     var val = $(this).val();
            //     console.log()
            //     if (val.indexOf("@") != -1) {
            //         // почта
            //         if (!~val.indexOf("@") || !~val.indexOf(".") || val.length < 4) {
            //             $(this).parent().addClass("error");
            //             error = true;
            //             var index = $("form input").index($(this));
            //             create_timer('input', index, $(this));
            //         };
            //     } else {
            //         // телефон
            //         if (val.length < 10) {
            //             error = true;
            //             var index = $("form input").index($(this));
            //             alert(index)
            //             create_timer('input', index, $(this));
            //         }
            //     }
            // };
        });
        form.find("textarea").each(function () {
            if ($(this).val() == "") {
                error = true;
                create_timer('textarea', $("form input").index($(this)), $(this));
            };
        });
        if (!error) {
            $.ajax({
                type: "POST",
                url: "send_mail.php",
                data: form.serialize() + "&form-name=" + form.attr("data-form-name")
            }).done(function () {
                $("form").trigger("reset").find("label").removeClass("hover error");
                if (form.closest(".contact_form_main").length){
                    thanks_show_main_page();
                } else {
                    thanks_show();
                }
                    // alert("Форма отправлена")
            });
        }
        return false;
    });
});