<!doctype html>
<title>Site Maintenance</title>
<style>
  body { text-align: center; padding: 50px 0px 0px; }
  h1 { font-size: 20px; }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; max-width: 650px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>

<img src="/img/logo.svg" alt="logo" width="300px"><br/><br/>
<article>
    <h1>Gerbiamas kliente,</h1>
    <div>
        <p>Mes atnaujiname elektroninį puslapį į naują versiją. Prašome būkite kantrūs!</p>
        <p>cargobus@cargobus.lt</p>
        <p>+370 5 2336677</p>
    </div>

    <br/><br/>

    <h1>Dear customer,</h1>
    <div>
        <p>We are updating our web page to a new version. Please be pattient!</p>
        <p>cargobus@cargobus.lv</p>
        <p>+371 67225566</p>
    </div>
</article>

