<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cargobus</title>
    <link rel="stylesheet" href="/css/fonts.css">
    <link rel="stylesheet" href="/css/ie.css">
</head>

<body>
    <div class="wrapper" id='wrapper'>
        <div class="parallax_wrap" id="scene">
            <div class="parallax el1" style="width:64px; height: 67px;">
                <svg class="layer" data-depth="0.05" width="64" height="67" viewBox="0 0 64 67" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M43.9446 44.3471L0.0776608 44.0876L39.7638 0.679066L43.9446 44.3471Z" fill="white" />
                </svg>
            </div>
            <div class="parallax el2" style="width:148px; height: 109px;">
                <svg class="layer" data-depth="0.02" width="148" height="109" viewBox="0 0 148 109" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M84.8374 19.5125L125.667 108.088L0.848111 69.0952L84.8374 19.5125Z" fill="white" />
                </svg>
            </div>
            <div class="parallax el3" style="width:175px; height: 175px;">
                <svg class="layer" data-depth="0.01" width="175" height="175" viewBox="0 0 175 175" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M118.691 56.6914L112.466 174.966L0.416815 62.9172L118.691 56.6914Z" fill="white" />
                </svg>
            </div>
            <div class="parallax el4" style="width:148px; height: 109px;">
                <svg class="layer" data-depth="0.06" width="148" height="109" viewBox="0 0 148 109" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M84.8374 19.5125L125.667 108.088L0.848111 69.0952L84.8374 19.5125Z" fill="white" />
                </svg>
            </div>
            <div class="parallax el5" style="width:51px; height: 92px;">
                <svg class="layer" data-depth="0.01" width="51" height="92" viewBox="0 0 51 92" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M51 46L3.30637e-06 91.8993L7.31902e-06 0.100652L51 46Z" fill="white" />
                </svg>
            </div>
            <div class="parallax el6" style="width:270px; height: 261px;">
                <svg class="layer" data-depth="0.09" width="270" height="261" viewBox="0 0 270 261" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g opacity="0.2" filter="url(#filter0_f)">
                        <path d="M20 171C49.6667 181.974 110.4 204.538 116 207L130 175.923L20 171Z" fill="black" />
                    </g>
                    <g opacity="0.2" filter="url(#filter1_f)">
                        <path d="M99 184C107.361 191.011 124.476 205.427 126.055 207L130 187.145L99 184Z" fill="black" />
                    </g>
                    <g opacity="0.2" filter="url(#filter2_f)">
                        <path d="M79 180C92.7545 188.231 120.913 205.154 123.509 207L130 183.692L79 180Z" fill="black" />
                    </g>
                    <path d="M242.485 147.33L110.791 220.096L156.766 23.6745L242.485 147.33Z" fill="white" />
                    <defs>
                        <filter id="filter0_f" x="0" y="151" width="150" height="76" filterUnits="userSpaceOnUse"
                            color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix" />
                            <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                            <feGaussianBlur stdDeviation="10" result="effect1_foregroundBlur" />
                        </filter>
                        <filter id="filter1_f" x="79" y="164" width="71" height="63" filterUnits="userSpaceOnUse"
                            color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix" />
                            <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                            <feGaussianBlur stdDeviation="10" result="effect1_foregroundBlur" />
                        </filter>
                        <filter id="filter2_f" x="59" y="160" width="91" height="67" filterUnits="userSpaceOnUse"
                            color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix" />
                            <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                            <feGaussianBlur stdDeviation="10" result="effect1_foregroundBlur" />
                        </filter>
                    </defs>
                </svg>
            </div>
        </div>
        <!-- start stubBlock -->
        <div class="stubBlock container">
            <div class="logo">
                <a href="/">
                    <svg width="192" height="37" viewBox="0 0 192 37" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0)">
                            <path d="M4.45042 5.72534L16.1673 18.0863C16.2454 18.1692 16.2454 18.3352 16.1673 18.4181L4.45042 30.7791C4.29419 30.945 4.45042 31.1109 4.60664 31.1109H24.2129C24.291 31.1109 24.4473 31.0279 24.4473 30.862V5.55942C24.4473 5.47647 24.3691 5.31055 24.2129 5.31055H4.60664C4.37231 5.31055 4.29419 5.55942 4.45042 5.72534Z"
                                fill="#006ADE" />
                            <path d="M11.6387 18.1697L0.156225 5.97461C0.0781123 5.97461 0 5.97461 0 6.05757V30.4477C0 30.5307 0.0781123 30.5307 0.156225 30.5307L11.7168 18.3356C11.7168 18.2526 11.7168 18.2526 11.6387 18.1697Z"
                                fill="#DF0000" />
                            <path d="M122.327 8.79297C117.406 8.79297 113.422 13.0239 113.422 18.2503C113.422 23.4768 117.406 27.7077 122.327 27.7077C127.248 27.7077 131.231 23.4768 131.231 18.2503C131.309 13.0239 127.326 8.79297 122.327 8.79297ZM122.327 24.1404C119.28 24.1404 116.937 21.4857 116.937 18.2503C116.937 15.0149 119.28 12.3602 122.327 12.3602C125.373 12.3602 127.716 15.0149 127.716 18.2503C127.716 21.4857 125.451 24.1404 122.327 24.1404Z"
                                fill="black" />
                            <path d="M45.3071 12.3612C47.5724 12.3612 49.4471 13.7715 50.2282 15.8455H53.8995C52.884 11.7804 49.369 8.71094 45.229 8.71094C40.3079 8.71094 36.3242 12.9419 36.3242 18.1683C36.3242 23.3948 40.3079 27.6257 45.229 27.6257C49.369 27.6257 52.884 24.6392 53.8995 20.4912H50.2282C49.369 22.5652 47.4943 23.9755 45.3071 23.9755C42.2608 23.9755 39.9174 21.3208 39.9174 18.0854C39.9174 14.85 42.2608 12.3612 45.3071 12.3612Z"
                                fill="black" />
                            <path d="M143.499 8.79373C141.546 8.79373 139.905 9.54036 138.499 10.7018V0H134.984V27.2108H138.499V25.8005C139.905 26.9619 141.624 27.7085 143.499 27.7085C148.42 27.7085 152.403 23.4776 152.403 18.2511C152.403 13.0247 148.42 8.79373 143.499 8.79373ZM143.499 24.1413C141.39 24.1413 139.437 22.8969 138.499 20.9888V15.4305C139.437 13.6054 141.39 12.278 143.499 12.278C146.545 12.278 148.888 14.9327 148.888 18.1682C148.888 21.4036 146.545 24.1413 143.499 24.1413Z"
                                fill="black" />
                            <path d="M83.5834 9.29073H80.0684V27.2101H83.5834V15.9275C84.8332 13.8535 87.6453 12.3603 89.9886 12.3603V8.79297C87.7234 8.79297 85.38 9.87145 83.5834 11.4477V9.29073Z"
                                fill="black" />
                            <path d="M70.7731 10.701C69.3671 9.5396 67.6486 8.79297 65.7739 8.79297C60.8529 8.79297 56.8691 13.0239 56.8691 18.2503C56.8691 23.4768 60.8529 27.7077 65.7739 27.7077C67.7267 27.7077 69.3671 26.961 70.7731 25.7996V27.2099H74.2882V9.29072H70.7731V10.701ZM65.7739 24.1404C62.7276 24.1404 60.3842 21.4857 60.3842 18.2503C60.3842 15.0149 62.7276 12.3602 65.7739 12.3602C67.883 12.3602 69.8358 13.6046 70.7731 15.5127V21.0709C69.8358 22.896 67.883 24.1404 65.7739 24.1404Z"
                                fill="black" />
                            <path d="M169.193 20.7395C168.1 22.7305 165.756 24.1408 163.491 24.1408C160.054 24.1408 159.429 21.4032 159.429 18.6655V9.29102H155.914V18.6655C155.914 24.4727 158.179 27.7081 163.491 27.7081C165.6 27.7081 167.631 26.7955 169.193 25.3852V27.2103H172.708V9.29102H169.193V20.7395Z"
                                fill="black" />
                            <path d="M109.435 9.29073H105.92V10.7011C104.514 9.53961 102.795 8.79297 100.92 8.79297C95.9993 8.79297 92.0156 13.0239 92.0156 18.2504C92.0156 23.4769 95.9993 27.7079 100.92 27.7079C102.873 27.7079 104.514 26.9613 105.92 25.7998V28.2057C105.92 31.4411 103.811 33.3492 100.764 33.3492C98.3427 33.3492 96.3118 32.1048 95.6088 30.0308H91.9375C92.7186 34.2617 96.3899 36.9994 100.686 36.9994C105.607 36.9994 109.357 33.4321 109.357 28.2057V9.29073H109.435ZM100.92 24.1406C97.874 24.1406 95.5307 21.4859 95.5307 18.2504C95.5307 15.015 97.874 12.3603 100.92 12.3603C103.029 12.3603 104.982 13.6047 105.92 15.5127V21.0711C104.982 22.8962 103.029 24.1406 100.92 24.1406Z"
                                fill="black" />
                            <path d="M180.677 14.5172C180.677 13.4387 181.692 12.3602 183.801 12.3602C186.379 12.3602 187.785 13.0239 188.175 14.5172H191.768C191.3 9.9544 187.16 8.79297 184.192 8.79297C179.349 8.79297 177.162 11.7795 177.162 14.5172C177.162 21.9835 188.566 18.4992 188.566 21.9835C188.566 22.979 188.019 24.1404 184.504 24.1404C182.395 24.1404 180.989 23.2279 180.443 22.2324H176.693C177.24 25.1359 180.208 27.7077 184.426 27.7077C189.503 27.7077 192.003 24.8871 192.003 21.9835C192.003 14.5172 180.677 18.2503 180.677 14.5172Z"
                                fill="black" />
                        </g>
                        <defs>
                            <clipPath id="clip0">
                                <rect width="192" height="37" fill="white" />
                            </clipPath>
                        </defs>
                    </svg>
                </a>
            </div>
            <h1>Your browser is out of date</h1>
            <p>Please install a modern browser</p>
            <div class="browsers">
                <a href="https://www.opera.com" target="_blank" class="opera">
                    <img src="/img/browsers/opera.png" alt="logo">
                </a>
                <a href="https://support.apple.com/downloads/safari" target="_blank" class="safari">
                    <img src="/img/browsers/safari.png" alt="logo">
                </a>
                <a href="https://www.google.com/chrome/" target="_blank" class="chrome">
                    <img src="/img/browsers/chrome.png" alt="logo">
                </a>
                <a href="https://www.mozilla.org/" target="_blank" class="firefox">
                    <img src="/img/browsers/firefox-logo.png" alt="logo">
                </a>
            </div>
        </div>
        <!-- end stubBlock -->
    </div>
    <script src="/js/jquery-2.2.4.min.js"></script>
    <script src="/js/parallax.min.js"></script>
    <script>
        $(document).ready(function () {
            var scene = document.getElementById('scene');
            var parallaxInstance = new Parallax(scene);
            $(".parallax").fadeTo(250, 1)
        });
    </script>
</body>

</html>