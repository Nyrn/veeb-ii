<!doctype html>
<title>Site Maintenance</title>
<style>
  body { text-align: center; padding: 50px 0px 0px; }
  h1 { font-size: 20px; }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; max-width: 650px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>

<img src="/img/logo.svg" alt="logo" width="300px"><br/><br/>
<article>
    <h1>Hea klient,</h1>
    <div>
        <p>Meie kodulehekülg on uuendamisel. Loodame mõistvale suhtumisele!</p>
        <p>info@cargobus.ee</p>
        <p>17799</p>
    </div>

    <br/><br/>

    <h1>Dear customer,</h1>
    <div>
        <p>We are updating our web page to a new version. Please be pattient!</p>
        <p>info@cargobus.ee</p>
        <p>17799</p>
    </div>
</article>

