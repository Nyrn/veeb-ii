Do not delete this folder. Images are used in webmail signatures. Contact IT support if you have questions.
Ära selle kausta ära kustuta. Pildid on kasutuses webmailis. Kui on küsimusi, kontakteeru IT toega.


///

Asenda cblogo.png fail uue pildiga nii, et nimi jääb samaks. Siis muutub automaatselt 
OWA-s signatuuri pilt. Arvesta ainult brauseri cache-ga.