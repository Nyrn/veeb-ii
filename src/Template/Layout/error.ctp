<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?php echo $this->Html->css('bootstrap.min.css'); ?>
    <?= $this->Html->css('error.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <div id="wrapper">
        <div id="content">
            <div class="container-fluid">
                <a href="/" class="logo"><img src="/img/logo.svg"></a>

                <?php echo $this->fetch('content') ?>
            </div>
        </div>
    </div>
</body>
</html>
