<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,  user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php if (HOSTNAME == 'www.cargobus.lv'): ?><meta name="verify-paysera" content="7c9a03a026e41516b04eb22d6f2ef6ac"><?php endif; ?>
    <?php if (HOSTNAME == 'www.cargobus.lt'): ?><meta name="verify-paysera" content="5e58cbb534aad1cae15900eec6c0fb89"><?php endif; ?>
    <?php if (HOSTNAME == 'www.cargobus.ee'): ?><meta name="verify-paysera" content="1487d3595b4ac21a5a246ea7fa191c37"><?php endif; ?>

    <?php if (HOSTNAME == 'www.cargobus.lv'): ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5HK3BV2');</script>
    <!-- End Google Tag Manager -->
    <?php endif; ?>

    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/site.webmanifest">
    <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <?php if (isset($useNoindex) && $useNoindex == true): ?>
        <meta name="robots" content="noindex">
    <?php endif;?>

    <title><?php echo (isset($seo['title'])) ? $seo['title'] : null; ?></title>

    <meta name="description" content="<?php echo (isset($seo['description'])) ? $seo['description'] : null; ?>">
    <meta name="keywords" content="<?php echo (isset($seo['keywords'])) ? $seo['keywords'] : null; ?>">

    <?php if (isset($useAutocomplete) && $useAutocomplete == true): ?>
        <link rel="stylesheet" href="/css/easy-autocomplete.min.css">
        <link rel="stylesheet" href="/css/easy-autocomplete.themes.min.css">
    <?php endif; ?>

    <?php if (isset($useChosen) && $useChosen == true): ?>
        <link rel="stylesheet" href="/css/chosen.min.css">
    <?php endif; ?>
    
    <link rel="stylesheet" href="/css/dropdown.css">
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link rel="stylesheet" href="/css/fonts.css">
    <link rel="stylesheet" href="/css/style.css?v=<?php echo time(); ?>">
    <link rel="stylesheet" href="/css/media.css?v=<?php echo time(); ?>">
    <link rel="stylesheet" href="/css/owl-carousel/owl.carousel.css">

    <link rel="stylesheet" href="/css/custom.css?v=<?php echo time(); ?>">

    <script src="/js/jquery-2.2.4.min.js" deffer></script>

<?php if (isset($jsVars)): ?>
    <script type="text/javascript">
    <?php foreach ($jsVars as $key => $val): ?>
        var <?php echo $key; ?> = '<?php echo $val; ?>';
    <?php endforeach; ?>

        var calcSizeContent = [];

        calcSizeContent.push('<?php pt('Fits in the palm of your hand'); ?>');
        calcSizeContent.push('<?php pt('(small box) %s like an iron', ['<br>']); ?>');
        calcSizeContent.push('<?php pt('(middle box) %s like a microwave', ['<br>']); ?>');
        calcSizeContent.push('<?php pt('(large box) %s like washing machine', ['<br>']); ?>');
        calcSizeContent.push('<?php pt('(very big box) %s like a fridge', ['<br>']); ?>');
        calcSizeContent.push('<?php pt('in the back of the truck will not fit'); ?>');

        var parcelText = '<?php pt('Parcel'); ?>';
        var parcelDeleteText = '<?php pt('Delete parcel'); ?>';
        var weightText = '<?php pt('Weight'); ?>';
        var lengthText = '<?php pt('Length'); ?>';
        var widthText = '<?php pt('Width'); ?>';
        var heightText = '<?php pt('Height'); ?>';
        var closedText = '<?php pt('Closed'); ?>';
    </script>
<?php endif; ?>

</head>

<script>
function detectIE() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }
  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf('rv:');
      return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }
//   var edge = ua.indexOf('Edge/');
//   if (edge > 0) {
//      // Edge (IE 12+) => return version number
//      return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
//   }
  // other browser
  return false;
}
if (detectIE()) window.location.href = '/ie.php';
</script>

<body>
<?php if (HOSTNAME == 'www.cargobus.lv'): ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HK3BV2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php endif; ?>

<header class='with_bg'>
    <div class="container">
        <div class="row margin0 nowrap top_row">
            <div class="menu-btn">
                <svg width="21" height="15" viewBox="0 0 21 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M1.40002 0C0.847741 0 0.400024 0.479679 0.400024 1.07143C0.400024 1.66314 0.847741 2.14286 1.40002 2.14286H19.4C19.9523 2.14286 20.4 1.66314 20.4 1.07143C20.4 0.479679 19.9523 0 19.4 0H1.40002ZM1.40002 6.42857C0.847741 6.42857 0.400024 6.90825 0.400024 7.5C0.400024 8.09171 0.847741 8.57143 1.40002 8.57143H19.4C19.9523 8.57143 20.4 8.09171 20.4 7.5C20.4 6.90825 19.9523 6.42857 19.4 6.42857H1.40002ZM1.40002 12.8571C0.847741 12.8571 0.400024 13.3368 0.400024 13.9286C0.400024 14.5203 0.847741 15 1.40002 15H19.4C19.9523 15 20.4 14.5203 20.4 13.9286C20.4 13.3368 19.9523 12.8571 19.4 12.8571H1.40002Z"
                        fill="black" />
                </svg>
            </div>
            <div class="left">
                <div class="main_logo">
                    <a href="/<?php echo (isset($cmsRequest::$urlParts['first'])) ? $cmsRequest::$urlParts['first'] : null; ?>">
                        <img src="/img/logo.svg" alt="logo">
                    </a>
                </div>
                <nav>
                    <ol>
                        <?php foreach ($pages as $page): ?>
                            <li>
                                <?php if ($page->children): ?>
                                    <div class="nav_dropdown">
                                        <div class="top">
                                            <a href="<?php echo ($page->module_id > 2) ? $page->url : 'javascript:void();'; ?>" class="<?php echo (isset($cmsRequest::$page->id) && $cmsRequest::$page->id == $page->id) ? 'active' : null; ?>"><?php echo $page->title; ?></a>
                                        </div>

                                        <ul class='ul_level1'>
                                            <?php foreach ($page->children as $child): ?>
                                                <li <?php /*class="multilevel"*/ ?>>
                                                    <a href="<?php echo $child->url; ?>">
                                                        <span class="link_item">
                                                            <?php 
                                                                if (!empty($child->svg) && isset($pagesIconList[$child->svg])) {
                                                                    echo $pagesIconList[$child->svg];
                                                                }
                                                            ?>
                                                        </span>

                                                        <span><?php echo $child->title; ?></span>
                                                    </a>

                                                    <?php /*<ul class="ul_level2">
                                                        <li><a href="javascript:void(0);">Tallinn - Narva Cargo</a></li>
                                                        <li><a href="javascript:void(0);">Western Estonia Cargo</a></li>
                                                        <li><a href="javascript:void(0);">Tallinn - Narva Cargo</a></li>
                                                        <li><a href="javascript:void(0);">Western Estonia Cargo</a></li>
                                                        <li><a href="javascript:void(0);">Tallinn - Narva Cargo</a></li>
                                                        <li><a href="javascript:void(0);">Western Estonia Cargo</a></li>
                                                    </ul>*/ ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php else: ?>
                                    <a href="<?php echo $page->url; ?>" class="<?php echo (isset($cmsRequest::$page->id) && $cmsRequest::$page->id == $page->id) ? 'active' : null; ?>"><?php echo $page->title; ?></a>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ol>
                </nav>
            </div>
            <div class="right">
                <div class="search_icon">
                    <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M6.69344 0.1C10.3346 0.1 13.2868 3.05231 13.2868 6.69344C13.2868 10.3346 10.3346 13.2867 6.69344 13.2867C3.05231 13.2867 0.1 10.3346 0.1 6.69344C0.1 3.05231 3.05231 0.1 6.69344 0.1ZM1.57341 6.69347C1.57341 9.51694 3.86998 11.8133 6.69344 11.8133C9.51682 11.8133 11.8136 9.51694 11.8136 6.69347C11.8136 3.87 9.51682 1.57344 6.69344 1.57344C3.86997 1.57344 1.57341 3.87001 1.57341 6.69347Z"
                            fill="white" stroke="#006ADE" stroke-width="0.2" />
                        <path
                            d="M16.2263 14.4269L16.2264 14.4269C16.724 14.9244 16.7237 15.7309 16.2266 16.227L16.2261 16.2276C15.7301 16.7245 14.9236 16.7249 14.4259 16.2273C14.4259 16.2273 14.4259 16.2273 14.4258 16.2273L10.671 12.4724C11.3603 11.9687 11.968 11.3613 12.4717 10.6719L16.2263 14.4269Z"
                            fill="white" stroke="#006ADE" stroke-width="0.8" />
                    </svg>
                </div>
                <!--  -->
                <?php /*
                <div class="custom_dropdown lang_box">
                    <div class="top">Eng</div>
                    <div class="ul">
                        <ul>
                            <li>
                                <a href="javascript:void(0);" class="active">Eng</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Est</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Ru</a>
                            </li>
                        </ul>
                    </div>
                </div>
                */ ?>
                <!--  -->
                <div class="search_block">
                    <div class='close'>
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15 15L1.00064 1M15 1.00064L1 15L15 1.00064Z" stroke="#A1A1A1"
                                stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </div>
                    <div class="buttons">
                        <a href="<?php echo $aliasUrls['order']; ?>" class="main_btn blue"><?php pt('Get a quote'); ?></a>
                        <a href="javascript:void(0);" class="main_btn blue" onclick='show_search_block_form()'
                            data-mob-text='<?php pt('Track'); ?>'><span><?php pt('Track shipment'); ?></span></a>
                    </div>
                    <form method="post" action="<?php echo $aliasUrls['order_tracking']; ?>">
                        <div class="wrap">
                                <input name="parcel_code" type="text" placeholder="<?php pt('Enter number package'); ?>">
                                <div>
                                    <svg onclick="show_search_block_default()" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M10.5858 12.0002L5.63604 16.95L7.05025 18.3642L12 13.4144L16.9497 18.3642L18.364 16.95L13.4142 12.0002L18.364 7.05048L16.9497 5.63627L12 10.586L7.05025 5.63627L5.63604 7.05048L10.5858 12.0002Z"
                                            fill="#CFCFCF" />
                                    </svg>
                                    <button class='main_btn blue' data-mob-text='<?php pt('Track'); ?>'><span><?php pt('Track shipment'); ?></span></button>
                                </div>
                            </div>
                    </form>
                </div>
                <?php /*<div class="account_block">
                        <!-- remove class "logged_user" if user is logged -->

                        <!-- <span class="mobile_text">Account panel</span> -->
                        <!-- <button class='main_btn no_bg'>Log out</button> -->
                        <svg width="15" height="17" viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M7.5 7C9.433 7 11 5.433 11 3.5C11 1.567 9.433 0 7.5 0C5.567 0 4 1.567 4 3.5C4 5.433 5.567 7 7.5 7Z"
                                fill="white" />
                            <path
                                d="M7.5 9.34479C3.35786 9.34479 0 12.7026 0 16.8448H15C15 12.7026 11.6421 9.34479 7.5 9.34479Z"
                                fill="white" />
                        </svg>
                        <div class="registration_dropdown">
                            <ol>
                                <li><a href="javascript:void(0);">quick order</a></li>
                                <li><a href="javascript:void(0);">Classic order</a></li>
                                <li><a href="javascript:void(0);">mass order</a></li>
                                <li><a href="javascript:void(0);">orders</a></li>
                                <li><a href="javascript:void(0);">Adresses</a></li>
                                <li><a href="javascript:void(0);">Settings</a></li>
                            </ol>
                            <div class="registration_out">
                                <a href="javascript:void(0);">log out <span>
                                        <svg width="11" height="11" viewBox="0 0 11 11" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <g clip-path="url(#clip0)">
                                                <path
                                                    d="M5.48174 10.0674H1.37042C1.11826 10.0674 0.913623 9.86277 0.913623 9.61063V1.38802C0.913623 1.13586 1.11828 0.931223 1.37042 0.931223H5.48174C5.73435 0.931223 5.93854 0.727035 5.93854 0.474422C5.93854 0.221809 5.73435 0.0175781 5.48174 0.0175781H1.37042C0.614861 0.0175781 0 0.632461 0 1.38802V9.61061C0 10.3662 0.614861 10.981 1.37042 10.981H5.48174C5.73435 10.981 5.93854 10.7768 5.93854 10.5242C5.93854 10.2716 5.73435 10.0674 5.48174 10.0674Z"
                                                    fill="black" />
                                                <path
                                                    d="M10.8637 5.17512L8.08626 2.43425C7.90719 2.257 7.61758 2.2593 7.44034 2.43882C7.26309 2.61835 7.26492 2.9075 7.44491 3.08475L9.42975 5.04355H4.1111C3.85848 5.04355 3.6543 5.24773 3.6543 5.50035C3.6543 5.75296 3.85848 5.95717 4.1111 5.95717H9.42975L7.44491 7.91596C7.26494 8.09321 7.26356 8.38237 7.44034 8.56189C7.52986 8.65234 7.64773 8.69802 7.76559 8.69802C7.88163 8.69802 7.99764 8.65417 8.08626 8.56645L10.8637 5.82558C10.9505 5.7397 10.9998 5.62274 10.9998 5.50033C10.9998 5.37795 10.9509 5.26146 10.8637 5.17512Z"
                                                    fill="black" />
                                            </g>
                                            <defs>
                                                <clipPath id="clip0">
                                                    <rect width="11" height="11" fill="white" />
                                                </clipPath>
                                            </defs>
                                        </svg></span></a>
                            </div>
                        </div>
                    </div>*/ ?>
            </div>
        </div>
        <div class="header_mobile_block">
            <div class="wrap">
                <div class="mobile_block_wrap">
                    <div class="mobile_menu_wrap"></div>
                    <div class="mobile_menu_btn">
                        <a href="<?php echo $aliasUrls['order']; ?>" class="main_btn"><?php pt('Get a quote'); ?></a>
                    </div>

                    <div class="mobile_account_block <?php /*logged_user*/ ?>">
                        <!-- remove class "logged_user" if user is logged -->
                        <div class="mobile_account_menu">
                            <ol>
                                <li><a href="javascript:void(0);">orders</a></li>
                                <li><a href="javascript:void(0);">Adresses</a></li>
                                <li><a href="javascript:void(0);">Settings</a></li>
                            </ol>
                        </div>
                        <div class="account_control">
                            <?php /*<div class="account_btn">
                                <button type="button" class="mobile-out">
                                    <span>
                                        <svg width="14" height="14" viewBox="0 0 14 14" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M6.97676 12.8142H1.74418C1.42324 12.8142 1.16279 12.5537 1.16279 12.2328V1.76764C1.16279 1.44671 1.42327 1.18626 1.74418 1.18626H6.97676C7.29827 1.18626 7.55814 0.926383 7.55814 0.604875C7.55814 0.283367 7.29827 0.0234375 6.97676 0.0234375H1.74418C0.782551 0.0234375 0 0.806016 0 1.76764V12.2328C0 13.1944 0.782551 13.9769 1.74418 13.9769H6.97676C7.29827 13.9769 7.55814 13.7171 7.55814 13.3955C7.55814 13.074 7.29827 12.8142 6.97676 12.8142Z"
                                                fill="white" />
                                            <path
                                                d="M13.8269 6.58545L10.2921 3.09707C10.0641 2.87148 9.69555 2.87441 9.46996 3.10289C9.24438 3.33138 9.2467 3.6994 9.47579 3.92498L12.0019 6.41799H5.23275C4.91124 6.41799 4.65137 6.67787 4.65137 6.99938C4.65137 7.32088 4.91124 7.58079 5.23275 7.58079H12.0019L9.47579 10.0738C9.24673 10.2994 9.24498 10.6674 9.46996 10.8959C9.58391 11.011 9.73391 11.0691 9.88392 11.0691C10.0316 11.0691 10.1793 11.0133 10.2921 10.9017L13.8269 7.4133C13.9374 7.30401 14.0002 7.15515 14.0002 6.99935C14.0002 6.8436 13.938 6.69534 13.8269 6.58545Z"
                                                fill="white" />
                                        </svg>
                                    </span>
                                    log out</button>

                                <button type="button" class="mobile-in">
                                    <span>
                                        <svg width="15" height="16" viewBox="0 0 15 16" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M7.0588 6.64901C8.87811 6.64901 10.3529 5.16058 10.3529 3.32451C10.3529 1.48843 8.87811 0 7.0588 0C5.23949 0 3.76465 1.48843 3.76465 3.32451C3.76465 5.16058 5.23949 6.64901 7.0588 6.64901Z"
                                                fill="white" />
                                            <path
                                                d="M7.05889 8.875C3.16037 8.875 0 12.0645 0 15.9989H14.1178C14.1178 12.0645 10.9574 8.875 7.05889 8.875Z"
                                                fill="white" />
                                        </svg>
                                    </span>
                                    log in</button>
                            </div>*/ ?>

                            <div class="lang_box">
                                <?php foreach ($languageList as $lng): ?>
                                    <?php if ($lng->id == $language->id): ?>
                                        <span><?php echo strtoupper($lng->slug); ?></span>
                                    <?php else: ?>
                                        <a href="/<?php echo $lng->slug; ?>/"><?php echo strtoupper($lng->slug); ?></a>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_bg">
        <!-- empty -->
    </div>
</header>

<?php echo $this->fetch('content'); ?>

<footer>
    <div class="container">
        <?php if (isset($partnerLogos) && !empty($partnerLogos)): ?>
            <div class="brands">
                <ul id='brands_footer'>
                    <?php foreach ($partnerLogos as $logo): ?>
                        <li>
                            <?php if (!empty($logo->url)): ?>
                                <a href="<?php echo $logo->url; ?>" target="_blank"><img class='owl-lazy' src='<?php echo $logo->img_path; ?>' alt=""></a>
                            <?php else: ?>
                                <img class='owl-lazy' src='<?php echo $logo->img_path; ?>' alt="">
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>

        <div class="row nowrap margin0">
            <div class="column main">
                <div class="main_logo">
                    <a href="/">
                        <img src="/img/logo.svg" alt="logo">
                    </a>
                </div>
                <div class="lang_block">
                    <span><?php pt('Language'); ?>:</span>
                    <div class=" custom_dropdown">
                        <div class="top"><span><?php echo strtoupper($language->slug); ?></span></div>

                        <ul class="ul">
                            <?php foreach ($languageList as $lng): ?>
                                <?php if ($lng->slug == 'ru') continue; ?>
                                <?php if ($lng->slug != $language->slug): ?>
                                    <li><a href="/<?php echo $lng->slug; ?>"><?php echo strtoupper($lng->slug); ?></a></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>

                    </div>
                </div>
                <div class="social">
                    <?php if (isset($socialUrls['facebook']) && !empty($socialUrls['facebook'])): ?>
                        <a href="<?php echo $socialUrls['facebook']; ?>" target='_blank'>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M24 12C24 5.3736 18.6264 0 12 0C5.3736 0 0 5.3736 0 12C0 18.6264 5.3736 24 12 24C12.0705 24 12.1406 23.9985 12.2109 23.9973V14.6558H9.63281V11.6512H12.2109V9.44019C12.2109 6.87598 13.7763 5.48035 16.0637 5.48035C17.159 5.48035 18.1005 5.56201 18.375 5.59845V8.27838H16.7977C15.5533 8.27838 15.3124 8.86981 15.3124 9.73755V11.6512H18.2878L17.9 14.6558H15.3124V23.5364C20.3282 22.0984 24 17.4774 24 12Z" fill="#006ADE"/>
                            </svg>
                        </a>
                    <?php endif; ?>

                    <?php if (isset($socialUrls['instagram']) && !empty($socialUrls['instagram'])): ?>
                        <a href="<?php echo $socialUrls['instagram']; ?>" target='_blank'>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M14.2969 12C14.2969 13.2686 13.2686 14.2969 12 14.2969C10.7314 14.2969 9.70312 13.2686 9.70312 12C9.70312 10.7314 10.7314 9.70312 12 9.70312C13.2686 9.70312 14.2969 10.7314 14.2969 12Z" fill="#006ADE"/>
                                <path d="M17.3713 7.93671C17.2609 7.63751 17.0848 7.3667 16.8559 7.14441C16.6336 6.91553 16.363 6.73938 16.0636 6.62897C15.8208 6.53467 15.4561 6.42242 14.7842 6.39185C14.0575 6.3587 13.8396 6.35156 11.9998 6.35156C10.1597 6.35156 9.94183 6.35852 9.21527 6.39166C8.54346 6.42242 8.17853 6.53467 7.93591 6.62897C7.63654 6.73938 7.36572 6.91553 7.14362 7.14441C6.91473 7.3667 6.73859 7.63733 6.62799 7.93671C6.53369 8.1795 6.42145 8.54443 6.39087 9.21625C6.35773 9.94281 6.35059 10.1607 6.35059 12.0007C6.35059 13.8406 6.35773 14.0585 6.39087 14.7852C6.42145 15.457 6.53369 15.8218 6.62799 16.0646C6.73859 16.364 6.91455 16.6346 7.14343 16.8569C7.36572 17.0858 7.63635 17.2619 7.93573 17.3723C8.17853 17.4668 8.54346 17.579 9.21527 17.6096C9.94183 17.6428 10.1595 17.6497 11.9996 17.6497C13.8398 17.6497 14.0577 17.6428 14.7841 17.6096C15.4559 17.579 15.8208 17.4668 16.0636 17.3723C16.6646 17.1405 17.1395 16.6655 17.3713 16.0646C17.4656 15.8218 17.5779 15.457 17.6086 14.7852C17.6418 14.0585 17.6487 13.8406 17.6487 12.0007C17.6487 10.1607 17.6418 9.94281 17.6086 9.21625C17.5781 8.54443 17.4658 8.1795 17.3713 7.93671ZM11.9998 15.5389C10.0455 15.5389 8.46124 13.9548 8.46124 12.0005C8.46124 10.0463 10.0455 8.46222 11.9998 8.46222C13.9539 8.46222 15.5381 10.0463 15.5381 12.0005C15.5381 13.9548 13.9539 15.5389 11.9998 15.5389ZM15.678 9.14923C15.2213 9.14923 14.8511 8.77899 14.8511 8.32233C14.8511 7.86566 15.2213 7.49542 15.678 7.49542C16.1346 7.49542 16.5049 7.86566 16.5049 8.32233C16.5047 8.77899 16.1346 9.14923 15.678 9.14923Z" fill="#006ADE"/>
                                <path d="M12 0C5.3736 0 0 5.3736 0 12C0 18.6264 5.3736 24 12 24C18.6264 24 24 18.6264 24 12C24 5.3736 18.6264 0 12 0ZM18.8491 14.8409C18.8157 15.5744 18.6991 16.0752 18.5288 16.5135C18.1708 17.4391 17.4391 18.1708 16.5135 18.5288C16.0754 18.6991 15.5744 18.8156 14.8411 18.8491C14.1063 18.8826 13.8715 18.8906 12.0002 18.8906C10.1287 18.8906 9.8941 18.8826 9.15912 18.8491C8.42578 18.8156 7.9248 18.6991 7.48663 18.5288C7.02667 18.3558 6.61029 18.0846 6.26605 17.7339C5.91559 17.3899 5.64441 16.9733 5.47137 16.5135C5.30109 16.0754 5.18445 15.5744 5.15112 14.8411C5.11725 14.1061 5.10938 13.8713 5.10938 12C5.10938 10.1287 5.11725 9.89392 5.15094 9.15912C5.18427 8.4256 5.30072 7.9248 5.47101 7.48645C5.64404 7.02667 5.91541 6.61011 6.26605 6.26605C6.61011 5.91541 7.02667 5.64423 7.48645 5.47119C7.9248 5.3009 8.4256 5.18445 9.15912 5.15094C9.89392 5.11743 10.1287 5.10938 12 5.10938C13.8713 5.10938 14.1061 5.11743 14.8409 5.15112C15.5744 5.18445 16.0752 5.3009 16.5135 5.47101C16.9733 5.64404 17.3899 5.91541 17.7341 6.26605C18.0846 6.61029 18.356 7.02667 18.5288 7.48645C18.6993 7.9248 18.8157 8.4256 18.8492 9.15912C18.8828 9.89392 18.8906 10.1287 18.8906 12C18.8906 13.8713 18.8828 14.1061 18.8491 14.8409Z" fill="#006ADE"/>
                            </svg>
                        </a>
                    <?php endif; ?>

                    <?php if (isset($socialUrls['twitter']) && !empty($socialUrls['twitter'])): ?>
                        <a href="<?php echo $socialUrls['twitter']; ?>" target='_blank'>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12 0C5.3736 0 0 5.3736 0 12C0 18.6264 5.3736 24 12 24C18.6264 24 24 18.6264 24 12C24 5.3736 18.6264 0 12 0ZM17.4791 9.35632C17.4844 9.47443 17.4869 9.59308 17.4869 9.71228C17.4869 13.3519 14.7166 17.5488 9.65021 17.549H9.65039H9.65021C8.09473 17.549 6.64728 17.0931 5.42834 16.3118C5.64386 16.3372 5.86322 16.3499 6.08533 16.3499C7.37585 16.3499 8.56348 15.9097 9.50629 15.1708C8.30054 15.1485 7.28394 14.3522 6.93311 13.2578C7.10101 13.29 7.27368 13.3076 7.45074 13.3076C7.70215 13.3076 7.94568 13.2737 8.17712 13.2105C6.91681 12.9582 5.96741 11.8444 5.96741 10.5106C5.96741 10.4982 5.96741 10.487 5.96777 10.4755C6.33893 10.6818 6.76337 10.806 7.21527 10.8199C6.47571 10.3264 5.98956 9.48285 5.98956 8.52722C5.98956 8.02258 6.12598 7.5498 6.36255 7.14276C7.72083 8.80939 9.75073 9.90546 12.0399 10.0206C11.9927 9.81885 11.9683 9.60864 11.9683 9.39258C11.9683 7.87207 13.2019 6.63849 14.723 6.63849C15.5153 6.63849 16.2308 6.97339 16.7335 7.50879C17.361 7.38501 17.9502 7.15576 18.4825 6.84027C18.2765 7.48315 17.84 8.02258 17.2712 8.36371C17.8284 8.29706 18.3594 8.14929 18.8529 7.92993C18.4843 8.48236 18.0168 8.96759 17.4791 9.35632Z" fill="#006ADE"/>
                            </svg>
                        </a>
                    <?php endif; ?>
                </div>
            </div>

            <div class="big-column">

            <?php foreach ($footerBlocks as $block => $blockTitle): ?>
                <div class="column">
                    <div class="top">
                        <strong><?php echo $blockTitle; ?></strong>
                        <img src="/img/dropdown_arrow.svg" alt="arrow">
                    </div>
                    <?php if (isset($footerPages[$block])): ?>
                        <div class="ul_wrap">
                            <ul>
                                <?php foreach ($footerPages[$block] as $fp): ?>
                                    <li>
                                        <a href="<?php echo $fp->url; ?>"><?php echo $fp->title; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>

            <div class="column contacts">
                <div class="top">
                    <strong><?php pt('Contact'); ?></strong>
                </div>
                <div class='content'>
                    <?php 
                        if (isset($contactUs->phone) && !empty($contactUs->phone)) {
                            $phoneTemplate = '<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13.2921 10.4971L10.8626 8.90908C10.619 8.75478 10.3253 8.70024 10.0426 8.75677C9.7598 8.81331 9.50969 8.97658 9.3442 9.21266C8.78355 9.8899 8.10609 11.0108 5.5598 8.46537C3.0135 5.91991 4.11145 5.21933 4.7889 4.65886C5.02506 4.49342 5.18839 4.24339 5.24494 3.96071C5.3015 3.67803 5.24693 3.38443 5.09259 3.14092L3.50407 0.712225C3.29383 0.408637 3.0135 -0.0817725 2.35941 0.011639C1.70532 0.10505 0 1.06252 0 3.16427C0 5.26603 1.65859 7.83485 3.92456 10.1001C6.19053 12.3653 8.76019 14 10.8393 14C12.9184 14 13.9229 12.1318 13.9929 11.6647C14.063 11.1977 13.5958 10.7072 13.2921 10.4971Z" fill="#006ADE" /></svg>';
                            echo parsePhone($contactUs->phone, '<a href="tel:{phone}">' . $phoneTemplate . ' <span>{phone}</span></a>', null, 1); 
                        }
                    ?>
                   
                    <?php 
                        if (isset($contactUs->email) && !empty($contactUs->email)) {
                            $emailTemplate = '<svg width="15" height="10" viewBox="0 0 15 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M0 0.695422V9.28595L4.32409 4.87538L0 0.695422ZM0.700209 10.0002H14.3L9.95715 5.57042L7.84763 7.60961C7.65383 7.79695 7.34641 7.79695 7.15261 7.60961L5.0431 5.57042L0.700209 10.0002ZM15.0002 9.28595V0.695422L10.6762 4.87538L15.0002 9.28595ZM7.50012 6.5547L1.23664 0.5H13.7636L7.50012 6.5547Z" fill="#006ADE" /></svg>';
                            echo parseEmail($contactUs->email, '<a href="mailto:{email}">' . $emailTemplate . ' <span>{email}</span></a>', null, 1); 
                        }
                    ?>

                    <?php if (isset($contactUs) && !empty($contactUs->address)): ?>
                        <a href="https://www.google.com/maps/search/<?php echo urlencode($contactUs->address); ?>" target="_blank">
                            <svg width="11" height="15" viewBox="0 0 11 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 5.18182C0 9.78068 5.18182 14.25 5.18182 14.25C5.18182 14.25 10.3636 9.78068 10.3636 5.18182C10.3636 2.33182 8.03182 0 5.18182 0C2.33182 0 0 2.33182 0 5.18182Z" fill="#006ADE"/>
                            <path d="M5.1822 7.19007C6.25538 7.19007 7.12538 6.32008 7.12538 5.24689C7.12538 4.1737 6.25538 3.30371 5.1822 3.30371C4.10901 3.30371 3.23901 4.1737 3.23901 5.24689C3.23901 6.32008 4.10901 7.19007 5.1822 7.19007Z" fill="white"/>
                            </svg>
                            <span><?php echo $contactUs->address; ?></span>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
            <div class="column">
            <div class="footer-text">
                <p><?php pt('Cargobus expands its delivery services to Sweden, and now it is also possible to deliver packages'); ?></p>
            <p>
                    <span>
                    <svg width="57" height="15" viewBox="0 0 57 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M31.0084 4.05043H28.901C28.6995 4.05043 28.5109 4.15035 28.398 4.31745L25.4907 8.59883L24.2589 4.48455C24.1816 4.22713 23.9445 4.05043 23.6758 4.05043H21.6045C21.3544 4.05043 21.1783 4.29656 21.259 4.5331L23.5792 11.3447L21.3968 14.4242C21.2257 14.6658 21.3986 15 21.6948 15H23.8C23.9992 15 24.1861 14.9023 24.3001 14.7386L31.3082 4.62342C31.4758 4.38124 31.3031 4.05043 31.0084 4.05043ZM16.9197 8.13134C16.717 9.32926 15.7663 10.1337 14.5532 10.1337C13.9452 10.1337 13.458 9.93783 13.1452 9.5675C12.8353 9.20055 12.7185 8.67724 12.8167 8.09521C13.0052 6.90744 13.9717 6.07759 15.1662 6.07759C15.7618 6.07759 16.2451 6.27517 16.564 6.64889C16.8852 7.02543 17.0117 7.55157 16.9197 8.13134ZM19.8417 4.05037H17.745C17.5655 4.05037 17.4125 4.18078 17.3843 4.3586L17.2923 4.94458L17.146 4.73232C16.6916 4.07352 15.6794 3.85279 14.6689 3.85279C12.3526 3.85279 10.374 5.60846 9.98896 8.07037C9.78856 9.29878 10.0731 10.4724 10.7697 11.2916C11.4093 12.0441 12.3222 12.3574 13.41 12.3574C15.2775 12.3574 16.3128 11.1583 16.3128 11.1583L16.2191 11.7409C16.1841 11.9622 16.3551 12.1626 16.5798 12.1626H18.4676C18.7674 12.1626 19.022 11.9453 19.0688 11.6495L20.2024 4.47207C20.2374 4.25078 20.0658 4.05037 19.8417 4.05037ZM7.25986 4.10073C7.0205 5.67237 5.82032 5.67237 4.65909 5.67237H3.99859L4.46207 2.73796C4.48971 2.5607 4.64271 2.43029 4.82223 2.43029H5.12539C5.91572 2.43029 6.66203 2.43029 7.04704 2.88022C7.27737 3.1495 7.3468 3.54918 7.25986 4.10073ZM6.75461 0H2.37614C2.07637 0 1.82177 0.217907 1.77492 0.51372L0.00455745 11.741C-0.0304433 11.9623 0.140609 12.1627 0.364726 12.1627H2.45574C2.75494 12.1627 3.00954 11.9448 3.05639 11.6496L3.53455 8.62033C3.58084 8.32452 3.83601 8.10661 4.13521 8.10661H5.52055C8.40473 8.10661 10.0695 6.7111 10.5042 3.94435C10.7001 2.73513 10.5121 1.78447 9.94589 1.11889C9.32321 0.387266 8.21956 0 6.75461 0Z" fill="#A3A3A3"/>
                    <path d="M53.5397 0.308517L51.7428 11.7413C51.7078 11.9626 51.8788 12.163 52.1029 12.163H53.9106C54.2098 12.163 54.4649 11.9451 54.5112 11.6493L56.2833 0.421986C56.3183 0.200692 56.1472 0.000284262 55.9225 0.000284262H53.9004C53.7203 0.000284262 53.5673 0.13069 53.5397 0.308517ZM48.1465 8.13134C47.9438 9.32927 46.9932 10.1337 45.78 10.1337C45.172 10.1337 44.6848 9.93783 44.3721 9.5675C44.0616 9.20056 43.9453 8.67724 44.0435 8.09521C44.2321 6.90744 45.1986 6.07759 46.3931 6.07759C46.9887 6.07759 47.4719 6.27517 47.7909 6.64889C48.1121 7.02543 48.2385 7.55157 48.1465 8.13134ZM51.0685 4.05037H48.9718C48.7923 4.05037 48.6393 4.18078 48.6111 4.3586L48.5191 4.94458L48.3723 4.73232C47.9184 4.07352 46.9062 3.85279 45.8957 3.85279C43.5795 3.85279 41.6008 5.60847 41.2158 8.07037C41.0154 9.29878 41.2999 10.4724 41.9966 11.2916C42.6362 12.0441 43.549 12.3574 44.6368 12.3574C46.5043 12.3574 47.5396 11.1583 47.5396 11.1583L47.4459 11.7409C47.4109 11.9622 47.582 12.1626 47.8067 12.1626H49.6944C49.9942 12.1626 50.2488 11.9453 50.2957 11.6495L51.4292 4.47207C51.4642 4.25078 51.2926 4.05037 51.0685 4.05037ZM38.4867 4.10073C38.2473 5.67237 37.0471 5.67237 35.8859 5.67237H35.2254L35.6889 2.73796C35.7165 2.5607 35.8695 2.43029 36.049 2.43029H36.3522C37.1425 2.43029 37.8888 2.43029 38.2738 2.88022C38.5042 3.1495 38.5736 3.54918 38.4867 4.10073ZM37.9814 0H33.6029C33.3032 0 33.0486 0.217908 33.0017 0.51372L31.2314 11.741C31.1964 11.9623 31.368 12.1627 31.5915 12.1627H33.8383C34.0478 12.1627 34.2262 12.0103 34.2589 11.8037L34.7613 8.62033C34.8076 8.32452 35.0628 8.10661 35.362 8.10661H36.7474C39.6315 8.10661 41.2963 6.7111 41.731 3.94435C41.9269 2.73514 41.7389 1.78447 41.1727 1.11889C40.55 0.387266 39.4464 0 37.9814 0Z" fill="#B5B5B5"/>
                    </svg>
                    </span>
                    <span>
                    <svg width="40" height="24" viewBox="0 0 40 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M24.9453 21.436H14.4463V2.56738H24.9453V21.436Z" fill="#B5B5B5"/>
                    <path d="M15.1188 12C15.1188 8.17242 16.9109 4.76292 19.7017 2.5657C17.6609 0.958989 15.0852 -1.98311e-06 12.2861 -1.98311e-06C5.65933 -1.98311e-06 0.287598 5.37249 0.287598 12C0.287598 18.6275 5.65933 24 12.2861 24C15.0852 24 17.6609 23.041 19.7017 21.4343C16.9109 19.2371 15.1188 15.8276 15.1188 12Z" fill="#A3A3A3"/>
                    <path d="M39.1054 12C39.1054 18.6275 33.7337 24 27.1069 24C24.3078 24 21.7321 23.041 19.6906 21.4343C22.4821 19.2371 24.2742 15.8276 24.2742 12C24.2742 8.17242 22.4821 4.76292 19.6906 2.5657C21.7321 0.958989 24.3078 -1.98311e-06 27.1069 -1.98311e-06C33.7337 -1.98311e-06 39.1054 5.37249 39.1054 12Z" fill="#D8D8D8"/>
                    </svg>
                    </span>
                    <span>
                    <svg width="41" height="13" viewBox="0 0 41 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M12.2343 12.6482L15.9879 0.271484H19.3372L15.5833 12.6482H12.2343Z" fill="#B5B5B5"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M10.738 0.275391L7.72523 5.52188C6.95828 6.89597 6.50999 7.59002 6.29441 8.45836H6.24855C6.3019 7.35754 6.14836 6.00615 6.13388 5.24233L5.80122 0.275391H0.163407L0.105469 0.60829C1.55391 0.60829 2.41283 1.33589 2.64917 2.82416L3.74806 12.6487H7.2178L14.2336 0.275391H10.738Z" fill="#B5B5B5"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M36.7842 12.6482L36.6918 10.8075L32.5096 10.8041L31.6543 12.6479H28.0182L34.6113 0.294922H39.0744L40.1909 12.6479H36.7842V12.6482ZM36.4001 5.34757C36.363 4.43288 36.3318 3.19132 36.3941 2.44006H36.3446C36.1409 3.05444 35.2653 4.89952 34.8802 5.80696L33.6341 8.51989H36.5706L36.4001 5.34757Z" fill="#B5B5B5"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M22.8537 13C20.4891 13 18.9199 12.2499 17.7996 11.5813L19.395 9.14473C20.4007 9.70721 21.1904 10.3564 23.0072 10.3564C23.5914 10.3564 24.1544 10.2045 24.474 9.65096C24.9399 8.84587 24.3666 8.41279 23.0581 7.67215L22.4121 7.2521C20.4727 5.92678 19.6338 4.66808 20.5468 2.4708C21.131 1.06533 22.6719 0 25.2139 0C26.967 0 28.6105 0.758259 29.5681 1.49914L27.7339 3.65031C26.7994 2.89447 26.0248 2.51208 25.1383 2.51208C24.4315 2.51208 23.8946 2.78463 23.7092 3.15229C23.3604 3.84344 23.8219 4.31466 24.8412 4.94739L25.6093 5.436C27.964 6.92113 28.525 8.47845 27.9348 9.93437C26.9199 12.4404 24.9326 13 22.8537 13Z" fill="#B5B5B5"/>
                    </svg>
                </span>
                </p>
                <p><?php pt('Cargobus. Reg. nr.'); ?> <?php echo (isset($contactUs->company_regnr)) ? $contactUs->company_regnr : ''; ?></p>
            </div>
            </div>
        </div>
        <div class="bottom row between margin0">
            <span class='copy'><?php pt('© All rights reserved.'); ?></span>
            <?php /*<a class='company' href="https://hexagon.design" target='_blank'>
                <span><?php pt('Made by'); ?></span>
                <svg width="53" height="9" viewBox="0 0 53 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 8.82535V0.163001H4.06222V3.12031H9.20931V0.163001H13.2836V8.82535H9.20931V5.64683H4.06222V8.82535H0ZM18.2716 8.82535H13.4379L17.1868 4.30789L13.6188 0.163001H18.6453L20.6704 2.72445L22.7075 0.163001H27.3966L23.9853 4.29625L27.6859 8.82535H22.7557L20.5016 6.11255L18.2716 8.82535ZM33.1011 9H32.5466C27.0981 9 26.4352 6.26391 26.4352 4.63389V4.35446C26.4352 2.77102 27.0981 0 32.5827 0H33.1252C37.9107 0 38.8268 1.67658 38.8268 2.79431V2.84088H34.7404C34.656 2.72445 34.3788 2.2238 32.9805 2.2238C31.0398 2.2238 30.6059 3.27167 30.6059 3.93532V4.01682C30.8349 3.85382 31.7631 3.21345 34.3185 3.21345H34.4632C38.1879 3.21345 39.2366 4.29625 39.2366 5.78655V5.89133C39.2366 7.2652 38.5375 9 33.1011 9ZM30.8711 5.94955V5.97283C30.8711 6.36869 31.2327 6.76455 32.9685 6.76455C34.7043 6.76455 35.0297 6.39198 35.0297 5.98448V5.96119C35.0297 5.57697 34.7043 5.19275 32.9685 5.19275C31.2327 5.19275 30.8711 5.56533 30.8711 5.94955ZM39.7164 0.163001H44.2849L48.9498 4.54075V0.163001H53V8.82535H48.8896L43.7666 4.08668V8.82535H39.7164V0.163001Z"
                        fill="#101010" />
                </svg>
            </a>*/ ?>
        </div>
    </div>
</footer>

<div class="popup_wrap">
    <div class="popup_bg"></div>
        <?php if (isset($showPopup['payment_error']) && $showPopup['payment_error'] == true): echo $this->Element('Cargobus.Popup/payment-error'); endif; ?>
        <?php if (isset($showPopup['success_message']) && $showPopup['success_message'] == true): echo $this->Element('Cargobus.Popup/success-message'); endif; ?>
        <?php if (isset($showPopup['registration_message']) && $showPopup['registration_message'] == true): echo $this->Element('Cargobus.Popup/registration-message'); endif; ?>
        <?php if (isset($showPopup['registration_login']) && $showPopup['registration_login'] == true): echo $this->Element('Cargobus.Popup/registration-login'); endif; ?>
        <?php if (isset($showPopup['restore_password']) && $showPopup['restore_password'] == true): echo $this->Element('Cargobus.Popup/restore-password'); endif; ?>
        <?php if (isset($showPopup['tracking_form']) && $showPopup['tracking_form'] == true): echo $this->Element('Cargobus.Popup/tracking-form'); endif; ?>
    </div>
</body>

<script src="/js/ScrollMagic_parallax_liba.min.js" deffer></script>
<script src="/js/owl-carousel/owl.carousel.js" defer></script>
<script src="/js/dropdown.js" deffer></script>
<script src="/js/jquery.nicescroll.min.js" deffer></script>
<script src="/js/jquery-ui.js" deffer></script>
<script src="/js/jquery.ui.touch-punch.min.js" deffer></script>
<script src="/js/jquery.mask.js" deffer></script>
<!-- <script src="/js/jquery.fixedheadertable.min.js" deffer></script> -->

<script type="text/javascript" src="/js/jquery.fixedheadertable.min.js" deffer></script>
<link rel="stylesheet" href="/css/fixed-header-table.css" media="screen" />

<script src="https://unpkg.com/vh-check/dist/vh-check.min.js" deffer></script>
<script src="/js/jquery.pagepiling.min.js" deffer></script>
<script src="/js/jquery.sticky-kit.min.js" deffer></script>

<?php if (isset($useAutocomplete) && $useAutocomplete == true): ?><script src="/js/jquery.easy-autocomplete.min.js" deffer></script><?php endif; ?>
<?php if (isset($useChosen) && $useChosen == true): ?><script src="/js/chosen.jquery.js" deffer></script><?php endif; ?>

<script src="/js/script.js?v=<?php echo time(); ?>" deffer></script>
<script src="/js/custom.js?v=<?php echo time(); ?>" deffer></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrHb_fDcX1L7opNiQsrLSmHxBkBbINELQ&language=<?php echo ($cmsRequest::$urlParts['first'] == 'ee') ? 'ee' : (($cmsRequest::$urlParts['first'] == 'lv') ? 'lv' : (($cmsRequest::$urlParts['first'] == 'lt') ? 'lt' : 'en')); ?>"></script>
<!-- <script src="js/script.js?v=<?=rand()?>"></script> -->
<!-- <script src="js/mail_script.js"></script> -->

<?php echo $this->fetch('script'); ?>
</body>

</html>