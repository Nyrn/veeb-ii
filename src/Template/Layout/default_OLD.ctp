<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1">
        <meta name="format-detection" content="telephone=no">
        
        <title><?php echo (isset($seo['title'])) ? $seo['title'] : null; ?></title>

        <meta name="description" content="<?php echo (isset($seo['description'])) ? $seo['description'] : null; ?>">
        <meta name="keywords" content="<?php echo (isset($seo['keywords'])) ? $seo['keywords'] : null; ?>">

        <link href="https://fonts.googleapis.com/css?family=Ubuntu:200,300,400,500,700&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet"> 

        <link href="/css/bootstrap.css" rel="stylesheet">

        <?php if ($useUikit): ?><link href="/css/vendor/uikit/uikit.docs.min.css" rel="stylesheet"><?php endif; ?>

        <?php if ($useUnslider): ?>
            <link href="/css/unslider.css" rel="stylesheet">
            <link href="/css/unslider-dots.css" rel="stylesheet">
        <?php endif; ?>

        <link href="/css/fontfaces.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">
        <link href="/css/responsive.css" rel="stylesheet">
        <link href="/css/jquery.fileupload.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body> 
        <!-- Wrapper -->
        <div id="wrapper">
            <?php echo $this->element('header'); ?>

            <!-- Content -->
            <div id="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-24">
                            <?php echo $this->element('breadcrumbs'); ?>
                        </div>
                    </div>
                </div>

                <?php if (isset($catalogCategories)): ?>
                    <div id="content-tabs">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-24">
                                    <ul>
                                        <?php foreach ($catalogCategories as $cat): ?>
                                            <li><a href="<?php echo $cat->url; ?>" class="<?php echo (isset($category) && ($category->mirror_id == $cat->mirror_id || $category->parent_id == $cat->mirror_id))  ? 'active' : null; ?>"><?php echo $cat->title; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-24">
                            <div class="content">
                                <div class="row">
                                <?php //echo $this->element('left_sidebar'); ?>

                                    <div class="col-xs-24 col-md-24">
                                        <?php echo $this->fetch('content'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // Content -->

            <?php 
                if (isset($showMap) && $showMap == true) {
                    echo $this->Element('Page.map'); 
                }
            ?>

            <?php echo $this->element('footer'); ?>
        </div>
        <!-- // Wrapper -->

        <?php 
            if (isset($showRequestPrice) && $showRequestPrice == true) {
                echo $this->Element('request_price'); 
            }
        ?>

        <!-- Overlay box // -->
        <div id="overlay-box"></div>
        <!-- // Overlay box -->

        <?php echo $this->Element('google-analytics'); ?>

        <?php 
            if (isset($jsVars)) {
                foreach ($jsVars as $key => $val) {
                    ?><script type="text/javascript">var <?php echo $key; ?> = '<?php echo $val; ?>';</script><?php
                }
            }
        ?>

        <script src="/js/vendor/jquery-2.1.3.min.js"></script>
        <script src="/js/vendor/jquery.ui.widget.js"></script>
        <script src="/js/vendor/jquery.iframe-transport.js"></script>
        <script src="/js/vendor/jquery.fileupload.js"></script>
        <script src="/js/vendor/jquery.sticky.js"></script>
        <script src="/js/vendor/bootstrap.min.js"></script>

        <?php if ($useUikit): ?><script src="/js/vendor/uikit/uikit.min.js"></script><?php endif; ?>
        <?php if ($useUiKitLightbox): ?><script src="/js/vendor/uikit/components/lightbox.min.js"></script><?php endif; ?>

        <?php if ($useUnslider): ?>
            <script src="/js/vendor/unslider-min.js"></script>
        <?php endif; ?>

        <script src="/js/script.js"></script>
    </body>
</html>