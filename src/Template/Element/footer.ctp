<footer id="footer">
    <div class="container-fluid">
        <div class="row" style="margin: 0px;">
            <div class="col-md-18 left" style="padding: 0px;">
                <div class="content">
                    <div class="inner">
                        <h3><?php echo __('Partneri'); ?></h3>

                        <div class="partners">
                            <ul>
                                <li class="logo"><a href="http://www.gealan.de" target="_blank"><img src="/img/logos/gealan.png" class="filter"></a></li>
                                <li class="logo"><a href="http://www.ponzioaluminium.com" target="_blank"><img src="/img/logos/ponzio.png" class="filter"></a></li>
                                <li class="logo"><a href="http://www.schueco.com" target="_blank"><img src="/img/logos/schuco.png" class="filter"></a></li>
                                <li class="logo"><a href="http://www.roto-frank.com/lv/" target="_blank"><img src="/img/logos/roto.png" class="filter"></a></li>
                                <li class="logo"><a href="https://www.kbe-online.com/en/" target="_blank"><img src="/img/logos/kbe.png" class="filter"></a></li>
                                <li class="logo"><a href="http://www.stiklucentrs.lv" target="_blank"><img src="/img/logos/stiklu-centrs.png" class="filter"></a></li>
                                <li class="logo"><a href="http://www.inspecta.com/lv" target="_blank"><img src="/img/logos/inspecta.png" class="filter"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 right" style="padding: 0px;">
                <div class="content">
                    <div class="inner">
                        <div class="contacts">
                            <h3><?php echo __('Kontakti'); ?></h3>

                            <ul>
                                <?php if (isset($contactUs->phone)): ?><li><?php echo $contactUs->phone; ?></li><?php endif; ?>
                                <?php if (isset($contactUs->email)): ?><li><?php echo $contactUs->email; ?></li><?php endif; ?>
                                <?php if (isset($contactUs->address)): ?><li><?php echo $contactUs->address; ?></li><?php endif; ?>
                            </ul>

                            <h3><?php echo __('Darba laiks'); ?></h3>

                            <ul>
                                <?php if (isset($contactUs->working_hours_monday_friday)): ?><li><?php echo __('P-Pk'); ?>: <?php echo $contactUs->working_hours_monday_friday; ?></li><?php endif; ?>
                                <?php if (isset($contactUs->working_hours_saturday)): ?><li><?php echo __('S'); ?>: <?php echo $contactUs->working_hours_saturday; ?></li><?php endif; ?>
                                <?php if (isset($contactUs->working_hours_sunday)): ?><li><?php echo __('Sv'); ?>: <?php echo $contactUs->working_hours_sunday; ?></li><?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>