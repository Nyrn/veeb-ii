<?php
use App\Lib\CmsRequest;
?>

<?php if ($this->Paginator->param('pageCount') > 1): ?>
	<div class="text-center">
	    <ul class="pagination">
	     	<?php for ($i = 1; $i <= $this->Paginator->param('pageCount'); $i++): ?>
	     		<li class="<?php echo ((isset($this->request->param('?')['page']) && $this->request->param('?')['page'] == $i) || (!isset($this->request->param('?')['page']) && $i == 1)) ? 'active' : null; ?>"><a href="<?php echo CmsRequest::$page->url; ?>?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
	     	<?php endfor; ?>
	    </ul>
	</div>
<?php endif; ?>