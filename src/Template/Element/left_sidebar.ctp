<?php /*if (isset($catalogCategories)): ?>
    <div class="col-xs-12 col-sm-3 hidden-xs hidden-sm">
        <ul id="categories">
            <?php foreach ($catalogCategories as $cat): ?>
                <li class="<?php echo (isset($category) && ($category->mirror_id == $cat->mirror_id || $category->parent_id == $cat->mirror_id)) ? 'active' : null; ?>"><a href="<?php echo $cat->url; ?>"><?php echo $cat->title; ?></a>

                    <?php if ($cat['children']): ?>
                        <ul class="sub">
                            <?php foreach ($cat['children'] as $child): ?>
                                <li class="<?php echo (isset($category) && $category->mirror_id == $child->mirror_id) ? 'active' : null; ?>"><a href="<?php echo $child->url; ?>"><?php echo $child->title; ?></a>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif;*/ ?>

<?php if (isset($catalogCategories)): ?>
    <div class="col-sm-8 col-md-7">
        <div id="sidebar">
            <ul id="submenu">
                <?php foreach ($catalogCategories as $cat): ?>
                    <li><a href="<?php echo $cat->url; ?>" class="<?php echo (isset($category) && ($category->mirror_id == $cat->mirror_id || $category->parent_id == $cat->mirror_id))  ? 'active' : null; ?>"><?php echo $cat->title; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>