		<!-- Request price box // -->
		<div class="modal fade" id="request-price" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  	<div class="modal-dialog">
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="myModalLabel"><?php echo __('Nosūtīt cenas pieprasījumu'); ?></h4>
		      		</div>
		      		<div class="modal-body">
		      			<p class="notice"></p>

		        		<form id="request-price-form" class="form-horizontal" action="#" method="post" enctype="multipart/form-data">
						  	<div class="form-group">
						    	<label class="col-sm-7 control-label"><?php echo __('Vārds, uzvārds'); ?> <span class="required">*</span></label>
						    	<div class="col-sm-14">
						    	  	<input type="text" name="name_surname" class="form-control">
						    	</div>
						  	</div>

						  	<div class="form-group">
						    	<label class="col-sm-7 control-label"><?php echo __('Tālruņa numurs'); ?></label>
						    	<div class="col-sm-14">
						    	  	<input type="text" name="phone" class="form-control">
						    	</div>
						  	</div>

						  	<div class="form-group">
						    	<label class="col-sm-7 control-label"><?php echo __('E-pasta adrese'); ?> <span class="required">*</span></label>
						    	<div class="col-sm-14">
						    	  	<input type="text" name="email" class="form-control">
						    	</div>
						  	</div>

						  	<div class="form-group">
						  		<label class="col-sm-7 control-label"><?php echo __('Tips'); ?> <span class="required">*</span></label>
						  		<div class="col-sm-14">
						  			<label class="checkbox-inline">
									  	<input type="checkbox" name="type[]" value="aluminium"> <?php echo __('Alumīnija'); ?>
									</label>
									<label class="checkbox-inline">
									  	<input type="checkbox" name="type[]" value="glass"> <?php echo __('Pilnstikla'); ?>
									</label>
									<label class="checkbox-inline">
									  	<input type="checkbox" name="type[]" value="wooden"> <?php echo __('Koka'); ?>
									</label>
									<label class="checkbox-inline">
									  	<input type="checkbox" name="type[]" value="pvc"> <?php echo __('PVC'); ?>
									</label>
								</div>
						  	</div>

						  	<div class="form-group">
						    	<label class="col-sm-7 control-label"><?php echo __('Skaits'); ?> <span class="required">*</span></label>
						    	<div class="col-sm-14">
						    	  	<input type="text" name="count" class="form-control">
						    	</div>
						  	</div>

						  	<div class="form-group">
						    	<label class="col-sm-7 control-label"><?php echo __('Pielikums'); ?></label>
						    	
						    	<div class="col-sm-14">
							    	<span class="btn btn-success fileinput-button">
								        <span><?php echo __('Pievienot failus'); ?></span>

								        <input id="fileupload" type="file" multiple>
								    </span>
								    
								    <div id="progress" class="progress hide" style="margin-top: 20px;">
								        <div class="progress-bar progress-bar-success"></div>
								    </div>
								    
								    <div id="files" class="files" style="margin-top: 20px;"></div>
								    <div id="files_input"></div>
							    </div>
						  	</div>

						  	<div class="form-group">
						    	<label class="col-sm-7 control-label"><?php echo __('Jūsu piezīmes'); ?></label>
						    	<div class="col-sm-14">
						    	  	<textarea name="notes" class="form-control" rows="4"></textarea>
						    	</div>
						  	</div>

						  	<input type="hidden" name="lang_id" value="<?php echo $cmsRequest::$page->language->id; ?>">
						  	<input type="hidden" name="time" value="<?php echo time(); ?>">
						  	<input type="hidden" name="url" value="">

						  	<div class="form-group">
							    <div class="col-sm-7 pull-right">
							      	<button type="submit" class="btn btn-default"><?php echo __('Nosūtīt'); ?></button>
							    </div>
						  	</div>
						</form>
		      		</div>
		    	</div>
		  	</div>
		</div>
		<!-- // Request price box -->