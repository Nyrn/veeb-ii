<?php if ($showBreadcrumbs): ?>
	<div class="breadcrumbs">
		<ul class="clearfix">
			<?php $i = 0; ?>
			<?php foreach ($breadcrumbs as $level => $crumb): ?>
			    <li>
			    	<?php if ($i == count($breadcrumbs)-1): ?>
			    		<?php echo $crumb['title']; ?>
			    	<?php else: ?>
			    		<a href="<?php echo $crumb['url']; ?>"><?php echo $crumb['title']; ?></a>
			    	<?php endif; ?>
			    </li>

			    <?php if ($i < count($breadcrumbs)-1): ?>
			    	<li><span></span></li>
			    <?php endif; ?>

			    <?php $i++; ?>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>