<div role="alert" class="alert dark alert-icon alert-danger">
    <i aria-hidden="true" class="icon wb-close"></i> 
    
    <?php echo h($message); ?>
</div>