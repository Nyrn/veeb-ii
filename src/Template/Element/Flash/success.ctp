<div role="alert" class="alert dark alert-icon alert-success">
    <i aria-hidden="true" class="icon wb-check"></i> 
    
    <?php echo h($message); ?>
</div>