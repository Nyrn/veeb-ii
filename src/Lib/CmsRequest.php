<?php
/**
 * Parse url request
 *
 * First check if request is from cms or pub side because each side has different methods
 * Requested url should be converted to /plugin/controller/action/
 *
 * For example: if url has request like this /cms/users/ converted url should looks like /user/cms_users/index/
 * New converted request url params read from database table
 * 
 * Cms side has "cms_page" table but pub side has "page" table which is related with "module" page
 *
 * From converted request url should create new request
 *
 * @author Artis Bautra <bautra.artis@gmail.com>
 */
namespace App\Lib;

use Cake\Network\Request as CakeRequest;
use Cake\ORM\TableRegistry;
use Cake\Network\Session;

class CmsRequest extends CakeRequest {
	/**
	 * Request
	 *
	 * @access private
	 */
	public static $request;

	/**
	 * Request url
	 *
	 * @access private
	 */
	public static $requestUrl;

	/**
	 * Converted request url
	 *
	 * @access private
	 */
	public static $convertedRequestUrl;

	/**
	 * Page data
	 */
	public static $page = [];

	/**
	 * Language data
	 */
	public static $language = [];

	/**
	 * Parent page date
	 */
	public static $parentPage = [];

	/**
	 * Url explode in parts
	 *
	 * @access private
	 */
	public static $urlParts = [];

	/**
	 * Environment (pub / cms)
	 *
	 * @access private
	 */
	public static $env = 'pub';

	/**
	 * Is home page
	 *
	 * @access protected
	 */
	public static $isHome = false;

	/**
	 * Parse request url
	 * Change request params from parsed url and overwrite request
	 *
	 * TODO: check if request url has only one end slash 
	 *
	 * @access public
	 */
	public static function parse() {
		// Empty request
		self::$request = CakeRequest::createFromGlobals();

		// Request url
		self::$requestUrl = '/' . self::$request->url;

		if (empty(self::$request->url)) {
			self::$isHome = true;
		}

		// Set ennd slash if its not passed
		if (!(substr(self::$requestUrl, -1, 1) == '/')) {
			self::$requestUrl = self::$requestUrl . '/';
		}

		self::splitUrlParts();		
		self::handlePub();

		// If converted request url isn't created
		// Set converted url as default request url
		if (!self::$convertedRequestUrl) {
			self::$convertedRequestUrl = self::$requestUrl;
		}

		// Create request from converted url
		self::$request = CakeRequest::createFromGlobals();
		self::$request->url = self::$convertedRequestUrl;

		return self::$request;
	}

	/**
	 * Split url in parts
	 *
	 * @access private
	 */
	private static function splitUrlParts($parts = []) {
		$explodeParts = explode('/', self::$requestUrl);

		foreach ($explodeParts as $key => $part) {
			if (empty(trim($part))) {
				unset($explodeParts[$key]);
			}
		}

		if (!$explodeParts) {
			$explodeParts[] = '/';
		}

		self::$urlParts = array(
			'urls' => $explodeParts,
			'count' => count($explodeParts),
			'first' => current($explodeParts)
		);

		if (in_array('id', $parts)) self::$urlParts['id'] = end($explodeParts);
	}

	/**
	 * Handle pub
	 *
	 * @access private
	 */
	private static function handlePub() {
		$pageTable = TableRegistry::get('Page.Page');
		$pageAliasTable = TableRegistry::get('Page.PageAlias');
		$moduleTable = TableRegistry::get('Page.Module');
		$languageTable = TableRegistry::get('Language.Language');

		// Try to find page
		if (self::$isHome) {
			$query = $pageTable
				->find('moduleId', ['module_id' => 1])
				->find('langId')
				->find('active')
				->find('notDeleted')
				->matching('Language')
				->contain('Language')
				->where(['Language.domain' => getDomain()])
				->first(); 
		} else {
			$query = $pageTable
				->find()
				->find('notDeleted')
				->matching('Language')
				->contain('Language')
				->where(['Page.url' => self::$requestUrl])
				->where(['Language.domain' => getDomain()])
				->andWhere(function($q) {
					return $q->or_(['Page.is_public' => 'y', 'Page.module_id' => 7]);
				})
				->first();
		}

		if ($query && $query->redirect_id > 0) {
			$redirectPage = $pageTable
				->find()
				->find('notDeleted')
				->where(['Page.id' => $query->redirect_id])
				->first();

			if ($redirectPage) {
				header("Location:" . $redirectPage->url);
				exit;
			}
		}

		// Page not found, try to find page alias
		if (!$query) {
			$query = $pageAliasTable
				->find()
				->matching('Language')
				->contain('Language')
				->where(['PageAlias.url' => self::$requestUrl])
				->where(['Language.domain' => getDomain()])
				->first();
			
			if (!$query) {
				$parentUrl = self::getParsedUrl(self::$urlParts['count'] - 1);

				$query = $pageAliasTable
					->find()
					->matching('Language')
					->contain('Language')
					->where(['PageAlias.url' => $parentUrl, 'PageAlias.module_id >' => 0])
					->where(['Language.domain' => getDomain()])
					->first();

				self::splitUrlParts(['id']);
			}

			if ($query) {
				if ($query->module_id == 0) {
					$parts = explode('/', $query->url);
					
					self::$convertedRequestUrl = '/' . $parts[1] . '/' . $query->plugin . '/' . $query->controller . '/' . $query->action . '/';
				}
			}
		}

		/*if (!$query) {
			$parentUrl = self::getParsedUrl(self::$urlParts['count'] - 1);

			$query = $pageAliasTable
				->find()
				->where(['PageAlias.url' => $parentUrl, 'PageAlias.module_id' => 0])
				->first();

			if ($query) {
				self::$convertedRequestUrl = '/en/' . $query->plugin . '/' . $query->controller . '/' . $query->action . '/';
			}
		}*/

		
		// Page alias not found, try to find parent page
		if (!$query) {
			$parentUrl = self::getParsedUrl(self::$urlParts['count'] - 1);

			$query = $pageTable
				->find()
				//->find('active')
				->find('notDeleted')
				->matching('Language')
				->contain('Language')
				->where(['Page.url' => $parentUrl])
				->where(['Language.domain' => getDomain()])
				->andWhere(function($q) {
					return $q->or_(['Page.is_public' => 'y', 'Page.module_id' => 7]);
				})
				->first();
		}

		$page = $query;

		// Page found
		if ($page) {
			$query = $moduleTable
				->find()
				->where(['Module.id' => $page->module_id])
				->first();

			$module = $query;

			if ($module) {
				self::$convertedRequestUrl = $page->language->slug . '/' . $module->plugin . '/' . $module->controller . '/' . $module->action . '/';
			}

			self::$page = $page;
		} else {
			self::$convertedRequestUrl = '/' . self::$urlParts['first'] . '/page/page/error404';
			//self::$convertedRequestUrl = self::$request->here;
		}

		if (count(self::$request->query) > 0) {
			self::$convertedRequestUrl = self::$convertedRequestUrl . '?' . http_build_query(self::$request->query);
		}

		// Find language 
		$query = $languageTable
            ->find()
            ->find('slug', ['slug' => self::$urlParts['first']])
            ->first();

        if ($query) {
        	self::$language = $query;
        }
	}

	/**
     * Split url in parts and return
     *
     * @return string
     */
    private static function getParsedUrl($parts = 0) {
        $url = self::$requestUrl;

        if ($parts) {
            $explodeParts = explode('/', $url);

            foreach ($explodeParts as $key => $part) {
                if ($key > $parts) {
                    unset($explodeParts[$key]);
                }
            }

            $url = implode('/', $explodeParts) . '/';
        }   

        return $url;
    }
}
?>