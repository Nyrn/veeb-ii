<?php
namespace App\Shell;

require_once(ROOT . DS . 'vendor' . DS  . 'API' . DS . 'CargobusApi.php');

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use CargobusApi;

class CargobusShell extends Shell
{
    public $countries = [
        'LV' => 'Latvia',
        'EE' => 'Estonia',
        'LT' => 'Lithuania'
    ];

    public function parcelShops()
    {
        $cargobusTable = TableRegistry::get('Cargobus.Cargobus');  
        $parcelShopTable = TableRegistry::get('Cargobus.ParcelShop');  
        $settingsTable = TableRegistry::get('Settings');  

        $terminalIds = [];

        $api = new CargobusApi();
        $api->token = $settingsTable->getValue('cargobus_api_token', 0, 'cargobus.ee');

        if ($api->token) {
            foreach ($this->countries as $code => $country) {
                $terminals = $api->getParcelShops($code);

                if ($terminals) {
                    foreach ($terminals as $terminal) {
                        $terminalId = $terminal['terminalId'];

                        $entity = $parcelShopTable
                            ->find()
                            ->where(['terminal_id' => $terminalId])
                            ->first();

                        if (!$entity) {
                            $entity = $parcelShopTable->newEntity();
                        }

                        $entity->terminal_id = $terminalId;
                        $entity->name = $terminal['name'];
                        $entity->type = $terminal['terminalType'];
                        $entity->city = $terminal['city'];
                        $entity->address = $terminal['address'];
                        $entity->country = $terminal['country'];
                        $entity->postal_code = $terminal['postalcode'];
                        $entity->availability = json_encode($terminal['availability']);
                        $entity->description = $terminal['description'];
                        $entity->email = $terminal['email'];
                        $entity->phone = $terminal['phone'];
                        $entity->pos_lat = $terminal['lat'];
                        $entity->pos_lng = $terminal['lng'];

                        $parcelShopTable->save($entity);

                        $terminalIds[] = $terminalId;
                    }
                }
            }

            $parcelShopTable->deleteAll(['terminal_id NOT IN' => $terminalIds]);
        }
    }

    public function generateToken() {
        $cargobusTable = TableRegistry::get('Cargobus.Cargobus');  
        $settingsTable = TableRegistry::get('Settings');  

        $domains = ['cargobus.ee', 'cargobus.lv', 'cargobus.lt'];

        foreach ($domains as $domain) {
            $api = new CargobusApi($cargobusTable->getApiUsername($domain), $cargobusTable->getApiPassword($domain), 1);
            $token = $api->login();

            if ($token) {
                $settingsTable->setValue('cargobus_api_token', 0, $token, $domain);
            }
        }
    }
}
