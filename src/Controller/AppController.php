<?php
// src/Controller/AppController.php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Network\Session;
use App\Lib\CmsRequest;
use Cake\Mailer\Email;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Routing\Router;
use Cake\I18n\I18n;
use Cake\Collection\Collection;


class AppController extends Controller
{

    public $helpers = [
        'Paginator' => ['templates' => 'paginator']
    ];

    public $language = [];
    public $languageList = [];

    public $seo = [];

    public $aliasUrls = [];
    public $pageSettings = [];

    public $showBreadcrumbs = true;
    public $showMap = false;
    public $showPopup = [];

    public $useNoindex = false;
    public $useAutocomplete = false;
    public $useChosen = false;

    public $jsVars = [];

    public function initialize() {
    	parent::initialize();

        $this->loadComponent('Flash');
        //$this->loadComponent('Breadcrumb');
    }

    public function beforeFilter(Event $event) {
        if (isset($_GET['domain'])) {
            $session = new Session();
            $session->write('useDomain', $_GET['domain']);
        }

        if (CmsRequest::$env == 'pub') {
            $this->setLanguage();

            I18n::locale($this->language->slug);

            // Set SEO
            $this->setSeo();

            // Page aliases urls
            $this->setAliasUrls();

            // Settings
            $this->setSettings();

            // Paysera config
            $this->setPayseraConfig();

            // Paypal config
            $this->setPaypalConfig();

            $this->jsVars['language'] = $this->language->slug;
            $this->jsVars['url'] = Router::url(null, true);
            $this->jsVars['ajax_find_address'] = $this->aliasUrls['ajax_find_address'];
            $this->jsVars['ajax_additional_service'] = $this->aliasUrls['ajax_additional_service'];
            $this->jsVars['ajax_terminal'] = $this->aliasUrls['ajax_terminal'];
        }
    }

    public function beforeRender(Event $event) {
        if (CmsRequest::$env == 'pub') {
            $languageTable = TableRegistry::get('Language.Language');
            $pageTable = TableRegistry::get('Page.Page');

            // Find languages
            $query = $languageTable
                ->find('active')
                ->orderAsc('seq')
                ->all();

            $languages = $query;

            // Append pages
            $this->appendPages();

            // Append contact us
            $this->appendContactUs();

            // Append partner logos
            $this->appendLogos();

            // Append social urls
            $this->appendSocial();

            // Set variables
            $this->set(compact(
                'languages'
            ));

            $this->set('pagesIconList', $pageTable->iconList);

            $this->set('language', $this->language);
            $this->set('languageList', $this->languageList);
        	$this->set('seo', $this->seo);

            $this->set('aliasUrls', $this->aliasUrls);
            $this->set('pageSettings', $this->pageSettings);

            $this->set('showBreadcrumbs', $this->showBreadcrumbs);
            $this->set('showMap', $this->showMap);
            $this->set('showPopup', $this->showPopup);

            $this->set('useNoindex', $this->useNoindex);
            $this->set('useAutocomplete', $this->useAutocomplete);
            $this->set('useChosen', $this->useChosen);

            $this->set('jsVars', $this->jsVars);
        }
    }

    /**
     * Set language method
     *
     * @return void
     */
    public function setLanguage() {
    	$languageTable = TableRegistry::get('Language.Language');

        $languageList = [];

        if (!CmsRequest::$language) {
            $query = $languageTable
                ->find('isMain')
                ->where(['domain' => getDomain()])
                ->first();


            if (CmsRequest::$isHome) {
                $this->redirect('/' . $query->slug);
            }
        } else {
            $query = $languageTable
                ->find()
                ->find('slug', ['slug' => CmsRequest::$urlParts['first']])
                ->where(['domain' => getDomain()])
                ->first();

            if (!$query) {
                //throw new NotFoundException();
            }
        }
        
        $this->language = $query;

        $query = $languageTable
            ->find()
            ->where(['domain' => getDomain()])
            ->all();

        $this->languageList = $query;

        $session = new Session();
        $session->write('Language', $this->language);
        $session->write('LanguageList', $languageList);
    }

    /** 
     * Set SEO 
     *
     * @param array $seo Passed SEO data
     * @return void
     */
    protected function setSeo($seo = []) {
        if (!isset($seo->id)) {
            if (isset(CmsRequest::$page->id)) {
                $this->seo = [
                    'title' => (CmsRequest::$page->seo_title) ? CmsRequest::$page->seo_title : CmsRequest::$page->title,
                    'description' => CmsRequest::$page->seo_description,
                    'keywords' => CmsRequest::$page->seo_keywords
                ];
            }
        } else {
            $this->seo = [
                'title' => $seo->seo_title,
                'description' => $seo->seo_description,
                'keywords' => $seo->seo_keywords
            ];
        }
    }

    /**
     * Alias urls
     */
    protected function setAliasUrls() {
        $aliasTable = TableRegistry::get('Page.PageAlias');

        $query = $aliasTable
            ->find('langId')
            ->find('list', ['keyField' => 'name', 'valueField' => 'url'])
            ->all()
            ->toArray();

        $this->aliasUrls = $query;
    }

    /**
     * Settings
     */
    protected function setSettings() {
        $table = TableRegistry::get('Settings');

        $query = $table->getList();

        $this->pageSettings = $query;
    }

    /**
     * Paysera config
     */
    protected function setPayseraConfig() {
        Configure::write('Paysera', [
            'projectId' => 139182, // EE: 139182
            'password' => 'f16ae7b81aea1828c86aa81e74c511de', // EE: f16ae7b81aea1828c86aa81e74c511de
            'acceptUrl' => HOSTNAME_PROTOCOL . HOSTNAME . $this->aliasUrls['payment_process'],
            'cancelUrl' => HOSTNAME_PROTOCOL . HOSTNAME . $this->aliasUrls['payment_canceled'],
            'callbackUrl' => HOSTNAME_PROTOCOL . HOSTNAME . $this->aliasUrls['payment_callback']
        ]);

        if (HOSTNAME == DOMAIN_LV) {
            Configure::write('Paysera.projectId', 137526);
            Configure::write('Paysera.password', '3e1d9bee7fa8982cbd0dd0c0aa5ce905');
        } elseif (HOSTNAME == DOMAIN_LT) {
            Configure::write('Paysera.projectId', 139178);
            Configure::write('Paysera.password', '22de1ba28b85c7acf35f6ad0b4fb42b5');
        }
    }

    /**
     * Paypal config
     */
    protected function setPaypalConfig() {
        $type = (IS_LIVE) ? 'live' : 'sandbox';

        $credentials = [
            // Estonia
            'estonia' => [
                'sandbox' => [
                    'clientId' => 'AbtRGU2DjJixZXsYWUaIw4kcIWVL5UC7qIbLbzLEZzMU7W-EmLCj-0so8LYwknatOThHIMwfNum8hUDf',
                    'secretId' => 'ED4wDcxM79tSgN3cUvg_xMN1Fz71luU0bUVhcWqAK9Fh5jIe82uN0pKn5F8fPVeqpnpE3kghO4ei-YPV'
                ],
                'live' => [
                    'clientId' => 'AUda_whCstdhqKe56lKQGoHBodiMTYpS6nKJLuNNNygeKYUvW2tRlQtNdnabBymo7qy_unh7qDmWsL-g',
                    'secretId' => 'EEdVmjIMtdA-pD0B984h1MwqLKW-evAC0U2yL9-SxVGvlxe_2SoZzpiKQAxLQ0xg5yNfcpOT2KuxOfIF'
                ]
            ],
            // Latvia
            'latvia' => [
                'sandbox' => [
                    'clientId' => 'AfK-t0EEA_iGZYfFbcPv8XmRu6ofPt8Y5hIQipoQwSCugQy1uSa100PLaEH9Jsue4gErXRZRIrhXHjhb',
                    'secretId' => 'ELLW5YOUzIwqZ2k5mokTJgi1qBUMV4soyMycy6YZFMhE0jAM7pnCvUcyz8lJ1nPOhx2wfsjw0spbjA4E'
                ],
                'live' => [
                    'clientId' => 'AabAcgsmN6VJLS6UIh-PvWRInfR_tzdYn-rdx_PA90ITvo0WqO7oIAWiLrkgxCt_DlejNRwQV56tMsjP',
                    'secretId' => 'EJ16jZp9AMXEmhBPiOGOSml3_h8NlXdY-PfCNdEzU11nz6NUa7yoPRw04SrCetBDiK_AZC2bVLLhX4fn'
                ]
            ],
            // Lithuania
            'lithuania' => [
                'sandbox' => [
                    'clientId' => 'AU72f7w3PpNYD38hRagbGtJKnNkYivwa-6bnEE76lOuIYWPIncCNgLJxiveOLhboVkDO-BeE4JYd-uuM',
                    'secretId' => 'EOUAyFOECV_8vJO9Ro7mrsoCG6ZaTemyFCpxdkcRrTA1mSGjE4z3W8TbkQxRebHCcpOquO3PbjX867vI'
                ],
                'live' => [
                    'clientId' => 'AX8Nei1NRQwdbSooQKDiLCe-68iEs3Phdt9JUkP3K4a0DnKG0pwtOaqq1fr63ZjxvAn5DLoCsEa5mF50',
                    'secretId' => 'EByfJgKSAt-mlr8iKkKqObn-hKsyxIKTsQdMqsxawZSQsg5tfEhT4CxsuxPHfM6v3X4oYBxK_f02XFMr'
                ]
            ]
        ];

        Configure::write('Paypal', [
            'clientId' => $credentials['estonia'][$type]['clientId'],
            'secretId' => $credentials['estonia'][$type]['secretId'],
            'returnUrl' => HOSTNAME_PROTOCOL . HOSTNAME . $this->aliasUrls['payment_paypal'],
            'cancelUrl' => HOSTNAME_PROTOCOL . HOSTNAME . $this->aliasUrls['payment_canceled']
        ]);

        if (HOSTNAME == DOMAIN_LV) {
            Configure::write('Paypal.clientId', $credentials['latvia'][$type]['clientId']);
            Configure::write('Paypal.secretId', $credentials['latvia'][$type]['secretId']);
        } elseif (HOSTNAME == DOMAIN_LT) {
            Configure::write('Paypal.clientId', $credentials['lithuania'][$type]['clientId']);
            Configure::write('Paypal.secretId', $credentials['lithuania'][$type]['secretId']);
        }

        Configure::write('Paypal.environment', $type);
    }

    protected function appendPages() {
        $pageTable = TableRegistry::get('Page.Page');

        // Find pages
        $query = $pageTable
            ->find('threaded')
            ->find('langId')
            ->find('active')
            ->where(function($eq) {
                $eq->not(['module_id' => 1]);
                $eq->isNull('block');

                return $eq;
            })
            ->orderAsc('seq')
            ->all();

        $pages = $query;

        // Find footer pages
        $query = $pageTable
            ->find('threaded')
            ->find('langId')
            ->find('active')
            ->where(function($eq) {
                return $eq->isNotNull('block');
            })
            ->orderAsc('seq')
            ->all();

        $collection = new Collection($query);
        $footerPages = $collection->groupBy('block')->toArray();

        // Find module urls
        $query = $pageTable
            ->find('all')
            ->find('langId')
            ->select(['Page.url', 'Module.slug'])
            ->contain(['Module'])
            ->toArray();

        $moduleUrls = (new Collection($query))->combine('module.slug', 'url')->toArray();

        // Footer blocks
        $footerBlocks = [
            'about_us' => gt('About us'),
            'useful_stuff' => gt('Useful stuff'),
            'good_to_know' => gt('Good to know'),
            //'services' => gt('Services')
        ];

        $this->set(compact('pages', 'footerPages', 'moduleUrls', 'footerBlocks'));
    }

    /**
     * Append contact us info
     *
     * @return void
     */
    protected function appendContactUs() {
        $contactUsTable = TableRegistry::get('Page.ContactUs');

        $query = $contactUsTable
            ->find('langId')
            ->order(['id' => 'asc'])
            ->first();
        
        $this->set('contactUs', $query);
    }

    /**
     * Append partner logos
     */
    protected function appendLogos() {
        $logoTable = TableRegistry::get('Page.Logo');

        $query = $logoTable
            ->find('domain')
            ->find('active')
            ->order(['id' => 'asc'])
            ->all();
        
        $this->set('partnerLogos', $query);
    }

    /**
     * Append social urls
     */
    protected function appendSocial() {
        $settingsTable = TableRegistry::get('Settings');

        $urls = [
            'facebook' => $settingsTable->getValue('facebook_url', 0, getDomain()),
            'instagram' => $settingsTable->getValue('instagram_url', 0, getDomain()),
            'twitter' => $settingsTable->getValue('twitter_url', 0, getDomain())
        ];
        
        $this->set('socialUrls', $urls);
    }

    /**
     * Split url in parts and return
     *
     * @return string
     */
    public function getParsedUrl($parts = 0) {
        $url = CmsRequest::$page->url;

        if ($parts) {
            $explodeParts = explode('/', $url);

            foreach ($explodeParts as $key => $part) {
                if ($key > $parts) {
                    unset($explodeParts[$key]);
                }
            }

            $url = implode('/', $explodeParts) . '/';
        }   

        return $url;
    }

    /**
     * Remove all order sessions
     */
    protected function removeOrderSession() {
        $session = new Session();

        $session->delete('Cargobus.Order');
        $session->delete('Cargobus.OrderApiResponse');
        $session->delete('Cargobus.OrderId');
    }
}
