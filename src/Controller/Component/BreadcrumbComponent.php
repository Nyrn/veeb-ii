<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

class BreadcrumbComponent extends Component
{
    public $controller = null;
    public $session = null;

    public $base = 'default';
    public $level = 1;
    public $home = [];
    public $breadcrumbs = [];

    public function initialize(array $config)
    {
    	$this->controller = $this->_registry->getController();
        $this->session = $this->controller->request->session();

        $this->breadcrumbs = $this->session->read('Breadcrumbs');
    }

    public function home($title = null, $url = null) {
        $pageTable = TableRegistry::get('Page.Page');

        $query = $pageTable
            ->find('langId')
            ->find('moduleId', ['module_id' => 1])
            ->first();

        $page = $query;

    	return array(
    		'title' => $page->title,
    		'url' => $page->url
    	);
    }

    public function add($title = null, $level = 0, $url = null, $base = null)
    {
    	if ($base) $this->base = $base;
    	if ($level) $this->level = $level;
    
        $this->breadcrumbs[$this->base][0] = $this->home();
        $this->breadcrumbs[$this->base][$this->level] = [
        	'title' => $title,
        	'url' => $url
       	];

       	foreach ($this->breadcrumbs[$this->base] as $level => $breadcrumb) {
       		if ($this->level < $level) {
       			unset($this->breadcrumbs[$this->base][$level]);
       		}
       	}

       	ksort($this->breadcrumbs[$this->base]);

       	$this->session->write('Breadcrumbs', $this->breadcrumbs);
   	}

   	public function get($base = null) {
   		if ($base) $this->base = $base;

   		return $this->breadcrumbs[$this->base];
   	}
}
?>