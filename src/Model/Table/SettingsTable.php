<?php
namespace App\Model\Table; 

use App\Model\Table\AppTable;
use Cake\Network\Session;

class SettingsTable extends AppTable
{
	public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('settings');
    }

    public function getList() {
        $session = new Session();

        $language = $session->read('Language');

        $langId = $language->id;

        return $this
            ->find('list', ['keyField' => 'type', 'valueField' => 'value'])
            ->where(function ($exp) use ($langId) {
                return $exp->in('lang_id', [0, $langId]);
            })
            ->all()
            ->toArray();
    }

    public function getValue($type = null, $langId = 0, $domain = null) {
        if ($type && $langId >= 0) {
            $q = $this
                ->find()
                ->where(['type' => $type, 'lang_id' => $langId]);

            if ($domain) {
                $q->where(['domain' => $domain]);
            }

            $v = $q->first();

            if ($v) {
                return $v->value;
            }
        }

        return false;
    }

    public function setValue($type = null, $langId = 0, $value = null, $domain = null) {
        if ($type && $langId >= 0) {
            $q = $this
                ->find()
                ->where(['type' => $type, 'lang_id' => $langId]);

            if ($domain) {
                $q->where(['domain' => $domain]);
            }

            $entity = $q->first();

            if ($entity) {
                $entity->value = $value;

                $this->save($entity);
            } else {
                $entity = $this->newEntity();
                $entity->type = $type;
                $entity->lang_id = $langId;
                $entity->value = $value;
                $entity->domain = $domain;

                $this->save($entity);
            }
        }

        return false;
    }
}
?>