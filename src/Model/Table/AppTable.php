<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * App model
 */
class AppTable extends Table
{
    public $columns = [];

	public function initialize(array $config)
    {
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created' => 'new',
                    'modified' => 'always'
                ]
            ]
        ]);

        //$this->columns = $this->schema()->columns();
    }
    
    /**
     * Find by id
     */
    public function findId(Query $query, array $options) {
        return $query->where([$this->alias() . '.id' => (isset($options['id']) ? $options['id'] : null)]);
    }

    /**
     * Find by mirror id
     */
    public function findMirrorId(Query $query, array $options) {
        return $query->where([$this->alias() . '.mirror_id' => (isset($options['mirror_id']) ? $options['mirror_id'] : null)]);
    }

    public function findDomain(Query $query, array $options) {
        return $query->where([$this->alias() . '.domain' => (isset($options['domain']) ? $options['domain'] : getDomain())]);
    }

    /**
     * Find by language id
     */
    public function findLangId(Query $query, array $options) {
        $session = new Session();
        $langId = $session->read('Language.id');

        return $query->where([$this->alias() . '.lang_id' => (isset($options['lang_id']) ? $options['lang_id'] : $langId)]);
    }

    /**
     * Find by page id
     */
    public function findPageId(Query $query, array $options) {
        return $query->where([$this->alias() . '.page_id' => (isset($options['page_id']) ? $options['page_id'] : null)]);
    }

    /**
     * Find by slug
     */
    public function findSlug(Query $query, array $options) {
        return $query->where([$this->alias() . '.slug' => (isset($options['slug']) ? $options['slug'] : null)]);
    }

    /**
     * Find by url
     */
    public function findUrl(Query $query, array $options) {
        return $query->where([$this->alias() . '.url' => (isset($options['url']) ? $options['url'] : null)]);
    }

    /**
     * Find by level
     */
    public function findLevel(Query $query, array $options) {
        return $query->where([$this->alias() . '.level' => (isset($options['level']) ? $options['level'] : null)]);
    }

	/**
     * Find public item
     */
    public function findIsPublic(Query $query) {
    	return $query->where([$this->alias() . '.is_public' => 'y']);
    }

    /**
     * Find main item
     */
    public function findIsMain(Query $query, array $options) {
        return $query->where([$this->alias() . '.is_main' => (isset($options['is_main']) ? $options['is_main'] : 'y')]);
    }

    /**
     * Find NOT deleted item
     */
    public function findNotDeleted(Query $query) {
        $columns = array_flip($this->schema()->columns());
        
        if (isset($columns['is_deleted'])) {
    	   $query->where([$this->alias() . '.is_deleted' => 'n']);
        }

        return $query;
    }

    /**
     * Find active item 
     * Item which is public and not deleted
     */ 
    public function findActive(Query $query) {
    	return $query->find('isPublic')->find('notDeleted');
    }

    /**
     * Find paths
     *
     * @return void
     */
    public function findPaths(Query $query, array $options) {
        if ($this->behaviors()->get('FileUpload')) {
            $config = $this->behaviors()->get('FileUpload')->config();

            return $query->formatResults(function($results) use ($config) {
                return $results->map(function($row) use ($config) {
                    $p = $config['field'] . "_path";
                    $row->$p = '/file/' . $this->behaviors()->get('FileUpload')->config('dir') . '/' . 'default' . '/' . $row->file;

                    foreach ($config['sizes'] as $size => $dimmensions) {
                        $p = $config['field'] . "_path_" . $size;
                        $row->$p = '/file/' . $this->behaviors()->get('FileUpload')->config('dir') . '/' . $size . '/' . $row->file;
                    }

                    return $row;
                });
            });
        }
    }

    /**
     * Save mirror items in other languages
     *
     * @return void
     */
    public function saveMirrorItems($item) {
        if ($item) {
            $item->mirror_id = $item->id;
            $this->save($item);

            $languages = TableRegistry::get('Language.Language')
                ->find('notDeleted')
                ->where(['not' => ['id' => $item->lang_id]]);

            $clone = $item;
            unset($clone->id, $clone->created, $clone->modified);
            
            foreach ($languages as $language) {
                $item = $this->newEntity();
                $item = $this->patchEntity($item, $clone->toArray());

                $item->lang_id = $language->id;
                $this->save($item);
            }
        }
    }

    /**
     * Update mirror items common fields
     *
     * @return void
     */
    public function updateMirrorItems($item) {
        if ($item) {
            $id = $item->id;
            $mirrorId = $item->mirror_id;

            $mirrors = $this->getMirrorItems($item);

            foreach ($mirrors as $mirror) {
                foreach ($this->mirrorCommonFields as $field) {
                    $mirror->$field = $item->$field;

                    $this->save($mirror);
                }
            }
        }
    }
}
?>