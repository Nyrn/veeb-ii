<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'Catalog' => $baseDir . '/plugins/Catalog/',
        'Cms' => $baseDir . '/plugins/Cms/',
        'Home' => $baseDir . '/plugins/Home/',
        'Language' => $baseDir . '/plugins/Language/',
        'Page' => $baseDir . '/plugins/Page/',
        'Translation' => $baseDir . '/plugins/Translation/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/'
    ]
];
