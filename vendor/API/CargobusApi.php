<?php
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;

class CargobusApi {
	//const API_DOMAIN = 'http://cargokassa.northeurope.cloudapp.azure.com';
	//const API_DOMAIN = 'https://cargoapp-live.northeurope.cloudapp.azure.com/';
	const API_DOMAIN = 'https://prelive.cargobus.ee';

	const METHOD_POST = 'POST';
	const METHOD_GET = 'GET';

	public $apiDomain = self::API_DOMAIN;

	public $apiPath = '/apiv2/selfservice/';

	private $username;
	private $password;
	private $webShopId;
	private $sessionId;
	public $token;

	public $isPdfResponse = false;

	/**
	 * @param int $email User email
	 * @param string $apiKey Your API key
	 */
	public function __construct($username = null, $password = null, $webShopId = null, $sessionId = null){
		$this->username = $username;
		$this->password = $password;
		$this->webShopId = $webShopId;
		$this->sessionId = $sessionId;
	}

	/**
	 * @throws Exception
	 */
	public function login() {
		$response = $this->call('Auth', self::METHOD_POST, [
			'username' => $this->username,
			'password' => $this->password,
			'webShopId' => $this->webShopId,
			'sessionId' => $this->sessionId,
		]);

		if (!empty($response)) {
			$this->token = $response;
		}
		
		return $this->token;
	}

	/** 
	 * Get terminals
	 */
	public function getParcelShops($country) {
		return $this->call('ParcelShops/' . $country, self::METHOD_GET);
	}
 	
 	/**
 	 * Find google addresses
 	 */
 	public function getGoogleAddresses($data = []) {
		$response = $this->call('Address/FindAddress', self::METHOD_POST, $data);
		
		if (!empty($response)) {
			return $response;
		}

		return false;
	}

	/**
	 * Get service
	 */
	public function getQuote($data = []) {
		$response = $this->call('Quote', self::METHOD_POST, $data);
		
		if (!empty($response)) {
			return $response;
		}

		return false;
	}

	/**
	 * Get label
	 */
	public function getLabel($code) {
		$this->isPdfResponse = true;
		return $this->call('ParcelLabel/byParcelCode/' . $code, self::METHOD_GET);
	}

	/**
	 * Get full parcel info
	 */ 
	public function getParcelInfo($orderId = 0) {
		return $this->call('ParcelInfo/' . $orderId, self::METHOD_GET);
	}

	/**
	 * Get tracking
	 */
	public function getTracking($orderId = 0) {
		return $this->call('ParcelTracking/' . $orderId, self::METHOD_GET);
	}

	/**
	 * Create order
	 */
	public function createOrder($data = []) {
		$response = $this->call('ParcelTransportOrder', self::METHOD_POST, [
			'transportOrderParcels' => $data
		]);
		
		if (!empty($response)) {
			return $response;
		}

		return false;
	}

	/**
	 * Add payment
	 */
	public function addPayment($data = []) {
		$response = $this->call('Payments/Add', self::METHOD_POST, $data);
		
		if (!empty($response)) {
			return $response;
		}

		return false;
	}

	protected function call($action, $method, $data = []){
		$url = $this->apiDomain . $this->apiPath . $action;

		$headers = [
		 	"Accept: application/json",
		 	"Content-Type: application/json"
		];

		$isPost = $method == self::METHOD_POST;

		if(!$isPost){
			$url .= '?' . http_build_query($data);
		}

		// Add token header
		if($this->token) {
			$headers[] = 'Authorization: Bearer ' . $this->token;
		}

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, $isPost ? 1 : 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);  

		if($isPost){
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		}
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		//curl_setopt($ch, CURLOPT_HEADER, 1);
		//curl_setopt($ch, CURLOPT_HEADERFUNCTION, 'readHeader');
		//curl_setopt($ch, CURLOPT_NOBODY, 1);

		$output = curl_exec($ch);
		$error = curl_error($ch);
		$info = curl_getinfo($ch);

		curl_close($ch);

		if ($info['http_code'] == 200) {
			return ($this->isPdfResponse) ? $output : json_decode($output, true);
		} else {
			if (in_array($info['http_code'], [401, 500])) {
				$cargobusTable = TableRegistry::get('Cargobus.Cargobus');

				$api = new CargobusApi($cargobusTable->getApiUsername(getDomain()), $cargobusTable->getApiPassword(getDomain()), 1);

				$api->token = $api->login();

				if ($api->token) {
					$settingsTable = TableRegistry::get('Settings');

					$settingsTable->setValue('cargobus_api_token', 0, $api->token, getDomain());
				}
			}

			Log::emergency($info, 'api_error');
			Log::emergency($output, 'api_error');

			throw new ApiException('Could not parse API', $info['http_code']);
		}
	}
}

class ApiException extends \Exception{
}