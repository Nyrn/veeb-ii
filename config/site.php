<?php
use Cake\Core\Configure;

//define('HOSTNAME', exec('hostname'));
// define('HOSTNAME', isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'cargobus.ee');
define('HOSTNAME', isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'cargobus.ee');
define('HOSTNAME_PROTOCOL', (((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443)) ? "https://" : "http://"));

define('IS_LIVE', (in_array(HOSTNAME, ['www.cargobus.ee', 'www.cargobus.lv', 'www.cargobus.lt'])) ? true : false);

define('DOMAIN_LV', 'www.cargobus.lv');
define('DOMAIN_LT', 'www.cargobus.lt');
define('DOMAIN_EE', 'www.cargobus.ee');

// define('LOCAL_FILE_LOCATION', dirname(dirname(dirname(__FILE__))) . DS . 'cargobus.lv' . DS . 'webroot' . DS . 'file');
define('LOCAL_FILE_LOCATION', dirname(dirname(dirname(__FILE__))) . DS . 'cargobus.ee' . DS . 'webroot' . DS . 'file');
define('PUB_FILE_LOCATION', '/webroot/file');

define('WEB_FILE_PATH', (in_array(HOSTNAME, ['www.cargobus.ee', 'www.cargobus.lv', 'www.cargobus.lt'])) ? PUB_FILE_LOCATION : LOCAL_FILE_LOCATION);

Configure::write('Site', [
	'email_from' => 'info@cargobus.ee',
	'email_to' => 'bautra.artis@gmail.com'
]);

Configure::write('Countries', [
	'LV' => 'Latvia',
	'LT' => 'Lithuania',
	'EE' => 'Estonia'
]);
?>