<?php
use App\Lib\CmsRequest;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\Network\Session;

function getDomain() {
    $session = new Session();

    $domain = 'cargobus.ee';
            
    if (HOSTNAME == 'www.cargobus.lv') $domain = 'cargobus.lv'; 
    if (HOSTNAME == 'www.cargobus.lt') $domain = 'cargobus.lt'; 
    if (HOSTNAME == 'www.cargobus.ee') $domain = 'cargobus.ee'; 


    if (IS_LIVE == false) {
        // $domain = 'cargobus.lv';
        $domain = 'cargobus.ee';
    }

    if ($session->read('useDomain')) {
        $domain = $session->read('useDomain');
    }

    return $domain;
}

function gt($text, $sp = []) {
	$langId = @CmsRequest::$language->id;

	$translationTable = TableRegistry::get('Translation.Translation');

	$translations = Cache::read('translations_' . $langId);

	if (!isset($translations[$text])) {
		$translations = $translationTable
			->find('list', ['keyField' => 'title', 'valueField' => 'translation'])
            ->find('notDeleted')
			->where(function($exp, $q) use ($langId) {
				return $exp->eq('lang_id', $langId);
			})
			->all()
			->toArray();

		Cache::write('translations_' . $langId, $translations);
	}
	
	$translation = isset($translations[$text]) ? $translations[$text] : null;
	
	if ($translation) {
		$text = $translation;
	} else {
		$translation = $translationTable->newEntity();
		$translation->lang_id = $langId;
		$translation->title = $text;
		$translation->translation = $text;

		if ($translationTable->save($translation)) {
			$translationTable->saveMirrorItems($translation);
		}
	}

	return ($sp) ? sprintf($text, @$sp[0], @$sp[1], @$sp[2], @$sp[3], @$sp[4], @$sp[5]) : $text;
}

function pt($text, $sp =[]) {
	echo gt($text, $sp);
}

function removeQuotes($post) {
    if (get_magic_quotes_gpc()) {
        foreach ($post as &$var) {
            if (is_array($var)) {
                $var = removeQuotes($var);
            } else {
                $var = stripslashes($var);
            }
        }
    }
    return $post;
}


function customSort($a, $b) {
    static $charOrder = array('a', 'ā', 'b', 'c', 'č', 'd', 'e', 'ē', 'é',
                              'f', 'g', 'ģ', 'h', 'i', 'ī', 'j',
                              'k', 'ķ', 'l', 'ļ', 'm', 'n', 'ņ', 'o',
                              'p', 'q', 'r', 's', 'š', 't',
                              'u', 'ū', 'v', 'w', 'x', 'y', 'z', 'ž');

    $a = mb_strtolower($a);
    $b = mb_strtolower($b);

    for($i=0;$i<mb_strlen($a) && $i<mb_strlen($b);$i++) {
        $chA = mb_substr($a, $i, 1);
        $chB = mb_substr($b, $i, 1);
        $valA = array_search($chA, $charOrder);
        $valB = array_search($chB, $charOrder);
        if($valA == $valB) continue;
        if($valA > $valB) return 1;
        return -1;
    }

    if(mb_strlen($a) == mb_strlen($b)) return 0;
    if(mb_strlen($a) > mb_strlen($b))  return -1;
    return 1;

}

/**
 * Decimal to integer amount
 */
function dec2int($amount = 0) {
    return round($amount * 100);
}

/** 
 * Integer to decimal amount
 */
function int2dec($amount = 0) {
    return $amount / 100;
}

function roundNumber($number = 0) {
	return number_format($number, 2, '.', '');
}


function parsePhone($phone = null, $template = null, $delimiter = null, $returnNumber = false) {
    $numbers = [];

    if (!$template) {
        $template = '<a href="tel:{phone}" class="tel icon">{phone}</a>';
    }

    if ($phone) {
        $explode = explode(',', $phone);

        foreach ($explode as $p) {
        	$tmp = $template;

        	if ($p != end($explode) && $delimiter) $tmp .= $delimiter . ' ';

        	$numbers[] = str_replace('{phone}', trim($p), $tmp);
        }
    }

    return ($returnNumber && isset($numbers[$returnNumber-1])) ? $numbers[$returnNumber-1] : implode('', $numbers);
}

function parseEmail($email = null, $template = null, $delimiter = null, $returnNumber = false) {
    $emails = [];

    if (!$template) {
        $template = '<a href="mailto::{email}" class="mail icon" style="{style}">{email}</a>';
    }

    if ($email) {
        $explode = explode(',', $email);

        foreach ($explode as $p) {
            if (strlen($p) > 24) {
                $template = str_replace('{style}', 'font-size: ' . ((strlen($p) >= 27) ? 12 : 13) . 'px;', $template);
            }

        	$tmp = $template;

        	if ($p != end($explode) && $delimiter) $tmp .= $delimiter . ' ';

        	$emails[] = str_replace('{email}', trim($p), $tmp);
        }
    }

    return ($returnNumber && isset($emails[$returnNumber-1])) ? $emails[$returnNumber-1] : implode('', $emails);
}

function parseTrackingLocation($location = null) {
    if ($location) {
        $exp = explode(',', $location);
        $count = count($exp)-1;

        return implode(', ', [$exp[$count-2], $exp[$count-1], $exp[$count]]);
    }

    return false;
}
?>